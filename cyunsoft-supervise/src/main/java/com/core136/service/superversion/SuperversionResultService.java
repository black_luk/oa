package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionResult;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionResultMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SuperversionResultService {
    private SuperversionResultMapper superversionResultMapper;

    @Autowired
    public void setSuperversionResultMapper(SuperversionResultMapper superversionResultMapper) {
        this.superversionResultMapper = superversionResultMapper;
    }

    public int insertSuperversionResult(SuperversionResult superversionResult) {
        return superversionResultMapper.insert(superversionResult);
    }

    public int deleteSuperversionResult(SuperversionResult superversionResult) {
        return superversionResultMapper.delete(superversionResult);
    }

    public int updateSuperversionResult(Example example, SuperversionResult superversionResult) {
        return superversionResultMapper.updateByExampleSelective(superversionResult, example);
    }

    public SuperversionResult selectOneSuperversionResult(SuperversionResult superversionResult) {
        return superversionResultMapper.selectOne(superversionResult);
    }

    /**
     * 获取任务处理过程列表
     *
     * @param orgId
     * @param processId
     * @return
     */
    public List<Map<String, String>> getSuperversionResultList(String orgId, String processId) {
        return superversionResultMapper.getSuperversionResultList(orgId, processId);
    }

    /**
     * 获取任务处理过程列表
     *
     * @param pageParam
     * @param processId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSuperversionResultList(PageParam pageParam, String processId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSuperversionResultList(pageParam.getOrgId(), processId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
