package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberTarget;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberTargetMapper extends MyMapper<PartyMemberTarget> {

    /**
     * @param orgId
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getTargetRecordList
     * @Description:  获取发展对象审批列表
     */
    public List<Map<String, String>> getTargetRecordList(@Param(value = "orgId") String orgId, @Param(value = "status") String status,
                                                         @Param(value = "partyOrgId") String partyOrgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                         @Param(value = "search") String search);

}
