package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionOrg;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface PartyUnionOrgMapper extends MyMapper<PartyUnionOrg> {

    public PartyUnionOrg getPartyUnionOrg(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime);
}
