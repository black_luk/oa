package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyElection;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyElectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

@Service
public class PartyElectionService {


    private PartyElectionMapper partyElectionMapper;

    @Autowired
    public void setPartyElectionMapper(PartyElectionMapper partyElectionMapper) {
        this.partyElectionMapper = partyElectionMapper;
    }

    public int insertPartyElection(PartyElection partyElection) {
        return partyElectionMapper.insert(partyElection);
    }


    public int deletePartyElection(PartyElection partyElection) {
        return partyElectionMapper.delete(partyElection);
    }

    public int updatePartyElection(Example example, PartyElection partyElection) {
        return partyElectionMapper.updateByExampleSelective(partyElection, example);
    }

    public PartyElection selectOnePartyElection(PartyElection partyElection) {
        return partyElectionMapper.selectOne(partyElection);
    }

    /**
     * @param orgId
     * @return Map<String, String>
     * @Title: electionIsExist
     * @Description:  当前时间是否存在届次
     */
    public Map<String, String> electionIsExist(String orgId, String partyOrgId) {
        return partyElectionMapper.electionIsExist(orgId, partyOrgId, SysTools.getTime("yyyy-MM-dd"));
    }

}
