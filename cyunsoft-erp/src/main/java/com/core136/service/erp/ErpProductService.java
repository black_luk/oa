package com.core136.service.erp;

import com.core136.bean.erp.ErpOrderDetail;
import com.core136.bean.erp.ErpProduct;
import com.core136.mapper.erp.ErpProductMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ErpProductService
 * @Description: 产品数据操作服务类
 * @author: 稠云信息
 * @date: 2018年12月19日 上午10:15:13
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class ErpProductService {
    private ErpProductMapper erpProductMapper;

    @Autowired
    public void setErpProductMapper(ErpProductMapper erpProductMapper) {
        this.erpProductMapper = erpProductMapper;
    }

    /**
     * @param example
     * @param pageNumber
     * @param pageSize
     * @return PageInfo<ErpProduct>
     * @Title getErpProductBySort
     * @Description  按产品分类获取分类下的列表
     */
    public PageInfo<ErpProduct> getErpProductBySort(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<ErpProduct> datalist = erpProductMapper.selectByExample(example);
        PageInfo<ErpProduct> pageInfo = new PageInfo<ErpProduct>(datalist);
        return pageInfo;
    }

    /**
     * @param erpProduct
     * @return int
     * @Title insertErpProduct
     * @Description  添加新产品
     */
    public int insertErpProduct(ErpProduct erpProduct) {
        return erpProductMapper.insert(erpProduct);
    }

    /**
     * @param erpProduct
     * @return int
     * @Title delErpProduct
     * @Description  删除产品
     */
    public int delErpProduct(ErpProduct erpProduct) {
        return erpProductMapper.delete(erpProduct);
    }

    /**
     * @param erpProduct
     * @param example
     * @return int
     * @Title updateErpProduct
     * @Description  更新产品
     */
    public int updateErpProduct(ErpProduct erpProduct, Example example) {
        return erpProductMapper.updateByExampleSelective(erpProduct, example);
    }

    /**
     * 按名称模糊查询产品
     */

    public List<ErpProduct> selectProductByName(String productName, String orgId) {
        //  Auto-generated method stub
        return erpProductMapper.selectProductByName(productName, orgId);
    }

    public ErpProduct selectOne(ErpProduct erpProduct) {
        return erpProductMapper.selectOne(erpProduct);
    }

    /**
     * @param erpOrderDetailList
     * @return List<ErpProduct>
     * @Title getErpProductListByOrderDetailList
     * @Description  通过订单明细获取产品列表
     */
    public List<ErpProduct> getErpProductListByOrderDetailList(List<ErpOrderDetail> orderDetailList) {
        List<ErpProduct> returnList = new ArrayList<ErpProduct>();
        for (int i = 0; i < orderDetailList.size(); i++) {
            ErpProduct ep = new ErpProduct();
            ep.setProductId(orderDetailList.get(i).getProductId());
            ep.setOrgId(orderDetailList.get(i).getOrgId());
            returnList.add(erpProductMapper.selectOne(ep));
        }

        return returnList;
    }


    /**
     * 获取产品与BOM的对应信息
     */

    public Map<String, Object> getProuctAndBomInfoByProductId(String productId, String orgId) {
        //  Auto-generated method stub
        return erpProductMapper.getProuctAndBomInfoByProductId(productId, orgId);
    }

    /**
     * 获取产品的select2列表
     */

    public List<Map<String, Object>> getProductSelect2(String orgId, String search) {
        //  Auto-generated method stub
        return erpProductMapper.getProductSelect2(orgId, search);
    }
}
