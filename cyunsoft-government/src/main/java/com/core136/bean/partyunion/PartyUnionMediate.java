package com.core136.bean.partyunion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 调解管理
 */
@Table(name = "party_union_mediate")
public class PartyUnionMediate implements Serializable {
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String title;
    private String mediateType;
    private String mediateTime;
    //当事人
    private String partyUser;
    //调解人
    private String mediateUser;
    //事由
    private String content;
    private String mediateModel;
    private String address;
    //调解结果
    private String resContent;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMediateType() {
        return mediateType;
    }

    public void setMediateType(String mediateType) {
        this.mediateType = mediateType;
    }

    public String getMediateTime() {
        return mediateTime;
    }

    public void setMediateTime(String mediateTime) {
        this.mediateTime = mediateTime;
    }

    public String getPartyUser() {
        return partyUser;
    }

    public void setPartyUser(String partyUser) {
        this.partyUser = partyUser;
    }

    public String getMediateUser() {
        return mediateUser;
    }

    public void setMediateUser(String mediateUser) {
        this.mediateUser = mediateUser;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMediateModel() {
        return mediateModel;
    }

    public void setMediateModel(String mediateModel) {
        this.mediateModel = mediateModel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getResContent() {
        return resContent;
    }

    public void setResContent(String resContent) {
        this.resContent = resContent;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
