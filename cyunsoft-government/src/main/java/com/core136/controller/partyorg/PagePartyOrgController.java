package com.core136.controller.partyorg;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/partyorg")
public class PagePartyOrgController {

    /**
     * @return ModelAndView
     * @Title: goOrgMap 党组织位置坐标
     * @Description:
     */
    @RequestMapping("/orgmap")
    @RequiresPermissions("/app/core/partyorg/orgmap")
    public ModelAndView goOrgMap() {
        try {
            return new ModelAndView("app/core/party/map/maporg");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goFundsDetails
     * @Description:  经费详情
     */
    @RequestMapping("/fundsdetails")
    @RequiresPermissions("/app/core/partyorg/fundsdetails")
    public ModelAndView goFundsDetails() {
        try {
            return new ModelAndView("app/core/party/funds/fundsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyDuesDetails
     * @Description:  党组织党费详情
     */
    @RequestMapping("/partyduesdetails")
    @RequiresPermissions("/app/core/partyorg/partyduesdetails")
    public ModelAndView goPartyDuesDetails() {
        try {
            return new ModelAndView("app/core/party/dues/orgduesdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goLifeMeetDetails
     * @Description:  党组织生活会详情
     */
    @RequestMapping("/lifemeetdetails")
    @RequiresPermissions("/app/core/partyorg/lifemeetdetails")
    public ModelAndView goLifeMeetDetails() {
        try {
            return new ModelAndView("app/core/partyorg/lifemeetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPrimaryMeetDetails
     * @Description: 主题党日活动详情
     */
    @RequestMapping("/primarymeetdetails")
    @RequiresPermissions("/app/core/partyorg/primarymeetdetails")
    public ModelAndView goPrimaryMeetDetails() {
        try {
            return new ModelAndView("app/core/partyorg/primarymeetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goLesMeetDetails
     * @Description:  三会一课详情
     */
    @RequestMapping("/lesmeetdetails")
    @RequiresPermissions("/app/core/partyorg/lesmeetdetails")
    public ModelAndView goLesMeetDetails() {
        try {
            return new ModelAndView("app/core/partyorg/lesmeetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLesMeet
     * @Description: 三会一课
     */
    @RequestMapping("/lesMeet")
    @RequiresPermissions("/app/core/partyorg/lesMeet")
    public ModelAndView goLesMeet(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/lesmeetmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyorg/lesmeet");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLifeMeet
     * @Description:  党组织生活会
     */
    @RequestMapping("/lifeMeet")
    @RequiresPermissions("/app/core/partyorg/lifeMeet")
    public ModelAndView goLifeMeet(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/lifemeetmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyorg/lifemeet");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMzMeet
     * @Description:  主民生活会
     */
    @RequestMapping("/mzMeet")
    @RequiresPermissions("/app/core/partyorg/mzMeet")
    public ModelAndView goMzMeet(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/mzmeetmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyorg/mzmeet");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPrimaryMeet
     * @Description:  主题党日活动
     */
    @RequestMapping("/primaryMeet")
    @RequiresPermissions("/app/core/partyorg/primaryMeet")
    public ModelAndView goPrimaryMeet(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/primarymeetmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyorg/primarymeet");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goCheckMeet
     * @Description:  党建述评考
     */
    @RequestMapping("/checkMeet")
    @RequiresPermissions("/app/core/partyorg/checkMeet")
    public ModelAndView goCheckMeet(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/checkmeetmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyorg/checkmeet");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMzMeetDetails
     * @Description:  民主生活会详情
     */
    @RequestMapping("/mzmeetdetails")
    @RequiresPermissions("/app/core/partyorg/mzmeetdetails")
    public ModelAndView goMzMeetDetails() {
        try {
            return new ModelAndView("app/core/partyorg/mzmeetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goCheckMeetDetails
     * @Description:  述评考详情
     */
    @RequestMapping("/checkmeetdetails")
    @RequiresPermissions("/app/core/partyorg/checkmeetdetails")
    public ModelAndView goCheckMeetDetails() {
        try {
            return new ModelAndView("app/core/partyorg/checkmeetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTalk
     * @Description:  书记/副书记谈话
     */
    @RequestMapping("/talk")
    @RequiresPermissions("/app/core/partyorg/talk")
    public ModelAndView goTalk(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/talkmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/talk");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goTalkDetails
     * @Description:  书记谈话详情
     */
    @RequestMapping("/talkdetails")
    @RequiresPermissions("/app/core/partyorg/talkdetails")
    public ModelAndView goTalkDetails() {
        try {
            return new ModelAndView("app/core/partyorg/talkdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goOldParty
     * @Description:  历史组织机构
     */
    @RequestMapping("/oldparty")
    @RequiresPermissions("/app/core/partyorg/oldparty")
    public ModelAndView goOldParty() {
        try {
            return new ModelAndView("app/core/partyorg/oldparty");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goRebuildDetails
     * @Description:  党组织改建详情
     */
    @RequestMapping("/rebuilddetails")
    @RequiresPermissions("/app/core/partyorg/rebuilddetails")
    public ModelAndView goRebuildDetails() {
        try {
            return new ModelAndView("app/core/partyorg/rebuilddetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTempOrg
     * @Description:  临时党组织
     */
    @RequestMapping("/temporg")
    @RequiresPermissions("/app/core/partyorg/temporg")
    public ModelAndView goTempOrg(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/temporgmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/temporg");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRebuild
     * @Description:  党组织改建
     */
    @RequestMapping("/rebuild")
    @RequiresPermissions("/app/core/partyorg/rebuild")
    public ModelAndView goRebuild(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/rebuildmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/rebuild");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRename
     * @Description: 党组织更名
     */
    @RequestMapping("/rename")
    @RequiresPermissions("/app/core/partyorg/rename")
    public ModelAndView goRename(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/renamemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/rename");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 党组织更名详情
     * @return
     */
    @RequestMapping("/renamedetails")
    public ModelAndView goRenameDetails() {
        try {
            return new ModelAndView("app/core/partyorg/renamedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }



    /**
     * @param view
     * @return ModelAndView
     * @Title: goRevoke
     * @Description:  撤销党组织
     */
    @RequestMapping("/revoke")
    @RequiresPermissions("/app/core/partyorg/revoke")
    public ModelAndView goRevoke(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/revokemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/revoke");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goElection
     * @Description:  换届选举
     */
    @RequestMapping("/election")
    @RequiresPermissions("/app/core/partyorg/election")
    public ModelAndView goElection() {
        try {
            return new ModelAndView("app/core/partyorg/election");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /***
     *
     * @Title: goCommittee
     * @Description:  委员查询
     * @return
     * ModelAndView
     */
    @RequestMapping("/committee")
    @RequiresPermissions("/app/core/partyorg/committee")
    public ModelAndView goCommittee() {
        try {
            return new ModelAndView("app/core/partyorg/committee");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSetParty
     * @Description:  设置党组织机构
     */
    @RequestMapping("/partyorg")
    @RequiresPermissions("/app/core/partyorg/partyorg")
    public ModelAndView goPartyOrg() {
        try {
            return new ModelAndView("/app/core/partyorg/partyorg");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goUnitQuery
     * @Description: 单位信息查询
     */
    @RequestMapping("/unitquery")
    @RequiresPermissions("/app/core/partyorg/unitquery")
    public ModelAndView goUnitQuery() {
        try {
            return new ModelAndView("/app/core/partyorg/unitquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goUnit
     * @Description:  组织单位基本信息
     */
    @RequestMapping("/unit")
    @RequiresPermissions("/app/core/partyorg/unit")
    public ModelAndView goUnit(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyorg/unitmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyorg/unit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
