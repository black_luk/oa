/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetTaskController.java
 * @Package com.core136.controller.task
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月9日 下午1:43:08
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.task;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.task.Task;
import com.core136.bean.task.TaskGanttData;
import com.core136.bean.task.TaskGanttLink;
import com.core136.bean.task.TaskProcess;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.task.TaskGanttDataService;
import com.core136.service.task.TaskGanttLinkService;
import com.core136.service.task.TaskProcessService;
import com.core136.service.task.TaskService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

/**
 * @ClassName: RoutSetTaskController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月9日 下午1:43:08
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/taskset")
public class RouteSetTaskController {
    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    private TaskGanttDataService taskGanttDataService;

    @Autowired
    public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
        this.taskGanttDataService = taskGanttDataService;
    }

    private TaskGanttLinkService taskGanttLinkService;

    @Autowired
    public void setTaskGanttLinkService(TaskGanttLinkService taskGanttLinkService) {
        this.taskGanttLinkService = taskGanttLinkService;
    }

    private TaskProcessService taskProcessService;

    @Autowired
    public void setTaskProcessService(TaskProcessService taskProcessService) {
        this.taskProcessService = taskProcessService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     *
     * @Title: insertTaskProcess
     * @Description:  添加子任务处理结果
     * @param  taskProcess
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertTaskProcess", method = RequestMethod.POST)
    public RetDataBean insertTaskProcess(TaskProcess taskProcess, Double progress) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (progress > 100) {
                return RetDataTools.NotOk(MessageCode.MSG_00027);
            }
            if (progress <= 0) {
                return RetDataTools.NotOk(MessageCode.MSG_00028);
            }
            TaskGanttData taskGanttData = new TaskGanttData();
            taskGanttData.setProgress(progress);
            taskGanttData.setOrgId(account.getOrgId());
            Example example = new Example(TaskGanttData.class);
            example.createCriteria().andEqualTo("orgId", taskGanttData.getOrgId()).andEqualTo("taskDataId", taskProcess.getTaskDataId());
            taskGanttDataService.updateTaskGanttData(example, taskGanttData);
            taskProcess.setProcessId(SysTools.getGUID());
            taskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            taskProcess.setCreateUser(account.getAccountId());
            taskProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskProcessService.insertTaskProcess(taskProcess));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteTaskProcess
     * @Description:  删除子任务处理结果
     * @param  taskProcess
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteTaskProcess", method = RequestMethod.POST)
    public RetDataBean deleteTaskProcess(TaskProcess taskProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                taskProcess.setCreateUser(account.getAccountId());
            }
            taskProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskProcessService.deleteTaskProcess(taskProcess));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateTaskGanttLink
     * @Description:  修改子任务的处理过程
     * @param  taskProcess
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateTaskProcess", method = RequestMethod.POST)
    public RetDataBean updateTaskGanttLink(TaskProcess taskProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(TaskProcess.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", taskProcess.getProcessId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", taskProcess.getProcessId()).andEqualTo("createUser").andEqualTo(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskProcessService.updateTaskProcess(example, taskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertTaskGanttLink
     * @Description:  创建关联
     * @param  taskGanttLink
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertTaskGanttLink", method = RequestMethod.POST)
    public RetDataBean insertTaskGanttLink(TaskGanttLink taskGanttLink) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            taskGanttLink.setTaskLinkId(SysTools.getGUID());
            taskGanttLink.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            taskGanttLink.setCreateUser(account.getAccountId());
            taskGanttLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskGanttLinkService.insertTaskGanttLink(taskGanttLink));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: TaskGanttLink
     * @Description:  删除关联关系
     * @param  taskGanttLink
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteTaskGanttLink", method = RequestMethod.POST)
    public RetDataBean deleteTaskGanttLink(TaskGanttLink taskGanttLink) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskGanttLink.getTaskLinkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                taskGanttLink.setCreateUser(account.getAccountId());
            }
            taskGanttLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskGanttLinkService.deleteTaskGanttLink(taskGanttLink));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateTaskGanttLink
     * @Description:  更新子任务路径
     * @param  taskGanttLink
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateTaskGanttLink", method = RequestMethod.POST)
    public RetDataBean updateTaskGanttLink(TaskGanttLink taskGanttLink) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskGanttLink.getTaskLinkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(TaskGanttLink.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskLinkId", taskGanttLink.getTaskLinkId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskLinkId", taskGanttLink.getTaskLinkId()).andEqualTo("createUser").andEqualTo(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskGanttLinkService.updateTaskGanttLink(example, taskGanttLink));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: insertTaskGanttData
     * @Description:  创建子任务
     * @param  taskGanttData
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertTaskGanttData", method = RequestMethod.POST)
    public RetDataBean insertTaskGanttData(TaskGanttData taskGanttData) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            taskGanttData.setTaskDataId(SysTools.getGUID());
            taskGanttData.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            taskGanttData.setCreateUser(account.getAccountId());
            taskGanttData.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, taskGanttDataService.insertTaskGanttData(taskGanttData));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: TaskGanttData
     * @Description:  删除子任务
     * @param  taskGanttData
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteTaskGanttData", method = RequestMethod.POST)
    public RetDataBean TaskGanttData(TaskGanttData taskGanttData) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskGanttData.getTaskDataId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                taskGanttData.setCreateUser(account.getAccountId());
            }
            taskGanttData.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskGanttDataService.deleteTaskGanttData(taskGanttData));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateTaskGanttData
     * @Description:  更新子任务
     * @param  taskGanttData
     * @param  @return
     * @return: RetDataBean

     */

    @RequestMapping(value = "/updateTaskGanttData", method = RequestMethod.POST)
    public RetDataBean updateTaskGanttData(TaskGanttData taskGanttData) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(taskGanttData.getTaskDataId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(TaskGanttData.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskDataId", taskGanttData.getTaskDataId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskDataId", taskGanttData.getTaskDataId()).andEqualTo("createUser").andEqualTo(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskGanttDataService.updateTaskGanttData(example, taskGanttData));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: addTask
     * @Description:  创建任务
     * @param  task
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public RetDataBean addTask(Task task) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            task.setTaskId(SysTools.getGUID());
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            task.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            task.setStatus("0");
            task.setCreateUser(account.getAccountId());
            task.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskService.addTask(account, userInfo, task));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: deleteTask
     * @Description:  删除任务
     * @param  task
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteTask", method = RequestMethod.POST)
    public RetDataBean deleteTask(Task task) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(task.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                task.setCreateUser(account.getAccountId());
            }
            task.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskService.deleteTask(task));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateTask
     * @Description:  更新任务
     * @param  task
     * @param  @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateTask", method = RequestMethod.POST)
    public RetDataBean updateTask(Task task) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            if (StringUtils.isBlank(task.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(Task.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskId", task.getTaskId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskId", task.getTaskId()).andEqualTo("createUser").andEqualTo(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskService.updateTask(account, userInfo, task, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
