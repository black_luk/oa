package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyPostRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyPostRecordMapper extends MyMapper<PartyPostRecord> {
    /**
     * @param orgId
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyPostList
     * @Description:  获取党内任职记录列表
     */
    public List<Map<String, String>> getPartyPostList(@Param(value = "orgId") String orgId, @Param(value = "postType") String postType, @Param(value = "takeType") String takeType,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyPartyPostList
     * @Description:  获取个人党内任职记录
     */
    public List<Map<String, String>> getMyPartyPostList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

}
