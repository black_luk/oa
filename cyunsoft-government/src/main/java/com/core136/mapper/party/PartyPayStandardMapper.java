package com.core136.mapper.party;

import com.core136.bean.party.PartyPayStandard;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyPayStandardMapper extends MyMapper<PartyPayStandard> {

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPayStandardList
     * @Description:  获取党费缴纳标准列表
     */
    public List<Map<String, String>> getPayStandardList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
