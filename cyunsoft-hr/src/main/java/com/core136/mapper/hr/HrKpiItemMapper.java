package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiItem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrKpiItemMapper extends MyMapper<HrKpiItem> {

    /**
     * @param orgId
     * @param kpiType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrKpiItemList
     * @Description:  获取考核指标列表
     */
    public List<Map<String, String>> getHrKpiItemList(@Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser, @Param(value = "kpiType") String kpiType, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param kpiType
     * @return List<Map < String, String>>
     * @Title: getHrKpiItemListForSelect
     * @Description:  获取考核指标集
     */
    public List<Map<String, String>> getHrKpiItemListForSelect(@Param(value = "orgId") String orgId, @Param(value = "kpiType") String kpiType);
}
