package com.core136.mapper.hr;

import com.core136.bean.hr.HrPersonnelTransfer;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrPersonnelTransferMapper extends MyMapper<HrPersonnelTransfer> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param transferType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrPersonnelTransferList
     * @Description:  获取人员调动列表
     */
    public List<Map<String, String>> getHrPersonnelTransferList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                                @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "transferType") String transferType,
                                                                @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrPersonnelTransferList
     * @Description:  人个工作调动记录
     */
    public List<Map<String, String>> getMyHrPersonnelTransferList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
