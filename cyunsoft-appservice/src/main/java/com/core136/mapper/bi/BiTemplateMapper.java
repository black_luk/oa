package com.core136.mapper.bi;

import com.core136.bean.bi.BiTemplate;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BiTemplateMapper extends MyMapper<BiTemplate> {
    /**
     * @param @param  orgId
     * @param @param  levelId
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getBiTemplateList
     * @Description:  按分类获取模版列表
     */
    public List<Map<String, Object>> getBiTemplateList(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param levelId
     * @param accountId
     * @return List<Map < String, Object>>
     * @Title: getBiTemplateTree
     * @Description:  获取权限内的报表
     */
    public List<Map<String, Object>> getBiTemplateTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId, @Param(value = "accountId") String accountId);
}
