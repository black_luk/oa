package com.core136.bean.party;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyIntegral
 * @Description: 党员积分
 * @author: 稠云技术
 * @date: 2020年11月21日 下午11:51:16
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_integral")
public class PartyIntegral implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String memberId;
    private String year;
    private String verifyType;
    private Double score;
    private String remark;

    private String verifyTeam;
    private String verifyTime;//审核时间
    private String verifyIdea;//审核意见

    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVerifyTeam() {
        return verifyTeam;
    }

    public void setVerifyTeam(String verifyTeam) {
        this.verifyTeam = verifyTeam;
    }

    public String getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(String verifyTime) {
        this.verifyTime = verifyTime;
    }

    public String getVerifyIdea() {
        return verifyIdea;
    }

    public void setVerifyIdea(String verifyIdea) {
        this.verifyIdea = verifyIdea;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVerifyType() {
        return verifyType;
    }

    public void setVerifyType(String verifyType) {
        this.verifyType = verifyType;
    }


}
