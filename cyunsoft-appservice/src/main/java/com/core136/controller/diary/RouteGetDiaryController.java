package com.core136.controller.diary;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.oa.Diary;
import com.core136.bean.oa.DiaryPriv;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.oa.DiaryCommentsService;
import com.core136.service.oa.DiaryPrivService;
import com.core136.service.oa.DiaryService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/oaget")
public class RouteGetDiaryController {
    private DiaryPrivService diaryPrivService;

    @Autowired
    public void setDiaryPrivService(DiaryPrivService diaryPrivService) {
        this.diaryPrivService = diaryPrivService;
    }

    private DiaryService diaryService;

    @Autowired
    public void setDiaryService(DiaryService diaryService) {
        this.diaryService = diaryService;
    }

    private DiaryCommentsService diaryCommentsService;

    @Autowired
    public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
        this.diaryCommentsService = diaryCommentsService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @Title: getHomePageByAccountId
     * @Description:  获取通讯录的中的个人主页信息
     * @param: request
     * @param: accountId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHomePageByAccountId", method = RequestMethod.POST)
    public RetDataBean getHomePageByAccountId(String accountId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getHomePageByAccountId(account.getOrgId(), accountId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyDiaryInfo
     * @Description:  获取个人日志信息
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyDiaryInfo", method = RequestMethod.POST)
    public RetDataBean getMyDiaryInfo() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getMyDiaryInfo(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getDiaryCommentsList
     * @Description:  获取日志评论
     * @param: request
     * @param: diaryId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getDiaryCommentsList", method = RequestMethod.POST)
    public RetDataBean getDiaryCommentsList(String diaryId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryCommentsService.getDiaryCommentsList(account.getOrgId(), diaryId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getShowDiaryList
     * @Description: 获取他人分享的工作日志
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getShowDiaryList", method = RequestMethod.POST)
    public RetDataBean getShowDiaryList(
            PageParam pageParam,
            String beginTime,
            String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = diaryService.getShowDiaryList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMySubordinatesDiaryList
     * @Description:  获取下属的工作日志
     */
    @RequestMapping(value = "/getMySubordinatesDiaryList", method = RequestMethod.POST)
    public RetDataBean getMySubordinatesDiaryList(
            PageParam pageParam,
            String accountId,
            String beginTime,
            String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            List<String> list = new ArrayList<String>();
            if (StringUtils.isBlank(accountId)) {
                List<Map<String, String>> tempList = accountService.getMySubordinates(account.getOrgId(), account.getAccountId());
                for (int i = 0; i < tempList.size(); i++) {
                    list.add(tempList.get(i).get("accountId"));
                }
            } else {
                String[] arr = null;
                if (accountId.indexOf(",") > 0) {
                    arr = accountId.split(",");
                } else {
                    arr = new String[]{accountId};
                }
                list = new ArrayList<String>(Arrays.asList(arr));
            }
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = diaryService.getMySubordinatesDiaryList(pageParam, list, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param diary
     * @return RetDataBean
     * @Title: getMobileDiaryInfo
     * @Description:  获取个人工作日志
     */
    @RequestMapping(value = "/getMobileDiaryInfo", method = RequestMethod.POST)
    public RetDataBean getMobileDiaryInfo(Diary diary) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(diary.getDiaryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            diary.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.selectOneDiary(diary));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getDiaryPriv
     * @Description:  获取工作日志设置
     */
    @RequestMapping(value = "/getDiaryPriv", method = RequestMethod.POST)
    public RetDataBean getDiaryPriv() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            DiaryPriv diaryPriv = new DiaryPriv();
            diaryPriv.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryPrivService.selectOneDiaryPriv(diaryPriv));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getMyDiaryList
     * @Description:  获取当前用户的历史工作日志
     */
    @RequestMapping(value = "/getMyDiaryList", method = RequestMethod.POST)
    public RetDataBean getMyDiaryList(
            PageParam pageParam,
            String diaryDay,
            String accountId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrgId(account.getOrgId());
            if (StringUtils.isNotBlank(accountId)) {
                pageParam.setAccountId(accountId);
            } else {
                pageParam.setAccountId(account.getAccountId());
            }
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, Object>> pageInfo = diaryService.getMyDiaryList(pageParam, diaryDay);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @param  accountId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getOtherDiaryList
     * @Description:  获取他人的工作日志列表
     */
    @RequestMapping(value = "/getOtherDiaryList", method = RequestMethod.POST)
    public RetDataBean getOtherDiaryList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder,
            String accountId,
            String beginTime,
            String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(sort)) {
                sort = "create_time";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = sort + " " + sortOrder;
            PageInfo<Map<String, Object>> pageInfo = diaryService.getOtherDiaryList(pageNumber, pageSize, orderBy, account.getOrgId(), accountId, beginTime, endTime, "%" + search + "%");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  diary
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getDiaryById
     * @Description:  获取工作日志详情
     */
    @RequestMapping(value = "/getDiaryById", method = RequestMethod.POST)
    public RetDataBean getDiaryById(Diary diary) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(diary.getDiaryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            diary.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.selectOneDiary(diary));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
