$(function () {
    $('#remark').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        insertDuesRecord();
    })
    jeDate("#year", {
        format: "YYYY"
    })
    jeDate("#duesTime", {
        format: "YYYY-MM-DD"
    })
})

function insertDuesRecord() {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("关联党员不能为空！")
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyDues",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            remark: $("#remark").code(),
            duesType: $("#duesType").val(),
            year: $("#year").val(),
            month: $("#month").val(),
            dues: $("#dues").val(),
            realDues: $("#realDues").val(),
            duesTime: $("#duesTime").val(),
            onTime: $("#onTime").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
