package com.core136.controller.notice;

import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core")
public class PageNoticeController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /***
     * 通知公告查看情况
     * @return
     */
    @RequestMapping("/notice/readstatus")
    public ModelAndView goNoticeReadStatus() {
        try {
            return new ModelAndView("app/core/notice/readstatus");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    @RequestMapping("/notice/details")
    @RequiresPermissions("/app/core/notice/details")
    public ModelAndView details(HttpServletRequest request) {
        ModelAndView mv = null;
        try {
            if (SysTools.isMobileDevice(request)) {
                mv = new ModelAndView("app/mobile/main/notice/details");
            } else {
                mv = new ModelAndView("app/core/notice/details");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title sendNotice
     * @Description  跳转通知公告
     */
    @RequestMapping(value = "/notice/index", method = RequestMethod.GET)
    @RequiresPermissions("/app/core/notice/index")
    public ModelAndView sendNotice(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/notice/index");
            } else {
                if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/notice/editnotice");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @param request
     * @return ModelAndView 返回类型
     * @Title: setNoticePriv
     * @Description:  设置通知公告权限
     */
    @RequestMapping(value = "/notice/setnoticepriv", method = RequestMethod.GET)
    @RequiresPermissions("/app/core/notice/setnoticepriv")
    public ModelAndView setNoticePriv(HttpServletRequest request) {
        return new ModelAndView("app/core/notice/priv");
    }

    /**
     * @param @param request
     * @return ModelAndView 返回类型
     * @Title: setNoticePriv
     * @Description:  通知公告维护
     */
    @RequestMapping(value = "/notice/managenotice", method = RequestMethod.GET)
    @RequiresPermissions("/app/core/notice/managenotice")
    public ModelAndView ManageNotice(HttpServletRequest request) {
        return new ModelAndView("app/core/notice/managenotice");
    }

    /**
     * @param @param request
     * @return ModelAndView 返回类型
     * @Title: setNoticePriv
     * @Description:  通知公告维护
     */
    @RequestMapping(value = "/notice/readnotice", method = RequestMethod.GET)
    @RequiresPermissions("/app/core/notice/readnotice")
    public ModelAndView ReadNotice(HttpServletRequest request) {
        return new ModelAndView("app/core/notice/readnotice");
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: doApprovalNotice
     * @Description: 通知公告审批
     */
    @RequestMapping(value = "/notice/approvalnotice", method = RequestMethod.GET)
    @RequiresPermissions("/app/core/notice/approvalnotice")
    public ModelAndView doApprovalNotice(HttpServletRequest request) {
        return new ModelAndView("app/core/notice/approvalnotice");
    }

}
