/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetOfficesuppliesController.java
 * @Package com.core136.controller.officesupplies
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月31日 下午2:22:12
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.officesupplies;


import com.alibaba.fastjson.JSON;
import com.core136.bean.account.Account;
import com.core136.bean.officesupplies.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.officesupplies.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author lsq
 *
 */
@RestController
@RequestMapping("/set/officesuppliesset")
public class RouteSetOfficesuppliesController {
    private OfficeSuppliesSortService officeSuppliesSortService;

    @Autowired
    public void setOfficeSuppliesSortService(OfficeSuppliesSortService officeSuppliesSortService) {
        this.officeSuppliesSortService = officeSuppliesSortService;
    }

    private OfficeSuppliesService officeSuppliesService;

    @Autowired
    public void setOfficeSuppliesService(OfficeSuppliesService officeSuppliesService) {
        this.officeSuppliesService = officeSuppliesService;
    }

    private OfficeSuppliesUnitService officeSuppliesUnitService;

    @Autowired
    public void setOfficeSuppliesUnitService(OfficeSuppliesUnitService officeSuppliesUnitService) {
        this.officeSuppliesUnitService = officeSuppliesUnitService;
    }

    private OfficeSuppliesApprovalService officeSuppliesApprovalService;

    @Autowired
    public void setOfficeSuppliesApprovalService(OfficeSuppliesApprovalService officeSuppliesApprovalService) {
        this.officeSuppliesApprovalService = officeSuppliesApprovalService;
    }

    private OfficeSuppliesApplyService officeSuppliesApplyService;

    @Autowired
    public void setOfficeSuppliesApplyService(OfficeSuppliesApplyService officeSuppliesApplyService) {
        this.officeSuppliesApplyService = officeSuppliesApplyService;
    }

    private OfficeSuppliesGrantService officeSuppliesGrantService;

    @Autowired
    public void setOfficeSuppliesGrantService(OfficeSuppliesGrantService officeSuppliesGrantService) {
        this.officeSuppliesGrantService = officeSuppliesGrantService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     *
     * @Title: importOfficeSuppliesInfo
     * @Description:  办公用品导入
     * @param file
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/importOfficeSuppliesInfo", method = RequestMethod.POST)
    public void importOfficeSuppliesInfo(HttpServletResponse response, MultipartFile file) {
        PrintWriter out = null;
        try {
            RetDataBean retDataBean = new RetDataBean();
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:insert")) {
                retDataBean = RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                retDataBean = officeSuppliesService.importOfficeSuppliesInfo(account, file);
            }
            String s = JSON.toJSONString(retDataBean);
            response.setContentType("text/html; charset=utf-8");
            out = response.getWriter().append(s);
            out.close();
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     *
     * @Title: insertOfficeSuppliesGrant
     * @Description:  添加发放记录
     * @param: request
     * @param: officeSuppliesGrant
     * @param: @return
     * @return: RetDataBean
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/insertOfficeSuppliesGrant", method = RequestMethod.POST)
    public RetDataBean insertOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            OfficeSuppliesApply officeSuppliesApply = new OfficeSuppliesApply();
            officeSuppliesApply.setOrgId(account.getOrgId());
            officeSuppliesApply.setApplyId(officeSuppliesGrant.getApplyId());
            officeSuppliesApply = officeSuppliesApplyService.selectOneOfficeSuppliesApply(officeSuppliesApply);
            int quantity = officeSuppliesApply.getQuantity();
            int count = officeSuppliesGrantService.getGrantCount(account.getOrgId(), officeSuppliesGrant.getApplyId()) + officeSuppliesGrant.getQuantity();
            if (count <= quantity) {
                if (count == quantity) {
                    OfficeSuppliesApply officeSuppliesApply1 = new OfficeSuppliesApply();
                    officeSuppliesApply1.setOrgId(officeSuppliesApply.getOrgId());
                    officeSuppliesApply1.setStatus("2");
                    officeSuppliesApply1.setApplyId(officeSuppliesApply.getApplyId());
                    Example example = new Example(OfficeSuppliesApply.class);
                    example.createCriteria().andEqualTo("orgId", officeSuppliesApply.getApplyId()).andEqualTo("applyId", officeSuppliesApply.getApplyId());
                    officeSuppliesApplyService.updateOfficeSuppliesApply(example, officeSuppliesApply1);
                }

                OfficeSupplies officeSupplies = new OfficeSupplies();
                officeSupplies.setOrgId(account.getOrgId());
                officeSupplies.setSuppliesId(officeSuppliesApply.getSuppliesId());
                officeSupplies = officeSuppliesService.selectOneOfficeSupplies(officeSupplies);
                officeSupplies.setQuantity(officeSupplies.getQuantity() - quantity);
                Example example1 = new Example(OfficeSupplies.class);
                example1.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("suppliesId", officeSupplies.getSuppliesId());
                officeSuppliesService.updateOfficeSupplies(example1, officeSupplies);

                officeSuppliesGrant.setGrantId(SysTools.getGUID());
                officeSuppliesGrant.setSuppliesId(officeSuppliesApply.getSuppliesId());
                officeSuppliesGrant.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                officeSuppliesGrant.setCreateUser(account.getAccountId());
                officeSuppliesGrant.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesGrantService.insertOfficeSuppliesGrant(officeSuppliesGrant));
            } else {
                return RetDataTools.NotOk(MessageCode.MSG_00020);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertOfficeSuppliesApproval
     * @Description:  添加审批记录
     * @param: request
     * @param: officeSuppliesApproval
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertOfficeSuppliesApproval", method = RequestMethod.POST)
    public RetDataBean insertOfficeSuppliesApproval(OfficeSuppliesApproval officeSuppliesApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesApproval.setApprovalId(SysTools.getGUID());
            officeSuppliesApproval.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            officeSuppliesApproval.setCreateUser(account.getAccountId());
            officeSuppliesApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, officeSuppliesApprovalService.approvalOfficeSuppliesApply(officeSuppliesApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteOfficeSuppliesApproval
     * @Description:  删除审批记录
     * @param: request
     * @param: officeSuppliesApproval
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteOfficeSuppliesApproval", method = RequestMethod.POST)
    public RetDataBean deleteOfficeSuppliesApproval(OfficeSuppliesApproval officeSuppliesApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSuppliesApproval.getApprovalId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            officeSuppliesApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesApprovalService.deleteOfficeSuppliesApproval(officeSuppliesApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateofficeSuppliesApproval
     * @Description:  更新审批记录
     * @param: request
     * @param: officeSuppliesApproval
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateofficeSuppliesApproval", method = RequestMethod.POST)
    public RetDataBean updateofficeSuppliesApproval(OfficeSuppliesApproval officeSuppliesApproval) {
        try {
            if (StringUtils.isBlank(officeSuppliesApproval.getApprovalId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(OfficeSuppliesApproval.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("approvalId", officeSuppliesApproval.getApprovalId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesApprovalService.updateOfficeSuppliesApproval(example, officeSuppliesApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertOfficeSuppliesApply
     * @Description:  发起申请
     * @param: request
     * @param: officeSuppliesApply
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertOfficeSuppliesApply", method = RequestMethod.POST)
    public RetDataBean insertOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesApply.setApplyId(SysTools.getGUID());
            officeSuppliesApply.setStatus("0");
            officeSuppliesApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            officeSuppliesApply.setCreateUser(account.getAccountId());
            officeSuppliesApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, officeSuppliesApplyService.insertOfficeSuppliesApply(officeSuppliesApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteOfficeSuppliesApply
     * @Description:  删除申请
     * @param: request
     * @param: officeSuppliesApply
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteOfficeSuppliesApply", method = RequestMethod.POST)
    public RetDataBean deleteOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSuppliesApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            officeSuppliesApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesApplyService.deleteOfficeSuppliesApply(officeSuppliesApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateOfficeSuppliesApply
     * @Description:  更新申请
     * @param: request
     * @param: officeSuppliesApply
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateOfficeSuppliesApply", method = RequestMethod.POST)
    public RetDataBean updateOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        try {
            if (StringUtils.isBlank(officeSuppliesApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(OfficeSuppliesApply.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("applyId", officeSuppliesApply.getApplyId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesApplyService.updateOfficeSuppliesApply(example, officeSuppliesApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: insertOfficeSuppliesUnit
     * @Description:  添加办公用品计量单位
     * @param: request
     * @param: officeSuppliesUnit
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertOfficeSuppliesUnit", method = RequestMethod.POST)
    public RetDataBean insertOfficeSuppliesUnit(OfficeSuppliesUnit officeSuppliesUnit) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesUnit.setUnitId(SysTools.getGUID());
            officeSuppliesUnit.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            officeSuppliesUnit.setCreateUser(account.getAccountId());
            officeSuppliesUnit.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesUnitService.insertOfficeSuppliesUnit(officeSuppliesUnit));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteOfficeSuppliesUnit
     * @Description:  删除办公用品计量单位
     * @param officeSuppliesUnit
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteOfficeSuppliesUnit", method = RequestMethod.POST)
    public RetDataBean deleteOfficeSuppliesUnit(OfficeSuppliesUnit officeSuppliesUnit) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSuppliesUnit.getUnitId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            officeSuppliesUnit.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesUnitService.deleteOfficeSuppliesUnit(officeSuppliesUnit));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateOfficeSuppliesUnit
     * @Description:  更新办公用品计量单位
     * @param officeSuppliesUnit
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateOfficeSuppliesUnit", method = RequestMethod.POST)
    public RetDataBean updateOfficeSuppliesUnit(OfficeSuppliesUnit officeSuppliesUnit) {
        try {
            if (StringUtils.isBlank(officeSuppliesUnit.getUnitId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(OfficeSuppliesUnit.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("unitId", officeSuppliesUnit.getUnitId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesUnitService.updateOfficeSuppliesUnit(example, officeSuppliesUnit));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertOfficeSuppliesSort
     * @Description:  添加办公用分类
     * @param officeSuppliesSort
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/insertOfficeSuppliesSort", method = RequestMethod.POST)
    public RetDataBean insertOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesSort.setSortId(SysTools.getGUID());
            officeSuppliesSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            officeSuppliesSort.setCreateUser(account.getAccountId());
            officeSuppliesSort.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(officeSuppliesSort.getParentId())) {
                officeSuppliesSort.setParentId("0");
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesSortService.insertOfficeSuppliesSort(officeSuppliesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteOfficeSuppliesSort
     * @Description:  删除办公用分类
     * @param officeSuppliesSort
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteOfficeSuppliesSort", method = RequestMethod.POST)
    public RetDataBean deleteOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSuppliesSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            officeSuppliesSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesSortService.deleteOfficeSuppliesSort(officeSuppliesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateOfficeSuppliesSort
     * @Description:  更新办公用分类
     * @param officeSuppliesSort
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateOfficeSuppliesSort", method = RequestMethod.POST)
    public RetDataBean updateOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        try {
            if (StringUtils.isBlank(officeSuppliesSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (officeSuppliesSort.getSortId().equals(officeSuppliesSort.getParentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSuppliesSort.getParentId())) {
                officeSuppliesSort.setParentId("0");
            }
            Example example = new Example(OfficeSuppliesSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", officeSuppliesSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesSortService.updateOfficeSuppliesSort(example, officeSuppliesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertOfficeSupplies
     * @Description:  添加办公用品
     * @param: request
     * @param: officeSupplies
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertOfficeSupplies", method = RequestMethod.POST)
    public RetDataBean insertOfficeSupplies(OfficeSupplies officeSupplies) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSupplies.setSuppliesId(SysTools.getGUID());
            officeSupplies.setStatus("0");
            officeSupplies.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            officeSupplies.setCreateUser(account.getAccountId());
            officeSupplies.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesService.insertOfficeSupplies(officeSupplies));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteOfficeSupplies
     * @Description:  删除办公用品
     * @param: request
     * @param: officeSupplies
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteOfficeSupplies", method = RequestMethod.POST)
    public RetDataBean deleteOfficeSupplies(OfficeSupplies officeSupplies) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(officeSupplies.getSuppliesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            officeSupplies.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesService.deleteOfficeSupplies(officeSupplies));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateOfficeSupplies
     * @Description:  更新办公用品信息
     * @param: request
     * @param: officeSupplies
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateOfficeSupplies", method = RequestMethod.POST)
    public RetDataBean updateOfficeSupplies(OfficeSupplies officeSupplies) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(officeSupplies.getSuppliesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(OfficeSupplies.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("suppliesId", officeSupplies.getSuppliesId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesService.updateOfficeSupplies(example, officeSupplies));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
