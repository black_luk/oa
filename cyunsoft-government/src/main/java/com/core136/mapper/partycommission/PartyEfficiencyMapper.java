package com.core136.mapper.partycommission;

import com.core136.bean.partycommission.PartyEfficiency;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyEfficiencyMapper extends MyMapper<PartyEfficiency> {
    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyEfficiencyList
     * @Description:  效能监察工作记录列表
     */
    public List<Map<String, String>> getPartyEfficiencyList(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
