let zTree;
let priv;
let fileId = "";
let fileType = "";
let optType = ""
let setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/fileget/getMyPublicFolderInPriv",// Ajax 获取数据的 URL 地址
        autoParam: ["folderId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "folderId",
            pIdKey: "folderLevel",
            rootPId: "0"
        },
        key: {
            name: "folderName"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    $("#titdiv").hide();
    $("#fileList").show();
    $("#folderNameHead").html(treeNode.folderName);
    $("#folderNameHead").attr("data-value", treeNode.folderId);
    createOptDiv(treeNode.folderId)
    showFiles(treeNode.folderId);
}

$(function () {
    $.ajax({
        url: "/ret/fileget/getMyPublicFolderInPriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: "0"
        },
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
});

function createOptDiv(folderId) {
    getSmsConfig("msgType", "publicFile");
    $.ajax({
        url: "/ret/fileget/getPublicFolderPrivInfo",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId
        },
        success: function (data) {
            priv = data.list;
            if (data.status == "200") {
                var optSet = [];
                if (data.list.createPriv == true) {
                    optSet = [{
                        name: '新建文件夹',
                        onClick: function () {
                            $("#createfoldermodal").modal("show");
                            $(".js-foldersave").unbind("click").click(function () {
                                createFolder();
                            });
                        }
                    }, {
                        name: '新建文件',
                        value: $(this).attr("data-value"),
                        onClick: function () {
                            $("#show_publicFile").html("");
                            $("#publicFile").val();
                            $("#publicFile").attr("data_value", "");
                            $("#createfilemodal").modal("show");
                            $(".js-filesave").unbind("click").click(function () {
                                createFile();
                            });
                        }
                    }];
                }
                if (data.list.managePriv == true) {
                    optSet = [{
                        name: '新建文件夹',
                        onClick: function () {
                            $("#createfoldermodal").modal("show");
                            $(".js-foldersave").unbind("click").click(function () {
                                createFolder();
                            });
                        }
                    }, {
                        name: '新建文件',
                        value: $(this).attr("data-value"),
                        onClick: function () {
                            $("#show_publicFile").html("");
                            $("#publicFile").val();
                            $("#publicFile").attr("data_value", "");
                            $("#createfilemodal").modal("show");
                            $(".js-filesave").unbind("click").click(function () {
                                createFile();
                            });
                        }
                    }, {
                        name: '粘贴',
                        value: $(this).attr("data-value"),
                        onClick: function () {
                            paste();
                        }
                    }];
                }
                if (optSet.length > 0) {
                    new BootstrapMenu("#fileList", {
                        actions: optSet
                    });
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function createFile() {
    $.ajax({
        url: "/set/fileset/createPublicFile",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#fileSortNo").val(),
            folderId: $("#folderNameHead").attr("data-value"),
            attach: $("#publicFile").attr("data_value"),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                console.log(data.msg);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#createfilemodal").modal("hide");
                showFiles($("#folderNameHead").attr("data-value"))
            }
        }
    });
}

function showFiles(folderId) {
    $.ajax({
        url: "/ret/fileget/getPublicFilelist",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                console.log(data.msg);
            } else {
                crateFileDiv(data.list, folderId)
            }
        }
    });
}

function crateFileDiv(data, folderId) {
    $(".bs-glyphicons-list").html("");
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].isFile == "true") {
                $(".bs-glyphicons-list").append("<li class='js-file' ext_name=" + data[i].extName + " id='file_id_" + i + "' data-value='" + data[i].fileId + "' attach='" + data[i].attach + "'  title='" + data[i].fileName + "'><span style='font-size:40px;color: #428bca;' class='"
                    + getfileClass(data[i].fileName) + "'></span><span class='glyphicon-class filename'>" + data[i].fileName
                    + "</span><span style='padding-top: 5px;' class='glyphicon-class'>" + parseFloat(data[i].fileSize / 1024).toFixed(2) + "KB</span></li>");
            } else {
                $(".bs-glyphicons-list").append(
                    "<li class='js-floder' data-floder-name='" + data[i].fileName + "' id='file_id_" + i + "' data-value='" + data[i].fileId
                    + "' title='" + data[i].fileName + "'><span style='font-size:40px;color: #428bca;' class='fa fa-folder-open-o'></span><span class='glyphicon-class filename'>"
                    + data[i].fileName + "</span></li>");
            }
        }
    } else {
        $(".bs-glyphicons-list").html("");
    }

    $(".js-floder").each(function () {
        $(this).unbind("mousedown").mousedown(function (e) {
            var floderId = $("#folderNameHead").attr("data-value");
            var id = $(this).attr("id");
            var value = $(this).attr("data-value");
            var floderName = $(this).attr("data-floder-name");
            var actArr = [];
            if (priv.managePriv == true) {
                var json8 = {};
                json8.name = "打开目录";
                json8.value = value;
                json8.onClick = function () {
                    openFolder(value, floderName)
                }
                actArr.push(json8);
                var json3 = {};
                json3.name = "复制";
                json3.value = value;
                json3.onClick = function () {
                    copyFolder(value)
                }
                actArr.push(json3);
                var json5 = {};
                json5.name = "剪切";
                json5.value = value;
                json5.onClick = function () {
                    shearFolder(value)
                }
                actArr.push(json5);
                var json9 = {};
                json9.name = "删除";
                json9.value = value;
                json9.onClick = function () {
                    delFolder(value)
                }
                actArr.push(json9);
                var json6 = {};
                json6.name = "重命名";
                json6.value = value;
                json6.onClick = function () {
                    renameFolder(value, 'folder')
                }
                actArr.push(json6);
            } else {
                var json7 = {};
                json7.name = "打开目录";
                json7.value = value;
                var value = $(this).attr("data-value");
                var floderName = $(this).attr("data-floder-name");
                json7.onClick = function () {
                    openFolder(value, floderName)
                }
                actArr.push(json7);
            }
            showMenu(id, actArr);
            //event.stopPropagation();
        });
    });
    $(".js-file").each(function () {
        $(this).unbind("mousedown").mousedown(function (e) {
            var floderId = $("#folderNameHead").attr("data-value");
            $(".js-file").removeAttr("style");
            $(this).css({
                "background-color": "#e46f61",
                "color": "#fff"
            });
            var id = $(this).attr("id");
            var value = $(this).attr("data-value");
            var attachId = $(this).attr("attach");
            var extName = $(this).attr("ext_name");
            var actArr = [];
            if (priv.managePriv == true) {
                var json3 = {};
                json3.name = "复制";
                json3.value = value;
                json3.onClick = function () {
                    copyFile(value)
                }
                actArr.push(json3);
                var json5 = {};
                json5.name = "剪切";
                json5.value = value;
                json5.onClick = function () {
                    shearFile(value)
                }
                actArr.push(json5);
                var json9 = {};
                json9.name = "删除";
                json9.value = value;
                json9.onClick = function () {
                    delFile(value)
                }
                actArr.push(json9);
                if (extName != '.txt' && extName != '.jpg' && extName != '.png' && extName != '.jpeg' && extName != '.gif' && extName != '.tit' && extName != '.bmp') {
                    var json7 = {};
                    json7.name = "编辑";
                    json7.onClick = function () {
                        openFileOnLine(extName, attachId, 4);
                    }
                    actArr.push(json7);
                }
                var json20 = {};
                json20.name = "历史版本";
                json20.onClick = function () {
                    showFileVersionList(attachId);
                }
                actArr.push(json20);
                var json30 = {};
                json30.name = "重命名";
                json30.value = value;
                json30.onClick = function () {
                    renameFolder(value, 'file')
                }
                actArr.push(json30);
                var json8 = {};
                json8.name = "查看";
                json8.onClick = function () {
                    openFileOnLine(extName, attachId, 1);
                }
                actArr.push(json8);
                var json9 = {};
                json9.name = "下载";
                json9.value = value;
                json9.onClick = function () {
                    downFile(value)
                }
                actArr.push(json9);
                if(officetype!="3") {
                    var json10 = {};
                    json10.name = "预览";
                    json10.value = value;
                    json10.onClick = function () {
                        previewonline(attachId)
                    }
                    actArr.push(json10);
                }
            } else {
                var json7 = {};
                json7.name = "查看";
                json7.value = value;
                json7.onClick = function () {
                    openFileOnLine(extName, attachId, 1);
                }
                actArr.push(json7);
                var json8 = {};
                json8.name = "下载";
                json8.value = value;
                json8.onClick = function () {
                    downFile(value)
                }
                actArr.push(json8);
                if(officetype!="3") {
                    var json10 = {};
                    json10.name = "预览";
                    json10.value = value;
                    json10.onClick = function () {
                        previewonline(attachId)
                    }
                    actArr.push(json10);
                }
            }
            showMenu(id, actArr);
            //event.stopPropagation();
        });
    });
    $(".js-floder").each(function () {
        $(this).dblclick(function () {
            var levelFolder = $(this).attr("data-value");
            $("#folderNameHead").html($(this).attr("data-floder-name"));
            $("#folderNameHead").attr("data-value", levelFolder);
            showFiles(levelFolder);

        })
    });
}

function openFolder(levelFolder, floderName) {
    $("#folderNameHead").html(floderName);
    $("#folderNameHead").attr("data-value", levelFolder);
    showFiles(levelFolder);
}

function renameFolder(fileId, fType) {
    document.getElementById("form3").reset();
    $("#renamemodal").modal("show");
    $(".js-rename").unbind("click").click(function () {
        if (confirm("确定修改当前文件名称吗？")) {
            $.ajax({
                url: "/set/fileset/updatePublicFileFolder",
                type: "post",
                dataType: "json",
                data: {
                    fileId: fileId,
                    type: fType,
                    fileName: $("#fileName").val()
                },
                success: function (data) {
                    if (data.status == "200") {
                        layer.msg(sysmsg[data.msg]);
                        $("#renamemodal").modal("hide");
                        var folderId = $("#folderNameHead").attr("data-value");
                        showFiles(folderId);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        } else {
            return;
        }
    });
}

function showMenu(id, actArr) {
    //$(".bootstrapMenu").remove();
    new BootstrapMenu("#" + id, {
        actions: actArr
    });
    //event.stopPropagation();
}

function createFolder() {
    $.ajax({
        url: "/set/fileset/createPublicFileFolder",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            folderName: $("#folderName").val(),
            folderLevel: $("#folderNameHead").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                console.log(data.msg);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#createfoldermodal").modal("hide");
                showFiles($("#folderNameHead").attr("data-value"));
            }
        }
    });
}

function downFile(value, netDiskId) {
    window.open("/ret/fileget/downPublicFileFile?fileId=" + value);
}


function previewonline(value) {
    window.open("/app/core/previewonline?attachId=" + value);
}

function delFolder(folderId) {
    if (confirm("确定删除文件夹吗？")) {
        $.ajax({
            url: "/set/fileset/deletePublicFileFolder",
            type: "post",
            dataType: "json",
            data: {
                folderId: folderId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    showFiles($("#folderNameHead").attr("data-value"));
                }
            }
        });
    }
}

function shearFolder(value) {
    optType = "1";
    fileId = value;
    layer.msg("剪切文件夹成功!请选择粘贴位置!");
    fileType = "folder";
}

function shearFile(value) {
    optType = "1";
    fileId = value;
    layer.msg("剪切文件成功!请选择粘贴位置!");
    fileType = "file";
}

function copyFile(value) {
    optType = "2";
    fileId = value;
    layer.msg("复制文件成功!请选择粘贴位置!");
    fileType = "file";
}

function copyFolder(value) {
    optType = "2";
    fileId = value;
    layer.msg("复制文件夹成功!请选择粘贴位置!");
    fileType = "folder";
}


function paste() {
    if (fileId == "" || fileId == null) {
        layer.msg("请选择需要粘贴的文件!");
        return;
    }
    if (optType == "2") {
        $.ajax({
            url: "/set/fileset/pastePublicFile",
            type: "post",
            dataType: "json",
            data: {
                fileId: fileId,
                currentLocation: $("#folderNameHead").attr("data-value"),
                type: fileType

            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    } else if (optType == "1") {
        if (fileId == "" || fileId == null) {
            layer.msg("请选择需要粘贴的文件!");
            return;
        }
        $.ajax({
            url: "/set/fileset/shearPublicFile",
            type: "post",
            dataType: "json",
            data: {
                fileId: fileId,
                currentLocation: $("#folderNameHead").attr("data-value"),
                type: fileType

            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function delFile(fileId) {
    if (confirm("确定删除文件吗？")) {
        $.ajax({
            url: "/set/fileset/deletePublicFile",
            type: "post",
            dataType: "json",
            data: {
                fileId: fileId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    showFiles($("#folderNameHead").attr("data-value"));
                }
            }
        });
    }
}
