package com.core136.mapper.personnel;

import com.core136.bean.personnel.PerDept;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PerDeptMapper extends MyMapper<PerDept> {
}
