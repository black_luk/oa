/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetNoticeController.java
 * @Package com.core136.controller.notice
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年7月20日 上午10:17:48
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.notice;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.notice.Notice;
import com.core136.bean.notice.NoticeConfig;
import com.core136.bean.notice.NoticeTemplate;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.notice.NoticeConfigService;
import com.core136.service.notice.NoticeService;
import com.core136.service.notice.NoticeTemplateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;


/**
 * @author lsq
 * @ClassName: RoutSetNoticeController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年7月20日 上午10:17:48
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/noticeset")
public class RouteSetNoticeController {
    private NoticeTemplateService noticeTemplateService;

    @Autowired
    public void setNoticeTemplateService(NoticeTemplateService noticeTemplateService) {
        this.noticeTemplateService = noticeTemplateService;
    }

    private NoticeConfigService noticeConfigService;

    @Autowired
    public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
        this.noticeConfigService = noticeConfigService;
    }

    private NoticeService noticeService;

    @Autowired
    public void setNoticeService(NoticeService noticeService) {
        this.noticeService = noticeService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
    /**
     * @param notice
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: sendNotice
     * @Description:  发布公告
     */
    @RequestMapping(value = "/sendNotice", method = RequestMethod.POST)
    public RetDataBean sendNotice(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            if (StringUtils.isBlank(notice.getNoticeType())) {
                return RetDataTools.NotOk(MessageCode.MSG_00019);
            }
            if (StringUtils.isNotBlank(notice.getContent())) {
                Document htmlDoc = Jsoup.parse(notice.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                notice.setSubheading(subheading);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            notice.setNoticeId(SysTools.getGUID());
            notice.setCreateUser(account.getAccountId());
            notice.setDelFlag("0");
            notice.setApprovalStatus("0");
            notice.setOnclickCount("0");
            notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            notice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeService.sendNotice(notice, userInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新通知公告状态
     *
     * @param notice
     * @return
     */
    @RequestMapping(value = "/setNoticeStatus", method = RequestMethod.POST)
    public RetDataBean setNoticeStatus(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(Notice.class);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", account.getOrgId());
            notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeService.updateNotice(notice, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param notice
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateNotice
     * @Description:  更新通知公告
     */
    @RequestMapping(value = "/updateNotice", method = RequestMethod.POST)
    public RetDataBean updateNotice(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isNotBlank(notice.getContent())) {
                Document htmlDoc = Jsoup.parse(notice.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                notice.setSubheading(subheading);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            notice.setOrgId(account.getOrgId());
            Example example = new Example(Notice.class);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());
            }
            if (StringUtils.isNotBlank(notice.getIsTop())) {
                if (notice.getIsTop().equals("1")) {
                    notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                }
            }
            criteria.andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", account.getOrgId());
            notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeService.reEditNotice(userInfo, notice, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param notice
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: approval
     * @Description:  审批通知公告
     */

    @RequestMapping(value = "/approval", method = RequestMethod.POST)
    public RetDataBean approval(Notice notice) {
        try {
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isBlank(notice.getStatus())) {
                notice.setStatus("0");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            notice.setOrgId(account.getOrgId());
            Example example = new Example(Notice.class);
            example.createCriteria().andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", notice.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, noticeService.updateNotice(notice, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param noticeTemplate
     * @param @return        设定文件
     * @return RetDataBean 返回类型
     * @Title: inertNoticeTemplate
     * @Description:  添加通知公告红头模版
     */
    @RequestMapping(value = "/addNoticeTemplate", method = RequestMethod.POST)
    public RetDataBean inertNoticeTemplate(NoticeTemplate noticeTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            noticeTemplate.setTemplateId(SysTools.getGUID());
            noticeTemplate.setOrgId(account.getOrgId());
            noticeTemplate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            noticeTemplate.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeTemplateService.insertNoticeTemplate(noticeTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: initNocticeConfig
     * @Description:  初始化通知公告配置
     */
    @RequestMapping(value = "/initNocticeConfig", method = RequestMethod.POST)
    public RetDataBean initNocticeConfig() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeConfigService.initNocticeConfig(account, "notice"));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param noticeTemplate
     * @param @return        设定文件
     * @return RetDataBean 返回类型
     * @Title: delNoticeTemplate
     * @Description:  删除红头模版
     */
    @RequestMapping(value = "/delNoticeTemplate", method = RequestMethod.POST)
    public RetDataBean delNoticeTemplate(NoticeTemplate noticeTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(noticeTemplate.getTemplateId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                noticeTemplate.setCreateUser(account.getAccountId());
            }
            noticeTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, noticeTemplateService.delNoticeTemplate(noticeTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: delNotice
     * @Description:  删除通知公告
     * @param: request
     * @param: notice
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delNotice", method = RequestMethod.POST)
    public RetDataBean delNotice(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                notice.setCreateUser(account.getAccountId());
            }
            notice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, noticeService.deleteNotice(notice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param noticeTemplate
     * @param @return        设定文件
     * @return RetDataBean 返回类型
     * @Title: updateNoticeTemplate
     * @Description:  更新模版信息
     */
    @RequestMapping(value = "/updateNoticeTemplate", method = RequestMethod.POST)
    public RetDataBean updateNoticeTemplate(NoticeTemplate noticeTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(noticeTemplate.getTemplateId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(NoticeTemplate.class);
            if (!account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("templateId", noticeTemplate.getTemplateId()).andEqualTo("createUser", account.getAccountId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("templateId", noticeTemplate.getTemplateId());
            }
            noticeTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeTemplateService.updateNoticeTemplate(noticeTemplate, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param noticeConfig
     * @param @return      设定文件
     * @return RetDataBean 返回类型
     * @Title: updateNoticeConfig
     * @Description:  更新配置
     */
    @RequestMapping(value = "/updateNoticeConfig", method = RequestMethod.POST)
    public RetDataBean updateNoticeConfig(NoticeConfig noticeConfig) {
        try {
            if (StringUtils.isBlank(noticeConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            noticeConfig.setOrgId(account.getOrgId());
            Example example = new Example(NoticeConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("configId", noticeConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeConfigService.updateNoticeConfig(noticeConfig, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
