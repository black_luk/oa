$(function () {
    jeDate("#pointsYear", {
        format: "YYYY",
        isinitVal: true
    });
    $("#createbut").unbind("click").click(function () {
        insertPartyMemberPoints();
    })
})

function insertPartyMemberPoints() {
    $.ajax({
        url: "/set/partymemberset/insertPartyMemberPoints",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            item: $("#item").val(),
            pointsYear: $("#pointsYear").val(),
            points: $("#points").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
