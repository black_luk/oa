let ue = UE.getEditor("remark");
$(function () {
    jeDate("#careTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-add-save").unbind("click").click(function () {
        addHrCareRecord();
    })
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })

})

function addHrCareRecord() {
    if($("#subject").val()=="")
    {
        layer.msg("关怀标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrCareRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            subject: $("#subject").val(),
            userId: $("#userId").attr("data-value"),
            joinUser: $("#joinUser").attr("data-value"),
            careType: $("#careType").val(),
            careTime: $("#careTime").val(),
            careFunds: $("#careFunds").val(),
            careResult: $("#careResult").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
