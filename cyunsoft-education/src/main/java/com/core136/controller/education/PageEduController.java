package com.core136.controller.education;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/education")
public class PageEduController {

    /**
     * 任课教师管理
     *
     * @return
     */
    @RequestMapping("/teaching/forteach")
    @RequiresPermissions("/app/core/education/teaching/forteach")
    public ModelAndView goForTeach() {
        try {
            return new ModelAndView("app/core/education/teaching/forteach");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 教师进修培训
     *
     * @param view
     * @return
     */
    @RequestMapping("/teacher/teachertrain")
    @RequiresPermissions("/app/core/education/teacher/teachertrain")
    public ModelAndView goTeacherTrain(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/education/teacher/teachertrainmange");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/education/teacher/teachertrain");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return
     */
    @RequestMapping("/teacher/teachertraindetails")
    public ModelAndView goTeacherTrainDetails() {
        try {
            return new ModelAndView("app/core/education/teacher/teachertraindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 教师进修记录详情
     *
     * @return
     */
    @RequestMapping("/teacher/experiencedetails")
    public ModelAndView goExperienceDetails() {
        try {
            return new ModelAndView("app/core/education/teacher/experiencedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTeachingExperience
     * @Description:  教学经历
     */
    @RequestMapping("/teacher/teachingexperience")
    @RequiresPermissions("/app/core/education/teacher/teachingexperience")
    public ModelAndView goTeachingExperience(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/education/teacher/teachingexperiencemanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/education/teacher/teachingexperience");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goTeacherBaseInfo
     * @Description:  教师管理
     */
    @RequestMapping("/teacher/baseinfo")
    @RequiresPermissions("/app/core/education/teacher/baseinfo")
    public ModelAndView goTeacherBaseInfo(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/education/teacher/baseinfomanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/education/teacher/baseinfo");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/education/teacher/baseinfoimport");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 教师档案详情
     *
     * @return
     */
    @RequestMapping("/teacher/details")
    public ModelAndView goTeacherDetails() {
        try {
            return new ModelAndView("app/core/education/teacher/baseinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goDuty 新生报到
     * @Description:
     */
    @RequestMapping("/entrance/duty")
    @RequiresPermissions("/app/core/education/entrance/duty")
    public ModelAndView goDuty(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/education/entrance/duty");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/education/entrance/dutymanage");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/education/entrance/dutyimport");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goCourse   课程管理
     * @Description:
     */
    @RequestMapping("/teaching/course")
    @RequiresPermissions("/app/core/education/teaching/course")
    public ModelAndView goCourse() {
        ModelAndView mv = null;
        try {
            return new ModelAndView("app/core/education/teaching/course");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goCampusset
     * @Description:  校区设置
     */
    @RequestMapping("/param/campusset")
    @RequiresPermissions("/app/core/education/param/campusset")
    public ModelAndView goCampusset() {
        try {
            return new ModelAndView("app/core/education/param/campusset");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 基本设置
     * @return
     */
    @RequestMapping("/param/setconfig")
    @RequiresPermissions("/app/core/education/param/setconfig")
    public ModelAndView setConfig() {
        try {
            return new ModelAndView("app/core/education/param/config");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 设置专业
     * @return
     */
    @RequestMapping("/param/setmajor")
    @RequiresPermissions("/app/core/education/param/setmajor")
    public ModelAndView setMajor() {
        try {
            return new ModelAndView("app/core/education/param/major");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 教育分类码
     *
     * @return
     */
    @RequestMapping("/param/classcode")
    @RequiresPermissions("/app/core/education/param/classcode")
    public ModelAndView goClassCode() {
        try {
            return new ModelAndView("app/core/education/param/classcode");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goSemester
     * @Description:  学期设置
     */
    @RequestMapping("/param/semester")
    @RequiresPermissions("/app/core/education/param/semester")
    public ModelAndView goSemester() {
        try {
            return new ModelAndView("app/core/education/param/semester");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goClassseter
     * @Description:  班级设置
     */
    @RequestMapping("/param/classster")
    @RequiresPermissions("/app/core/education/param/classster")
    public ModelAndView goClassseter() {
        try {
            return new ModelAndView("app/core/education/param/classster");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDepartments
     * @Description:  学校院系设置
     */
    @RequestMapping("/param/departments")
    @RequiresPermissions("/app/core/education/param/departments")
    public ModelAndView goDepartments() {
        try {
            return new ModelAndView("app/core/education/param/departments");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 年级设置
     *
     * @return
     */
    @RequestMapping("/param/grade")
    @RequiresPermissions("/app/core/education/param/grade")
    public ModelAndView goGrade() {
        try {
            return new ModelAndView("app/core/education/param/grade");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
