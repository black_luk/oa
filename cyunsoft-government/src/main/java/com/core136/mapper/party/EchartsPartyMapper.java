package com.core136.mapper.party;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: EchartsPartyMapper
 * @Description: 党政分析
 * @author: 稠云技术
 * @date: 2020年11月21日 下午12:36:01
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Mapper
public interface EchartsPartyMapper {

    /**
     * @param orgId
     * @param year
     * @param memberId
     * @return List<Map < String, Object>>
     * @Title: getMyPointsByMonthLine
     * @Description: 获取个人一年内的积分获取情况
     */
    public List<Map<String, Object>> getMyPointsByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "year") String year, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, Object>>
     * @Title: getPartyJoinByMonthLine
     * @Description:  近一年内党员发展情况
     */
    public List<Map<String, Object>> getPartyJoinByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyMemberJoinToPartyOrgPie
     * @Description:  获取各党组织党员发展情况
     */
    public List<Map<String, String>> getPartyMemberJoinToPartyOrgPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBarPartyMemberJoinByStatus
     * @Description:  党员发展各阶段对比
     */
    public List<Map<String, String>> getBarPartyMemberJoinByStatus(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getPartyJoinByYearLine
     * @Description:  获取过去10年内的党员入党情况
     */
    public List<Map<String, Object>> getPartyJoinByYearLine(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param nowTime
     * @return List<Map < String, String>>
     * @Title: getBiPartyFundsAnalysis
     * @Description:  按党组织对比经费使用情况
     */
    public List<Map<String, String>> getBiPartyFundsAnalysis(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyFundsPie
     * @Description:  按经费类型饼状图
     */
    public List<Map<String, String>> getPartyFundsPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param nowTime
     * @return List<Map < String, Object>>
     * @Title: getBiPartyFundsByMonthLine
     * @Description:  按月份汇总经费使用情况
     */
    public List<Map<String, Object>> getBiPartyFundsByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime);

    /**
     * @param year1
     * @param year2
     * @param year3
     * @param year4
     * @param year5
     * @param year6
     * @param year7
     * @return Map<String, Integer>
     * @Title: getPartyMemberByAgeCountList
     * @Description:  按年龄段获取党员数量
     */
    public Map<String, Integer> getPartyMemberByAgeCountList(@Param(value = "orgId") String orgId, @Param(value = "year1") String year1,
                                                             @Param(value = "year2") String year2, @Param(value = "year3") String year3, @Param(value = "year4") String year4, @Param(value = "year5") String year5,
                                                             @Param(value = "year6") String year6, @Param(value = "year7") String year7);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiMemberByEducationPie
     * @Description:  按学历获取党员占比
     */
    List<Map<String, String>> getBiMemberByEducationPie(@Param(value = "orgId") String orgId);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiMemberByPartyOrgIdPie
     * @Description:  按所属党组织分析
     */
    List<Map<String, String>> getBiMemberByPartyOrgIdPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiMemberBySexPie
     * @Description:  按性名分析党员
     */
    List<Map<String, String>> getBiMemberBySexPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getBiJoinPartyByYearLine
     * @Description:  入党趋势
     */
    List<Map<String, Object>> getBiJoinPartyByYearLine(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyRulesLearnPie
     * @Description:  获取规章制度分类前10的占比
     */
    public List<Map<String, String>> getPartyRulesLearnPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyRulesLearnByDeptPie
     * @Description:  按部门学习对比前10的占比
     */
    public List<Map<String, String>> getPartyRulesLearnByDeptPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyRulesLearnByAccountPie
     * @Description:  前10位人员学习次数占比
     */
    public List<Map<String, String>> getPartyRulesLearnByAccountPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getPartyRulesLearnByMonthLine
     * @Description:  按月份学习趋势分析
     */
    public List<Map<String, Object>> getPartyRulesLearnByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * 预备党员转正
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return
     */
    public List<Map<String, Object>> getPartyLesMeetByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getBiPartyDeusByYearLine
     * @Description:  按年份汇总收缴的党费
     */
    public List<Map<String, Object>> getBiPartyDeusByYearLine(@Param(value = "orgId") String orgId);


    /**
     * @param orgId
     * @return Map<String, String>
     * @Title: getPartyOrgCountList
     * @Description:  获取各类型党组织数量
     */
    public Map<String, String> getPartyOrgCountList(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyBigEventList
     * @Description:  重大事件前10条记录
     */
    public List<Map<String, String>> getPartyBigEventList(@Param(value = "orgId") String orgId);

    /**
     * 按类型统计三会一课
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getPartyLesMeetForBar(@Param(value = "orgId") String orgId);

}
