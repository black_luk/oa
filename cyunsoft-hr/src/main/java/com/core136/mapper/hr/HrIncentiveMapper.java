package com.core136.mapper.hr;

import com.core136.bean.hr.HrIncentive;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrIncentiveMapper extends MyMapper<HrIncentive> {

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param incentiveType
     * @param incentiveItem
     * @return List<Map < String, String>>
     * @Title: getHrIncentiveList
     * @Description:  获取奖惩记录列表
     */
    public List<Map<String, String>> getHrIncentiveList(@Param(value = "orgId") String orgId,
                                                        @Param(value = "userId") String userId, @Param(value = "beginTime") String beginTime,
                                                        @Param(value = "endTime") String endTime, @Param(value = "incentiveType") String incentiveType,
                                                        @Param(value = "incentiveItem") String incentiveItem
    );

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrIncentiveList
     * @Description:  个人查询奖惩记录
     */
    public List<Map<String, String>> getMyHrIncentiveList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
