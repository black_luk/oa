$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getMainProject();
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/budgetget/getChildBudgetCostList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'pTitle',
            width: '150px',
            title: '项目名称'
        }, {
            field: 'title',
            width: '150px',
            title: '费用标题'
        }, {
            field: 'projectCode',
            title: '项目编号',
            width: '100px'
        }, {
            field: 'pTotalCost',
            width: '100px',
            title: '项目预算'
        }, {
            field: 'cTotalCost',
            width: '100px',
            title: '费用支出'
        }, {
            field: 'projectType',
            width: '50px',
            title: '项目类型',
            formatter: function (value, row, index) {
                return getBudgetTypeName(value);
            }
        }, {
            field: 'happenTime',
            width: '100px',
            title: '支出时间'
        }, {
            field: 'chargeUser',
            width: '100px',
            title: '项目负责人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'budgetAccount',
            width: '50px',
            title: '预算科目',
            formatter: function (value, row, index) {
                return getBudgetAccountName(value);
            }
        }, {
            field: 'createUser',
            width: '50px',
            title: '立项人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        projectId: $("#projectIdQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/budget/costdetails?recordId=" + recordId);
}

function getMainProject() {
    $.ajax({
        url: "/ret/budgetget/getBudgetProjectListForProject",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var datalist = data.list;
                var html = "<option value=''>全部</option>";
                for (var i = 0; i < datalist.length; i++) {
                    html += "<option value='" + datalist[i].projectId + "'>" + datalist[i].title + "</option>";
                }
                $("#projectIdQuery").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}

function getBudgetTypeName(budgetTypeId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetTypeById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetTypeId: budgetTypeId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}
