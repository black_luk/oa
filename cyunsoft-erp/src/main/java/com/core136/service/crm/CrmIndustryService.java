package com.core136.service.crm;

import com.core136.bean.crm.CrmIndustry;
import com.core136.mapper.crm.CrmIndustryMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CrmIndustryService {
    private CrmIndustryMapper crmIndustryMapper;

    @Autowired
    public void setCrmIndustryMapper(CrmIndustryMapper crmIndustryMapper) {
        this.crmIndustryMapper = crmIndustryMapper;
    }

    public List<CrmIndustry> selectCrmIndustry(Example example) {
        example.setOrderByClause("sort_no desc");
        return crmIndustryMapper.selectByExample(example);
    }

    public int insertCrmIndustry(CrmIndustry crmIndustry) {
        return crmIndustryMapper.insert(crmIndustry);
    }

    public int updateCrmIndustry(CrmIndustry crmIndustry, Example example) {
        return crmIndustryMapper.updateByExampleSelective(crmIndustry, example);
    }

    public int deleteCrmIndustry(CrmIndustry crmIndustry) {
        return crmIndustryMapper.delete(crmIndustry);
    }

    public CrmIndustry selectOneCrmIndustry(CrmIndustry crmIndustry) {
        return crmIndustryMapper.selectOne(crmIndustry);
    }

    /*
     * 获取企业分类列表
     */

    public List<Map<String, Object>> getAllIndustryList(String orgId, String search) {
        //  Auto-generated method stub
        return crmIndustryMapper.getAllIndustryList(orgId, search);
    }


    public PageInfo<Map<String, Object>> getAllIndustryList(int pageNumber, int pageSize, String orderBy, String orgId, String search) {
        PageHelper.startPage(pageNumber, pageSize, orderBy);
        List<Map<String, Object>> datalist = this.getAllIndustryList(orgId, search);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

}
