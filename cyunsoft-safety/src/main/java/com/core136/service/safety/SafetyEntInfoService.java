package com.core136.service.safety;

import com.core136.bean.safety.SafetyEntInfo;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyEntInfoMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyEntInfoService {

    private SafetyEntInfoMapper safetyEntInfoMapper;

    @Autowired
    public void setSafetyEntInfoMapper(SafetyEntInfoMapper safetyEntInfoMapper) {
        this.safetyEntInfoMapper = safetyEntInfoMapper;
    }

    public int insertSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        return safetyEntInfoMapper.insert(safetyEntInfo);
    }

    public int deleteSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        return safetyEntInfoMapper.delete(safetyEntInfo);
    }

    public int updateSafetyEntInfo(Example example, SafetyEntInfo safetyEntInfo) {
        return safetyEntInfoMapper.updateByExampleSelective(safetyEntInfo, example);
    }

    public SafetyEntInfo selectOneSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        return safetyEntInfoMapper.selectOne(safetyEntInfo);
    }

    public List<Map<String, String>> selectEntInfoByName(String orgId, String search) {
        return safetyEntInfoMapper.selectEntInfoByName(orgId, "%" + search + "%");
    }

    public List<Map<String,String>>getEntInfoAllList(String orgId)
    {
        return safetyEntInfoMapper.getEntInfoAllList(orgId);
    }

    /**
     * 获取企业列表
     *
     * @param orgId
     * @param type
     * @param search
     * @return
     */
    public List<Map<String, String>> getEntInfoList(String orgId, String type, String search) {
        return safetyEntInfoMapper.getEntInfoList(orgId, type, "%" + search + "%");
    }

    /**
     * 获取企业列表
     *
     * @param pageParam
     * @param type
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getEntInfoList(PageParam pageParam, String type) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEntInfoList(pageParam.getOrgId(), type, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
