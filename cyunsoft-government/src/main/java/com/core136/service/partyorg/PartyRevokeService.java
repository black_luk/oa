package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyRevoke;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyRevokeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRevokeService {
    private PartyRevokeMapper partyRevokeMapper;

    @Autowired
    public void setPartyRevokeMapper(PartyRevokeMapper partyRevokeMapper) {
        this.partyRevokeMapper = partyRevokeMapper;
    }

    public int insertPartyRevoke(PartyRevoke partyRevoke) {
        return partyRevokeMapper.insert(partyRevoke);
    }

    public int deletePartyRevoke(PartyRevoke partyRevoke) {
        return partyRevokeMapper.delete(partyRevoke);
    }

    public int updatePartyRevoke(Example example, PartyRevoke partyRevoke) {
        return partyRevokeMapper.updateByExampleSelective(partyRevoke, example);
    }

    public PartyRevoke selectOnePartyRevoke(PartyRevoke partyRevoke) {
        return partyRevokeMapper.selectOne(partyRevoke);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRevokeList
     * @Description:  获取党组织撤销记录
     */
    public List<Map<String, String>> getPartyRevokeList(String orgId, String beginTime, String endTime, String search) {
        return partyRevokeMapper.getPartyRevokeList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyRevokeList
     * @Description:  获取党组织撤销记录
     */
    public PageInfo<Map<String, String>> getPartyRevokeList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyRevokeList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
