package com.core136.mapper.education;

import com.core136.bean.education.EduConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EduConfigMapper extends MyMapper<EduConfig> {
}
