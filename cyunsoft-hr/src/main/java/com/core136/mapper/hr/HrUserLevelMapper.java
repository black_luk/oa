/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserLevelMapper.java
 * @Package com.core136.mapper.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月30日 上午10:41:09
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.hr;

import com.core136.bean.hr.HrUserLevel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface HrUserLevelMapper extends MyMapper<HrUserLevel> {
    /**
     *
     * @Title: getHrUserLevelChart
     * @Description:  获取行政级别CHART数据
     * @param: orgId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, Object>>

     */
    public List<Map<String, Object>> getHrUserLevelChart(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     *
     * @Title: getHrUserLevelByStr
     * @Description:  获取HR的行政级别名称
     * @param orgId
     * @param list
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserLevelByStr(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);
}
