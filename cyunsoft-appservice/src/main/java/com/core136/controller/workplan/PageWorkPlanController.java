package com.core136.controller.workplan;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/workplan")
public class PageWorkPlanController {
    /**
     * @param view
     * @return ModelAndView
     * @Title: goPlanquery
     * @Description:  工作计划查询
     */
    @RequestMapping("/planquery")
    @RequiresPermissions("/app/core/workplan/planquery")
    public ModelAndView goPlanquery(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/workplan/planquery");
            } else {
                if (view.equals("month")) {
                    mv = new ModelAndView("app/core/workplan/planquery1");
                } else if (view.equals("query")) {
                    mv = new ModelAndView("app/core/workplan/planquery2");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMyWorkPlan
     * @Description:  我的工作计划
     */
    @RequestMapping("/myworkplan")
    @RequiresPermissions("/app/core/workplan/myworkplan")
    public ModelAndView goMyWorkPlan(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/workplan/myplan");
            } else {
                if (view.equals("share")) {
                    mv = new ModelAndView("app/core/workplan/shareplan");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goPlanMange
     * @Description:  工作计划管理
     */
    @RequestMapping("/planmange")
    @RequiresPermissions("/app/core/workplan/planmange")
    public ModelAndView goPlanMange(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/workplan/planmange");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/workplan/plan");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goHoldWorkPlan
     * @Description:  我负责的计划
     */
    @RequestMapping("/holdworkplan")
    @RequiresPermissions("/app/core/workplan/holdworkplan")
    public ModelAndView goHoldWorkPlan() {
        try {
            return new ModelAndView("app/core/workplan/holdplan");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSupWorkPlan
     * @Description:  我督查计划
     */
    @RequestMapping("/supworkplan")
    @RequiresPermissions("/app/core/workplan/supworkplan")
    public ModelAndView goSupWorkPlan() {
        try {
            return new ModelAndView("app/core/workplan/supplan");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goWorkPlandetails
     * @Description:  工作计划详情
     */
    @RequestMapping("/workplandetails")
    @RequiresPermissions("/app/core/workplan/workplandetails")
    public ModelAndView goWorkPlandetails() {
        try {
            return new ModelAndView("app/core/workplan/workplandetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
