let ue = UE.getEditor("remark");
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM",
        maxDate: getSysDate()
    });
    jeDate("#endTime", {
        format: "YYYY-MM"
    });
    $(".js-add-save").unbind("click").click(function () {
        addHrWorkRecord();
    })
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })

})

function addHrWorkRecord() {
    if($("#userId").attr("data-value")=="")
    {
        layer.msg("人员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrWorkRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            compName: $("#compName").val(),
            userId: $("#userId").attr("data-value"),
            post: $("#post").val(),
            deptName: $("#deptName").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            industry: $("#industry").val(),
            cerifier: $("#cerifier").val(),
            nature: $("#nature").val(),
            jobContent: $("#jobContent").val(),
            achievement: $("#achievement").val(),
            reasonForLevel: $("#reasonForLevel").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
