package com.core136.mapper.education;

import com.core136.bean.education.EduDepartments;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduDepartmentsMapper extends MyMapper<EduDepartments> {
    /**
     * @param orgId
     * @param sortLevel
     * @return List<Map < String, String>>
     * @Title: getEduDepartmentsTree
     * @Description:  获取院系树形结构
     */
    public List<Map<String, String>> getEduDepartmentsTree(@Param(value = "orgId") String orgId, @Param(value = "sortLevel") String sortLevel);


    public List<Map<String, String>> getEduClassTreeForSelect(@Param(value = "orgId") String orgId, @Param(value = "sortLevel") String sortLevel);

    public List<Map<String, String>> getEduClassTree(@Param(value = "orgId") String orgId, @Param(value = "sortLevel") String sortLevel);
}
