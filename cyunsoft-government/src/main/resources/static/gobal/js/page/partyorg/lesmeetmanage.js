$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getSmsConfig("msgType", "lesMeet");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#meetYear", {
        format: "YYYY",
        isinitVal: true
    });
    $('#institution').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyorgget/getLesMeetList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '100px',
            title: '会议标题',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'meetType',
            title: '会议类型',
            width: '100px',
            formatter: function (value, row, index) {
                if (value = "1") {
                    return "支部党员大会";
                } else if (value == "2") {
                    return "支委会";
                } else if (value == "3") {
                    return "党小组会";
                } else if (value = "4") {
                    return "党课";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'meetModel',
            title: '三会一课形式',
            width: '100px',
            formatter: function (value, row, index) {
                if (value = "1") {
                    return "会议";
                } else if (value == "2") {
                    return "活动";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'userCount',
            title: '参会人数',
            width: '100px'
        }, {
            field: 'beginTime',
            width: '100px',
            title: '开始时间'
        }, {
            field: 'endTime',
            title: '结束时间',
            width: '100px'
        }, {
            field: 'meetYear',
            width: '100px',
            title: '年度'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        meetType: $("#meetTypeQuery").val(),
        meetModel: $("#meetModelQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "";
    html += "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteLesMeet('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/partyorg/lesmeetdetails?recordId=" + recordId);
}

function deleteLesMeet(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partyorgset/deletePartyLesMeet",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#leslistdiv").hide();
    $("#lesdiv").show();
    $("#attach").attr("data_value", "");
    $("#show_attach").empty();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partyorgget/getPartyLesMeetById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "institution") {
                        $("#institution").code(recordInfo[id]);
                    } else if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "joinMember") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getPartyMemberName(recordInfo.joinMember));
                    } else if (id == "msgType") {

                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateLesMeet(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}

function updateLesMeet(recordId) {
    if ($("#title").val()== "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/updatePartyLesMeet",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            meetType: $("#meetType").val(),
            title: $("#title").val(),
            meetModel: $("#meetModel").val(),
            userCount: $("#userCount").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            address: $("#address").val(),
            meetYear: $("#meetYear").val(),
            joinMember: $("#joinMember").attr("data-value"),
            otherPriv: $("#otherPriv").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").val(),
            msgType: getCheckBoxValue("msgType"),
            institution: $("#institution").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#lesdiv").hide();
    $("#leslistdiv").show();
}
