/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetFixedassetController.java
 * @Package com.core136.controller.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 下午5:19:06
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.fixedassets;


import com.core136.bean.account.Account;
import com.core136.bean.fixedassets.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.fixedassets.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/set/fixedassetsset")
public class RouteSetFixedAssetsController {
    private FixedAssetsSortService fixedAssetsSortService;

    @Autowired
    public void setFixedAssetsSortService(FixedAssetsSortService fixedAssetsSortService) {
        this.fixedAssetsSortService = fixedAssetsSortService;
    }

    private FixedAssetsService fixedAssetsService;

    @Autowired
    public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
        this.fixedAssetsService = fixedAssetsService;
    }

    private FixedAssetsStorageService fixedAssetsStorageService;

    @Autowired
    public void setFixedAssetsStorageService(FixedAssetsStorageService fixedAssetsStorageService) {
        this.fixedAssetsStorageService = fixedAssetsStorageService;
    }

    private FixedAssetsApplyService fixedAssetApplayService;

    @Autowired
    public void setFixedAssetsApplyService(FixedAssetsApplyService fixedAssetApplayService) {
        this.fixedAssetApplayService = fixedAssetApplayService;
    }

    private FixedAssetsApprovalService fixedAssetsApprovalService;

    @Autowired
    public void setFixedAssetsApprovalService(FixedAssetsApprovalService fixedAssetsApprovalService) {
        this.fixedAssetsApprovalService = fixedAssetsApprovalService;
    }

    private FixedAssetsRepairService fixedAssetsRepairService;

    @Autowired
    public void setFixedAssetsRepairService(FixedAssetsRepairService fixedAssetsRepairService) {
        this.fixedAssetsRepairService = fixedAssetsRepairService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private FixedAssetsAllocationService fixedAssetsAllocationService;

    @Autowired
    public void setFixedAssetsAllocationService(FixedAssetsAllocationService fixedAssetsAllocationService) {
        this.fixedAssetsAllocationService = fixedAssetsAllocationService;
    }

    /**
     * @param fixedAssetsAllocation
     * @return RetDataBean
     * @Title: addFixedAssetsAllocation
     * @Description:  资产调拨
     */
    @RequestMapping(value = "/addFixedAssetsAllocation", method = RequestMethod.POST)
    public RetDataBean addFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation, String applyId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsAllocation.setRecordId(SysTools.getGUID());
            fixedAssetsAllocation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssetsAllocation.setCreateUser(account.getAccountId());
            fixedAssetsAllocation.setOrgId(account.getOrgId());
            return fixedAssetsAllocationService.addFixedAssetsAllocation(fixedAssetsAllocation, applyId);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertFixedAssetsRepair
     * @Description:  发起维修
     * @param: fixedAssetsRepair
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertFixedAssetsRepair", method = RequestMethod.POST)
    public RetDataBean insertFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsRepair.setRepairId(SysTools.getGUID());
            fixedAssetsRepair.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssetsRepair.setCreateUser(account.getAccountId());
            fixedAssetsRepair.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsRepairService.addFixedAssetsRepair(fixedAssetsRepair));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteFixedAssetsRepair
     * @Description:  删除维修
     * @param: fixedAssetsRepair
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteFixedAssetsRepair", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssetsRepair.getRepairId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fixedAssetsRepair.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsRepairService.deleteFixedAssetsRepair(fixedAssetsRepair));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateFixedAssetsRepair
     * @Description:  更新维修记录
     * @param: fixedAssetsRepair
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateFixedAssetsRepair", method = RequestMethod.POST)
    public RetDataBean updateFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        try {
            if (StringUtils.isBlank(fixedAssetsRepair.getRepairId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssetsRepair.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("repairId", fixedAssetsRepair.getRepairId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsRepairService.updateFixedAssetsRepair(example, fixedAssetsRepair));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateFixedAssetsApply
     * @Description:  更新审批意见
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateFixedAssetsApproval", method = RequestMethod.POST)
    public RetDataBean updateFixedAssetsApproval(FixedAssetsApproval fixedAssetsApproval) {
        try {
            if (StringUtils.isBlank(fixedAssetsApproval.getApprovalId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssetsApproval.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("approvalId", fixedAssetsApproval.getApprovalId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsApprovalService.updateFixedAssetsApproval(example, fixedAssetsApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteFixedAssetsApply
     * @Description:  册除审批意见
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteFixedAssetsApproval", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssetsApproval(FixedAssetsApproval fixedAssetsApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssetsApproval.getApprovalId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                fixedAssetsApproval.setCreateUser(account.getAccountId());
            }
            fixedAssetsApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsApprovalService.deleteFixedAssetsApproval(fixedAssetsApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertFixedAssetsApply
     * @Description:  审批
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertFixedAssetsApproval", method = RequestMethod.POST)
    public RetDataBean insertFixedAssetsApproval(FixedAssetsApproval fixedAssetsApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsApproval.setApprovalId(SysTools.getGUID());
            fixedAssetsApproval.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssetsApproval.setCreateUser(account.getAccountId());
            fixedAssetsApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsApprovalService.approveFixedAssets(fixedAssetsApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: updateFixedAssetsApply
     * @Description:  更新申请
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateFixedAssetsApply", method = RequestMethod.POST)
    public RetDataBean updateFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        try {
            if (StringUtils.isBlank(fixedAssetsApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssetsApply.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("applyId", fixedAssetsApply.getApplyId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetApplayService.updateFixedAssetsApply(example, fixedAssetsApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteFixedAssetsApply
     * @Description:  册除申请
     * @param: request
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteFixedAssetsApply", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssetsApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fixedAssetsApply.setCreateUser(account.getAccountId());
            fixedAssetsApply.setStatus("0");
            fixedAssetsApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetApplayService.deleteFixedAssetsApply(fixedAssetsApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertFixedAssetsApply
     * @Description:  发起申请
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertFixedAssetsApply", method = RequestMethod.POST)
    public RetDataBean insertFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsApply.setApplyId(SysTools.getGUID());
            fixedAssetsApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssetsApply.setCreateUser(account.getAccountId());
            fixedAssetsApply.setStatus("0");
            fixedAssetsApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetApplayService.insertFixedAssetsApply(fixedAssetsApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertFixedAssetsStorage
     * @Description: 创建仓库
     * @param: fixedAssetsStorage
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertFixedAssetsStorage", method = RequestMethod.POST)
    public RetDataBean insertFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsStorage.setStorageId(SysTools.getGUID());
            fixedAssetsStorage.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssetsStorage.setCreateUser(account.getAccountId());
            fixedAssetsStorage.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsStorageService.insertFixedAssetsStorage(fixedAssetsStorage));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteFixedAssetsStorage
     * @Description: 删除仓库
     * @param: fixedAssetsStorage
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteFixedAssetsStorage", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssetsStorage.getStorageId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fixedAssetsStorage.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsStorageService.deleteFixedAssetsStorage(fixedAssetsStorage));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateFixedAssetsStorage
     * @Description:  更新仓库信息
     * @param: request
     * @param: fixedAssetsStorage
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateFixedAssetsStorage", method = RequestMethod.POST)
    public RetDataBean updateFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        try {
            if (StringUtils.isBlank(fixedAssetsStorage.getStorageId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssetsStorage.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("storageId", fixedAssetsStorage.getStorageId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsStorageService.updateFixedAssetsStorage(example, fixedAssetsStorage));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param fixedAssets
     * @return RetDataBean
     * @Title: insertFixedAssets
     * @Description:  添加固定资产记录
     */
    @RequestMapping(value = "/insertFixedAssets", method = RequestMethod.POST)
    public RetDataBean insertFixedAssets(FixedAssets fixedAssets) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssets.setAssetsId(SysTools.getGUID());
            fixedAssets.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fixedAssets.setCreateUser(account.getAccountId());
            fixedAssets.setStatus("0");
            fixedAssets.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsService.insertFixedAssets(fixedAssets));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param fixedAssets
     * @return RetDataBean
     * @Title: deleteFixedAssets
     * @Description:  删除固定资产记录
     */
    @RequestMapping(value = "/deleteFixedAssets", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssets(FixedAssets fixedAssets) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssets.getAssetsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fixedAssets.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsService.deleteFixedAssets(fixedAssets));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param fixedAssets
     * @return RetDataBean
     * @Title: updateFixedAssets
     * @Description:  更新固定资产记录
     */
    @RequestMapping(value = "/updateFixedAssets", method = RequestMethod.POST)
    public RetDataBean updateFixedAssets(FixedAssets fixedAssets) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(fixedAssets.getAssetsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssets.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("assetsId", fixedAssets.getAssetsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsService.updateFixedAssets(example, fixedAssets));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/insertFixedAssetsSort", method = RequestMethod.POST)
    public RetDataBean insertFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsSort.setSortId(SysTools.getGUID());
            fixedAssetsSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            if (StringUtils.isBlank(fixedAssetsSort.getParentId())) {
                fixedAssetsSort.setParentId("0");
            }
            fixedAssetsSort.setCreateUser(account.getAccountId());
            fixedAssetsSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsSortService.insertFixedAssetsSort(fixedAssetsSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteFixedAssetsSort", method = RequestMethod.POST)
    public RetDataBean deleteFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fixedAssetsSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fixedAssetsSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsSortService.deleteFixedAssetsSort(fixedAssetsSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/updateFixedAssetsSort", method = RequestMethod.POST)
    public RetDataBean updateFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        try {
            if (StringUtils.isBlank(fixedAssetsSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (fixedAssetsSort.getSortId().equals(fixedAssetsSort.getParentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(fixedAssetsSort.getParentId())) {
                fixedAssetsSort.setParentId("0");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(FixedAssetsSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", fixedAssetsSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsSortService.updateFixedAssetsSort(example, fixedAssetsSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
