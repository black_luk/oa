package com.core136.controller.mobile;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.AUserInfo;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.account.UserPriv;
import com.core136.bean.attend.AttendConfig;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserPrivService;
import com.core136.service.attend.AttendConfigService;
import com.core136.service.attend.AttendService;
import com.core136.service.sys.AppConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/mobile")
public class PageMobileController {
    private AppConfigService appConfigService;

    @Autowired
    public void setAppConfigService(AppConfigService appConfigService) {
        this.appConfigService = appConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private AttendConfigService attendConfigService;

    @Autowired
    public void setAttendConfigService(AttendConfigService attendConfigService) {
        this.attendConfigService = attendConfigService;
    }

    private AttendService attendService;

    @Autowired
    public void setAttendService(AttendService attendService) {
        this.attendService = attendService;
    }

    private UserPrivService userPrivService;
    @Autowired
    public void setUserPrivService(UserPrivService userPrivService)
    {
        this.userPrivService = userPrivService;
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goMobileMyOffWork
     * @Description:  下班打卡
     */
    @RequestMapping("/attend/offwork")
    public ModelAndView goMobileMyOffWork(HttpServletRequest request) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            AttendConfig attendConfig = new AttendConfig();
            attendConfig.setConfigId(userInfo.getAttendConfigId());
            attendConfig.setOrgId(userInfo.getOrgId());
            attendConfig = attendConfigService.selectOneAttendConfig(attendConfig);
            int i = attendService.getOffWorkAttendStatusDay(account);
            ModelAndView mv = new ModelAndView("app/mobile/main/attend/offwork");
            if (i == 0) {
                mv.addObject("isAttend", false);
            } else {
                mv.addObject("isAttend", true);
            }
            mv.addObject("attendConfig", attendConfig);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goMobileMyDoAttend
     * @Description:  打卡上班
     */
    @RequestMapping("/attend/doattend")
    public ModelAndView goMobileMyDoAttend(HttpServletRequest request) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            AttendConfig attendConfig = new AttendConfig();
            attendConfig.setConfigId(userInfo.getAttendConfigId());
            attendConfig.setOrgId(userInfo.getOrgId());
            attendConfig = attendConfigService.selectOneAttendConfig(attendConfig);
            int i = attendService.getAttendStatusDay(account);
            ModelAndView mv = new ModelAndView("app/mobile/main/attend/doattend");
            if (i == 0) {
                mv.addObject("isAttend", false);
            } else {
                mv.addObject("isAttend", true);
            }
            mv.addObject("attendConfig", attendConfig);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @Title: goToMobileIndex
     * @Description:  跳转到移动端首页
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/main/index")
    public ModelAndView goToMobileIndex() {
        try {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            String[] privIdArr = aUserInfo.getUserPriv().split(",");
            String mobilePrivIds = "";
            for (int i = 0; i < privIdArr.length; i++) {
                if (StringUtils.isNotBlank(privIdArr[i])) {
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                    mobilePrivIds += userPriv.getMobilePriv() + ",";
                }
            }
            List<String> appList = StrTools.strToList(mobilePrivIds);
            JSONObject appMenuList = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
            ModelAndView mv = new ModelAndView("app/mobile/main/index");
            mv.addObject("r", System.currentTimeMillis());
            mv.addObject("appMenuList", appMenuList);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 移动端BPM列表
     * @param request
     * @return
     */
    @RequestMapping("/bpm/mybpmlist")
    public ModelAndView goMobileMyBpmList(HttpServletRequest request) {
        try {
            return new ModelAndView("app/mobile/main/bpm/mybpmlist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 待办公文列表
     * @param request
     * @return
     */
    @RequestMapping("/document/mydocumentlist")
    public ModelAndView goMobileMyDocumentList(HttpServletRequest request) {
        try {
            return new ModelAndView("app/mobile/main/document/mydocumentlist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * Bpm查询列表
     *
     * @return
     */
    @RequestMapping("/bpm/bpmquerylist")
    public ModelAndView getMobileBpmQueryList() {
        try {
            return new ModelAndView("app/mobile/main/bpm/bpmquerylist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 公文查询列表
     *
     * @return
     */
    @RequestMapping("/document/documentquerylist")
    public ModelAndView goMobileDocumentQueryList() {
        try {
            return new ModelAndView("app/mobile/main/document/documentquerylist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 公文查询
     *
     * @return
     */
    @RequestMapping("/document/documentquery")
    public ModelAndView goMobileDocumentQuery() {
        try {
            return new ModelAndView("app/mobile/main/document/documentquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileBpmCreate
     * @Description:  钉钉创建BPM列表
     * @param: unit
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/bpm/create")
    public ModelAndView goMobileBpmCreate() {
        try {
            return new ModelAndView("app/mobile/main/bpm/createbpm");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileBpmQuery
     * @Description:  BPM查询
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/bpm/bpmquery")
    public ModelAndView goMobileBpmQuery() {
        try {
            return new ModelAndView("app/mobile/main/bpm/bpmquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileBpmBegin
     * @Description: 发起BPM流程
     * @param: unit
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/bpm/bpmbegin")
    public ModelAndView goMobileBpmBegin() {
        try {
            return new ModelAndView("app/mobile/main/bpm/bpmbegin");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileBpmSendTo
     * @Description:  抄送给我的BPM流程
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/bpm/sendto")
    public ModelAndView goMobileBpmSendTo() {
        try {
            return new ModelAndView("app/mobile/main/bpm/bpmsendto");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 公文抄送
     *
     * @return
     */
    @RequestMapping("/document/sendto")
    public ModelAndView goMobileDocumentendTo() {
        try {
            return new ModelAndView("app/mobile/main/document/documentsendto");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 移动端通知公告应用
     *
     * @return
     */
    @RequestMapping("/notice/index")
    public ModelAndView goMobileMyNotice() {
        try {
            return new ModelAndView("app/mobile/main/notice/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileMyEmail
     * @Description: 个人电子邮件
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/email/index")
    public ModelAndView goMobileMyEmail() {
        try {
            return new ModelAndView("app/mobile/main/email/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileMyDiary
     * @Description: 个人工作日志
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/diary/index")
    public ModelAndView goMobileMyDiary() {
        try {
            return new ModelAndView("app/mobile/main/diary/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMobileMyAddDiary
     * @Description:  填写工作日志
     */
    @RequestMapping("/diary/add")
    public ModelAndView goMobileMyAddDiary() {
        try {
            return new ModelAndView("app/mobile/main/diary/add");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMobileMyNews
     * @Description:  移动端新闻
     * @param: unit
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/news/index")
    public ModelAndView goMobileMyNews() {
        try {
            return new ModelAndView("app/mobile/main/news/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goMobileMyMeeting
     * @Description:  我的会议
     */
    @RequestMapping("/meeting/mymeeting")
    public ModelAndView goMobileMyMeeting() {
        try {
            return new ModelAndView("/app/mobile/main/meeting/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 督查督办
     *
     * @return
     */
    @RequestMapping("/superversion/mywork")
    public ModelAndView goSuperversionMyWork() {
        try {
            return new ModelAndView("/app/mobile/main/superversion/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 任务管理
     *
     * @return
     */
    @RequestMapping("/task/mytask")
    public ModelAndView goMyTask() {
        try {
            return new ModelAndView("/app/mobile/main/task/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 领导日程
     *
     * @return
     */
    @RequestMapping("/leadactivity/myleadactivity")
    public ModelAndView goMyLeadActivity() {
        try {
            return new ModelAndView("/app/mobile/main/leadactivity/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 个人文件
     *
     * @return
     */
    @RequestMapping("/fileshare/myfile")
    public ModelAndView goMyFile() {
        try {
            return new ModelAndView("/app/mobile/main/fileshare/myfile");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 公共文件柜
     *
     * @return
     */
    @RequestMapping("/fileshare/publicfile")
    public ModelAndView goPublicFile() {
        try {
            return new ModelAndView("/app/mobile/main/fileshare/publicfile");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 网络硬盘
     *
     * @return
     */
    @RequestMapping("/fileshare/netdisk")
    public ModelAndView goNetDisk() {
        try {
            return new ModelAndView("/app/mobile/main/fileshare/netdisk");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 相册
     *
     * @return
     */
    @RequestMapping("/fileshare/photo")
    public ModelAndView goPhoto() {
        try {
            return new ModelAndView("/app/mobile/main/fileshare/photo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
