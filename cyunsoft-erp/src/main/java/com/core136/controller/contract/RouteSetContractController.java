/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetContractController.java
 * @Package com.core136.controller.contract
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月23日 下午2:08:39
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.contract;

import com.core136.bean.account.Account;
import com.core136.bean.contract.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.contract.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lsq
 * @ClassName: RoutSetContractController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月23日 下午2:08:39
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/contractset")
public class RouteSetContractController {
    private ContractSortService contractSortService;

    @Autowired
    public void setContractSortService(ContractSortService contractSortService) {
        this.contractSortService = contractSortService;
    }

    private ContractService contractService;

    @Autowired
    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    private ContractDetailsService contractDetailsService;

    @Autowired
    public void setContractDetailsService(ContractDetailsService contractDetailsService) {
        this.contractDetailsService = contractDetailsService;
    }

    private ContractPrivService contractPrivService;

    @Autowired
    public void setContractPrivService(ContractPrivService contractPrivService) {
        this.contractPrivService = contractPrivService;
    }

    private ContractReceivablesService contractReceivablesService;

    @Autowired
    public void setContractReceivablesService(ContractReceivablesService contractReceivablesService) {
        this.contractReceivablesService = contractReceivablesService;
    }

    private ContractPayableService contractPayableService;

    @Autowired
    public void setContractPayableService(ContractPayableService contractPayableService) {
        this.contractPayableService = contractPayableService;
    }

    private ContractBillService contractBillService;

    @Autowired
    public void setContractBillService(ContractBillService contractBillService) {
        this.contractBillService = contractBillService;
    }

    private ContractSendgoodsService contractSendgoodsService;

    @Autowired
    public void setContractSendgoodsService(ContractSendgoodsService contractSendgoodsService) {
        this.contractSendgoodsService = contractSendgoodsService;
    }

    private ContractReceivablesRecordService contractReceivablesRecordService;

    @Autowired
    public void setContractReceivablesRecordService(ContractReceivablesRecordService contractReceivablesRecordService) {
        this.contractReceivablesRecordService = contractReceivablesRecordService;
    }

    private ContractPayableRecordService contractPayableRecordService;

    @Autowired
    public void setContractPayableRecordService(ContractPayableRecordService contractPayableRecordService) {
        this.contractPayableRecordService = contractPayableRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @Title: insertContractPayableRecord
     * @Description:  添加付款记录
     * @param: request
     * @param: contractPayableRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractPayableRecord", method = RequestMethod.POST)
    public RetDataBean insertContractPayableRecord(HttpServletRequest request, ContractPayableRecord contractPayableRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractPayableRecord.setRecordId(SysTools.getGUID());
            contractPayableRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractPayableRecord.setCreateUser(account.getAccountId());
            contractPayableRecord.setOrgId(account.getOrgId());
            return contractPayableRecordService.payableAmount(contractPayableRecord);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractPayableRecord
     * @Description:  删除付款记录
     * @param: request
     * @param: contractPayableRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractPayableRecord", method = RequestMethod.POST)
    public RetDataBean deleteContractPayableRecord(HttpServletRequest request, ContractPayableRecord contractPayableRecord) {
        try {
            if (StringUtils.isBlank(contractPayableRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            contractPayableRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractPayableRecordService.deleteContractPayableRecord(contractPayableRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateContractPayableRecord
     * @Description:  更新付款记录
     * @param: request
     * @param: contractPayableRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractPayableRecord", method = RequestMethod.POST)
    public RetDataBean updateContractPayableRecord(HttpServletRequest request, ContractPayableRecord contractPayableRecord) {
        try {
            if (StringUtils.isBlank(contractPayableRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractPayableRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", contractPayableRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPayableRecordService.updateContractPayableRecord(example, contractPayableRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertContractReceivablesRecord
     * @Description:  添加收款记录
     * @param: request
     * @param: contractReceivablesRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractReceivablesRecord", method = RequestMethod.POST)
    public RetDataBean insertContractReceivablesRecord(HttpServletRequest request, ContractReceivablesRecord contractReceivablesRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractReceivablesRecord.setRecordId(SysTools.getGUID());
            contractReceivablesRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractReceivablesRecord.setCreateUser(account.getAccountId());
            contractReceivablesRecord.setOrgId(account.getOrgId());
            return contractReceivablesRecordService.receivableAmount(contractReceivablesRecord);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractReceivablesRecord
     * @Description:  删除收款记录
     * @param: request
     * @param: contractReceivablesRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractReceivablesRecord", method = RequestMethod.POST)
    public RetDataBean deleteContractReceivablesRecord(HttpServletRequest request, ContractReceivablesRecord contractReceivablesRecord) {
        try {
            if (StringUtils.isBlank(contractReceivablesRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            contractReceivablesRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractReceivablesRecordService.deleteContractReceivablesRecord(contractReceivablesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateContractReceivablesRecord
     * @Description:  更新收款记录
     * @param: request
     * @param: contractReceivablesRecord
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractReceivablesRecord", method = RequestMethod.POST)
    public RetDataBean updateContractReceivablesRecord(HttpServletRequest request, ContractReceivablesRecord contractReceivablesRecord) {
        try {
            if (StringUtils.isBlank(contractReceivablesRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractReceivablesRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", contractReceivablesRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractReceivablesRecordService.updateContractReceivablesRecord(example, contractReceivablesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertContractSendgoods
     * @Description:  添加发货记录
     * @param: request
     * @param: contractSendgoods
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractSendgoods", method = RequestMethod.POST)
    public RetDataBean insertContractSendgoods(HttpServletRequest request, ContractSendgoods contractSendgoods) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSendgoods.setRecordId(SysTools.getGUID());
            contractSendgoods.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractSendgoods.setCreateUser(account.getAccountId());
            contractSendgoods.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractSendgoodsService.insertContractSendgoods(contractSendgoods));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractSendgoods
     * @Description:  删除发货记录
     * @param: request
     * @param: contractSendgoods
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractSendgoods", method = RequestMethod.POST)
    public RetDataBean deleteContractSendgoods(HttpServletRequest request, ContractSendgoods contractSendgoods) {
        try {
            if (StringUtils.isBlank(contractSendgoods.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSendgoods.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractSendgoodsService.deleteContractSendgoods(contractSendgoods));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updatContractSendgoods
     * @Description:  更新发货记录
     * @param: request
     * @param: contractSendgoods
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractSendgoods", method = RequestMethod.POST)
    public RetDataBean updateContractSendgoods(HttpServletRequest request, ContractSendgoods contractSendgoods) {
        try {
            if (StringUtils.isBlank(contractSendgoods.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractSendgoods.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", contractSendgoods.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractSendgoodsService.updateContractSendgoods(example, contractSendgoods));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertContractBill
     * @Description:  添加票据
     * @param: request
     * @param: contractBill
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractBill", method = RequestMethod.POST)
    public RetDataBean insertContractBill(HttpServletRequest request, ContractBill contractBill) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractBill.setBillId(SysTools.getGUID());
            contractBill.setStatus("1");
            contractBill.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractBill.setCreateUser(account.getAccountId());
            contractBill.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractBillService.insertContractBill(contractBill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractBill
     * @Description:  删除票据
     * @param: request
     * @param: contractBill
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractBill", method = RequestMethod.POST)
    public RetDataBean deleteContractBill(HttpServletRequest request, ContractBill contractBill) {
        try {
            if (StringUtils.isBlank(contractBill.getBillId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                contractBill.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractBillService.deleteContractBill(contractBill));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updatContractBill
     * @Description:  更新票据记录
     * @param: request
     * @param: contractBill
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractBill", method = RequestMethod.POST)
    public RetDataBean updatContractBill(HttpServletRequest request, ContractBill contractBill) {
        try {
            if (StringUtils.isBlank(contractBill.getBillId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractBill.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("billId", contractBill.getBillId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractBillService.updateContractBill(contractBill, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertContractPayable
     * @Description:  创建应付款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractPayable", method = RequestMethod.POST)
    public RetDataBean insertContractPayable(HttpServletRequest request, ContractPayable contractPayable) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractPayable.setPayableId(SysTools.getGUID());
            contractPayable.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractPayable.setCreateUser(account.getAccountId());
            contractPayable.setPayabled(0.0);
            contractPayable.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractPayableService.insertContractPayable(contractPayable));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractPayable
     * @Description:  删除应付款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractPayable", method = RequestMethod.POST)
    public RetDataBean deleteContractPayable(HttpServletRequest request, ContractPayable contractPayable) {
        try {
            if (StringUtils.isBlank(contractPayable.getPayableId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                contractPayable.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractPayableService.deleteContractPayable(contractPayable));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateContractPayable
     * @Description:  更新应付款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractPayable", method = RequestMethod.POST)
    public RetDataBean updateContractPayable(HttpServletRequest request, ContractPayable contractPayable) {
        try {
            if (StringUtils.isBlank(contractPayable.getPayableId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractPayable.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("payableId", contractPayable.getPayableId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPayableService.updateContractPayable(contractPayable, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertContractReceivables
     * @Description:  创建应收款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertContractReceivables", method = RequestMethod.POST)
    public RetDataBean insertContractReceivables(HttpServletRequest request, ContractReceivables contractReceivables) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractReceivables.setReceivablesId(SysTools.getGUID());
            contractReceivables.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractReceivables.setCreateUser(account.getAccountId());
            contractReceivables.setReceived(0.0);
            contractReceivables.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractReceivablesService.insertContractReceivables(contractReceivables));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContractReceivables
     * @Description:  删除应收款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteContractReceivables", method = RequestMethod.POST)
    public RetDataBean deleteContractReceivables(HttpServletRequest request, ContractReceivables contractReceivables) {
        try {
            if (StringUtils.isBlank(contractReceivables.getReceivablesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                contractReceivables.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractReceivablesService.deleteContractReceivables(contractReceivables));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateContractReceivables
     * @Description:  更新应收款记录
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateContractReceivables", method = RequestMethod.POST)
    public RetDataBean updateContractReceivables(HttpServletRequest request, ContractReceivables contractReceivables) {
        try {
            if (StringUtils.isBlank(contractReceivables.getReceivablesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractReceivables.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("receivablesId", contractReceivables.getReceivablesId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractReceivablesService.updateContractReceivables(contractReceivables, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delContractSort
     * @Description:  删除分类
     */
    @RequestMapping(value = "/delContractSort", method = RequestMethod.POST)
    public RetDataBean delContractSort(HttpServletRequest request, ContractSort contractSort) {
        try {
            if (StringUtils.isBlank(contractSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractSortService.deleteContractSort(contractSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: insertContractSort
     * @Description:  创建合同分类
     */
    @RequestMapping(value = "/insertContractSort", method = RequestMethod.POST)
    public RetDataBean insertContractSort(HttpServletRequest request, ContractSort contractSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSort.setSortId(SysTools.getGUID());
            contractSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            contractSort.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(contractSort.getSortLevel())) {
                contractSort.setSortLevel("0");
            }
            contractSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractSortService.insertContractSrot(contractSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateContractSort
     * @Description:  更新合同分类信息
     */
    @RequestMapping(value = "/updateContractSort", method = RequestMethod.POST)
    public RetDataBean updateContractSort(HttpServletRequest request, ContractSort contractSort) {
        try {
            if (StringUtils.isBlank(contractSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ContractSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", contractSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractSortService.updateContractSort(contractSort, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加合同
     *
     * @param request
     * @param contract
     * @return
     */
    @RequestMapping("/addContract")
    public RetDataBean addContract(HttpServletRequest request, Contract contract) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contract.setOrgId(account.getOrgId());
            contract.setCreateUser(account.getAccountId());
            contract.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractService.addContract(contract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteContract
     * @Description:  删除合同
     * @param: request
     * @param: contract
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping("/deleteContract")
    public RetDataBean deleteContract(HttpServletRequest request, Contract contract) {
        try {
            if (StringUtils.isBlank(contract.getContractId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                contract.setCreateUser(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractService.deleteContract(contract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 编辑合同
     *
     * @param request
     * @param contract
     * @return
     */
    @RequestMapping("/updateContract")
    public RetDataBean updateContract(HttpServletRequest request, Contract contract) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(contract.getContractId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(Contract.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("contractId", contract.getContractId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractService.updateContract(contract, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmPriv
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: setContractPriv
     * @Description:  设置合同管理权限
     */
    @RequestMapping(value = "/setContractPriv", method = RequestMethod.POST)
    public RetDataBean setContractPriv(HttpServletRequest request, ContractPriv contractPriv) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                contractPriv.setOrgId(account.getOrgId());
                int count = contractPrivService.isExistChild(account.getOrgId());
                if (count <= 0) {
                    contractPriv.setPrivId(SysTools.getGUID());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPrivService.insertContractPriv(contractPriv));
                } else {
                    Example example = new Example(ContractPriv.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPrivService.updateContractPriv(contractPriv, example));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
