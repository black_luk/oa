package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionModel;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionModelMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionModelService {
    private PartyUnionModelMapper partyUnionModelMapper;

    @Autowired
    public void setPartyUnionModelMapper(PartyUnionModelMapper partyUnionModelMapper) {
        this.partyUnionModelMapper = partyUnionModelMapper;
    }

    public int insertPartyUnionModel(PartyUnionModel partyUnionModel) {
        return partyUnionModelMapper.insert(partyUnionModel);
    }

    public int deletePartyUnionModel(PartyUnionModel partyUnionModel) {
        return partyUnionModelMapper.delete(partyUnionModel);
    }

    public int updatePartyUnionModel(Example example, PartyUnionModel partyUnionModel) {
        return partyUnionModelMapper.updateByExampleSelective(partyUnionModel, example);
    }

    public PartyUnionModel selectOnePartyUnionModel(PartyUnionModel partyUnionModel) {
        return partyUnionModelMapper.selectOne(partyUnionModel);
    }

    /**
     * 获取劳模记录列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionModelList(String orgId, String beginTime, String endTime, String search) {
        return partyUnionModelMapper.getPartyUnionModelList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取劳模记录列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionModelList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionModelList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
