$(function () {
    query();
    $(".js-addcampus").unbind("click").click(function () {
        doadd();
    });
    $(".js-delAll").unbind("click").click(function () {
        dodelAll();
    });
});

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getEduCampusList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'campusId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'name',
                title: '校区名称',
                sortable: true,
                width: '100px'
            },
            {
                field: 'address',
                width: '150px',
                title: '校区地址'
            },
            {
                field: 'linkMan',
                title: '联系人',
                width: '100px',
            },
            {
                field: 'tel',
                title: '联系电话',
                width: '100px',
            },
            {
                field: 'remark',
                title: '备注',
                width: '300px',
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.campusId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
                layer.msg(sysmsg[res.msg]);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(campusId) {
    var html = "<a href=\"javascript:void(0);edit('" + campusId + "')\" class=\"btn btn-sky btn-xs\" >修改</a>&nbsp;&nbsp;<a href=\"javascript:void(0);del('" + campusId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function edit(campusId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/eduget/getEduCampusById",
        type: "post",
        dataType: "json",
        data: {
            campusId: campusId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    $("#" + name).val(data.list[name]);
                }
            }
        }
    });
    $("#setcampus").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/updateEduCampus",
            type: "post",
            dataType: "json",
            data: {
                campusId: campusId,
                sortNo: $("#sortNo").val(),
                name: $("#name").val(),
                address: $("#address").val(),
                linkMan: $("#linkMan").val(),
                tel: $("#tel").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setcampus").modal("hide");
    });

}

function del(campusId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/sysset/deleteEduCampus",
            type: "post",
            dataType: "json",
            data: {
                campusId: campusId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        });
    } else {
        return;
    }
}

function doadd() {
    document.getElementById("form1").reset();
    $("#setcampus").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/insertEduCampus",
            type: "post",
            dataType: "json",
            data: {
                sortNo: $("#sortNo").val(),
                name: $("#name").val(),
                address: $("#address").val(),
                linkMan: $("#linkMan").val(),
                tel: $("#tel").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setcampus").modal("hide");
    });
}

function dodelAll() {
    var a = $("#myTable").bootstrapTable('getSelections');
    var campusIdArr = [];
    for (var i = 0; i < a.length; i++) {
        campusIdArr.push(a[i].campusId);
    }
    if (campusIdArr.length <= 0) {
        layer.msg("至少选择一个校区码!")
        return;
    } else {
        if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
            $.ajax({
                url: "/set/eduset/deleteEduCampusBatch",
                type: "post",
                dataType: "json",
                data: {
                    campusIdArr: campusIdArr
                },
                success: function (data) {
                    if (data.status == 200) {
                        layer.msg(sysmsg[data.msg]);
                        $("#myTable").bootstrapTable("refresh");
                    } else if (data.status = "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        } else {
            return;
        }
    }
}
