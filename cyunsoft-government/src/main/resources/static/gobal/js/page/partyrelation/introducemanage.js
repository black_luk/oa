$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyrelationget/getIntroduceList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '80px',
            title: '党员姓名'
        }, {
            field: 'partyOrgName',
            title: '调出组织名称',
            width: '100px'
        }, {
            field: 'recUnitName',
            title: '接收单位名称',
            width: '100px'
        }, {
            field: 'docNum',
            title: '编号',
            width: '80px',
            formatter: function (value, row, index) {
                return "第" + value + "号";
            }
        },
            {
                field: 'beginTime',
                width: '80px',
                title: '起始日期'
            }, {
                field: 'effective',
                width: '80px',
                title: '有效期'
            },
            {
                field: 'mobile',
                width: '100px',
                title: '联系电话'
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        memberId: $("#memberIdQuery").attr("data-value"),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >打印预览</a>";
    return html;
}

function edit(recordId) {
    $("#intromodal").modal("show");
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/partyrelationget/getPartyIntroduceById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    $("#" + id).val(recordInfo[id]);
                }
                $(".js-save").unbind("click").click(function () {
                    $.ajax({
                        url: "/set/partyrelationset/updatePartyIntroduce",
                        type: "post",
                        dataType: "json",
                        data: {
                            recordId: recordId,
                            recUnitName: $("#recUnitName").val(),
                            docNum: $("#docNum").val(),
                            mobile: $("#mobile").val(),
                            beginTime: $("#beginTime").val(),
                            effective: $("#effective").val(),
                            tel: $("#tel").val(),
                            fax: $("#fax").val(),
                            postCode: $("#postCode").val()
                        },
                        success: function (data) {
                            if (data.status == "500") {
                                console.log(data.msg);
                            } else if (data.status == "100") {
                                layer.msg(sysmsg[data.msg]);
                            } else {
                                $("#myTable").bootstrapTable("refresh");
                                $("#intromodal").modal("hide");
                                layer.msg(sysmsg[data.msg]);
                            }
                        }
                    })
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}


function details(recordId) {
    window.open("/app/core/partyrelation/introducedetails?recordId=" + recordId);
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyrelationset/deletePartyIntroduce",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}
