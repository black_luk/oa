/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetTaskController.java
 * @Package com.core136.controller.unit
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月9日 下午1:42:35
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.task;

import com.core136.bean.account.Account;
import com.core136.bean.sys.PageParam;
import com.core136.bean.task.Task;
import com.core136.bean.task.TaskGanttData;
import com.core136.bean.task.TaskGanttLink;
import com.core136.bean.task.TaskProcess;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.task.TaskGanttDataService;
import com.core136.service.task.TaskGanttLinkService;
import com.core136.service.task.TaskProcessService;
import com.core136.service.task.TaskService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author lsq
 * @ClassName: RoutGetTaskController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月9日 下午1:42:35
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/taskget")
public class RouteGetTaskController {
    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    private TaskGanttDataService taskGanttDataService;

    @Autowired
    public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
        this.taskGanttDataService = taskGanttDataService;
    }

    private TaskGanttLinkService taskGanttLinkService;

    @Autowired
    public void setTaskGanttLinkService(TaskGanttLinkService taskGanttLinkService) {
        this.taskGanttLinkService = taskGanttLinkService;
    }

    private TaskProcessService taskProcessService;

    @Autowired
    public void setTaskProcessService(TaskProcessService taskProcessService) {
        this.taskProcessService = taskProcessService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param processId
     * @return RetDataBean
     * @Title: getTaskProcessInfo
     * @Description:  获取处理事件详情
     */
    @RequestMapping(value = "/getTaskProcessInfo", method = RequestMethod.POST)
    public RetDataBean getTaskProcessInfo(String processId) {
        try {
            if (StringUtils.isBlank(processId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskProcessService.getProcessInfo(account.getOrgId(), processId, account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getTaskProcess
     * @Description:  获取子任务处理详情
     * @param: request
     * @param: taskProcess
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskProcess", method = RequestMethod.POST)
    public RetDataBean getTaskProcess(TaskProcess taskProcess) {
        try {
            if (StringUtils.isBlank(taskProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            taskProcess.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskProcessService.selectOneTaskProcess(taskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: geTaskListForDesk
     * @Description:  获取个人桌面的任务待办列表
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskListForDesk", method = RequestMethod.POST)
    public RetDataBean getTaskListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            PageParam pageParam = new PageParam();
            pageParam.setSort("d.start_date");
            pageParam.setSortOrder("asc");
            pageParam.setPageNumber(1);
            pageParam.setPageSize(10);
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskGanttDataService.getTaskListForDesk(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getTaskListForDesk2", method = RequestMethod.POST)
    public RetDataBean getTaskListForDesk2() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskGanttDataService.getTaskListForDesk2(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getMyTaskWorkList
     * @Description:   获取我的待办任务
     * @param: request
     * @param: pageParam
     * @param: createUser
     * @param: taskType
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyTaskWorkList", method = RequestMethod.POST)
    public RetDataBean getMyTaskWorkList(
            PageParam pageParam, String createUser,
            String taskType, String beginTime, String endTime) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.start_date");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskGanttDataService.getMyTaskWorkList(pageParam, createUser, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param taskType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyTaskProcessList
     * @Description:  获取子任务的处理过程列表
     */
    @RequestMapping(value = "/getMyTaskProcessList", method = RequestMethod.POST)
    public RetDataBean getMyTaskProcessList(
            PageParam pageParam, String createUser,
            String taskType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.complete_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskProcessService.getMyTaskProcessList(pageParam, createUser, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAllUserPrivList
     * @Description:  获取已分配的子任务列表
     * @param: request
     * @param: pageParam
     * @param: status
     * @param: taskType
     * @param: beginTime
     * @param: endTime
     * @param: userPriv
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllUserPrivList", method = RequestMethod.POST)
    public RetDataBean getAllUserPrivList(
            PageParam pageParam, String status,
            String taskType, String beginTime, String endTime, String userPriv
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.start_date");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskGanttDataService.getAllUserPrivList(pageParam, status, userPriv, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getParticipantUserList
     * @Description:  获取责任人列表
     * @param: request
     * @param: taskId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getParticipantUserList", method = RequestMethod.POST)
    public RetDataBean getParticipantUserList(String taskId) {
        try {
            if (StringUtils.isBlank(taskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getParticipantUserList(account.getOrgId(), taskId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getTaskGantInfo
     * @Description:   获取子任甘特图信息
     * @param: request
     * @param: taskId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskGantInfo", method = RequestMethod.POST)
    public RetDataBean getTaskGantInfo(String taskId) {
        try {
            if (StringUtils.isBlank(taskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskGanttDataService.getTaskGantInfo(account.getOrgId(), taskId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getTaskGanttLinkById
     * @Description:  获取子任务关系详情
     * @param: request
     * @param: taskGanttLink
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskGanttLinkById", method = RequestMethod.POST)
    public RetDataBean getTaskGanttLinkById(TaskGanttLink taskGanttLink) {
        try {
            if (StringUtils.isBlank(taskGanttLink.getTaskLinkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            taskGanttLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskGanttLinkService.selectOneTaskGanttLink(taskGanttLink));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getTaskGanttDataById
     * @Description:  获取子任务详情
     * @param: request
     * @param: taskGanttData
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskGanttDataById", method = RequestMethod.POST)
    public RetDataBean getTaskGanttDataById(TaskGanttData taskGanttData) {
        try {
            if (StringUtils.isBlank(taskGanttData.getTaskDataId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            taskGanttData.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskGanttDataService.selectOneTaskGanttData(taskGanttData));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getTaskById
     * @Description:  获取任务详情
     * @param: request
     * @param: task
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTaskById", method = RequestMethod.POST)
    public RetDataBean getTaskById(Task task) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(task.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            task.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.selectOneTask(task));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getManageTaskList
     * @Description:  获取任务列表
     * @param: request
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getManageTaskList", method = RequestMethod.POST)
    public RetDataBean getManageTaskList(
            PageParam pageParam, String status,
            String taskType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskService.getManageTaskList(pageParam, status, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param status
     * @param taskType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyChargeTaskList
     * @Description:  获取个人负责的任务
     */
    @RequestMapping(value = "/getMyChargeTaskList", method = RequestMethod.POST)
    public RetDataBean getMyChargeTaskList(
            PageParam pageParam, String createUser, String status,
            String taskType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("task:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskService.getMyChargeTaskList(pageParam, createUser, status, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param status
     * @param taskType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMySupervisorTaskList
     * @Description:  获取个人督查的任务列表
     */
    @RequestMapping(value = "/getMySupervisorTaskList", method = RequestMethod.POST)
    public RetDataBean getMySupervisorTaskList(
            PageParam pageParam, String createUser, String status,
            String taskType, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskService.getMySupervisorTaskList(pageParam, createUser, status, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAssignmentTaskList
     * @Description:  获取待分解任务列表
     * @param: request
     * @param: pageParam
     * @param: taskType
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAssignmentTaskList", method = RequestMethod.POST)
    public RetDataBean getAssignmentTaskList(
            PageParam pageParam,
            String taskType, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = taskService.getAssignmentTaskList(pageParam, taskType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
