$(function () {
    $(".js-setcreatepriv").unbind("click").click(function () {
        setapprovalpriv();
    })
    $.ajax({
        url: "/ret/proget/getProPrivById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (let id in data.list) {
                    if (id == "approvalUserPriv") {
                        $("#approvalUserPriv").attr("data-value", data.list.approvalUserPriv);
                        $("#approvalUserPriv").val(getUserNameByStr(data.list.approvalUserPriv));
                    } else if (id == "notNeedApprovalPriv") {
                        $("#notNeedApprovalPriv").attr("data-value", data.list.notNeedApprovalPriv);
                        $("#notNeedApprovalPriv").val(getUserNameByStr(data.list.notNeedApprovalPriv));
                    }
                }
            }
        }
    })
})

function setapprovalpriv() {
    $.ajax({
        url: "/set/proset/setProPriv",
        type: "post",
        dataType: "json",
        data: {
            approvalUserPriv: $("#approvalUserPriv").attr("data-value"),
            notNeedApprovalPriv: $("#notNeedApprovalPriv").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
