package com.core136.service.education;

import com.core136.bean.education.EduMajor;
import com.core136.mapper.education.EduMajorMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduMajorService {
    private EduMajorMapper eduMajorMapper;
    @Autowired
    public void setEduMajorMapper(EduMajorMapper eduMajorMapper)
    {
        this.eduMajorMapper = eduMajorMapper;
    }

    public int insertEduMajor(EduMajor eduMajor)
    {
        return eduMajorMapper.insert(eduMajor);
    }

    public int deleteEduMajor(EduMajor eduMajor)
    {
        return eduMajorMapper.delete(eduMajor);
    }

    public int deleteEduMajor(Example example)
    {
        return eduMajorMapper.deleteByExample(example);
    }

    public EduMajor selectOneEduMajor(EduMajor eduMajor) {
        return eduMajorMapper.selectOne(eduMajor);
    }

    public int updateEduMajor(Example example, EduMajor eduMajor) {
        return eduMajorMapper.updateByExampleSelective(eduMajor, example);
    }

    public List<Map<String, String>> getEduMajorListForTags(String orgId) {
        return eduMajorMapper.getEduMajorListForTags(orgId);
    }

    public List<Map<String, String>> getEduMajorNameByIds(String orgId, List<String> list) {
        return eduMajorMapper.getEduMajorNameByIds(orgId, list);
    }

    public List<EduMajor> getEduMajorList(Example example) {
        return eduMajorMapper.selectByExample(example);
    }

    public PageInfo<EduMajor> getEduMajorList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<EduMajor> datalist = getEduMajorList(example);
        PageInfo<EduMajor> pageInfo = new PageInfo<EduMajor>(datalist);
        return pageInfo;
    }

}
