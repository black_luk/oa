/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmFlowXml.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2020年1月20日 下午4:56:44
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.bpm;

import java.io.Serializable;
import java.util.List;

/**
 * @author lsq
 *
 */
public class BpmFlowXml implements Serializable {

    private static final long serialVersionUID = 1L;
    private BpmFlow bpmFlow;
    private List<BpmProcess> bpmProcessList;

    /**
     * @return the bpmFlow
     */
    public BpmFlow getBpmFlow() {
        return bpmFlow;
    }

    /**
     * @param bpmFlow the bpmFlow to set
     */
    public void setBpmFlow(BpmFlow bpmFlow) {
        this.bpmFlow = bpmFlow;
    }

    /**
     * @return the bpmProcessList
     */
    public List<BpmProcess> getBpmProcessList() {
        return bpmProcessList;
    }

    /**
     * @param bpmProcessList the bpmProcessList to set
     */
    public void setBpmProcessList(List<BpmProcess> bpmProcessList) {
        this.bpmProcessList = bpmProcessList;
    }

}
