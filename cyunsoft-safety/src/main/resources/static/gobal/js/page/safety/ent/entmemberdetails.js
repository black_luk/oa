$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyEntMemberById",
        type: "post",
        dataType: "json",
        data: {
            memberId: memberId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "post") {
                        $("#post").html(getSafetyCodeClassName(info[id], "post"));
                    } else if (id == "photo") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=safetyphotos&fileName=" + info[id]);
                    } else if (id == "entId") {
                        $.ajax({
                            url: "/ret/safetyget/getSafetyEntInfoById",
                            type: "post",
                            dataType: "json",
                            data: {entId: info[id]},
                            success: function (res) {
                                let entInfo = res.list;
                                if (entInfo) {
                                    $("#entId").html(entInfo.entName);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
