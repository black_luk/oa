$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#getTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyunionget/getPartyUnionModelList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'modelName',
            width: '100px',
            title: '荣誉称号'
        }, {
            field: 'getTime',
            width: '100px',
            title: '授予时间'
        }, {
            field: 'grantingAuthority',
            title: '授予机关',
            width: '100px'
        }, {
            field: 'deptId',
            width: '100px',
            title: '工作部门',
            formatter: function (value, row, index) {
                return getDeptNameByDeptIds(value);
            }
        }, {
            field: 'levelId',
            width: '100px',
            title: '工作职务',
            formatter: function (value, row, index) {
                return getUserLevelStr(value);
            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '发布时间'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteRecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyunitonset/deletePartyUnionModel",
            type: "post",
            dataType: "json",
            data: {recordId: recordId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#baseinfolistdiv").hide();
    $("#baseinfodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#attach").attr("data_value", "");
    $("#remark").code("");
    $("#levelId").attr("data-value", "");
    $("#deptId").attr("data-value", "");
    $("#accountId").attr("data-value", "");
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionModelById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(info[id]);
                    } else if (id == "deptId") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getDeptNameByDeptIds(info[id]));
                    } else if (id == "levelId") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserLevelStr(info[id]));
                    } else if (id == "accountId") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserNameByStr(info[id]));
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyUnionModel(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(recordId) {
    window.open("/app/core/partyunion/modeldetails?recordId=" + recordId);
}

function updatePartyUnionModel(recordId) {
    if($("#userName").val()==""&&$("#accountId").attr("data-value"))
    {
        layer.msg("人员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/updatePartyUnionModel",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            grantingAuthority: $("#grantingAuthority").val(),
            userName: $("#userName").val(),
            accountId: $("#accountId").attr("data-value"),
            levelId: $("#levelId").attr("data-value"),
            deptId: $("#deptId").attr("data-value"),
            getTime: $("#getTime").val(),
            modelName: $("#modelName").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#baseinfodiv").hide();
                $("#baseinfolistdiv").show();
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#baseinfodiv").hide();
    $("#baseinfolistdiv").show();
}
