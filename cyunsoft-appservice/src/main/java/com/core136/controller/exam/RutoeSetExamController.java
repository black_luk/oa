package com.core136.controller.exam;

import com.core136.bean.account.Account;
import com.core136.bean.exam.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.exam.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/examset")
public class RutoeSetExamController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ExamSortService examSortService;

    @Autowired
    public void setExamSortService(ExamSortService examSortService) {
        this.examSortService = examSortService;
    }

    private ExamQuestionsService examQuestionsService;

    @Autowired
    public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
        this.examQuestionsService = examQuestionsService;
    }

    private ExamTestService examTestService;

    @Autowired
    public void setExamTestService(ExamTestService examTestService) {
        this.examTestService = examTestService;
    }

    private ExamQuestionRecordService examQuestionRecordService;

    @Autowired
    public void setExamQuestionRecordService(ExamQuestionRecordService examQuestionRecordService) {
        this.examQuestionRecordService = examQuestionRecordService;
    }

    private ExamTestSortService examTestSortService;

    @Autowired
    public void setExamTestSortService(ExamTestSortService examTestSortService) {
        this.examTestSortService = examTestSortService;
    }

    private ExamGradeService examGradeService;

    @Autowired
    public void setExamGradeService(ExamGradeService examGradeService) {
        this.examGradeService = examGradeService;
    }


    /**
     * 试卷判卷
     *
     * @param recordId
     * @param totalGrade
     * @return
     */
    @RequestMapping(value = "/approvalExamTest", method = RequestMethod.POST)
    public RetDataBean approvalExamTest(String recordId, Double totalGrade) {
        try {
            if (StringUtils.isBlank(recordId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            ExamGrade examGrade = new ExamGrade();
            Account account = accountService.getRedisAUserInfoToAccount();
            examGrade.setOrgId(account.getOrgId());
            examGrade.setRecordId(recordId);
            examGrade = examGradeService.selectOneExamGrade(examGrade);
            examGrade.setGrade(examGrade.getGrade() + totalGrade);
            examGrade.setStatus("1");
            Example example = new Example(ExamTest.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", recordId);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, examGradeService.updateExamGrade(example, examGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examGrade
     * @return RetDataBean
     * @Title: insertExamGrade
     * @Description:  提交考试成绩
     */
    @RequestMapping(value = "/addExamGread", method = RequestMethod.POST)
    public RetDataBean addExamGread(ExamGrade examGrade) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String endTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            examGrade.setRecordId(SysTools.getGUID());
            examGrade.setCreateTime(endTime);
            examGrade.setAccountId(account.getAccountId());
            examGrade.setCreateUser(account.getAccountId());
            examGrade.setStatus("0");
            examGrade.setOrgId(account.getOrgId());
            examGrade.setEndTime(endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examGradeService.addExamGread(examGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examGrade
     * @return RetDataBean
     * @Title: deleteExamGrade
     * @Description:  删除考试成绩
     */
    @RequestMapping(value = "/deleteExamGrade", method = RequestMethod.POST)
    public RetDataBean deleteExamGrade(ExamGrade examGrade) {
        try {
            if (StringUtils.isBlank(examGrade.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examGrade.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examGradeService.deleteExamGrade(examGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examGrade
     * @return RetDataBean
     * @Title: updateExamGrade
     * @Description:  更新考试成绩
     */
    @RequestMapping(value = "/updateExamGrade", method = RequestMethod.POST)
    public RetDataBean updateExamGrade(ExamGrade examGrade) {
        try {
            if (StringUtils.isBlank(examGrade.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamGrade.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", examGrade.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examGradeService.updateExamGrade(example, examGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param examQuestionRecord
     * @return RetDataBean
     * @Title: insertExamQuestionRecord
     * @Description:  添加试卷试题
     */
    @RequestMapping(value = "/insertExamQuestionRecord", method = RequestMethod.POST)
    public RetDataBean insertExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examQuestionRecord.setRecordId(SysTools.getGUID());
            examQuestionRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            examQuestionRecord.setCreateUser(account.getAccountId());
            examQuestionRecord.setStatus("0");
            examQuestionRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examQuestionRecordService.insertExamQuestionRecord(examQuestionRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examQuestionRecord
     * @return RetDataBean
     * @Title: deleteExamQuestionRecord
     * @Description:  删除试卷试题
     */
    @RequestMapping(value = "/deleteExamQuestionRecord", method = RequestMethod.POST)
    public RetDataBean deleteExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        try {
            if (StringUtils.isBlank(examQuestionRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examQuestionRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examQuestionRecordService.deleteExamQuestionRecord(examQuestionRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examQuestionRecord
     * @return RetDataBean
     * @Title: updateExamQuestionRecord
     * @Description:  更新试卷试题
     */
    @RequestMapping(value = "/updateExamQuestionRecord", method = RequestMethod.POST)
    public RetDataBean updateExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        try {
            if (StringUtils.isBlank(examQuestionRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamQuestionRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", examQuestionRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examQuestionRecordService.updateExamQuestionRecord(example, examQuestionRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTest
     * @return RetDataBean
     * @Title: insertExamTest
     * @Description:  添加试题测试
     */
    @RequestMapping(value = "/insertExamTest", method = RequestMethod.POST)
    public RetDataBean insertExamTest(ExamTest examTest) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examTest.setRecordId(SysTools.getGUID());
            examTest.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            examTest.setCreateUser(account.getAccountId());
            examTest.setStatus("0");
            examTest.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examTestService.insertExamTest(examTest));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTest
     * @return RetDataBean
     * @Title: deleteExamTest
     * @Description:  删除试题测试
     */
    @RequestMapping(value = "/deleteExamTest", method = RequestMethod.POST)
    public RetDataBean deleteExamTest(ExamTest examTest) {
        try {
            if (StringUtils.isBlank(examTest.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examTest.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examTestService.deleteExamTest(examTest));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTest
     * @return RetDataBean
     * @Title: updateExamTest
     * @Description:  更新试题测试
     */
    @RequestMapping(value = "/updateExamTest", method = RequestMethod.POST)
    public RetDataBean updateExamTest(ExamTest examTest) {
        try {
            if (StringUtils.isBlank(examTest.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamTest.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", examTest.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examTestService.updateExamTest(example, examTest));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param examQuestions
     * @return RetDataBean
     * @Title: insertExamQuestions
     * @Description:  添加试题
     */
    @RequestMapping(value = "/insertExamQuestions", method = RequestMethod.POST)
    public RetDataBean insertExamQuestions(ExamQuestions examQuestions) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(examQuestions.getExamType())) {
                if (examQuestions.getExamType().equals("2")) {
                    examQuestions.setContent("");
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MSG_00017);
            }
            examQuestions.setRecordId(SysTools.getGUID());
            examQuestions.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            examQuestions.setCreateUser(account.getAccountId());
            examQuestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examQuestionsService.insertExamQuestions(examQuestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examQuestions
     * @return RetDataBean
     * @Title: deleteExamQuestions
     * @Description:  删除试题类型
     */
    @RequestMapping(value = "/deleteExamQuestions", method = RequestMethod.POST)
    public RetDataBean deleteExamQuestions(ExamQuestions examQuestions) {
        try {
            if (StringUtils.isBlank(examQuestions.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examQuestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examQuestionsService.deleteExamQuestions(examQuestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examQuestions
     * @return RetDataBean
     * @Title: updateExamQuestions
     * @Description:  更新试题内容
     */
    @RequestMapping(value = "/updateExamQuestions", method = RequestMethod.POST)
    public RetDataBean updateExamQuestions(ExamQuestions examQuestions) {
        try {
            if (StringUtils.isBlank(examQuestions.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamQuestions.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", examQuestions.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examQuestionsService.updateExamQuestions(example, examQuestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param examSort
     * @return RetDataBean
     * @Title: insertExamSort
     * @Description:  添加试题类型
     */
    @RequestMapping(value = "/insertExamSort", method = RequestMethod.POST)
    public RetDataBean insertExamSort(ExamSort examSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examSort.setSortId(SysTools.getGUID());
            examSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            if (StringUtils.isBlank(examSort.getLevelId())) {
                examSort.setLevelId("0");
            }
            examSort.setCreateUser(account.getAccountId());
            examSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examSortService.insertExamSort(examSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examSort
     * @return RetDataBean
     * @Title: deleteExamSort
     * @Description:  删除试题类型
     */
    @RequestMapping(value = "/deleteExamSort", method = RequestMethod.POST)
    public RetDataBean deleteExamSort(ExamSort examSort) {
        try {
            if (StringUtils.isBlank(examSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examSortService.deleteExamSort(examSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examSort
     * @return RetDataBean
     * @Title: updateExamSort
     * @Description:  更新试题分类
     */
    @RequestMapping(value = "/updateExamSort", method = RequestMethod.POST)
    public RetDataBean updateExamSort(ExamSort examSort) {
        try {
            if (StringUtils.isBlank(examSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", examSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examSortService.updateExamSort(example, examSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param examTestSort
     * @return RetDataBean
     * @Title: insertExamTestSort
     * @Description:  添加考试类型
     */
    @RequestMapping(value = "/insertExamTestSort", method = RequestMethod.POST)
    public RetDataBean insertExamTestSort(ExamTestSort examTestSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examTestSort.setSortId(SysTools.getGUID());
            examTestSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            if (StringUtils.isBlank(examTestSort.getLevelId())) {
                examTestSort.setLevelId("0");
            }
            examTestSort.setCreateUser(account.getAccountId());
            examTestSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examTestSortService.insertExamTestSort(examTestSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTestSort
     * @return RetDataBean
     * @Title: deleteExamTestSort
     * @Description:  删除考试类型
     */
    @RequestMapping(value = "/deleteExamTestSort", method = RequestMethod.POST)
    public RetDataBean deleteExamTestSort(ExamTestSort examTestSort) {
        try {
            if (StringUtils.isBlank(examTestSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            examTestSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examTestSortService.deleteExamTestSort(examTestSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTestSort
     * @return RetDataBean
     * @Title: updateExamTestSort
     * @Description:  更新试题分类
     */
    @RequestMapping(value = "/updateExamTestSort", method = RequestMethod.POST)
    public RetDataBean updateExamTestSort(ExamTestSort examTestSort) {
        try {
            if (StringUtils.isBlank(examTestSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ExamTestSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", examTestSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examTestSortService.updateExamTestSort(example, examTestSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
