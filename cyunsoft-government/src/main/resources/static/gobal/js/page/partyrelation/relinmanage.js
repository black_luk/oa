$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdQueryTree"), partyOrgIdQuerysetting, newTreeNodes);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).offset().left + "px"
        }).slideDown(200);
    });
    $("#outPartyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#outPartyOrgIdQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).offset().left + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyrelationget/getRelInList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '80px',
            title: '转入党员'
        }, {
            field: 'userSex',
            width: '80px',
            title: '性别'
        }, {
            field: 'birthday',
            width: '80px',
            title: '出生日期'
        }, {
            field: 'oldPartyOrg',
            title: '转出党组织',
            width: '100px'
        }, {
            field: 'partyOrgName',
            title: '转入党支部',
            width: '100px'
        }, {
            field: 'inType',
            title: '转入类型',
            width: '80px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "新入党";
                } else if (value == "2") {
                    return "恢复党籍";
                } else if (value == "3") {
                    return "自国（境）外转入";
                } else if (value == "4") {
                    return "自军队转入";
                } else if (value == "5") {
                    return "自武警部队转入";
                } else if (value == "6") {
                    return "自其他系统转入";
                } else if (value == "7") {
                    return "自本系统内的其他单位转入";
                } else if (value == "8") {
                    return "自外省（区、市）转入";
                } else if (value == "9") {
                    return "自本省(区、市)直属的其他单位转入";
                } else if (value == "10") {
                    return "自本省(区、市)内的其他地(市、州、盟、区)转入";
                } else if (value == "11") {
                    return "自本地(市、州、盟、区)直属的其他单位转入";
                } else if (value == "12") {
                    return "自本地(市、州、盟、区)内的其他市(县、区、街道)转入";
                } else if (value == "13") {
                    return "自本市(县、区、街道)内的其他乡(镇、街道)转入";
                } else if (value == "14") {
                    return "自本乡（镇、街道）直属的其他单位转入";
                } else if (value == "15") {
                    return "内部转移";
                } else {
                    return "未知";
                }
            }
        },
            {
                field: 'inTime',
                width: '80px',
                title: '转入日期'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        inType: $("#inTypeQuery").val(),
        partyOrgId: $("#partyOrgIdQuery").attr("data-value"),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/partyrelation/relindetails?recordId=" + recordId);
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyrelationset/deletePartyRelIn",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
var partyOrgIdQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
