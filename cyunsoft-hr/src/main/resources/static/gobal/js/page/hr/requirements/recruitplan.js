let ue = UE.getEditor("remark");
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    jeDate("#planEndTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    $(".js-add-save").unbind("click").click(function () {
        addRecruitPlan();
    })
})

function addRecruitPlan() {
    if($("#title").val()=="")
    {
        layer.msg("计划标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrRecruitPlan",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            planEndTime: $("#planEndTime").val(),
            title: $("#title").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            approvedUser: $("#approvedUser").attr("data-value"),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
