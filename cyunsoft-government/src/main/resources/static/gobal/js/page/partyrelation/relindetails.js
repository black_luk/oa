$(function () {
    $.ajax({
        url: "/ret/partyrelationget/getPartyRelInById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "inType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("新入党");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("恢复党籍");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("自国（境）外转入");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("自军队转入");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("自武警部队转入");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("自其他系统转入");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("自本系统内的其他单位转入");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("自外省（区、市）转入");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("自本省(区、市)直属的其他单位转入");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("自本省(区、市)内的其他地(市、州、盟、区)转入");
                        } else if (recordInfo[id] == "11") {
                            $("#" + id).html("自本地(市、州、盟、区)直属的其他单位转入");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("自本地(市、州、盟、区)内的其他市(县、区、街道)转入");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("自本市(县、区、街道)内的其他乡(镇、街道)转入");
                        } else if (recordInfo[id] == "14") {
                            $("#" + id).html("自本乡（镇、街道）直属的其他单位转入");
                        } else if (recordInfo[id] == "15") {
                            $("#" + id).html("内部转移");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "userType") {
                        if (recordInfo[id] == "1") {
                            $("#userType").html("正式党员");
                        } else if (recordInfo[id] == "2") {
                            $("#userType").html("预备党员");
                        } else {
                            $("#userType").html("未知");
                        }
                    } else if (id == "nation") {
                        $("#nation").html(getCodeClassName(recordInfo[id], "nation"));
                    } else if (id == "nativePlace") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyNativePlaceById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#nativePlace").html(res.list.sortName);
                                    } else {
                                        $("#nativePlace").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "education") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyEducationById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#education").html(res.list.sortName);
                                    } else {
                                        $("#education").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "degree") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyDegreeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#degree").html(res.list.sortName);
                                    } else {
                                        $("#degree").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userPost") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyPostById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#userPost").html(res.list.sortName);
                                    } else {
                                        $("#userPost").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "photo") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + recordInfo[id]);
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
