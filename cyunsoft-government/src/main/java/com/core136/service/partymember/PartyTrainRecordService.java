package com.core136.service.partymember;

import com.core136.bean.partymember.PartyTrainRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyTrainRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTrainRecordService {
    private PartyTrainRecordMapper partyTrainRecordMapper;

    @Autowired
    public void setPartyTrainRecordMapper(PartyTrainRecordMapper partyTrainRecordMapper) {
        this.partyTrainRecordMapper = partyTrainRecordMapper;
    }

    public int insertPartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        return partyTrainRecordMapper.insert(partyTrainRecord);
    }

    public int deletePartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        return partyTrainRecordMapper.delete(partyTrainRecord);
    }

    public int updatePartyTrainRecord(Example example, PartyTrainRecord partyTrainRecord) {
        return partyTrainRecordMapper.updateByExampleSelective(partyTrainRecord, example);
    }

    public PartyTrainRecord selectOnePartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        return partyTrainRecordMapper.selectOne(partyTrainRecord);
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberTrainList
     * @Description:  获取党员自己的培训记录
     */
    public List<Map<String, String>> getMyMemberTrainList(String orgId, String memberId) {
        return partyTrainRecordMapper.getMyMemberTrainList(orgId, memberId);
    }

    /**
     * @param orgId
     * @param classType
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberTrainList
     * @Description:  获取党员培训记录列表
     */
    public List<Map<String, String>> getMemberTrainList(String orgId, String classType, String trainType, String beginTime, String endTime, String search) {
        return partyTrainRecordMapper.getMemberTrainList(orgId, classType, trainType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param classType
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberTrainList
     * @Description:  获取党员培训记录列表
     */
    public PageInfo<Map<String, String>> getMemberTrainList(PageParam pageParam, String classType, String trainType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberTrainList(pageParam.getOrgId(), classType, trainType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
