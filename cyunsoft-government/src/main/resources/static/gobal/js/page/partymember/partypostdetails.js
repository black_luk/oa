$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyPostRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "takeType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("党委决定任职");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("差额选举");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("等额选举");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "postType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("不在任");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("在任");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("挂任");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("试用");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "govPost") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyGovPostById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#govPost").html(res.list.sortName);
                                    } else {
                                        $("#govPost").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            data: {
                                partyOrgId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#partyOrgId").html(res.list.partyOrgName);
                                    } else {
                                        $("#partyOrgId").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
