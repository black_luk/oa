package com.core136.controller.education;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.education.*;
import com.core136.bean.sys.CodeClass;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.education.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController

@RequestMapping("/set/eduset")
public class RouteSetEduController {
    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private EduGradeService eduGradeService;

    @Autowired
    public void setEduGradeService(EduGradeService eduGradeService) {
        this.eduGradeService = eduGradeService;
    }

    private EduCampusService eduCampusService;

    @Autowired
    public void setEduCampusService(EduCampusService eduCampusService) {
        this.eduCampusService = eduCampusService;
    }

    private EduSemesterService eduSemesterService;

    @Autowired
    public void setEduSemesterService(EduSemesterService eduSemesterService) {
        this.eduSemesterService = eduSemesterService;
    }

    private EduClassService eduClassService;

    @Autowired
    public void setEduClassService(EduClassService eduClassService) {
        this.eduClassService = eduClassService;
    }

    private EduDepartmentsService eduDepartmentsService;

    @Autowired
    public void setEduDepartmentsService(EduDepartmentsService eduDepartmentsService) {
        this.eduDepartmentsService = eduDepartmentsService;
    }

    private EduCourseService eduCourseService;

    @Autowired
    public void setEduCourseService(EduCourseService eduCourseService) {
        this.eduCourseService = eduCourseService;
    }

    private EduStudentService eduStudentService;

    @Autowired
    public void setEduStudentService(EduStudentService eduStudentService) {
        this.eduStudentService = eduStudentService;
    }

    private EduTeacherService eduTeacherService;

    @Autowired
    public void setEduTeacherService(EduTeacherService eduTeacherService) {
        this.eduTeacherService = eduTeacherService;
    }

    public EduTeacherTrainService eduTeacherTrainService;

    @Autowired
    public void setEduTeacherTrainService(EduTeacherTrainService eduTeacherTrainService) {
        this.eduTeacherTrainService = eduTeacherTrainService;
    }

    private EduTeacherRePuService eduTeacherRePuService;

    @Autowired
    private void setEduTeacherRePuService(EduTeacherRePuService eduTeacherRePuService) {
        this.eduTeacherRePuService = eduTeacherRePuService;
    }

    private EduTeacherExperienceService eduTeacherExperienceService;

    @Autowired
    private void setEduTeacherExperienceService(EduTeacherExperienceService eduTeacherExperienceService) {
        this.eduTeacherExperienceService = eduTeacherExperienceService;
    }

    private EduClassCodeService eduClassCodeService;

    @Autowired
    public void setEduClassCodeService(EduClassCodeService eduClassCodeService) {
        this.eduClassCodeService = eduClassCodeService;
    }

    private EduMajorService eduMajorService;

    @Autowired
    public void setEduMajorService(EduMajorService eduMajorService) {
        this.eduMajorService = eduMajorService;
    }

    /**
     * 创建年级
     *
     * @param eduGrade
     * @return
     */
    @RequestMapping(value = "/insertEduGrade", method = RequestMethod.POST)
    public RetDataBean insertEduGrade(EduGrade eduGrade) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduGrade.setGradeId(SysTools.getGUID());
            eduGrade.setOrgId(account.getOrgId());
            eduGrade.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduGrade.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduGradeService.insertEduGrade(eduGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除年级
     *
     * @param eduGrade
     * @return
     */
    @RequestMapping(value = "/deleteEduGrade", method = RequestMethod.POST)
    public RetDataBean deleteEduGrade(EduGrade eduGrade) {
        try {
            if (StringUtils.isBlank(eduGrade.getGradeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduGrade.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduGradeService.deleteEduGrade(eduGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新年级信息
     *
     * @param eduGrade
     * @return
     */
    @RequestMapping(value = "/updateEduGrade", method = RequestMethod.POST)
    public RetDataBean updateEduGrade(EduGrade eduGrade) {
        try {
            if (StringUtils.isBlank(eduGrade.getGradeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduGrade.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("gradeId", eduGrade.getGradeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduGradeService.updateEduGrade(example, eduGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 批量删除专业
     *
     * @param majorIdArr
     * @return
     */
    @RequestMapping(value = "/deleteEduMajorBatch", method = RequestMethod.POST)
    public RetDataBean deleteCodeClassBatch(@RequestParam(value = "majorIdArr[]") String[] majorIdArr) {
        try {
            if (majorIdArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(majorIdArr));
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("majorId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduMajorService.deleteEduMajor(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
    /**
     * 创建专业
     *
     * @param eduMajor
     * @return
     */
    @RequestMapping(value = "/insertEduMajor", method = RequestMethod.POST)
    public RetDataBean insertEduMajor(EduMajor eduMajor) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduMajor.setMajorId(SysTools.getGUID());
            eduMajor.setOrgId(account.getOrgId());
            eduMajor.setStatus("1");
            eduMajor.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduMajor.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduMajorService.insertEduMajor(eduMajor));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除专业
     *
     * @param eduMajor
     * @return
     */
    @RequestMapping(value = "/deleteEduMajor", method = RequestMethod.POST)
    public RetDataBean deleteEduMajor(EduMajor eduMajor) {
        try {
            if (StringUtils.isBlank(eduMajor.getMajorId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduMajor.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduMajorService.deleteEduMajor(eduMajor));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新专业
     *
     * @param eduMajor
     * @return
     */
    @RequestMapping(value = "/updateEduMajor", method = RequestMethod.POST)
    public RetDataBean updateEduMajor(EduMajor eduMajor) {
        try {
            if (StringUtils.isBlank(eduMajor.getMajorId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduMajor.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("majorId", eduMajor.getMajorId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduMajorService.updateEduMajor(example, eduMajor));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 创建教师培训进修记录
     *
     * @param eduTeacherTrain
     * @return
     */
    @RequestMapping(value = "/insertEduTeacherTrain", method = RequestMethod.POST)
    public RetDataBean insertEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherTrain.setRecordId(SysTools.getGUID());
            eduTeacherTrain.setOrgId(account.getOrgId());
            eduTeacherTrain.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduTeacherTrain.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduTeacherTrainService.insertEduTeacherTrain(eduTeacherTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除教师培训进修记录
     *
     * @param eduTeacherTrain
     * @return
     */
    @RequestMapping(value = "/deleteEduTeacherTrain", method = RequestMethod.POST)
    public RetDataBean deleteEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        try {
            if (StringUtils.isBlank(eduTeacherTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduTeacherTrainService.deleteEduTeacherTrain(eduTeacherTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新教师培训进修记录
     *
     * @param eduTeacherTrain
     * @return
     */
    @RequestMapping(value = "/updateEduTeacherTrain", method = RequestMethod.POST)
    public RetDataBean updateEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        try {
            if (StringUtils.isBlank(eduTeacherTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduTeacherTrain.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", eduTeacherTrain.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduTeacherTrainService.updateEduTeacherTrain(example, eduTeacherTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 创建教师任教记录
     *
     * @param eduTeacherExperience
     * @return
     */
    @RequestMapping(value = "/insertEduTeacherExperience", method = RequestMethod.POST)
    public RetDataBean insertEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherExperience.setRecordId(SysTools.getGUID());
            eduTeacherExperience.setOrgId(account.getOrgId());
            eduTeacherExperience.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduTeacherExperience.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduTeacherExperienceService.insertEduTeacherExperience(eduTeacherExperience));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除教师任教记录
     *
     * @param eduTeacherExperience
     * @return
     */
    @RequestMapping(value = "/deleteEduTeacherExperience", method = RequestMethod.POST)
    public RetDataBean deleteEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        try {
            if (StringUtils.isBlank(eduTeacherExperience.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherExperience.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduTeacherExperienceService.deleteEduTeacherExperience(eduTeacherExperience));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新教师任教记录
     *
     * @param eduTeacherExperience
     * @return
     */
    @RequestMapping(value = "/updateEduTeacherExperience", method = RequestMethod.POST)
    public RetDataBean updateEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        try {
            if (StringUtils.isBlank(eduTeacherExperience.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduTeacherExperience.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", eduTeacherExperience.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduTeacherExperienceService.updateEduTeacherExperience(example, eduTeacherExperience));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建奖惩记录
     *
     * @param eduTeacherRePu
     * @return
     */
    @RequestMapping(value = "/insertEduTeacherRePu", method = RequestMethod.POST)
    public RetDataBean insertEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherRePu.setRecordId(SysTools.getGUID());
            eduTeacherRePu.setOrgId(account.getOrgId());
            eduTeacherRePu.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduTeacherRePu.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduTeacherRePuService.insertEduTeacherRePu(eduTeacherRePu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除教师奖惩记录
     *
     * @param eduTeacherRePu
     * @return
     */
    @RequestMapping(value = "/deleteEduTeacherRePu", method = RequestMethod.POST)
    public RetDataBean deleteEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        try {
            if (StringUtils.isBlank(eduTeacherRePu.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherRePu.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduTeacherRePuService.deleteEduTeacherRePu(eduTeacherRePu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新教师奖惩记录
     *
     * @param eduTeacherRePu
     * @return
     */
    @RequestMapping(value = "/updateEduTeacherRePu", method = RequestMethod.POST)
    public RetDataBean updateEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        try {
            if (StringUtils.isBlank(eduTeacherRePu.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduTeacherRePu.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", eduTeacherRePu.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduTeacherRePuService.updateEduTeacherRePu(example, eduTeacherRePu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduTeacher
     * @return RetDataBean
     * @Title: insertEduTeacher
     * @Description:  添加教师信息
     */
    @RequestMapping(value = "/insertEduTeacher", method = RequestMethod.POST)
    public RetDataBean insertEduTeacher(EduTeacher eduTeacher) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacher.setTeacherId(SysTools.getGUID());
            if (StringUtils.isNotBlank(eduTeacher.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(eduTeacher.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                if (userInfo != null) {
                    eduTeacher.setUserName(userInfo.getUserName());
                }
            }
            if (StringUtils.isBlank(eduTeacher.getUserName())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            eduTeacher.setPinYin(StrTools.getPinYin(eduTeacher.getUserName()));
            eduTeacher.setFirstPinYin(StrTools.getPinYinHeadChar(eduTeacher.getUserName()));
            eduTeacher.setStatus("0");
            eduTeacher.setOrgId(account.getOrgId());
            eduTeacher.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduTeacher.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduTeacherService.insertEduTeacher(eduTeacher));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduTeacher
     * @return RetDataBean
     * @Title: deleteEduTeacher
     * @Description:  删除教师信息
     */
    @RequestMapping(value = "/deleteEduTeacher", method = RequestMethod.POST)
    public RetDataBean deleteEduTeacher(EduTeacher eduTeacher) {
        try {
            if (StringUtils.isBlank(eduTeacher.getTeacherId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacher.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduTeacherService.deleteEduTeacher(eduTeacher));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduTeacher
     * @return RetDataBean
     * @Title: updateEduTeacher
     * @Description:  更新教师信息
     */
    @RequestMapping(value = "/updateEduTeacher", method = RequestMethod.POST)
    public RetDataBean updateEduTeacher(EduTeacher eduTeacher) {
        try {
            if (StringUtils.isBlank(eduTeacher.getTeacherId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(eduTeacher.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(eduTeacher.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                if (userInfo != null) {
                    eduTeacher.setUserName(userInfo.getUserName());
                }
            }
            if (StringUtils.isBlank(eduTeacher.getUserName())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            eduTeacher.setPinYin(StrTools.getPinYin(eduTeacher.getUserName()));
            eduTeacher.setFirstPinYin(StrTools.getPinYinHeadChar(eduTeacher.getUserName()));
            Example example = new Example(EduTeacher.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("teacherId", eduTeacher.getTeacherId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduTeacherService.updateEduTeacher(example, eduTeacher));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduStudent
     * @return RetDataBean
     * @Title: insertEduStudent
     * @Description:  添加加学生
     */
    @RequestMapping(value = "/insertEduStudent", method = RequestMethod.POST)
    public RetDataBean insertEduStudent(EduStudent eduStudent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduStudent.setOrgId(account.getOrgId());
            eduStudent.setStudentId(SysTools.getGUID());
            eduStudent.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduStudent.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduStudentService.insertEduStudent(eduStudent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduStudent
     * @return RetDataBean
     * @Title: deleteEduStudent
     * @Description:  删除学生记录
     */
    @RequestMapping(value = "/deleteEduStudent", method = RequestMethod.POST)
    public RetDataBean deleteEduStudent(EduStudent eduStudent) {
        try {
            if (StringUtils.isBlank(eduStudent.getStudentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduStudent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduStudentService.deleteEduStudent(eduStudent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduStudent
     * @return RetDataBean
     * @Title: updateEduStudent
     * @Description:  更新学生信息
     */
    @RequestMapping(value = "/updateEduStudent", method = RequestMethod.POST)
    public RetDataBean updateEduStudent(EduStudent eduStudent) {
        try {
            if (StringUtils.isBlank(eduStudent.getStudentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduStudent.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("studentId", eduStudent.getStudentId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduStudentService.updateEduStudent(example, eduStudent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param courseArr
     * @return RetDataBean
     * @Title: deleteEduCourseBatch
     * @Description:  批量删除课程
     */
    @RequestMapping(value = "/deleteEduCourseBatch", method = RequestMethod.POST)
    public RetDataBean deleteEduCourseBatch(@RequestParam(value = "courseArr[]") String[] courseArr) {
        try {
            if (courseArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(courseArr));
            Example example = new Example(EduCourse.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("courseId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduCourseService.deleteEduCourse(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCourse
     * @return RetDataBean
     * @Title: updateEduCourse
     * @Description:  更新课程
     */
    @RequestMapping(value = "/updateEduCourse", method = RequestMethod.POST)
    public RetDataBean updateEduCourse(EduCourse eduCourse) {
        try {
            if (StringUtils.isBlank(eduCourse.getCourseId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduCourse.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("courseId", eduCourse.getCourseId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduCourseService.updateEduCourse(example, eduCourse));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCourse
     * @return RetDataBean
     * @Title: deleteEduCourse
     * @Description:  删除课程
     */
    @RequestMapping(value = "/deleteEduCourse", method = RequestMethod.POST)
    public RetDataBean deleteEduCourse(EduCourse eduCourse) {
        try {
            if (StringUtils.isBlank(eduCourse.getCourseId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCourse.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduCourseService.deleteEduCourse(eduCourse));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCourse
     * @return RetDataBean
     * @Title: insertEduCourse
     * @Description:  添加课程
     */
    @RequestMapping(value = "/insertEduCourse", method = RequestMethod.POST)
    public RetDataBean insertEduCourse(EduCourse eduCourse) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCourse.setOrgId(account.getOrgId());
            eduCourse.setCourseId(SysTools.getGUID());
            eduCourse.setStatus("1");
            eduCourse.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduCourse.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduCourseService.insertEduCourse(eduCourse));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduDepartments
     * @return RetDataBean
     * @Title: insertEduDepartments
     * @Description:  创建院系
     */
    @RequestMapping(value = "/insertEduDepartments", method = RequestMethod.POST)
    public RetDataBean insertEduDepartments(EduDepartments eduDepartments) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(eduDepartments.getLevelId())) {
                eduDepartments.setLevelId("0");
            }
            eduDepartments.setOrgId(account.getOrgId());
            eduDepartments.setDepartmentsId(SysTools.getGUID());
            eduDepartments.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduDepartments.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduDepartmentsService.insertEduDepartments(eduDepartments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduDepartments
     * @return RetDataBean
     * @Title: deleteEduDepartments
     * @Description:  删除院系
     */
    @RequestMapping(value = "/deleteEduDepartments", method = RequestMethod.POST)
    public RetDataBean deleteEduDepartments(EduDepartments eduDepartments) {
        try {
            if (StringUtils.isBlank(eduDepartments.getDepartmentsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduDepartments.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduDepartmentsService.deleteEduDepartments(eduDepartments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduDepartments
     * @return RetDataBean
     * @Title: updateEduDepartments
     * @Description:  更新院系信息
     */
    @RequestMapping(value = "/updateEduDepartments", method = RequestMethod.POST)
    public RetDataBean updateEduDepartments(EduDepartments eduDepartments) {
        try {
            if (StringUtils.isBlank(eduDepartments.getDepartmentsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduDepartments.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("departmentsId", eduDepartments.getDepartmentsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduDepartmentsService.updateEduDepartments(example, eduDepartments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduClass
     * @return RetDataBean
     * @Title: insertEduClass
     * @Description:  创建班级
     */
    @RequestMapping(value = "/insertEduClass", method = RequestMethod.POST)
    public RetDataBean insertEduClass(EduClass eduClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String semesterId = eduSemesterService.getCurrentSemesterId(account.getOrgId());
            if (StringUtils.isBlank(semesterId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
            }
            eduClass.setClassId(SysTools.getGUID());
            eduClass.setSemesterId(semesterId);
            eduClass.setOrgId(account.getOrgId());
            eduClass.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduClass.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduClassService.insertEduClass(eduClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduClass
     * @return RetDataBean
     * @Title: deleteEduClass
     * @Description:  删除班级
     */
    @RequestMapping(value = "/deleteEduClass", method = RequestMethod.POST)
    public RetDataBean deleteEduClass(EduClass eduClass) {
        try {
            if (StringUtils.isBlank(eduClass.getClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduClassService.deleteEduClass(eduClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduClass
     * @return RetDataBean
     * @Title: updateEduClass
     * @Description:  更新班级信息
     */
    @RequestMapping(value = "/updateEduClass", method = RequestMethod.POST)
    public RetDataBean updateEduClass(EduClass eduClass) {
        try {
            if (StringUtils.isBlank(eduClass.getClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("classId", eduClass.getClassId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduClassService.updateEduClass(example, eduClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduSemester
     * @return RetDataBean
     * @Title: insertEduSemester
     * @Description:  设置学期
     */
    @RequestMapping(value = "/insertEduSemester", method = RequestMethod.POST)
    public RetDataBean insertEduSemester(EduSemester eduSemester) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduSemester.setOrgId(account.getOrgId());
            eduSemester.setSemesterId(SysTools.getGUID());
            eduSemester.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduSemester.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduSemesterService.insertEduSemester(eduSemester));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduSemester
     * @return RetDataBean
     * @Title: deleteEduSemester
     * @Description:  删除学期
     */
    @RequestMapping(value = "/deleteEduSemester", method = RequestMethod.POST)
    public RetDataBean deleteEduSemester(EduSemester eduSemester) {
        try {
            if (StringUtils.isBlank(eduSemester.getSemesterId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduSemester.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduSemesterService.deleteEduSemester(eduSemester));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduSemester
     * @return RetDataBean
     * @Title: updateEduSemester
     * @Description:  更新学期信息
     */
    @RequestMapping(value = "/updateEduSemester", method = RequestMethod.POST)
    public RetDataBean updateEduSemester(EduSemester eduSemester) {
        try {
            if (StringUtils.isBlank(eduSemester.getSemesterId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduSemester.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("semesterId", eduSemester.getSemesterId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduSemesterService.updateEduSemester(example, eduSemester));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCampus
     * @return RetDataBean
     * @Title: insertEduCampus
     * @Description:  创建校区
     */
    @RequestMapping(value = "/insertEduCampus", method = RequestMethod.POST)
    public RetDataBean insertEduCampus(EduCampus eduCampus) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCampus.setOrgId(account.getOrgId());
            eduCampus.setCampusId(SysTools.getGUID());
            eduCampus.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduCampus.setCreateUser(account.getAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduCampusService.insertEduCampus(eduCampus));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduCampus
     * @return RetDataBean
     * @Title: deleteEduCampus
     * @Description:  删除校区
     */
    @RequestMapping(value = "/deleteEduCampus", method = RequestMethod.POST)
    public RetDataBean deleteEduCampus(EduCampus eduCampus) {
        try {
            if (StringUtils.isBlank(eduCampus.getCampusId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCampus.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduCampusService.deleteEduCampus(eduCampus));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCampus
     * @return RetDataBean
     * @Title: updateEduCampus
     * @Description:  更新校区信息
     */
    @RequestMapping(value = "/updateEduCampus", method = RequestMethod.POST)
    public RetDataBean updateEduCampus(EduCampus eduCampus) {
        try {
            if (StringUtils.isBlank(eduCampus.getCampusId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduCampus.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("campusId", eduCampus.getCampusId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduCampusService.updateEduCampus(example, eduCampus));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduClassCode
     * @return RetDataBean
     * @Title: insertEduClassCode
     * @Description:  添加HR分类代码
     */
    @RequestMapping(value = "/insertEduClassCode", method = RequestMethod.POST)
    public RetDataBean insertEduClassCode(EduClassCode eduClassCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(eduClassCode.getParentId())) {
                eduClassCode.setParentId("");
            }
            eduClassCode.setCodeFlag("1");
            eduClassCode.setCodeId(SysTools.getGUID());
            eduClassCode.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            eduClassCode.setCreateUser(account.getAccountId());
            eduClassCode.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, eduClassCodeService.insertEduClassCode(eduClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduClassCode
     * @return RetDataBean
     * @Title: updateEduClassCode
     * @Description:  更新HR分类代码
     */
    @RequestMapping(value = "/updateEduClassCode", method = RequestMethod.POST)
    public RetDataBean updateEduClassCode(EduClassCode eduClassCode) {
        try {
            if (StringUtils.isBlank(eduClassCode.getCodeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduClassCode.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("codeId", eduClassCode.getCodeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, eduClassCodeService.updateEduClassCode(example, eduClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduClassCode
     * @return RetDataBean
     * @Title: deleteEduClassCode
     * @Description:  删除分类码及以下的子集
     */
    @RequestMapping(value = "/deleteEduClassCode", method = RequestMethod.POST)
    public RetDataBean deleteEduClassCode(EduClassCode eduClassCode) {
        try {
            if (StringUtils.isBlank(eduClassCode.getCodeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            eduClassCode.setOrgId(account.getOrgId());
            eduClassCode = eduClassCodeService.selectOneEduClassCode(eduClassCode);
            EduClassCode eduClassCode1 = new EduClassCode();
            eduClassCode1.setParentId(eduClassCode.getCodeId());
            eduClassCodeService.deleteEduClassCode(eduClassCode1);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, eduClassCodeService.deleteEduClassCode(eduClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
