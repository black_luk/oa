package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyAppraisalRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyAppraisalRecordMapper extends MyMapper<PartyAppraisalRecord> {

    /**
     * @param orgId
     * @param appResult
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAppraisalList
     * @Description:  获取民主评议列表
     */
    public List<Map<String, String>> getAppraisalList(@Param(value = "orgId") String orgId, @Param(value = "appResult") String appResult,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyAppraisalList
     * @Description: 获取个人民主评议记录
     */
    public List<Map<String, String>> getMyAppraisalList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

}
