package com.core136.bean.party;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyAlbumConfig
 * @Description: 专辑参数设置
 * @author: 稠云技术
 * @date: 2020年11月10日 下午7:25:10
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_album_config")
public class PartyAlbumConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String configId;
    private String approvelUser;
    private String anonymousFlag;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getApprovelUser() {
        return approvelUser;
    }

    public void setApprovelUser(String approvelUser) {
        this.approvelUser = approvelUser;
    }

    public String getAnonymousFlag() {
        return anonymousFlag;
    }

    public void setAnonymousFlag(String anonymousFlag) {
        this.anonymousFlag = anonymousFlag;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
