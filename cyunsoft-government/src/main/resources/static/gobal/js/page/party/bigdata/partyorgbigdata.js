var myChart1 = echarts.init(document.getElementById('main1'));
$(function () {
    $.ajax({
        url: "/ret/partychartsget/getPartyOrgCountList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    $("#" + id).html(recordInfo[id]);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    $.ajax({
        url: "/ret/partychartsget/getPartyBigEventList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                var html = "";
                for (var i = 0; i < recordInfo.length; i++) {
                    html += "<li><div>【" + recordInfo[i].eventType + "】</div><div>" + recordInfo[i].title + "</div><div>" + recordInfo[i].happenTime + "</div></li> ";
                }
                $(".maquee").find("ul").html(html);

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

    $.ajax({
        url: "/ret/partychartsget/getPartyMemberByAgeCountList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                init_myChart1(recordInfo);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    $.ajax({
        url: "/ret/partychartsget/getPartyLesMeetForBar",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main7'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    init_myChart1();

    $.ajax({
        url: "/ret/partychartsget/getPartyLesMeetByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main6'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

})

function init_myChart1(dataList) {
    option = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        color: ['#8fc31f', '#f35833', '#00ccff', '#ffcc00', '#f5e965', '#a74faf', '#ff9668'],

        series: [
            {
                name: '党员年龄分布',
                type: 'pie',
                radius: '40%',
                center: ['50%', '50%'],
                data: dataList,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            //	                            position:'inside',
                            formatter: '{b} : {c} ({d}%)',
                            fontSize: 14,    //文字的字体大小
                        }
                    },
                    labelLine: {show: true}
                }
            }
        ]
    };
// 使用刚指定的配置项和数据显示图表。
    myChart1.setOption(option);
}

