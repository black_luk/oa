package com.core136.mapper.erp;

import com.core136.bean.erp.ErpMateriel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ErpMaterielMapper extends MyMapper<ErpMateriel> {

    /**
     * @param materielId
     * @param orgId
     * @return int
     * @Title isExistMaterielById
     * @Description  按物料Id判断物料是否存在
     */
    public int isExistMaterielById(@Param(value = "materielId") String materielId, @Param(value = "orgId") String orgId);

    /**
     * @param materielId
     * @param orgId
     * @return List<ErpMateriel>
     * @Title select2ById
     * @Description  按物料ID的模糊查询，用于SELECT2插件的选择
     */
    public List<ErpMateriel> selectMateriel2ById(@Param(value = "materielCode") String materielCode, @Param(value = "orgId") String orgId);

    /**
     * @param materielCode
     * @param orgId
     * @return ErpMateriel
     * @Title selectOneByCode
     * @Description  控物料编码查询物料
     */
    public ErpMateriel selectOneByCode(@Param(value = "materielCode") String materielCode, @Param(value = "orgId") String orgId);


}
