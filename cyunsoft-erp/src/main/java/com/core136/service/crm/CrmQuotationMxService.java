package com.core136.service.crm;

import com.core136.bean.crm.CrmQuotationMx;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmQuotationMxMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CrmQuotationMxService {
    private CrmQuotationMxMapper crmQuotationMxMapper;

    @Autowired
    public void setCrmQuotationMxMapper(CrmQuotationMxMapper crmQuotationMxMapper) {
        this.crmQuotationMxMapper = crmQuotationMxMapper;
    }

    public int insertCrmQuotationMx(CrmQuotationMx crmQuotationMx) {
        return crmQuotationMxMapper.insert(crmQuotationMx);
    }

    public int deleteCrmQuotationMx(CrmQuotationMx crmQuotationMx) {
        return crmQuotationMxMapper.delete(crmQuotationMx);
    }

    public int updateCrmQuotationMx(Example example, CrmQuotationMx crmQuotationMx) {
        return crmQuotationMxMapper.updateByExampleSelective(crmQuotationMx, example);
    }

    public CrmQuotationMx selectOneCrmQuotationMx(CrmQuotationMx crmQuotationMx) {
        return crmQuotationMxMapper.selectOne(crmQuotationMx);
    }

    /**
     * @param orgId
     * @param inquiryId
     * @param quotationId
     * @return List<Map < String, String>>
     * @Title: getCrmInquiryDetailListForQuotation
     * @Description:  获取报价单明细
     */
    public List<Map<String, String>> getCrmInquiryDetailListForQuotation(String orgId, String inquiryId, String quotationId) {
        return crmQuotationMxMapper.getCrmInquiryDetailListForQuotation(orgId, inquiryId, quotationId);
    }

    /**
     * @param pageParam
     * @param inquiryId
     * @param quotationId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCrmInquiryDetailListForQuotation
     * @Description:  获取报价单明细
     */
    public PageInfo<Map<String, String>> getCrmInquiryDetailListForQuotation(PageParam pageParam, String inquiryId, String quotationId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCrmInquiryDetailListForQuotation(pageParam.getOrgId(), inquiryId, quotationId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
