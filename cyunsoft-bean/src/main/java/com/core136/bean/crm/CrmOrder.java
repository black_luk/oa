package com.core136.bean.crm;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "crm_order")
public class CrmOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String orderId;
    private String orderTitle;
    private String linkManId;
    private String createTime;
    private String createUser;
    private String payDay;
    private String payType;
    private String paySmsType;
    private String attach;
    private String orgId;

    public String getOrderId() {
        return orderId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getLinkManId() {
        return linkManId;
    }

    public void setLinkManId(String linkManId) {
        this.linkManId = linkManId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getPayDay() {
        return payDay;
    }

    public void setPayDay(String payDay) {
        this.payDay = payDay;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPaySmsType() {
        return paySmsType;
    }

    public void setPaySmsType(String paySmsType) {
        this.paySmsType = paySmsType;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
