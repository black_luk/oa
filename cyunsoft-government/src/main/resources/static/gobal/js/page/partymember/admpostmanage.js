$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#postTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#lostTime", {
        format: "YYYY-MM-DD"
    });
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $.ajax({
        url: "/ret/partyparamget/getAdmPositionTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#admPostQueryTree"), admpostquerysetting, newTreeNodes);
            $.fn.zTree.init($("#admPostTree"), admpostsetting, newTreeNodes);
        }
    });
    $("#admPostQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#admPostQueryContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#admPost").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#admPostContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getAdmRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '50px',
            title: '党员姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'partyOrgName',
            title: '任职机构名称',
            width: '150px'
        },
            {
                field: 'otherPartyOrgName',
                width: '100px',
                title: '任职机构名称补充'
            }, {
                field: 'postType',
                width: '100px',
                title: '任职状态',
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "不在任";
                    } else if (value == "2") {
                        return "在任";
                    } else if (value == "3") {
                        return "挂任";
                    } else if (value == "3") {
                        return "试用";
                    } else {
                        return "未知";
                    }
                }
            }, {
                field: 'admPost',
                width: '100px',
                title: '行政职务',
                formatter: function (value, row, index) {
                    return getAdmPostName(value);
                }
            }, {
                field: 'postTime',
                width: '120px',
                title: '任职日期',
                formatter: function (value, row, index) {
                    return value + " 至 " + row.lostTime;
                }
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        postType: $("#postTypeQuery").val(),
        admPost: $("#admPostQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(recordId) {
    $("#admlistdiv").hide();
    $("#admdiv").show();
    $("#attach").attr("data_value", "");
    $("#admPost").attr("data_value", "");
    $("#memberId").attr("data_value", "");
    $("#show_attach").empty();
    document.getElementById("form1").reset();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyAdmRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "admPost") {
                        $("#admPost").attr("data-value", recordInfo[id]);
                        $("#admPost").val(getAdmPostName(recordInfo[id]));
                    } else if (id == "postLevel") {
                        $("#postLevel").attr("data-value", recordInfo[id]);
                        $("#postLevel").val(getPostLevelName(recordInfo[id]));
                    } else if (id == "partyOrgId") {
                        $("#partyOrgId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyAdmPost(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyAdmRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function goback() {
    $("#admdiv").hide();
    $("#admlistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/admpostdetails?recordId=" + recordId);
}


function updatePartyAdmPost(recordId) {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyAdmRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            otherPartyOrgName: $("#otherPartyOrgName").val(),
            admPost: $("#admPost").attr("data-value"),
            otherAdmPost: $("#otherAdmPost").val(),
            manageWork: $("#manageWork").val(),
            postTime: $("#postTime").val(),
            postType: $("#postType").val(),
            lostTime: $("#lostTime").val(),
            postLevel: $("#postLevel").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var admpostquerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getGovPostTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("admPostQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#admPostQuery");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var admpostsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getAdmPositionTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("admPostTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#admPost");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};


var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree");
            var nodes = zTree.getSelectedNodes();
            var v = "";
            var vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

function getAdmPostName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyAdmPositionById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}

function getPostLevelName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyPostLevelById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}
