package com.core136.controller.discuss;

import com.core136.bean.account.Account;
import com.core136.bean.discuss.Discuss;
import com.core136.bean.discuss.DiscussNotice;
import com.core136.bean.discuss.DiscussRecord;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.discuss.DiscussNoticeService;
import com.core136.service.discuss.DiscussRecordService;
import com.core136.service.discuss.DiscussService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/oaset")
public class RouteSetDiscussController {
    private DiscussRecordService discussRecordService;

    @Autowired
    public void setDiscussRecordService(DiscussRecordService discussRecordService) {
        this.discussRecordService = discussRecordService;
    }

    private DiscussService discussService;

    @Autowired
    public void setDiscussService(DiscussService discussService) {
        this.discussService = discussService;
    }

    private DiscussNoticeService discussNoticeService;

    @Autowired
    public void setDiscussNoticeService(DiscussNoticeService discussNoticeService) {
        this.discussNoticeService = discussNoticeService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param discussNotice
     * @return RetDataBean
     * @Title: insertDiscussNotice
     * @Description:  发布通知公告
     */
    @RequestMapping(value = "/insertDiscussNotice", method = RequestMethod.POST)
    public RetDataBean insertDiscussNotice(DiscussNotice discussNotice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussNotice.setRecordId(SysTools.getGUID());
            discussNotice.setStatus("0");
            discussNotice.setReader("");
            discussNotice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            discussNotice.setCreateUser(account.getAccountId());
            discussNotice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussNoticeService.insertDiscussNotice(discussNotice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除通知公告
     *
     * @param discussNotice
     * @return
     */
    @RequestMapping(value = "/deleteDiscussNotice", method = RequestMethod.POST)
    public RetDataBean deleteDiscussNotice(DiscussNotice discussNotice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussNotice.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(discussNotice.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussNoticeService.deleteDiscussNotice(discussNotice));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新通知公告
     *
     * @param discussNotice
     * @return
     */
    @RequestMapping(value = "/updateDiscussNotice", method = RequestMethod.POST)
    public RetDataBean updateDiscussNotice(DiscussNotice discussNotice) {
        try {
            if (StringUtils.isBlank(discussNotice.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(DiscussNotice.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", discussNotice.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussNoticeService.updateDiscussNotice(example, discussNotice));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 回帖
     *
     * @param discussRecord
     * @return
     */
    @RequestMapping(value = "/revDiscussRecord", method = RequestMethod.POST)
    public RetDataBean revDiscussRecord(DiscussRecord discussRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            DiscussRecord tempDiscussRecord = new DiscussRecord();
            tempDiscussRecord.setRecordId(discussRecord.getLevelId());
            tempDiscussRecord.setOrgId(discussRecord.getOrgId());
            tempDiscussRecord = discussRecordService.selectOneDiscussRecord(tempDiscussRecord);
            discussRecord.setTitle(tempDiscussRecord.getTitle());
            discussRecord.setDiscussId(tempDiscussRecord.getDiscussId());
            discussRecord.setAccountId(account.getAccountId());
            discussRecord.setStatus(tempDiscussRecord.getStatus());
            Document htmlDoc = Jsoup.parse(discussRecord.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 100) {
                subheading = subheading.substring(0, 100) + "...";
            }
            discussRecord.setSubheading(subheading);
            discussRecord.setRecordId(SysTools.getGUID());
            discussRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            discussRecord.setCreateUser(account.getAccountId());
            discussRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussRecordService.insertDiscussRecord(discussRecord));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discussRecord
     * @return RetDataBean
     * @Title: insertDiscussRecord
     * @Description:  发贴
     */
    @RequestMapping(value = "/insertDiscussRecord", method = RequestMethod.POST)
    public RetDataBean insertDiscussRecord(DiscussRecord discussRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussRecord.setAccountId(account.getAccountId());
            if (StringUtils.isNotBlank(discussRecord.getLevelId())) {
                discussRecord.setStatus("1");
                discussRecord.setLevelId("0");
            } else {
                discussRecord.setLevelId("0");
                Discuss discuss = new Discuss();
                discuss.setOrgId(account.getOrgId());
                discuss.setRecordId(discussRecord.getDiscussId());
                discuss = discussService.selectOneDiscuss(discuss);
                if(StringUtils.isNotBlank(discuss.getNeedApproval())) {
                    if (discuss.getNeedApproval().equals("1")) {
                        discussRecord.setStatus("0");
                    } else {
                        discussRecord.setStatus("1");
                    }
                }else
                {
                    discussRecord.setStatus("1");
                }
            }
            Document htmlDoc = Jsoup.parse(discussRecord.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 100) {
                subheading = subheading.substring(0, 100) + "...";
            }
            discussRecord.setSubheading(subheading);
            discussRecord.setRecordId(SysTools.getGUID());
            discussRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            discussRecord.setCreateUser(account.getAccountId());
            discussRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussRecordService.insertDiscussRecord(discussRecord));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删帖
     *
     * @param discussRecord
     * @return
     */
    @RequestMapping(value = "/deleteDiscussRecord", method = RequestMethod.POST)
    public RetDataBean deleteDiscussRecord(DiscussRecord discussRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussRecord.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(discussRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussRecordService.deleteDiscussRecord(discussRecord));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/setDiscussRecordStatus", method = RequestMethod.POST)
    public RetDataBean setDiscussRecordStatus(DiscussRecord discussRecord) {
        try {
            if (StringUtils.isBlank(discussRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(DiscussRecord.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", discussRecord.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussRecordService.updateDiscussRecord(example, discussRecord));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 更新帖子
     *
     * @param discussRecord
     * @return
     */
    @RequestMapping(value = "/updateDiscussRecord", method = RequestMethod.POST)
    public RetDataBean updateDiscussRecord(DiscussRecord discussRecord) {
        try {
            if (StringUtils.isBlank(discussRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Document htmlDoc = Jsoup.parse(discussRecord.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 100) {
                    subheading = subheading.substring(0, 100) + "...";
                }
                discussRecord.setSubheading(subheading);
                Example example = new Example(DiscussRecord.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", discussRecord.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussRecordService.updateDiscussRecord(example, discussRecord));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param discuss
     * @return RetDataBean
     * @Title: insertDiscuss
     * @Description:  创建讨论区
     */
    @RequestMapping(value = "/insertDiscuss", method = RequestMethod.POST)
    public RetDataBean insertDiscuss(Discuss discuss) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discuss.setRecordId(SysTools.getGUID());
            discuss.setStatus("1");
            discuss.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            discuss.setCreateUser(account.getAccountId());
            discuss.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussService.insertDiscuss(discuss));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除讨论区
     *
     * @param discuss
     * @return
     */
    @RequestMapping(value = "/deleteDiscuss", method = RequestMethod.POST)
    public RetDataBean deleteDiscuss(Discuss discuss) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discuss.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(discuss.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussService.deleteDiscuss(discuss));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新讨论区
     *
     * @param discuss
     * @return
     */
    @RequestMapping(value = "/updateDiscuss", method = RequestMethod.POST)
    public RetDataBean updateDiscuss(Discuss discuss) {
        try {
            if (StringUtils.isBlank(discuss.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(Discuss.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", discuss.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussService.updateDiscuss(example, discuss));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
