$(function () {
    $.ajax({
        url: "/ret/hrget/getMyHrKpiScoreList",
        type: "post",
        dataType: "json",
        data: {planId: planId, accountId: accountId},
        success: function (data) {
            if (data.status == "200") {
                var itemList = data.list;
                for (var i = 0; i < itemList.length; i++) {
                    var title = itemList[i].title;
                    var optType = itemList[i].optType;
                    var itemId = itemList[i].itemId;
                    var childItem = JSON.parse(itemList[i].childItem);
                    var score = itemList[i].score;
                    var comment = itemList[i].comment;
                    getItemList(i, itemId, title, childItem, score)
                }

            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
})

function getItemList(index, itemId, title, arr, score) {
    var html = "<tr  class='itemrow' data-id=\"" + itemId + "\" optType='1'><td>" + (index + 1) + ":" + title + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;考核得分:" + score + "</td></tr>"
    $("#child-item-table").append(html);
}
