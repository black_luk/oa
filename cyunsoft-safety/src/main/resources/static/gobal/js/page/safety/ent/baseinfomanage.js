$(function () {
    $('#businessScope').summernote({height: 200});
    query();
    jeDate("#incorporation", {
        format: "YYYY-MM-DD"
    });
    jeDate("#businessTerm", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/safetyget/getEntInfoList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'entId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'entName',
                title: '企业名称',
                //sortable : true,
                width: '200px',
                formatter: function (value, row, index) {
                    return "<a style=\"cursor: pointer;\" href=\"javascript:void(0);details('" + row.entId + "');\">" + value + "</a>";
                }
            },
            {
                field: 'type',
                title: '企业类型',
                width: '50px',
                //visible:false,
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "私有";
                    } else if (value == "1") {
                        return "国有";
                    } else if (value == "2") {
                        return "合资";
                    } else if (value == "3") {
                        return "外资";
                    } else {
                        return "未知";
                    }

                }
            }, {
                field: 'legalPerson',
                title: '企业法人',
                width: '100px'
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'userName',
                width: '50px',
                title: '创建人'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.entId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        type: $("#typeStatus").val()
    };
    return temp;
}

function createOptBtn(entId) {
    let html = "<a href=\"javascript:void(0);edit('" + entId + "')\" class=\"btn btn-primary btn-xs\" >编辑</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);deleteEnt('" + entId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(entId) {
    $("#listdiv").hide();
    $("#entInfodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#businessScope").code("")
    $("#attach").attr("data_value", "");
    $("#show_attach").empty();
    $.ajax({
        url: "/ret/safetyget/getSafetyEntInfoById",
        type: "post",
        dataType: "json",
        data: {
            entId: entId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "businessScope") {
                        $("#businessScope").code(info[id]);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateEnt(entId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

}

function goback() {
    $("#entInfodiv").hide();
    $("#listdiv").show();
}


function updateEnt(entId) {
    if($("#entName").val()=="")
    {
        layer.msg("企业各称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/updateSafetyEntInfo",
        type: "post",
        dataType: "json",
        data: {
            entId: entId,
            sortNo: $("#sortNo").val(),
            type: $("#type").val(),
            entName: $("#entName").val(),
            oldEntName: $("#oldEntName").val(),
            capital: $("#capital").val(),
            tel: $("#tel").val(),
            incorporation: $("#incorporation").val(),
            businessTerm: $("#businessTerm").val(),
            credit: $("#credit").val(),
            legalPerson: $("#legalPerson").val(),
            address: $("#address").val(),
            businessScope: $("#businessScope").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                goback();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteEnt(entId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/safetyset/deleteSafetyEntInfo",
            type: "post",
            dataType: "json",
            data: {entId: entId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}

function details(entId) {
    window.open("/app/core/safety/entinfodetails?entId=" + entId);
}
