$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#meetYear", {
        format: "YYYY",
        isinitVal: true
    });
    $('#remark').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        insertLifeMeet();
    })

    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });

})

function insertLifeMeet() {
    if ($("#title").val()== "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyLifeMeet",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            meetType: $("#meetType").val(),
            title: $("#title").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            sponsor: $("#sponsor").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinUserCount: $("#joinUserCount").val(),
            participantsCount: $("#participantsCount").val(),
            scope: $("#scope").val(),
            resolution: $("#resolution").val(),
            address: $("#address").val(),
            meetYear: $("#meetYear").val(),
            joinMember: $("#joinMember").attr("data-value"),
            noStaff: $("#noStaff").attr("data-value"),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
