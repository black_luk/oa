package com.core136.bean.erp;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ErpBomDetail
 * @Description: 物料表单
 * @author: 稠云信息
 * @date: 2018年12月14日 上午10:01:01
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "erp_bom_detail")
public class ErpBomDetail implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String bomDetailId;
    private String bomId;
    private String materielCode;
    private String childBomId;
    private Double useCount;
    private String createUser;
    private String param;
    private String createTime;
    private String remark;
    private String orgId;

    public String getChildBomId() {
        return childBomId;
    }

    public void setChildBomId(String childBomId) {
        this.childBomId = childBomId;
    }

    public String getRemark() {
        return remark;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBomDetailId() {
        return bomDetailId;
    }

    public void setBomDetailId(String bomDetailId) {
        this.bomDetailId = bomDetailId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getBomId() {
        return bomId;
    }

    public void setBomId(String bomId) {
        this.bomId = bomId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public Double getUseCount() {
        return useCount;
    }

    public void setUseCount(Double useCount) {
        this.useCount = useCount;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
