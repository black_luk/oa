$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#joinTime", {
        format: "YYYY-MM-DD"
    });
    query();
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdQueryTree"), partyOrgIdQuerysetting, newTreeNodes);
        }
    });
    $("#partyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).offset().left
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberJoinApplyList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '申请标题',
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        },
            {
                field: 'cardId',
                width: '100px',
                title: '身份证号'
            }, {
                field: 'userName',
                width: '100px',
                title: '申请人姓名'
            }, {
                field: 'tel',
                width: '100px',
                title: '联系电话'
            }, {
                field: 'joinTime2',
                width: '100px',
                title: '确定预备党员日期'
            }, {
                field: 'partyOrgName',
                width: '100px',
                title: '接受党支部名称'
            }, {
                field: 'trainUser1',
                width: '100px',
                title: '培养人',
                formatter: function (value, row, index) {
                    var temp = [];
                    if (value) {
                        temp.push(value);
                    }
                    if (row.linkMan2) {
                        temp.push(row.trainUser2);
                    }
                    return getPartyMemberName(temp.join(","));
                }
            }, {
                field: 'linkMan1',
                width: '100px',
                title: '介绍人',
                formatter: function (value, row, index) {
                    var temp = [];
                    if (value) {
                        temp.push(value);
                    }
                    if (row.linkMan2) {
                        temp.push(row.linkMan2);
                    }
                    return getPartyMemberName(temp.join(","));
                }
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '180px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId, row.statusFlag);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        statusFlag: "3",
        partyOrgId: $("#partyOrgIdQuery").attr("data-value"),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);approvel('" + recordId + "')\" class=\"btn btn-primary btn-xs\" >党员转正</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/partymember/partyjoinapplydetails?recordId=" + recordId);
}

function approvel(recordId) {
    $("#approvelModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/partymemberset/setPartyMemberFormal",
            type: "post",
            dataType: "json",
            data: {
                joinRecordId: recordId,
                joinTime: $("#joinTime").val(),
                status: getCheckBoxValue("status"),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                    $("#approvelModal").modal("hide");
                }
            }
        })
    })
}

var partyOrgIdQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
