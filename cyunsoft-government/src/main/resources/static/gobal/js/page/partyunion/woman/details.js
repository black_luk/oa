$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionWomanById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "accountId") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "deptId") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "levelId") {
                        $("#" + id).html(getUserLevelStr(info[id]));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
