package com.core136.service.partyrelation;

import com.core136.bean.partyrelation.PartyIntroduce;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyIntroduceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyIntroduceService {
    private PartyIntroduceMapper partyIntroduceMapper;

    @Autowired
    public void setPartyIntroduceMapper(PartyIntroduceMapper partyIntroduceMapper) {
        this.partyIntroduceMapper = partyIntroduceMapper;
    }

    public int insertPartyIntroduce(PartyIntroduce partyIntroduce) {
        return partyIntroduceMapper.insert(partyIntroduce);
    }

    public int deletePartyIntroduce(PartyIntroduce partyIntroduce) {
        return partyIntroduceMapper.delete(partyIntroduce);
    }

    public int updatePartyIntroduce(Example example, PartyIntroduce partyIntroduce) {
        return partyIntroduceMapper.updateByExampleSelective(partyIntroduce, example);
    }

    public PartyIntroduce selectOnePartyIntroduce(PartyIntroduce partyIntroduce) {
        return partyIntroduceMapper.selectOne(partyIntroduce);
    }


    public List<Map<String, String>> getIntroduceList(String orgId, String beginTime, String endTime, String memberId, String search) {
        return partyIntroduceMapper.getIntroduceList(orgId, beginTime, endTime, memberId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getIntroduceList(PageParam pageParam, String beginTime, String endTime, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getIntroduceList(pageParam.getOrgId(), beginTime, endTime, memberId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
