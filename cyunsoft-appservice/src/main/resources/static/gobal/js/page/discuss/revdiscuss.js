$(function () {
    getDisccusRecord();
    $('#content').summernote({height: 200});
})

function getDisccusRecord() {
    $.ajax({
        url: "/ret/oaget/getDiscussRecordListById",
        type: "post",
        dataType: "json",
        data: {
            recordId: parentRecordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (let i = 0; i < data.list.length; i++) {
                    let info = data.list[i];
                    if (info.levelId == "0") {
                        $("#discusslist").append("<tr><td><h4><b>标题：" + info.title + "</b><a style='float: right' class=\"btn btn-primary btn-sm\" href=\"javascript:void(0);\" onclick='history.go(-1);'><i class=\"glyphicon glyphicon-arrow-left\"></i>返回</a></h4></td></td></tr>");
                    }
                    let html = "<tr><td><div class='well well-lg'><h4>" + info.title + "<span style='font-size: 14px;float: right;'>发帖人：" + info.userName + "&nbsp;&nbsp;发帖时间：" + info.createTime + "</span></h4>" +
                        "<div>" + info.content + "</div>" +
                        "<div style='text-align: right;'><span><a class=\"btn btn-link\" onclick='revRecord(\"" + info.recordId + "\")'>回复</a><a class=\"btn btn-link\" onclick='quote(\"" + info.recordId + "\")'>引用</a>";
                    if (info.isMe) {
                        html += "<a class=\"btn btn-link\" onclick='edit(\"" + info.recordId + "\")'>编辑</a><a class=\"btn btn-link\" onclick='del(\"" + info.recordId + "\")'>删除</a>";
                    }
                    html += "</span></div></div>" +
                        "</td></td></tr>";
                    $("#discusslist").append(html);
                    console.log(333);
                }
            }
        }
    })
}

function revRecord(recordId) {
    $('#content').code("");
    $("#revModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        revDiscussRecord(parentRecordId);
    })
}

function quote(recordId) {
    $('#content').code("");
    $.ajax({
        url: "/ret/oaget/getDiscussRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $('#content').code("<div style='background-color: #ccc'><p>引用 <span>\"" + data.list.title + "\"</span><span>作者：" + getUserNameByStr(data.list.accountId) + "</span></p><p>" + data.list.content + "</p></div><p>回复</p>");
                $("#revModal").modal("show");
                $(".js-save").unbind("click").click(function () {
                    revDiscussRecord(parentRecordId);
                })
            }
        }
    });
}

function edit(recordId) {
    $('#content').code("");
    $.ajax({
        url: "/ret/oaget/getDiscussRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $('#content').code(data.list.content);
                $("#revModal").modal("show");
                $(".js-save").unbind("click").click(function () {
                    updateDiscussRecord(recordId);
                })
            }
        }
    });

}

function del(recordId) {
    $.ajax({
        url: "/set/oaset/deleteDiscussRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            }
        }
    });
}

function updateDiscussRecord(recordId) {
    $.ajax({
        url: "/set/oaset/updateDiscussRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            content: $("#content").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            }
        }
    });
}

function revDiscussRecord(recordId) {
    $.ajax({
        url: "/set/oaset/revDiscussRecord",
        type: "post",
        dataType: "json",
        data: {
            levelId: recordId,
            content: $("#content").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            }
        }
    });
}
