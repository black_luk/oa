var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            var nameem = $("#partyOrgName");
            nameem.val(v);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(vid);
        }
    }
};

var nativePlacesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getNativePlaceTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("nativePlaceTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#nativePlace");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var educationsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getGovEducationTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("educationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#education");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var degreesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getDegreeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("degreeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#degree");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var userpostsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPostTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("userPostTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#userPost");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var admpositionsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getAdmPositionTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("admPositionTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#admPosition");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
var classSituationsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getStratumTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("classSituationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#classSituation");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var positionalTitlessetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getTitleTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("positionalTitlesTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#positionalTitles");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
$(function () {
    getCodeClass("workingConditions", "gov_working_conditions");
    getCodeClass("nation", "nation");
    jeDate("#joinTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#workTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#regularTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#lostTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });

    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    $("#createbut").unbind("click").click(function () {
        addPartyMember();
    });
    $("#cbut").unbind("click").click(function () {
        document.getElementById("form").reset();
        $("#education").attr("data-value", "");
        $("#degree").attr("data-value", "");
        $("#partyOrgId").attr("data-value", "");
        $("#positionalTitles").attr("data-value", "");
        $("#classSituation").attr("data-value", "");
        $("#userPost").attr("data-value", "");
        $("#nativePlace").attr("data-value", "");
        $("#admPosition").attr("data-value", "");
        $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
        $("#datalist").hide();
        $("#updatabut").hide();
        $("#creatediv").show();
        $("#createbut").show();
        $("#accountId").removeAttr("readonly");
    });
    $("#cquery").unbind("click").click(function () {
        $("#creatediv").hide();
        $("#datalist").show();
        query("");
    });
    $("#partyOrgName").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#importbtn").unbind("click").click(function () {
        $("#exdiv").modal("show");
    });
    $.ajax({
        url: "/ret/partyparamget/getNativePlaceTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#nativePlaceTree"), nativePlacesetting, newTreeNodes);
        }
    });

    $("#nativePlace").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#nativePlaceContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getGovEducationTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#educationTree"), educationsetting, newTreeNodes);
        }
    });

    $("#education").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#educationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });


    $.ajax({
        url: "/ret/partyparamget/getDegreeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#degreeTree"), degreesetting, newTreeNodes);
        }
    });

    $("#degree").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#degreeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("#userPost").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#userPostContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getAdmPositionTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#admPositionTree"), admpositionsetting, newTreeNodes);
        }
    });

    $("#admPosition").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#admPositionContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getPostTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#userPostTree"), userpostsetting, newTreeNodes);
        }
    });

    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });


    $.ajax({
        url: "/ret/partyparamget/getStratumTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#classSituationTree"), classSituationsetting, newTreeNodes);
        }
    });
    $("#classSituation").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#classSituationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $.ajax({
        url: "/ret/partyparamget/getTitleTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#positionalTitlesTree"), positionalTitlessetting, newTreeNodes);
        }
    });
    $("#positionalTitles").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#positionalTitlesContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $("#butdel").unbind("click").click(function () {
        var selected = $('#myTable').bootstrapTable('getSelections');
        var ids = new Array();
        for (var i = 0; i < selected.length; i++) {
            ids.push(selected[i].recordId);
        }
        if (ids.length == 0) {
            layer.msg("请先选择需要删除的党员信息！")
        } else {
            if (confirm("确定删除当前党员信息吗？")) {
                $.ajax({
                    url: "/set/partymemberset/deletePartyMenbers",
                    type: "post",
                    dataType: "json",
                    data: {
                        recordIds: ids.join(',')
                    },
                    success: function (data) {
                        if (data.status == 200) {
                            layer.msg(sysmsg[data.msg]);
                            location.reload();
                        } else if (data.status == 100) {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            console.log(data.msg);
                        }
                    }
                });
            }
        }
    })
});

function zTreeOnClick(event, treeId, treeNode) {
    $("#creatediv").hide();
    $("#datalist").show();
    $("#myTable").bootstrapTable('destroy');
    if (treeNode.partyOrgId == "0") {
        query("");
    } else {
        query(treeNode.partyOrgId);
    }
}

function query(partyOrgId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getPartyMemberListByPartyOrgId?partyOrgId=' + partyOrgId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'memberId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.memberId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '所属党组织'
        }, {
            field: 'userType',
            title: '人员类别',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                }
            }
        }, {
            field: 'partyStatus',
            title: '党籍状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                } else if (value == "3") {
                    return "已出党";
                } else if (value == "4") {
                    return "已停止党籍";
                } else if (value == "5") {
                    return "已死亡";
                }
            }
        }, {
            field: 'joinTime',
            title: '入党时间',
            width: '100px'
        }, {
            field: 'mobileNo',
            title: '联系电话',
            width: '100px',
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '180px',
            formatter: function (value, row, index) {
                return createOptBtn(row.memberId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(memberId) {
    var html = "<a href=\"javascript:void(0);edit('" + memberId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deletePartyMember('" + memberId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function deletePartyMember(memberId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partymemberset/deletePartyMember",
            type: "post",
            dataType: "json",
            data: {memberId: memberId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function details(memberId) {
    window.open("/app/core/partymember/partymemberdetails?memberId=" + memberId);
}

function edit(memberId) {
    $("#creatediv").show();
    $("#datalist").hide();
    $("#createbut").hide();
    $("#updatabut").show();
    document.getElementById("form").reset();
    $("#education").attr("data-value", "");
    $("#degree").attr("data-value", "");
    $("#partyOrgId").attr("data-value", "");
    $("#positionalTitles").attr("data-value", "");
    $("#classSituation").attr("data-value", "");
    $("#userPost").attr("data-value", "");
    $("#nativePlace").attr("data-value", "");
    $("#admPosition").attr("data-value", "");
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");

    $.ajax({
        url: "/ret/partymemberget/getPartyMemberById",
        type: "post",
        dataType: "json",
        data: {
            memberId: memberId
        },
        success: function (data) {
            if (data.status == 200) {
                var info = data.list;
                for (var id in info) {
                    if (id == "partyOrgId") {
                        $("#partyOrgId").attr("data-value", info[id]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                partyOrgId: info[id]
                            },
                            success: function (res) {
                                if (res.status == 200) {
                                    if (res.list) {
                                        $("#partyOrgId").val(res.list.partyOrgName);
                                    }
                                } else if (res.status == "100") {
                                    console.log(res.msg);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "userSex") {
                        $("input[name='userSex'][value='" + info[id] + "']").attr("checked", true);
                    } else if (id == "isLost") {
                        $("input[name='isLost'][value='" + info[id] + "']").attr("checked", true);
                    } else if (id == "isFlow") {
                        $("input[name='isFlow'][value='" + info[id] + "']").attr("checked", true);
                    } else if (id == "isMg") {
                        $("input[name='isMg'][value='" + info[id] + "']").attr("checked", true);
                    } else if (id == "nativePlace") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyNativePlaceById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#nativePlace").attr("data-value", res.list.sortId);
                                        $("#nativePlace").val(res.list.sortName);
                                    } else {
                                        $("#nativePlace").attr("data-value", "");
                                        $("#nativePlace").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "education") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyEducationById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#education").attr("data-value", res.list.sortId);
                                        $("#education").val(res.list.sortName);
                                    } else {
                                        $("#education").attr("data-value", "");
                                        $("#education").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "degree") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyDegreeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#degree").attr("data-value", res.list.sortId);
                                        $("#degree").val(res.list.sortName);
                                    } else {
                                        $("#degree").attr("data-value", "");
                                        $("#degree").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userPost") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyPostById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#userPost").attr("data-value", res.list.sortId);
                                        $("#userPost").val(res.list.sortName);
                                    } else {
                                        $("#userPost").attr("data-value", "");
                                        $("#userPost").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "admPosition") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAdmPositionById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#admPosition").attr("data-value", res.list.sortId);
                                        $("#admPosition").val(res.list.sortName);
                                    } else {
                                        $("#admPosition").attr("data-value", "");
                                        $("#admPosition").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "positionalTitles") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTitleById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#positionalTitles").attr("data-value", res.list.sortId);
                                        $("#positionalTitles").val(res.list.sortName);
                                    } else {
                                        $("#positionalTitles").attr("data-value", "");
                                        $("#positionalTitles").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "classSituation") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyStratumById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#classSituation").attr("data-value", res.list.sortId);
                                        $("#classSituation").val(res.list.sortName);
                                    } else {
                                        $("#classSituation").attr("data-value", "");
                                        $("#classSituation").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "photo") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + info[id]);
                        $("#file").attr("data-value", info[id])
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $("#updatabut").unbind("click").click(function () {
                    updatePartyMember(memberId);
                });
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function deletePartyMember(memberId) {
    if (confirm("确定删除当前党员信息吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyMember",
            type: "post",
            dataType: "json",
            data: {
                memberId: memberId
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function updatePartyMember(memberId) {
    $.ajax({
        url: "/set/partymemberset/updatePartyMember",
        type: "post",
        dataType: "json",
        data: {
            memberId: memberId,
            sortNo: $("#sortNo").val(),
            userName: $("#userName").val(),
            photo: $("#file").attr("data-value"),
            userSex: $("input[name='userSex']:checked").val(),
            birthday: $("#birthday").val(),
            nation: $("#nation").val(),
            nativePlace: $("#nativePlace").attr("data-value"),
            userType: $("#userType").val(),
            cardId: $("#cardId").val(),
            education: $("#education").attr("data-value"),
            degree: $("#degree").attr("data-value"),
            workTime: $("#workTime").val(),
            userPost: $("#userPost").attr("data-value"),
            joinTime: $("#joinTime").val(),
            regularTime: $("#regularTime").val(),
            partyStatus: $("#partyStatus").val(),
            admPosition: $("#admPosition").attr("data-value"),
            positionalTitles: $("#positionalTitles").attr("data-value"),
            classSituation: $("#classSituation").attr("data-value"),
            workingConditions: $("#workingConditions").val(),
            mobileNo: $("#mobileNo").val(),
            tel: $("#tel").val(),
            email: $("#email").val(),
            address: $("#address").val(),
            isMg: getCheckBoxValue("isMg"),
            isLost: $("input:radio[name='isLost']:checked").val(),
            lostTime: $("#lostTime").val(),
            isFlow: $("input:radio[name='isFlow']:checked").val(),
            flowDirection: $("#flowDirection").val(),
            flowRemark: $("#flowRemark").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            duesBase: $("#duesBase").val(),
            dues: $("#dues").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}

function addPartyMember() {
    $.ajax({
        url: "/set/partymemberset/insertPartyMember",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            userName: $("#userName").val(),
            photo: $("#file").attr("data-value"),
            userSex: $("input[name='userSex']:checked").val(),
            birthday: $("#birthday").val(),
            nation: $("#nation").val(),
            nativePlace: $("#nativePlace").attr("data-value"),
            userType: $("#userType").val(),
            cardId: $("#cardId").val(),
            education: $("#education").attr("data-value"),
            degree: $("#degree").attr("data-value"),
            workTime: $("#workTime").val(),
            userPost: $("#userPost").attr("data-value"),
            joinTime: $("#joinTime").val(),
            regularTime: $("#regularTime").val(),
            partyStatus: $("#partyStatus").val(),
            admPosition: $("#admPosition").attr("data-value"),
            positionalTitles: $("#positionalTitles").attr("data-value"),
            classSituation: $("#classSituation").attr("data-value"),
            workingConditions: $("#workingConditions").val(),
            mobileNo: $("#mobileNo").val(),
            tel: $("#tel").val(),
            email: $("#email").val(),
            address: $("#address").val(),
            isMg: getCheckBoxValue("isMg"),
            isLost: $("input:radio[name='isLost']:checked").val(),
            lostTime: $("#lostTime").val(),
            isFlow: $("input:radio[name='isFlow']:checked").val(),
            flowDirection: $("#flowDirection").val(),
            flowRemark: $("#flowRemark").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            duesBase: $("#duesBase").val(),
            dues: $("#dues").val(),
            sourceType: "0"
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function importPartyMemberForExcel()
{
    $.ajaxFileUpload({
        url: '/set/partymemberset/importPartyMemberByExcel', //上传文件的服务端
        secureuri: false, //是否启用安全提交
        async: false,
        dataType: 'json', //数据类型
        fileElementId: 'file', //表示文件域ID
        success: function (data, status) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                $("#exdiv").modal("hide");
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            layer.msg("文件上传出错!请检查文件格式!");
            console.log(data.msg);
        }
    });
}
