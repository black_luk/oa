$(function () {
    jeDate("#armyTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#partyTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    $('#rewards').summernote({height: 300});
    $('#remark').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        addArmy();
    })
})

function addArmy() {
    var userName = "";
    if ($("#accountId").val() != "") {
        userName = $("#accountId").val();
    } else {
        userName = $("#userName").val();
    }
    if(userName=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyArmy",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            userName: userName,
            userSex: $("#userSex").val(),
            cardId: $("#cardId").val(),
            birthday: $("#birthday").val(),
            userType: $("#userType").val(),
            partyMember: $("#partyMember").val(),
            partyTime: $("#partyTime").val(),
            partyOrg: $("#partyOrg").val(),
            party: $("#party").val(),
            userLevel: $("#userLevel").val(),
            tel: $("#tel").val(),
            address: $("#address").val(),
            armyTime: $("#armyTime").val(),
            armyRank: $("#armyRank").val(),
            education: $("#education").val(),
            userPost: $("#userPost").val(),
            attach: $("#attach").attr("data_value"),
            rewards: $("#rewards").code(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
