package com.core136.bean.project;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 项目权限
 */
@Table(name = "pro_priv")
public class ProPriv implements Serializable {
    private String privId;
    private String createUserPriv;
    private String createDeptPriv;
    private String createLevelPriv;
    private String approvalUserPriv;
    private String notNeedApprovalPriv;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getPrivId() {
        return privId;
    }

    public void setPrivId(String privId) {
        this.privId = privId;
    }

    public String getCreateUserPriv() {
        return createUserPriv;
    }

    public void setCreateUserPriv(String createUserPriv) {
        this.createUserPriv = createUserPriv;
    }

    public String getCreateDeptPriv() {
        return createDeptPriv;
    }

    public void setCreateDeptPriv(String createDeptPriv) {
        this.createDeptPriv = createDeptPriv;
    }

    public String getCreateLevelPriv() {
        return createLevelPriv;
    }

    public void setCreateLevelPriv(String createLevelPriv) {
        this.createLevelPriv = createLevelPriv;
    }

    public String getApprovalUserPriv() {
        return approvalUserPriv;
    }

    public void setApprovalUserPriv(String approvalUserPriv) {
        this.approvalUserPriv = approvalUserPriv;
    }

    public String getNotNeedApprovalPriv() {
        return notNeedApprovalPriv;
    }

    public void setNotNeedApprovalPriv(String notNeedApprovalPriv) {
        this.notNeedApprovalPriv = notNeedApprovalPriv;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
