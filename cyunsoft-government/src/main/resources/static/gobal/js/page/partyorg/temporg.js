$(function () {
    jeDate("#approvalTime", {
        format: "YYYY-MM-DD",
        isinitVal: true
    });
    jeDate("#dissolutionTime", {
        format: "YYYY-MM-DD",
        isinitVal: true
    });

    $(".js-add-save").unbind("click").click(function () {
        addTempOrgRecord();
    })
})

function addTempOrgRecord() {
    if ($("#partyOrgName").val()== "") {
        layer.msg("党组织名称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyTemporg",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            partyOrgName: $("#partyOrgName").val(),
            approvalTime: $("#approvalTime").val(),
            approvalResults: $("#approvalResults").val(),
            dissolutionTime: $("#dissolutionTime").val(),
            memberId: $("#memberId").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
