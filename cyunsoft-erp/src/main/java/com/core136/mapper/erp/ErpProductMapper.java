package com.core136.mapper.erp;

import com.core136.bean.erp.ErpProduct;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpProductMapper extends MyMapper<ErpProduct> {

    /**
     * @param productName
     * @param orgId
     * @return ErpProduct
     * @Title selectProductByName
     * @Description  按名称模糊查询产品
     */
    public List<ErpProduct> selectProductByName(@Param(value = "productName") String productName, @Param(value = "orgId") String orgId);

    /**
     * @param productId
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getProuctAndBomInfoByProductId
     * @Description  获取产品与BOM的对应信息
     */
    public Map<String, Object> getProuctAndBomInfoByProductId(@Param(value = "productId") String productId, @Param(value = "orgId") String orgId);

    /**
     * @param @param  orgId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getProductSelect2
     * @Description:  获取产品的select2列表
     */
    public List<Map<String, Object>> getProductSelect2(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
