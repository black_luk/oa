package com.core136.controller.crm;


import com.core136.bean.account.Account;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/crm")
public class PageCrmController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return ModelAndView
     * @Title: goInquiryBi
     * @Description:  询价分析
     */
    @RequestMapping("/quotebi")
    @RequiresPermissions("/app/core/crm/quotebi")
    public ModelAndView goQuoteBi() {
        try {
            return new ModelAndView("app/core/crm/bi/quotationbi");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goInquiryBi
     * @Description:  询价分析
     */
    @RequestMapping("/inquirybi")
    @RequiresPermissions("/app/core/crm/inquirybi")
    public ModelAndView goInquiryBi() {
        try {
            return new ModelAndView("app/core/crm/bi/inquirybi");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param view
     * @return ModelAndView
     * @Title: goQuotationapproved
     * @Description:  报价审批
     */
    @RequestMapping("/quotationapproved")
    @RequiresPermissions("/app/core/crm/quotationapproved")
    public ModelAndView goQuotationapproved(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/crm/quotation/quotationapproved");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/crm/quotation/quotationapprovedmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goQuotationquery
     * @Description:  询价单查询
     */
    @RequestMapping("/quotationquery")
    @RequiresPermissions("/app/core/crm/quotationquery")
    public ModelAndView goQuotationquery() {
        try {
            return new ModelAndView("app/core/crm/quotation/quotationquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goQuotationDetails
     * @Description:  报价单详情
     */
    @RequestMapping("/quotationdetails")
    @RequiresPermissions("/app/core/crm/quotationdetails")
    public ModelAndView goQuotationDetails() {
        try {
            return new ModelAndView("app/core/crm/quotation/quotationdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goQuotation
     * @Description:  报价单管理
     */
    @RequestMapping("/quotation")
    public ModelAndView goQuotationManage(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/crm/quotation/quotationmanage");
            } else {
                if (view.equals("input")) {
                    Account account = accountService.getRedisAUserInfoToAccount();
                    mv = new ModelAndView("app/core/crm/quotation/quotation");
                    String code = SysTools.getCode(account, "报[yyyy][MM][dd]-[R]");
                    mv.addObject("code", code);
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/crm/quotation/quotationedit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goCreateCustomer
     * @Description  新建客户
     */
    @RequestMapping("/manage/new")
    @RequiresPermissions("/app/core/crm/manage/new")
    public ModelAndView goCreateCustomer(String customerId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/crm/customer/createcustomer");
            mv.addObject("customerId", customerId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goQueryCustomer
     * @Description  个人管理客户
     */
    @RequestMapping("/manage/query")
    @RequiresPermissions("/app/core/crm/manage/query")
    public ModelAndView goQueryCustomer() {
        try {
            return new ModelAndView("app/core/crm/customer/query");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goLinkmanr
     * @Description  联系人管理
     */
    @RequestMapping("/linkman")
    @RequiresPermissions("/app/core/crm/linkman")
    public ModelAndView goLinkmanr() {
        try {
            return new ModelAndView("app/core/crm/linkman/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goMylinkman
     * @Description: 业务员人个客户联系人
     */
    @RequestMapping("/mylinkman")
    @RequiresPermissions("/app/core/crm/mylinkman")
    public ModelAndView goMylinkman() {
        try {
            return new ModelAndView("app/core/crm/customer/mylinkman");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title createLinkman
     * @Description  创建联系人
     */
    @RequestMapping("/createLinkman")
    @RequiresPermissions("/app/core/crm/createLinkman")
    public ModelAndView createLinkman(String linkManId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/crm/linkman/add");
            mv.addObject("linkManId", linkManId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: createcrmcontractinfo
     * @Description:  设置合同信息
     */
    @RequestMapping("/crmcontractinfo")
    @RequiresPermissions("/app/core/crm/crmcontractinfo")
    public ModelAndView createcrmcontractinfo() {
        try {
            return new ModelAndView("app/core/crm/set/contractinfo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goSetpriv
     * @Description  CRM权限设置
     */
    @RequestMapping("/setpriv")
    @RequiresPermissions("/app/core/crm/setpriv")
    public ModelAndView goSetpriv() {
        try {
            return new ModelAndView("app/core/crm/set/mangeset");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goCrmmanage
     * @Description  客户信息维护
     */
    @RequestMapping("/crmmanage")
    @RequiresPermissions("/app/core/crm/crmmanage")
    public ModelAndView goCrmmanage() {
        try {
            return new ModelAndView("app/core/crm/manage/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goCustomerdetails
     * @Description  客户信息详情页
     */
    @RequestMapping("/customerdetails")
    @RequiresPermissions("/app/core/crm/customerdetails")
    public ModelAndView goCustomerdetails(String customerId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/crm/customer/customerdetails");
            mv.addObject("customerId", customerId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param customerId
     * @return ModelAndView
     * @Title goLinkMandetails
     * @Description  企业联系人详情列表
     */
    @RequestMapping("/linkMandetails")
    @RequiresPermissions("/app/core/crm/linkMandetails")
    public ModelAndView goLinkMandetails(String customerId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/crm/linkman/details");
            mv.addObject("customerId", customerId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goTags
     * @Description:  企来标签设置
     */
    @RequestMapping("/tags")
    @RequiresPermissions("/app/core/crm/tags")
    public ModelAndView goTags() {
        try {
            return new ModelAndView("app/core/crm/set/tags");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goMyproduct
     * @Description:  可供产品设置
     */
    @RequestMapping("/myproduct")
    @RequiresPermissions("/app/core/crm/myproduct")
    public ModelAndView goMyproduct() {
        try {
            return new ModelAndView("app/core/crm/set/myproduct");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goIndustry
     * @Description:  企业行业设置
     */
    @RequestMapping("/industry")
    @RequiresPermissions("/app/core/crm/industry")
    public ModelAndView goIndustry() {
        try {
            return new ModelAndView("app/core/crm/set/industry");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goInquiry
     * @Description:  新建询价单
     */
    @RequestMapping("/inquiry")
    @RequiresPermissions("/app/core/crm/inquiry")
    public ModelAndView goInquiry(HttpServletRequest request, String inquiryId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String inquiryCode = SysTools.getCode(account, "[yyyy][MM][dd]-[HH][mm][ss]-[R]");
            ModelAndView mv = new ModelAndView("app/core/crm/inquiry/create");
            mv.addObject("inquiryCode", inquiryCode);
            mv.addObject("inquiryId", inquiryId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goInquiryManage
     * @Description:  询价单管理
     */
    @RequestMapping("/inquiryManage")
    @RequiresPermissions("/app/core/crm/inquiryManage")
    public ModelAndView goInquiryManage() {
        try {
            return new ModelAndView("app/core/crm/inquiry/manage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goInquiryDetails
     * @Description:  询价单详情
     */
    @RequestMapping("/inquirydetails")
    @RequiresPermissions("/app/core/crm/inquirydetails")
    public ModelAndView goInquiryDetails() {
        try {
            return new ModelAndView("app/core/crm/inquiry/inquirydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goCustomerbi
     * @Description:  客户分析
     */
    @RequestMapping("/customerbi")
    @RequiresPermissions("/app/core/crm/customerbi")
    public ModelAndView goCustomerbi() {
        try {
            return new ModelAndView("app/core/crm/bi/customerbi");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
