package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionInst;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionInstMapper extends MyMapper<PartyUnionInst> {

    /**
     * 获取法律法规列表
     *
     * @param orgId
     * @param sortId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionInstList(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId,
                                                           @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                           @Param(value = "search") String search);

}
