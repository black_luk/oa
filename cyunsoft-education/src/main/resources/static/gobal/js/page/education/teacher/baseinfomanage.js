$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD",
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
    });
    jeDate("#workTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $('#remark').summernote({height: 300});
    getBaseNation();
    getAllBaseEducation();
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getTeacherList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'teacherId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名',
            formatter: function (value, row, index) {
                if (value == undefined) {
                    return "<a href=\"javascript:void(0);details('" + row.teacherId + "')\" style='cursor: pointer'>" + row.tUserName + "</a>";
                } else {
                    return "<a href=\"javascript:void(0);details('" + row.teacherId + "')\" style='cursor: pointer'>" + value + "</a>";
                }
            }
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'sex',
            width: '50px',
            title: '性别',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "男";
                } else if (value == "0") {
                    return "女";
                } else {
                    return "其他";
                }
            }
        }, {
            field: 'major',
            width: '100px',
            title: '专业'
        }, {
            field: 'nation',
            title: '民族',
            width: '100px',
            formatter: function (value, row, index) {
                return getBaseNationById(value);
            }
        }, {
            field: 'education',
            title: '教师学历',
            width: '100px',
            formatter: function (value, row, index) {
                return getBaseEducationById(value);
            }
        }, {
            field: 'graduateSchool',
            title: '毕业学校',
            width: '100px'
        }, {
            field: 'mobileNo',
            title: '联系电话',
            width: '100px',
        }, {
            field: 'status',
            title: '状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "正常";
                } else if (value == "1") {
                    return "退休";
                } else if (value == "2") {
                    return "退体返聘";
                } else if (value == "3") {
                    return "离职";
                }
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.teacherId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(teacherId) {
    var html = "<a href=\"javascript:void(0);edit('" + teacherId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteRecord('" + teacherId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        accountId: $("#accountIdQuery").attr("data-value"),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        status: $("#statusQuery").val(),
        major: $("#majorQuery").val()
    };
    return temp;
};

function details(teacherId) {
    window.open("/app/core/education/teacher/details?teacherId=" + teacherId);
}

function goback() {
    $("#infodiv").hide();
    $("#listdiv").show();
}

function edit(teacherId) {
    $("#listdiv").hide();
    $("#infodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/eduget/getEduTeacherById",
        type: "post",
        dataType: "json",
        data: {
            teacherId: teacherId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "accountId") {
                        if (recordInfo.accountId != "" && recordInfo.accountId != null) {
                            $("#accountId").val(getUserNameByStr(recordInfo.accountId))
                            $("#accountId").attr("data-value", recordInfo.accountId);
                        }
                    } else if (id == "headImg") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=eduphotos&fileName=" + recordInfo[id]);
                        $("#file").attr("data-value", recordInfo[id])
                    } else if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateTeacherInfo(teacherId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteRecord(teacherId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/eduset/deleteEduTeacher",
            type: "post",
            dataType: "json",
            data: {
                teacherId: teacherId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function updateTeacherInfo(teacherId) {
    $.ajax({
        url: "/set/eduset/updateEduTeacher",
        type: "post",
        dataType: "json",
        data: {
            teacherId: teacherId,
            sortNo: $("#sortNo").val(),
            headImg: $("#file").attr("data-value"),
            userName: $("#userName").val(),
            accountId: $("#accountId").attr("data-value"),
            sex: $("#sex").val(),
            cardId: $("#cardId").val(),
            birthday: $("#birthday").val(),
            nation: $("#nation").val(),
            mobile: $("#mobile").val(),
            major: $("#major").val(),
            teacherType: $("#teacherType").val(),
            workTime: $("#workTime").val(),
            graduateSchool: $("#graduateSchool").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#infodiv").hide();
                $("#listdiv").show();
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getBaseNation() {
    $.ajax({
        url: "/ret/baseinfoget/getAllBaseNation",
        type: "post",
        dataType: "json",
        success: function (data) {
            $("#nation").append("<option value=''>请选择</option>");
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#nation").append("<option value='" + data.list[i].nationId + "'>" + data.list[i].nationName + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getAllBaseEducation() {
    $.ajax({
        url: "/ret/baseinfoget/getAllBaseEducation",
        type: "post",
        dataType: "json",
        success: function (data) {
            $("#education").append("<option value=''>请选择</option>");
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#education").append("<option value='" + data.list[i].educationId + "'>" + data.list[i].educationName + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getBaseNationById(nationId) {
    var retStr = "";
    if (nationId == "") {
        return retStr;
    } else {
        $.ajax({
            url: "/ret/baseinfoget/getBaseNationById",
            type: "post",
            dataType: "json",
            async: false,
            data: {nationId: nationId},
            success: function (data) {
                if (data.status == "200") {
                    retStr = data.list.nationName;
                }
            }
        });
    }
    return retStr;
}


function getBaseEducationById(educationId) {
    var retStr = "";
    if (educationId == "") {
        return retStr;
    } else {
        $.ajax({
            url: "/ret/baseinfoget/getBaseEducationById",
            type: "post",
            dataType: "json",
            async: false,
            data: {educationId: educationId},
            success: function (data) {
                if (data.status == "200") {
                    retStr = data.list.educationName;
                }
            }
        });
    }
    return retStr;
}

function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}
