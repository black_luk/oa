package com.core136.service.safety;

import com.core136.bean.safety.SafetyDrill;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyDrillMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyDrillService {
    private SafetyDrillMapper safetyDrillMapper;
    @Autowired
    public void setSafetyDrillMapper(SafetyDrillMapper safetyDrillMapper)
    {
        this.safetyDrillMapper = safetyDrillMapper;
    }

    public int insertSafetyDrill(SafetyDrill safetyDrill)
    {
        return safetyDrillMapper.insert(safetyDrill);
    }

    public int deleteSafetyDrill(SafetyDrill safetyDrill)
    {
        return safetyDrillMapper.delete(safetyDrill);
    }

    public SafetyDrill selectOneSafetyDrill(SafetyDrill safetyDrill)
    {
        return safetyDrillMapper.selectOne(safetyDrill);
    }

    public int updateSafetyDrill(Example example,SafetyDrill safetyDrill)
    {
        return safetyDrillMapper.updateByExampleSelective(safetyDrill,example);
    }

    /**
     * 获取安全演练列表
     * @param orgId
     * @param drillType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyDrillList(String orgId, String drillType, String beginTime, String endTime, String search)
    {
        return safetyDrillMapper.getSafetyDrillList(orgId,drillType,beginTime,endTime,"%"+search+"%");
    }

    /**
     * 获取安全演练列表
     * @param pageParam
     * @param drillType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyDrillList(PageParam pageParam, String drillType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyDrillList(pageParam.getOrgId(), drillType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
