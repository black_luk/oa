package com.core136.mapper.education;

import com.core136.bean.education.EduClass;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduClassMapper extends MyMapper<EduClass> {

    /**
     * @param orgId
     * @param departmentsId
     * @param semesterId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduClassList
     * @Description:  获取班级列表
     */
    public List<Map<String, String>> getEduClassList(@Param(value = "orgId") String orgId, @Param(value = "departmentsId") String departmentsId, @Param(value = "gradeId") String gradeId,
                                                     @Param(value = "majorId") String majorId, @Param(value = "semesterId") String semesterId, @Param(value = "search") String search);

    /**
     * 获取当前学期的班级列表
     *
     * @param orgId
     * @param orgFlag
     * @param accountId
     * @return
     */
    public List<Map<String, String>> getEduClassListForSet(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String orgFlag, @Param(value = "accountId") String accountId,
                                                           @Param(value = "departmentsId") String departmentsId, @Param(value = "gradeId") String gradeId, @Param(value = "majorId") String majorId
    );
}
