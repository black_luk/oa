package com.core136.service.safety;

import com.core136.bean.safety.SafetyCodeClass;
import com.core136.mapper.safety.SafetyCodeClassMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyCodeClassService {
    private SafetyCodeClassMapper safetyCodeClassMapper;

    @Autowired
    public void setSafetyCodeClassMapper(SafetyCodeClassMapper safetyCodeClassMapper) {
        this.safetyCodeClassMapper = safetyCodeClassMapper;
    }

    public int insertSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        return safetyCodeClassMapper.insert(safetyCodeClass);
    }

    public int deleteSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        return safetyCodeClassMapper.delete(safetyCodeClass);
    }

    public SafetyCodeClass selectOneSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        return safetyCodeClassMapper.selectOne(safetyCodeClass);
    }

    public int updateSafetyCodeClass(Example example, SafetyCodeClass safetyCodeClass) {
        return safetyCodeClassMapper.updateByExampleSelective(safetyCodeClass, example);
    }

    public int deleteCodeClass(Example example) {
        return safetyCodeClassMapper.deleteByExample(example);
    }

    public List<SafetyCodeClass> getCodeClassList(Example example) {
        return safetyCodeClassMapper.selectByExample(example);
    }

    public PageInfo<SafetyCodeClass> getCodeClassList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<SafetyCodeClass> datalist = getCodeClassList(example);
        PageInfo<SafetyCodeClass> pageInfo = new PageInfo<SafetyCodeClass>(datalist);
        return pageInfo;
    }

    /**
     * 按标识获取分类码列表
     */

    public List<Map<String, Object>> getCodeClassByModule(String orgId, String module) {
        return safetyCodeClassMapper.getCodeClassByModule(orgId, module);
    }

    /**
     * @param @param  codeClass
     * @param @return 设定文件
     * @return List<CodeClass> 返回类型
     * @Title: getCodeClassList
     * @Description:  获取分类列表
     */
    public List<SafetyCodeClass> getCodeClassList(SafetyCodeClass safetyCodeClass) {
        return safetyCodeClassMapper.select(safetyCodeClass);
    }
}
