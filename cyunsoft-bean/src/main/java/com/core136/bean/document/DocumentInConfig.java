package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: DocumentInConfig
 * @Description: 系统内部收发文配置
 * @author: 稠云技术
 * @date: 2020年6月11日 下午7:51:58
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "document_in_config")
public class DocumentInConfig implements Serializable {
    private Long id;
    private String configId;
    private String crateTime;
    private String createUser;
    private String orgId;

}
