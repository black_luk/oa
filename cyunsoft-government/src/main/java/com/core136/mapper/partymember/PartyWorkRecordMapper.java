package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyWorkRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyWorkRecordMapper extends MyMapper<PartyWorkRecord> {

    /**
     * @param orgId
     * @param workType
     * @param joinUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyWorkRecordList
     * @Description:  历史活动列表
     */
    public List<Map<String, String>> getPartyWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "workType") String workType,
                                                            @Param(value = "joinUser") String joinUser, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param workType
     * @param joinUser
     * @param nowTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyPartyWorkRecordList
     * @Description:  获取需我参加的党员活动列表
     */
    public List<Map<String, String>> getMyPartyWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "workType") String workType,
                                                              @Param(value = "accountId") String joinUser, @Param(value = "nowTime") String nowTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param workType
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHistoryPartyWorkRecordList
     * @Description:  获取我参加的党员活动
     */
    public List<Map<String, String>> getHistoryPartyWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "workType") String workType,
                                                                   @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
