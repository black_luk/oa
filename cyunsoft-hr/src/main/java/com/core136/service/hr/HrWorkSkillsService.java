package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrWorkSkills;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWorkSkillsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrWorkSkillsService {
    private HrWorkSkillsMapper hrWorkSkillsMapper;

    @Autowired
    public void setHrWorkSkillsMapper(HrWorkSkillsMapper hrWorkSkillsMapper) {
        this.hrWorkSkillsMapper = hrWorkSkillsMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.insert(hrWorkSkills);
    }

    public int deleteHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.delete(hrWorkSkills);
    }

    public int updateHrWorkSkills(Example example, HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.updateByExampleSelective(hrWorkSkills, example);
    }

    public HrWorkSkills selectOneHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.selectOne(hrWorkSkills);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param skillsLevel
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrWorkSkillsList
     * @Description:  工作特长列表
     */
    public List<Map<String, String>> getHrWorkSkillsList(String orgId, String userId, String beginTime, String endTime, String skillsLevel, String search) {
        return hrWorkSkillsMapper.getHrWorkSkillsList(orgId, userId, beginTime, endTime, skillsLevel, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param skillsLevel
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrWorkSkillsList
     * @Description:  工作特长列表
     */
    public PageInfo<Map<String, String>> getHrWorkSkillsList(PageParam pageParam, String userId, String beginTime, String endTime, String skillsLevel) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrWorkSkillsList(pageParam.getOrgId(), userId, beginTime, endTime, skillsLevel, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrWorkSkills
     * @Description:  工作技能导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrWorkSkills(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("关联档案人员", "user_id");
        fieldMap.put("技能名称", "name");
        fieldMap.put("技能等级", "skills_level");
        fieldMap.put("是否有证书", "skills_cerificate");
        fieldMap.put("发证机构", "notifie_body");
        fieldMap.put("发证日期", "begin_time");
        fieldMap.put("终止日期", "end_time");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("关联档案人员")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("是否有证书")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        valueString += "'1',";
                    } else {
                        valueString += "'0',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_work_skills(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }
}
