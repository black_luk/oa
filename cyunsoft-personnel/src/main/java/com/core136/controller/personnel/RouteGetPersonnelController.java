package com.core136.controller.personnel;

import com.core136.service.personnel.PerDeptService;
import com.core136.service.personnel.PerDocumentService;
import com.core136.service.personnel.PerLevelService;
import com.core136.service.personnel.PerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/perget")
public class RouteGetPersonnelController {
    private PerDeptService perDeptService;

    @Autowired
    public void setPerDeptService(PerDeptService perDeptService) {
        this.perDeptService = perDeptService;
    }

    private PerDocumentService perDocumentService;

    @Autowired
    public void setPerDocumentService(PerDocumentService perDocumentService) {
        this.perDocumentService = perDocumentService;
    }

    private PerLevelService perLevelService;

    @Autowired
    public void setPerLevelService(PerLevelService perLevelService) {
        this.perLevelService = perLevelService;
    }

    private PerUserService perUserService;

    @Autowired
    public void setPerUserService(PerUserService perUserService) {
        this.perUserService = perUserService;
    }

}
