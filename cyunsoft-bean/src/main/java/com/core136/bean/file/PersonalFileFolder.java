/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: PersonalFileFolder.java
 * @Package com.core136.bean.file
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午8:43:34
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PersonalFileFolder
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午8:43:34
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "personal_file_folder")
public class PersonalFileFolder implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String folderId;
    private Integer sortNo;
    private String folderName;
    private String folderLevel;
    private String createTime;
    private String createUser;
    private String downPriv;
    private String sharePriv;
    private String editPriv;
    private String orgId;

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getEditPriv() {
        return editPriv;
    }

    public void setEditPriv(String editPriv) {
        this.editPriv = editPriv;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderLevel() {
        return folderLevel;
    }

    public void setFolderLevel(String folderLevel) {
        this.folderLevel = folderLevel;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getDownPriv() {
        return downPriv;
    }

    public void setDownPriv(String downPriv) {
        this.downPriv = downPriv;
    }

    public String getSharePriv() {
        return sharePriv;
    }

    public void setSharePriv(String sharePriv) {
        this.sharePriv = sharePriv;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
