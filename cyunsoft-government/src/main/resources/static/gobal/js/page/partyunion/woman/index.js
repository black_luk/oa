$(function () {
    jeDate("#getTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionWoman();
    })
})

function insertPartyUnionWoman() {
    if($("#userName").val()==""&&$("#accountId").attr("data-value"))
    {
        layer.msg("人员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionWoman",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            grantingAuthority: $("#grantingAuthority").val(),
            userName: $("#userName").val(),
            accountId: $("#accountId").attr("data-value"),
            levelId: $("#levelId").attr("data-value"),
            deptId: $("#deptId").attr("data-value"),
            getTime: $("#getTime").val(),
            modelName: $("#modelName").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
