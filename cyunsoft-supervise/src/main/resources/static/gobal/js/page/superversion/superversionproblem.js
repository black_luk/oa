$(function () {
    query();
    $(".js-add-problem").unbind("click").click(function () {
        $("#myProblemModal").modal("show");
        document.getElementById("form1").reset();
        $("#show_attach").empty();
        $("#attach").attr("data_value","");
        $(".js-save-problem").unbind("click").click(function () {
            sendSuperversionProblem()
        })
    })
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    getSuperversionList();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getMySuperversionProblemList',
        method: 'post',
        cardView: true,
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: false,//排序
        search: true,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: false,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            formatter: function (value, row, index) {
                let html = "<div class=\"well well-lg\">" +
                    "<h4>" + row.title + "</h4>" +
                    "<div style='font-size: 14px;'>" + row.sContent + "</div>";
                if (row.sAttach != "" && row.sAttach != null) {
                    html += "<div style='margin: 10px;'>附件：" + createTableAttach(row.sAttach) + "</div>";
                }
                html += "<div style='margin: 10px;font-size: 18px;font-weight: 600'>问题" +
                    "<span style=\"float: right;font-size: 14px;\">反馈人：" + row.createUserName + "&nbsp;&nbsp;反馈时间：" + row.createTime + "</span>" +
                    "</div>" +
                    "<div style='background-color: #ccc;margin: 20px;'>" + row.content + "</div>";
                if (row.attach != "") {
                    html += "<div>附件：" + createTableAttach(row.attach) + "</div>";
                }
                html += "<div id='answer_" + row.recordId + "'>" +
                    getMySuperversionAnswerList(row.recordId, row.processId) +
                    "</div>" +
                    "</div>";
                return html;
            }
        }
        ],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        processId: $("#processIdQuery").val()
    };
    return temp;
};

function getSuperversionList() {
    $.ajax({
        url: "/ret/superversionget/getSuperversionProcessListForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#processId").append("<option value='" + data.list[i].processId + "'>" + data.list[i].title + "</option>");
                    $("#processIdQuery").append("<option value='" + data.list[i].processId + "'>" + data.list[i].title + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function sendSuperversionProblem() {
    $.ajax({
        url: "/set/superversionset/insertSuperversionProblem",
        type: "post",
        dataType: "json",
        data: {
            processId: $("#processId").val(),
            content: $("#content").val(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myProblemModal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getMySuperversionAnswerList(recordId, processId) {
    var html = "";
    $.ajax({
        url: "/ret/superversionget/getMySuperversionAnswerList",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            recordId: recordId,
            processId: processId
        },
        success: function (data) {
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    html += "<div style='margin: 10px;font-size: 18px;font-weight: 600'>回复" +
                        "<span style=\"float: right;font-size: 14px;\">回复人：" + data.list[i].createUserName + "&nbsp;&nbsp;回复时间：" + data.list[i].createTime + "</span>" +
                        "</div>" +
                        "<div style='background-color: #ccc;margin: 20px;'>" + data.list[i].content + "</div>";
                    if (data.list[i].attach != "") {
                        html += "<div>附件：" + createTableAttach(data.list[i].attach) + "</div>";
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    return html;
}

