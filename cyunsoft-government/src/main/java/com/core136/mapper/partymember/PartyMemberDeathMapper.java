package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberDeath;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberDeathMapper extends MyMapper<PartyMemberDeath> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberDeathList
     * @Description:  党员去世列表
     */
    public List<Map<String, String>> getMemberDeathList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
