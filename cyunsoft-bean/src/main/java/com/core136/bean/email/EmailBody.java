/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: EmailBody.java
 * @Package com.core136.bean.oa
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:28:33
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.email;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: EmailBody
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:28:33
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "email_body")
public class EmailBody implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String bodyId;
    private String fromId;
    private String toId;
    private String copyToId;
    private String subject;
    private String content;
    private String subheading;
    private String sendTime;
    private String readFlag;
    private String delFlag;
    private String sendFlag;
    private String attach;
    private String webEmailFlag;
    private String webEmailTo;
    private String webEmailCopy;
    private String msgType;
    private String orgId;

    public String getSendFlag() {
        return sendFlag;
    }

    public void setSendFlag(String sendFlag) {
        this.sendFlag = sendFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getBodyId() {
        return bodyId;
    }

    public void setBodyId(String bodyId) {
        this.bodyId = bodyId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(String readFlag) {
        this.readFlag = readFlag;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getCopyToId() {
        return copyToId;
    }

    public void setCopyToId(String copyToId) {
        this.copyToId = copyToId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getWebEmailFlag() {
        return webEmailFlag;
    }

    public void setWebEmailFlag(String webEmailFlag) {
        this.webEmailFlag = webEmailFlag;
    }

    public String getWebEmailTo() {
        return webEmailTo;
    }

    public void setWebEmailTo(String webEmailTo) {
        this.webEmailTo = webEmailTo;
    }

    public String getWebEmailCopy() {
        return webEmailCopy;
    }

    public void setWebEmailCopy(String webEmailCopy) {
        this.webEmailCopy = webEmailCopy;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

}
