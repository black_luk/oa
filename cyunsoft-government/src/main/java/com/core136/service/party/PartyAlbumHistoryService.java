package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.PartyAlbumHistory;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyAlbumHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyAlbumHistoryService {


    private PartyAlbumHistoryMapper partyAlbumHistoryMapper;

    @Autowired
    public void setPartyAlbumHistoryMapper(PartyAlbumHistoryMapper partyAlbumHistoryMapper) {
        this.partyAlbumHistoryMapper = partyAlbumHistoryMapper;
    }

    public int insertPartyAlbumHistory(PartyAlbumHistory partyAlbumHistory) {
        return partyAlbumHistoryMapper.insert(partyAlbumHistory);
    }

    public int deletePartyAlbumHistory(PartyAlbumHistory partyAlbumHistory) {
        return partyAlbumHistoryMapper.delete(partyAlbumHistory);
    }

    public int updatePartyAlbumHistory(Example example, PartyAlbumHistory partyAlbumHistory) {
        return partyAlbumHistoryMapper.updateByExampleSelective(partyAlbumHistory, example);
    }

    public PartyAlbumHistory selectOnePartyAlbumHistory(PartyAlbumHistory partyAlbumHistory) {
        return partyAlbumHistoryMapper.selectOne(partyAlbumHistory);
    }

    /**
     * @param account
     * @param videoId
     * @return int
     * @Title: addAlbumHistory
     * @Description:  添加浏览记录
     */
    public int addAlbumHistory(Account account, String videoId) {
        PartyAlbumHistory partyAlbumHistory = new PartyAlbumHistory();
        partyAlbumHistory.setVideoId(videoId);
        partyAlbumHistory.setOrgId(account.getOrgId());
        partyAlbumHistory.setCreateUser(account.getAccountId());
        int count = partyAlbumHistoryMapper.selectCount(partyAlbumHistory);
        if (count == 0) {
            partyAlbumHistory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            return insertPartyAlbumHistory(partyAlbumHistory);
        } else {
            Example example = new Example(PartyAlbumHistory.class);
            example.createCriteria().andEqualTo("orgId", partyAlbumHistory.getOrgId()).andEqualTo("videoId", partyAlbumHistory.getVideoId()).andEqualTo("createUser", account.getAccountId());
            partyAlbumHistory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            return updatePartyAlbumHistory(example, partyAlbumHistory);
        }

    }

}
