package com.core136.service.archives;

import com.core136.bean.account.Account;
import com.core136.bean.archives.ArchivesBorrowFile;
import com.core136.bean.archives.ArchivesFile;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesFileMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesFileService {
    private ArchivesFileMapper archivesFileMapper;

    @Autowired
    public void setArchivesFileMapper(ArchivesFileMapper archivesFileMapper) {
        this.archivesFileMapper = archivesFileMapper;
    }

    public int insertArchivesFile(ArchivesFile archivesFile) {
        return archivesFileMapper.insert(archivesFile);
    }

    public int deleteArchivesFile(ArchivesFile archivesFile) {
        return archivesFileMapper.delete(archivesFile);
    }

    public int updateArchivesFile(Example example, ArchivesFile archivesFile) {
        return archivesFileMapper.updateByExampleSelective(archivesFile, example);
    }

    public ArchivesFile selectOneArchivesFile(ArchivesFile archivesFile) {
        return archivesFileMapper.selectOne(archivesFile);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getArchivesFileList
     * @Description:  获取文件管理列表
     */
    public List<Map<String, String>> getArchivesFileList(String orgId, String opFlag, String accountId, String volumeId, String fileType, String secretLevel, String search) {
        return archivesFileMapper.getArchivesFileList(orgId, opFlag, accountId, volumeId, fileType, secretLevel, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesFileList
     * @Description:  获取文件管理列表
     */
    public PageInfo<Map<String, String>> getArchivesFileList(PageParam pageParam, String volumeId, String fileType, String secretLevel) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesFileList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), volumeId, fileType, secretLevel, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param repositoryId
     * @param accountId
     * @param volumeId
     * @param fileType
     * @param secretLevel
     * @param search
     * @return List<Map < String, String>>
     * @Title: getArchivesFileQueryList
     * @Description:  档案查询列表
     */
    public List<Map<String, String>> getArchivesFileQueryList(String orgId, String repositoryId, String accountId, String volumeId, String fileType, String secretLevel, String search) {
        return archivesFileMapper.getArchivesFileQueryList(orgId, repositoryId, accountId, volumeId, fileType, secretLevel, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param repositoryId
     * @param volumeId
     * @param fileType
     * @param secretLevel
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesFileQueryList
     * @Description:  档案查询列表
     */
    public PageInfo<Map<String, String>> getArchivesFileQueryList(PageParam pageParam, String repositoryId, String volumeId, String fileType, String secretLevel) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesFileQueryList(pageParam.getOrgId(), repositoryId, pageParam.getAccountId(), volumeId, fileType, secretLevel, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param archivesFile
     * @return int
     * @Title: isExist
     * @Description:  获取案卷下的文件个数
     */
    public int isExist(ArchivesFile archivesFile) {
        Example example = new Example(ArchivesFile.class);
        example.createCriteria().andEqualTo("orgId", archivesFile.getOrgId()).andEqualTo("volumeId", archivesFile.getVolumeId());
        return archivesFileMapper.selectCountByExample(example);
    }

    /**
     * ParseException
     *
     * @param account
     * @param archivesBorrowFile
     * @return ArchivesFile
     * @Title: getApprovalFile
     * @Description:  审批通过后有附件信息
     */
    public ArchivesFile getApprovalFile(Account account, ArchivesBorrowFile archivesBorrowFile) throws ParseException {
        ArchivesFile archivesFile = new ArchivesFile();
        archivesFile.setOrgId(archivesBorrowFile.getOrgId());
        archivesFile.setFileId(archivesBorrowFile.getFileId());
        archivesFile = selectOneArchivesFile(archivesFile);
        if (!account.getAccountId().equals(archivesBorrowFile.getCreateUser()) || !archivesBorrowFile.getApprovalStatus().equals("1")) {
            archivesFile.setAttach("");
        }
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        String beginTime = archivesBorrowFile.getBeginTime();
        String endTime = archivesBorrowFile.getEndTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date bt = sdf.parse(beginTime);
        Date et = sdf.parse(endTime);
        Date nt = sdf.parse(nowTime);
        if (et.before(bt)) {
            archivesFile.setAttach("");
        }
        if (nt.before(bt)) {
            archivesFile.setAttach("");
        }
        if (et.before(nt)) {
            archivesFile.setAttach("");
        }
        return archivesFile;

    }

    /**
     * @param orgId
     * @param volumeId
     * @return List<Map < String, String>>
     * @Title: getBorrowArchivesFileList
     * @Description:   获取借阅的文件列表
     */
    public List<Map<String, String>> getBorrowArchivesFileList(String orgId, String volumeId) {
        return archivesFileMapper.getBorrowArchivesFileList(orgId, volumeId);
    }

    /**
     * @param pageParam
     * @param volumeId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBorrowArchivesFileList
     * @Description:  获取借阅的文件列表
     */
    public PageInfo<Map<String, String>> getBorrowArchivesFileList(PageParam pageParam, String volumeId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBorrowArchivesFileList(pageParam.getOrgId(), volumeId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
