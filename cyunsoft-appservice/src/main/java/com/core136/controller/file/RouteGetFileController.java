package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.FileAESUtils;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.file.*;
import com.core136.unit.fileutils.DownUtils;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/fileget")
public class RouteGetFileController {
    @Value("${app.attachpath}")
    private String attachpath;
    private NetDiskService netDiskService;

    @Autowired
    public void setNetDiskService(NetDiskService netDiskService) {
        this.netDiskService = netDiskService;
    }

    private PersonalFileFolderService personalFileFolderService;

    @Autowired
    public void setPersonalFileFolderService(PersonalFileFolderService personalFileFolderService) {
        this.personalFileFolderService = personalFileFolderService;
    }

    private PersonalFileService personalFileService;

    @Autowired
    public void setPersonalFileService(PersonalFileService personalFileService) {
        this.personalFileService = personalFileService;
    }

    private PublicFileFolderService publicFileFolderService;

    @Autowired
    public void setPublicFileFolderService(PublicFileFolderService publicFileFolderService) {
        this.publicFileFolderService = publicFileFolderService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    private DownUtils downUtils;

    @Autowired
    public void setDownUtils(DownUtils downUtils) {
        this.downUtils = downUtils;
    }

    private PublicFileService publicFileService;

    @Autowired
    public void setPublicFileService(PublicFileService publicFileService) {
        this.publicFileService = publicFileService;
    }

    private PhotoService photoService;

    @Autowired
    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param folderId
     * @return RetDataBean
     * @Title: getShareFolderPrivInfo
     * @Description:  获取共享权限
     */
    @RequestMapping(value = "/getShareFolderPrivInfo", method = RequestMethod.POST)
    public RetDataBean getShareFolderPrivInfo(String folderId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileFolderService.getShareFolderPrivInfo(account.getOrgId(), folderId, account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSharePersonalFileFolder
     * @Description:   获取共享目录
     */
    @RequestMapping(value = "/getSharePersonalFileFolder", method = RequestMethod.POST)
    public RetDataBean getSharePersonalFileFolder() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileFolderService.getSharePersonalFileFolder(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param personalFileFolder
     * @return RetDataBean
     * @Title: getPersonalFolderById
     * @Description:  获取个人文件柜详情
     */
    @RequestMapping(value = "/getPersonalFolderById", method = RequestMethod.POST)
    public RetDataBean getPersonalFolderById(PersonalFileFolder personalFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            personalFileFolder.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileFolderService.selectOnePersonalFileFolder(personalFileFolder));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param attachId
     * @return RetDataBean
     * @Title: getAttachVersionList
     * @Description:  获取历史文件版本
     */
    @RequestMapping(value = "/getAttachVersionList", method = RequestMethod.POST)
    public RetDataBean getAttachVersionList(String attachId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attachService.getAttachVersionList(account.getOrgId(), attachId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param response
     * @param publicFile void
     * @Title: downPublicFileFile
     * @Description:  公共文件柜文件下载
     */
    @RequestMapping(value = "/downPublicFileFile", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public void downPublicFileFile(HttpServletResponse response, PublicFile publicFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            publicFile.setOrgId(account.getOrgId());
            publicFile = publicFileService.selectOnePublicFile(publicFile);
            Attach attach = new Attach();
            attach.setOrgId(account.getOrgId());
            attach.setAttachId(publicFile.getAttach());
            attach = attachService.selectOne(attach);
            if (StringUtils.isNotBlank(attach.getEncryptKey())) {
                File file = new File(attachpath+attach.getPath());
                String filename = file.getName();
                String utf8filename = URLEncoder.encode(filename, "UTF-8");
                response.setHeader("Content-disposition", "attachment; filename=" + utf8filename);
                response.setContentType("application/octet-stream");
                FileAESUtils.decryptFileToDown(attach.getEncryptKey(), attachpath+attach.getPath(), response);
            } else {
                downUtils.download(attachpath+attach.getPath(), response);
            }
        } catch (Exception e) {


        }
    }

    /**
     * @param response
     * @param netDisk
     * @param path     void
     * @Title: downNetDiskFile
     * @Description:  网络硬盘文件下载
     */
    @RequestMapping(value = "/downNetDiskFile", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public void downNetDiskFile(HttpServletResponse response, NetDisk netDisk, String path) {
        try {
            String attachPath = "";
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            attachPath = netDisk.getRootPath() + path;
            downUtils.download(attachPath, response);
        } catch (Exception e) {
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyPublicFolderInPrivForDesk
     * @Description:  获取桌面公共文件柜
     */
    @RequestMapping(value = "/getMyPublicFolderInPrivForDesk", method = RequestMethod.POST)
    public RetDataBean getMyPublicFolderInPrivForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, publicFileFolderService.getMyPublicFolderInPrivForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getNetDiskList
     * @Description  获取网络硬盘列表
     */
    @RequestMapping(value = "/getNetDiskList", method = RequestMethod.POST)
    public RetDataBean getNetDiskList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderStr = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(NetDisk.class);
            Criteria criteria = example.createCriteria();
            if (account.getOpFlag().equals("1")) {
                criteria.andEqualTo("orgId", account.getOrgId()).andLike("netDiskName", "%" + search + "%");
            } else {
                criteria.andEqualTo("orgId", account.getOrgId()).andEqualTo("diskCreateUser", account.getAccountId()).andLike("netDiskName", "%" + search + "%");
            }
            PageInfo<NetDisk> pageInfo = netDiskService.getNetDiskList(example, pageSize, pageNumber, orderStr);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param netDisk
     * @return RetDataBean
     * @Title getNetDisk
     * @Description  获取网盘信息
     */
    @RequestMapping(value = "/getNetDisk", method = RequestMethod.POST)
    public RetDataBean getNetDisk(NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, netDiskService.selectOneNetDisk(netDisk));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getPhotoFileList
     * @Description:  获取相册下的图片
     * @param: request
     * @param: photo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getPhotoFileList", method = RequestMethod.POST)
    public RetDataBean getPhotoFileList(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            photo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, photoService.getPhotoFileList(photo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getPhotoById
     * @Description:  获取相册详情
     * @param: request
     * @param: photo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getPhotoById", method = RequestMethod.POST)
    public RetDataBean getPhotoById(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            photo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, photoService.selectOnePhoto(photo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyPhotoList
     * @Description:  获取个人权限内的相册
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyPhotoList", method = RequestMethod.POST)
    public RetDataBean getMyPhotoList() {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, photoService.getMyPhotoList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyPhotoFileList
     * @Description:  获取相册下面的相片
     * @param: request
     * @param: photo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyPhotoFileList", method = RequestMethod.POST)
    public RetDataBean getMyPhotoFileList(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            photo.setOrgId(account.getOrgId());
            photo = photoService.selectOnePhoto(photo);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, photoService.getMyPhotoFileList(photo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getPhotoList
     * @Description:  获取相册列表
     * @param: request
     * @param: sortId
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getPhotoList", method = RequestMethod.POST)
    public RetDataBean getPhotoList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.sort_no");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                pageParam.setAccountId(account.getAccountId());
            }
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = photoService.getPhotoList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return List<Map < String, Object>>
     * @Title getNetDiskTree
     * @Description  获取权限内的网络硬盘目录
     */
    @RequestMapping(value = "/getNetDiskTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getNetDiskTree() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:query")) {
                return null;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return netDiskService.getNetDiskTree(account);
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param rootPath
     * @return List<Map < String, Object>>
     * @Title getNetDiskDirs
     * @Description  获取文件目录中的文件目录结构
     */
    @RequestMapping(value = "/getNetDiskDirs", method = RequestMethod.POST)
    public List<Map<String, Object>> getNetDiskDirs(String rootPath, NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:query")) {
                return null;
            }
            netDisk.setRootPath(null);
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            if (!rootPath.equals(netDisk.getRootPath())) {
                rootPath = netDisk.getRootPath() + rootPath;
            }
            return netDiskService.getNetDiskDirs(rootPath, netDisk);
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  netDiskId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getNetDiskPriv
     * @Description:  获取当前用户在本网络硬盘下的权限
     */
    @RequestMapping(value = "/getNetDiskPriv", method = RequestMethod.POST)
    public RetDataBean getNetDiskPriv(String netDiskId) {
        try {
            if (StringUtils.isBlank(netDiskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, netDiskService.getNetDiskPrivByAccount(account, netDiskId));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param rootPath
     * @return RetDataBean
     * @Title getNetDiskFiles
     * @Description  获取指定目录下的文件
     */
    @RequestMapping(value = "/getNetDiskFiles", method = RequestMethod.POST)
    public RetDataBean getNetDiskFiles(String rootPath, NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            netDisk.setRootPath(null);
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            if (rootPath.equals(netDisk.getRootPath())) {
                rootPath = "";
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, netDiskService.getFileAndFolder(rootPath, netDisk));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  rootPath
     * @param @param  netDisk
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getNetDiskFileInfo
     * @Description:  获取文件详情
     */
    @RequestMapping(value = "/getNetDiskFileInfo", method = RequestMethod.POST)
    public RetDataBean getNetDiskFileInfo(String sourcePath, NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            netDisk.setRootPath(null);
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, netDiskService.getFolderInfo(sourcePath, netDisk));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getPersonalFolderForSelect
     * @Description:  获取带Checkbox的树
     * @param: request
     * @param: folderId
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    @RequestMapping(value = "/getPersonalFolderForSelect", method = RequestMethod.POST)
    public List<Map<String, Object>> getPersonalFolderForSelect(String folderId) {
        try {
            if (StringUtils.isBlank(folderId)) {
                folderId = "0";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return personalFileFolderService.getPersonalFolderForSelect(folderId, account.getAccountId(), account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @return List<Map < String, Object>>
     * @Title getPersonalDir
     * @Description  获取个人文件柜目录
     */
    @RequestMapping(value = "/getPersonalDir", method = RequestMethod.POST)
    public List<Map<String, Object>> getPersonalDir(String folderId) {
        try {
            if (StringUtils.isBlank(folderId)) {
                folderId = "0";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return personalFileFolderService.getPersonalDir(folderId, account.getAccountId(), account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  folderId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getPublicFilelist
     * @Description:  获取公共文件夹下面的文件
     */
    @RequestMapping(value = "/getPublicFilelist", method = RequestMethod.POST)
    public RetDataBean getPublicFilelist(String folderId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            List<Map<String, String>> list1 = publicFileFolderService.getChildFolderList(account.getOrgId(), folderId);
            List<Map<String, String>> list2 = publicFileService.getPublicFilelist(account.getOrgId(), folderId);
            list1.addAll(list2);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, list1);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyPublicFolderInPrivForSelect
     * @Description:  获取公共文件柜目录
     * @param: request
     * @param: folderId
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    @RequestMapping(value = "/getMyPublicFolderInPrivForSelect", method = RequestMethod.POST)
    public List<Map<String, Object>> getMyPublicFolderInPrivForSelect(String folderId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:query")) {
                return null;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return publicFileFolderService.getMyPublicFolderInPriv(account.getOrgId(), folderId, account.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), account.getOpFlag());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getPublicFileByFolderId
     * @Description:
     * @param: request
     * @param: folderId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getPublicFileByFolderId", method = RequestMethod.POST)
    public RetDataBean getPublicFileByFolderId(String folderId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, publicFileService.getPublicFileByFolderId(account.getOrgId(), folderId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param folderId
     * @return RetDataBean
     * @Title: getSharefilelist
     * @Description:  获取共享文件列表
     */
    @RequestMapping(value = "/getSharefilelist", method = RequestMethod.POST)
    public RetDataBean getSharefilelist(String folderId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileService.getSharefilelist(folderId, account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param folderId
     * @return RetDataBean
     * @Title getfilelist
     * @Description  获取个人文件柜中的文件
     */
    @RequestMapping(value = "/getfilelist", method = RequestMethod.POST)
    public RetDataBean getfilelist(String folderId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileService.getfilelist(folderId, account.getAccountId(), account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFileListForSelect
     * @Description:  获取目录下文件
     * @param: request
     * @param: folderId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFileListForSelect", method = RequestMethod.POST)
    public RetDataBean getFileListForSelect(String folderId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileService.getFileListForSelect(folderId, account.getAccountId(), account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  folderId
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getPublicFileFolderTree
     * @Description:  获取公共文件柜的树结构
     */
    @RequestMapping(value = "/getPublicFileFolderTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getPublicFileFolderTree(String folderId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:query")) {
                return null;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return publicFileFolderService.getPublicFileFolderAllDir(account.getOrgId(), folderId, account.getAccountId(), account.getOpFlag());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getPublicFileFolderById
     * @Description:  获取当前文件夹的信息
     * @param: request
     * @param: publicFileFolder
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getPublicFileFolderById", method = RequestMethod.POST)
    public RetDataBean getPublicFileFolderById(PublicFileFolder publicFileFolder) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            publicFileFolder.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, publicFileFolderService.selectOnePublicFileFolder(publicFileFolder));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  folderId
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getMyPublicFolderInPriv
     * @Description:  获取当前用户权限内的公共文件柜
     */
    @RequestMapping(value = "/getMyPublicFolderInPriv", method = RequestMethod.POST)
    public List<Map<String, Object>> getMyPublicFolderInPriv(String folderId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return publicFileFolderService.getMyPublicFolderInPriv(account.getOrgId(), folderId, account.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), account.getOpFlag());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  folderId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getPublicFolderPrivInfo
     * @Description:  获取当前文件柜的操作权限
     */
    @RequestMapping(value = "/getPublicFolderPrivInfo", method = RequestMethod.POST)
    public RetDataBean getPublicFolderPrivInfo(String folderId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, publicFileFolderService.getPublicFolderPrivInfo(account, folderId, account.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  personalFileFolder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getPersonalFileFolderChild
     * @Description:  获取目录下的子目录
     */
    @RequestMapping(value = "/getPersonalFileFolderChild", method = RequestMethod.POST)
    public RetDataBean getPersonalFileFolderChild(PersonalFileFolder personalFileFolder) {
        try {
            if (StringUtils.isBlank(personalFileFolder.getFolderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            personalFileFolder.setCreateUser(account.getAccountId());
            personalFileFolder.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, personalFileFolderService.getPersonalFileFolderChild(personalFileFolder));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param response
     * @param @param request
     * @param @param personalFile 设定文件
     * @return void 返回类型
     * @Title: getFileDown
     * @Description:  下载个人文件柜的文件
     */
    @RequestMapping("/getFileDown")
    public void getFileDown(HttpServletResponse response, PersonalFile personalFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            personalFile.setOrgId(account.getOrgId());
            personalFile = personalFileService.selectOnePersonalFile(personalFile);
            Attach attach = new Attach();
            attach.setAttachId(personalFile.getAttach());
            attach.setOrgId(account.getOrgId());
            attach = attachService.selectOne(attach);
            if (StringUtils.isNotBlank(attach.getEncryptKey())) {
                File file = new File(attach.getPath());
                String filename = file.getName();
                String utf8filename = URLEncoder.encode(filename, "UTF-8");
                response.setHeader("Content-disposition", "attachment; filename=" + utf8filename);
                response.setContentType("application/octet-stream");
                FileAESUtils.decryptFileToDown(attach.getEncryptKey(), attachpath+attach.getPath(), response);
            } else {
                downUtils.download(attachpath+attach.getPath(), response);
            }
        } catch (Exception e) {


        }
    }

    /**
     * @param pageParam
     * @param extName
     * @param beginTime
     * @param endTime
     * @param modules
     * @param createAccount
     * @return RetDataBean
     * @Title: getAttachManageList
     * @Description:  获取附件列表
     */
    @RequestMapping(value = "/getAttachManageList", method = RequestMethod.POST)
    public RetDataBean getAttachManageList(
            PageParam pageParam,
            String extName,
            String beginTime,
            String endTime,
            String modules,
            String createAccount
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.up_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = attachService.getAttachManageList(pageParam, createAccount, modules, beginTime, endTime, extName);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
