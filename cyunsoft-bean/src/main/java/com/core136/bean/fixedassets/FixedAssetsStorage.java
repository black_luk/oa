/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsStorage.java
 * @Package com.core136.bean.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月17日 上午10:06:55
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "fixed_assets_storage")
public class FixedAssetsStorage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String storageId;
    private Integer sortNo;
    private String storageName;
    private String position;
    private String manager;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the storageId
     */
    public String getStorageId() {
        return storageId;
    }

    /**
     * @param storageId the storageId to set
     */
    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    /**
     * @return the storageName
     */
    public String getStorageName() {
        return storageName;
    }

    /**
     * @param storageName the storageName to set
     */
    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the manager
     */
    public String getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(String manager) {
        this.manager = manager;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

}
