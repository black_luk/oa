$(function () {
    $("#myTable").bootstrapTable({
        url: '/ret/partyunionget/getAllPartyUnionOrgList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: false,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: false,// 启用搜索
        sortOrder: "asc",
        showColumns: false,// 是否显示 内容列下拉框
        showRefresh: false,// 显示刷新按钮
        idField: 'unionOrgId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'deadLine',
            title: '届次',
            width: '100px'
        }, {
            field: 'createType',
            title: '产生方式',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "差额";
                } else if (value == "2") {
                    return "等额";
                } else if (value == "3") {
                    return "其他";
                } else {
                    return "未知";
                }

            }
        }, {
            field: 'beginTime',
            title: '届始日期',
            width: '100px'
        }, {
            field: 'endTime',
            title: '届满日期',
            width: '100px'
        }, {
            field: 'chairmanFir',
            width: '50px',
            title: '主席',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'chairmanSec',
            width: '50px',
            title: '副主席',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'memberIds',
            width: '200px',
            title: '工会成员',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        },
            {
                field: 'remark',
                width: '100px',
                title: '备注'
            }, {
                field: 'attach',
                width: '100px',
                title: '相关文件',
                formatter: function (value, row, index) {
                    return createTableAttach(value);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
})

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};
