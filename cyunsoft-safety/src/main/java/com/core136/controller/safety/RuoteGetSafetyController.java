package com.core136.controller.safety;

import com.core136.bean.account.Account;
import com.core136.bean.safety.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.safety.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/safetyget")
public class RuoteGetSafetyController {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SafetyEntInfoService safetyEntInfoService;

    @Autowired
    public void setSafetyEntInfoService(SafetyEntInfoService safetyEntInfoService) {
        this.safetyEntInfoService = safetyEntInfoService;
    }

    private SafetyTeamService safetyTeamService;

    @Autowired
    public void setSafetyTeamService(SafetyTeamService safetyTeamService) {
        this.safetyTeamService = safetyTeamService;
    }

    private SafetyEventService safetyEventService;

    @Autowired
    public void setSafetyEventService(SafetyEventService safetyEventService) {
        this.safetyEventService = safetyEventService;
    }

    private SafetyLicenceService safetyLicenceService;

    @Autowired
    public void setSafetyLicenceService(SafetyLicenceService safetyLicenceService) {
        this.safetyLicenceService = safetyLicenceService;
    }

    private SafetyCodeClassService safetyCodeClassService;

    @Autowired
    public void setSafetyCodeClassService(SafetyCodeClassService safetyCodeClassService) {
        this.safetyCodeClassService = safetyCodeClassService;
    }

    private SafetyEntMemberService safetyEntMemberService;

    @Autowired
    public void setSafetyEntMemberService(SafetyEntMemberService safetyEntMemberService) {
        this.safetyEntMemberService = safetyEntMemberService;
    }

    private SafetyConfigService safetyConfigService;

    @Autowired
    public void setSafetyConfigService(SafetyConfigService safetyConfigService) {
        this.safetyConfigService = safetyConfigService;
    }

    private SafetyTrainService safetyTrainService;

    @Autowired
    private void setSafetyTrainService(SafetyTrainService safetyTrainService) {
        this.safetyTrainService = safetyTrainService;
    }

    private SafetyDiaryService safetyDiaryService;

    @Autowired
    public void setSafetyDiaryService(SafetyDiaryService safetyDiaryService) {
        this.safetyDiaryService = safetyDiaryService;
    }

    private SafetyTeamMemberService safetyTeamMemberService;

    @Autowired
    public void setSafetyTeamMemberService(SafetyTeamMemberService safetyTeamMemberService) {
        this.safetyTeamMemberService = safetyTeamMemberService;
    }

    public SafetyPlanService safetyPlanService;

    @Autowired
    public void setSafetyPlanService(SafetyPlanService safetyPlanService) {
        this.safetyPlanService = safetyPlanService;
    }

    private SafetyDrillService safetyDrillService;

    @Autowired
    public void setSafetyDrillService(SafetyDrillService safetyDrillService) {
        this.safetyDrillService = safetyDrillService;
    }

    private SafetyMaterialService safetyMaterialService;

    @Autowired
    public void setSafetyMaterialService(SafetyMaterialService safetyMaterialService) {
        this.safetyMaterialService = safetyMaterialService;
    }

    private SafetyTargetService safetyTargetService;

    @Autowired
    public void setSafetyTargetService(SafetyTargetService safetyTargetService) {
        this.safetyTargetService = safetyTargetService;
    }

    private SafetyTargetSortService safetyTargetSortService;

    @Autowired
    public void setSafetyTargetSortService(SafetyTargetSortService safetyTargetSortService) {
        this.safetyTargetSortService = safetyTargetSortService;
    }

    private SafetyRegulatorService safetyRegulatorService;

    @Autowired
    public void setSafetyRegulatorService(SafetyRegulatorService safetyRegulatorService) {
        this.safetyRegulatorService = safetyRegulatorService;
    }

    /**
     * 获取监管机构列表
     * @param safetyRegulator
     * @return
     */
    @RequestMapping(value = "/getSafetyRegulatorById", method = RequestMethod.POST)
    public RetDataBean getSafetyRegulatorById(SafetyRegulator safetyRegulator) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyRegulator.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyRegulatorService.selectOneSafetyRegulator(safetyRegulator));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取管理目标列表
     *
     * @param pageParam
     * @param targetSort
     * @param targetYear
     * @return
     */
    @RequestMapping(value = "/getSafetyTargetList", method = RequestMethod.POST)
    public RetDataBean getSafetyTargetList(PageParam pageParam, String targetSort, String targetYear) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.sort_no");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyTargetService.getSafetyTargetList(pageParam, targetSort, targetYear);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取管理目标分类
     *
     * @param safetyTargetSort
     * @return
     */
    @RequestMapping(value = "/getSafetyTargetSortById", method = RequestMethod.POST)
    public RetDataBean getSafetyTargetSortById(SafetyTargetSort safetyTargetSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTargetSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyTargetSortService.selectOneSafetyTargetSort(safetyTargetSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取管理目标分类
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getTargetSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getTargetSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return safetyTargetSortService.getTargetSortTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 获取管理目标详情
     *
     * @param safetyTarget
     * @return
     */
    @RequestMapping(value = "/getSafetyTargetById", method = RequestMethod.POST)
    public RetDataBean getSafetyTargetById(SafetyTarget safetyTarget) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyTargetService.selectOneSafetyTarget(safetyTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取急应物资列表
     *
     * @param pageParam
     * @param materialType
     * @param entId
     * @param codeNo
     * @return
     */
    @RequestMapping(value = "/getSafetyMaterialList", method = RequestMethod.POST)
    public RetDataBean getSafetyMaterialList(PageParam pageParam, String materialType, String entId, String codeNo) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.ent_id");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyMaterialService.getSafetyMaterialList(pageParam, materialType, entId, codeNo);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取应急物资详情
     *
     * @param safetyMaterial
     * @return
     */
    @RequestMapping(value = "/getSafetyMaterialById", method = RequestMethod.POST)
    public RetDataBean getSafetyMaterialById(SafetyMaterial safetyMaterial) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyMaterial.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyMaterialService.selectOneSafetyMaterial(safetyMaterial));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取安全日志详情
     *
     * @param pageParam
     * @param diaryType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyDiaryList", method = RequestMethod.POST)
    public RetDataBean getSafetyDiaryList(PageParam pageParam, String diaryType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyDiaryService.getSafetyDiaryList(pageParam, diaryType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取安全演练详情
     *
     * @param pageParam
     * @param drillType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyDrillList", method = RequestMethod.POST)
    public RetDataBean getSafetyDrillList(PageParam pageParam, String drillType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyDrillService.getSafetyDrillList(pageParam, drillType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取培训列表
     *
     * @param pageParam
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyTrainList", method = RequestMethod.POST)
    public RetDataBean getSafetyTrainList(PageParam pageParam, String trainType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyTrainService.getSafetyTrainList(pageParam, trainType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取重大事件列表
     *
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyEventList", method = RequestMethod.POST)
    public RetDataBean getSafetyEventList(PageParam pageParam, String eventType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("e.create_time");
            } else {
                pageParam.setSort("e." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyEventService.getSafetyEventList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取事件详情
     *
     * @param safetyEvent
     * @return
     */
    @RequestMapping(value = "/getSafetyEventById", method = RequestMethod.POST)
    public RetDataBean getSafetyEventById(SafetyEvent safetyEvent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyEventService.selectOneSafetyEvent(safetyEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取应急小组成员详情
     *
     * @param safetyTeamMember
     * @return
     */
    @RequestMapping(value = "/getSafetyTeamMemberById", method = RequestMethod.POST)
    public RetDataBean getSafetyTeamMemberById(SafetyTeamMember safetyTeamMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeamMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyTeamMemberService.selectOneSafetyTeamMember(safetyTeamMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取应急演练详情
     *
     * @param safetyDrill
     * @return
     */
    @RequestMapping(value = "/getSafetyDrillById", method = RequestMethod.POST)
    public RetDataBean getSafetyDrillById(SafetyDrill safetyDrill) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyDrill.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyDrillService.selectOneSafetyDrill(safetyDrill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取应急方案详情
     *
     * @param safetyPlan
     * @return
     */
    @RequestMapping(value = "/getSafetyPlanById", method = RequestMethod.POST)
    public RetDataBean getSafetyPlanById(SafetyPlan safetyPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyPlanService.selectOneSafetyPlan(safetyPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取应急方案
     *
     * @param pageParam
     * @param planType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyPlanList", method = RequestMethod.POST)
    public RetDataBean getSafetyPlanList(PageParam pageParam, String planType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyPlanService.getSafetyPlanList(pageParam, planType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取安全应急小组
     *
     * @param safetyTeam
     * @return
     */
    @RequestMapping(value = "/getSafetyTeamById", method = RequestMethod.POST)
    public RetDataBean getSafetyTeamById(SafetyTeam safetyTeam) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeam.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyTeamService.selectOneSafetyTeam(safetyTeam));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取应急小组列表
     *
     * @param pageParam
     * @param teamType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyTeamList", method = RequestMethod.POST)
    public RetDataBean getSafetyTeamList(PageParam pageParam, String teamType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyTeamService.getSafetyTeamList(pageParam, teamType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取培训详情
     *
     * @param safetyTrain
     * @return
     */
    @RequestMapping(value = "/getSafetyTrainById", method = RequestMethod.POST)
    public RetDataBean getSafetyTrainById(SafetyTrain safetyTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyTrainService.selectOneSafetyTrain(safetyTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取安全日志详情
     *
     * @param safetyDiary
     * @return
     */
    @RequestMapping(value = "/getSafetyDiaryById", method = RequestMethod.POST)
    public RetDataBean getSafetyDiaryById(SafetyDiary safetyDiary) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyDiary.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyDiaryService.selectOneSafetyDiary(safetyDiary));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取企业人员列表
     *
     * @param pageParam
     * @param post
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getEntMemberList", method = RequestMethod.POST)
    public RetDataBean getEntMemberList(PageParam pageParam, String post, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyEntMemberService.getEntMemberList(pageParam, post, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取企业人员详情
     *
     * @param safetyEntMember
     * @return
     */
    @RequestMapping(value = "/getSafetyEntMemberById", method = RequestMethod.POST)
    public RetDataBean getSafetyEntMemberById(SafetyEntMember safetyEntMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyEntMemberService.selectOneSafetyEntMember(safetyEntMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取证照管理列表
     *
     * @param pageParam
     * @param licenceType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getSafetyLicenceList", method = RequestMethod.POST)
    public RetDataBean getSafetyLicenceList(PageParam pageParam, String licenceType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.sort_no");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyLicenceService.getSafetyLicenceList(pageParam, licenceType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取企业证照详情
     *
     * @param safetyLicence
     * @return
     */
    @RequestMapping(value = "/getSafetyLicenceById", method = RequestMethod.POST)
    public RetDataBean getSafetyLicenceById(SafetyLicence safetyLicence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyLicenceService.selectOneSafetyLicence(safetyLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取企业列表
     *
     * @param pageParam
     * @param type
     * @return
     */
    @RequestMapping(value = "/getEntInfoList", method = RequestMethod.POST)
    public RetDataBean getEntInfoList(PageParam pageParam, String type) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.sort_no");
            } else {
                pageParam.setSort("s." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = safetyEntInfoService.getEntInfoList(pageParam, type);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按名称或法人获取企业名称列表
     *
     * @param search
     * @return
     */
    @RequestMapping(value = "/selectEntInfoByName", method = RequestMethod.POST)
    public RetDataBean selectEntInfoByName(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyEntInfoService.selectEntInfoByName(account.getOrgId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getEntInfoAllList", method = RequestMethod.POST)
    public RetDataBean getEntInfoAllList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyEntInfoService.getEntInfoAllList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取分类码列表
     *
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return
     */
    @RequestMapping(value = "/getCodeClassList", method = RequestMethod.POST)
    public RetDataBean getCodeClassList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "module";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyCodeClass.class);
            example.setOrderByClause(orderBy);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Example.Criteria criteria2 = example.createCriteria();
                criteria2.orLike("codeName", "%" + search + "%").orLike("module", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<SafetyCodeClass> pageInfo = safetyCodeClassService.getCodeClassList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取分类码详情
     *
     * @param safetyCodeClass
     * @return
     */
    @RequestMapping(value = "/getSafetyCodeClassById", method = RequestMethod.POST)
    public RetDataBean getSafetyCodeClassById(SafetyCodeClass safetyCodeClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyCodeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyCodeClassService.selectOneSafetyCodeClass(safetyCodeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取企业信息详情
     *
     * @param safetyEntInfo
     * @return
     */
    @RequestMapping(value = "/getSafetyEntInfoById", method = RequestMethod.POST)
    public RetDataBean getSafetyEntInfoById(SafetyEntInfo safetyEntInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyEntInfoService.selectOneSafetyEntInfo(safetyEntInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getCodeClassByModule", method = RequestMethod.POST)
    public RetDataBean getCodeClassByModule(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyCodeClassService.getCodeClassByModule(account.getOrgId(), module));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取监管参数详情
     *
     * @param safetyConfig
     * @return
     */
    @RequestMapping(value = "/getSafetyConfigById", method = RequestMethod.POST)
    public RetDataBean getSafetyConfigById(SafetyConfig safetyConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, safetyConfigService.selectOneSafetyConfig(safetyConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
