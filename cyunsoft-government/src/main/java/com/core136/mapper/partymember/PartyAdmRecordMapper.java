package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyAdmRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyAdmRecordMapper extends MyMapper<PartyAdmRecord> {
    /**
     * @param orgId
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdmRecordList
     * @Description:  获取行政任职记录列表
     */
    public List<Map<String, String>> getAdmRecordList(@Param(value = "orgId") String orgId, @Param(value = "postType") String postType, @Param(value = "admPost") String admPost,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyAdmRecordList
     * @Description:  获取个人行政任职记录列表
     */
    public List<Map<String, String>> getMyAdmRecordList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

}
