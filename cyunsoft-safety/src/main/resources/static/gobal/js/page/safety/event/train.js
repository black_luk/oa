$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    getSafetyCodeClass("trainType", "trainType");
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        createTrain();
    })
})

function createTrain() {
    if($("#title").val()=="")
    {
        layer.msg("培训标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/insertSafetyTrain",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            trainType: $("#trainType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinUser: $("#joinUser").val(),
            entName: $("#entName").val(),
            sponsor: $("#sponsor").val(),
            linkMan :$("#linkMan").val(),
            tel:$("#tel").val(),
            address: $("#address").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
