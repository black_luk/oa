var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};
$(function () {
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);// 默认展开第一级节点
    }
    query("0");
})

function zTreeOnClick(event, treeId, treeNode) {
    $('#myTable').bootstrapTable('destroy');
    if (treeNode.partyOrgId != "0") {
        $("#partyOrgId").html(treeNode.partyOrgName);
        $.ajax({
            url: "/ret/partyorgget/electionIsExist",
            type: "post",
            dataType: "json",
            data: {
                partyOrgId: treeNode.partyOrgId
            },
            success: function (data) {
                if (data.status == "200") {
                    if (data.list == undefined) {
                        $("#partyOrgId").html("");
                        $("#election").html("");
                        $("#electionTime").html("");
                        $("#createType").html("");
                        $("#endTime").html("");
                        $("#due").html("");
                        $("#actual").html("");
                    } else {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyElectionById",
                            type: "post",
                            dataType: "json",
                            data: {
                                rercordId: data.list.rercordId
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    for (var id in res.list) {
                                        if (id == "recordId") {
                                            electionRecordId = res.list[id];
                                        } else if (id == "partyOrgId") {
                                        } else if (id == "createType") {
                                            if (res.list[id] == "1") {
                                                $("#" + id).html("差额");
                                            } else if (res.list[id] == "2") {
                                                $("#" + id).html("等额");
                                            } else if (res.list[id] == "3") {
                                                $("#" + id).html("其他");
                                            }
                                        } else {
                                            $("#" + id).html(res.list[id]);
                                        }
                                    }
                                } else if (data.status == "100") {
                                    layer.msg(rs.msg);
                                } else {
                                    console.log(res.msg);
                                }

                            }
                        });
                    }
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }

            }
        });
        query(treeNode.partyOrgId);
    } else {
        layer.msg("请选择党组织机构！")
    }
}

function query(partyOrgId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partyorgget/getNowCommitteeList?partyOrgId=' + partyOrgId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'partyOrgName',
            title: '所在支部',
            width: '100px'
        }, {
            field: 'userName',
            title: '姓名',
            width: '100px'
        },

            {
                field: 'workFlag',
                width: '50px',
                title: '兼职标识',
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "兼职";
                    } else if (value == "2") {
                        return "专职";
                    } else {
                        return "未知";
                    }
                }
            }, {
                field: 'takeType',
                title: '任职方式',
                width: '100px',
                visible: false,
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "党委决定任职";
                    } else if (value == "2") {
                        return "差额选举";
                    } else if (value == "3") {
                        return "等额选举";
                    } else {
                        return "未知";
                    }

                }
            }, {
                field: 'govPost',
                width: '100px',
                visible: false,
                title: '职务名称'
            }, {
                field: 'workBeginTime',
                width: '100px',
                title: '任职起始日期'
            }, {
                field: 'workEndTime',
                width: '100px',
                title: '任职结束日期'
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

