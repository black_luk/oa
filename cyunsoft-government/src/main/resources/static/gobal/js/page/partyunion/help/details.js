$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionHelpById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "helpType") {
                        if (info[id] == "1") {
                            $("#" + id).html("疾病");
                        } else if (info[id] == "2") {
                            $("#" + id).html("单亲");
                        } else if (info[id] == "3") {
                            $("#" + id).html("低保");
                        }
                    } else if (id == "deptId") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "levelId") {
                        $("#" + id).html(getUserLevelStr(info[id]));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
