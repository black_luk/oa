package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionProblem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuperversionProblemMapper extends MyMapper<SuperversionProblem> {

    /**
     * 获取问题列表
     *
     * @param orgId
     * @param processId
     * @param accountId
     * @param search
     * @return
     */
    public List<Map<String, String>> getMySuperversionProblemList(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId,
                                                                  @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * 获取问题回复
     *
     * @param orgId
     * @param recordId
     * @param processId
     * @return
     */
    public List<Map<String, String>> getMySuperversionAnswerList(@Param(value = "orgId") String orgId, @Param(value = "recordId") String recordId, @Param(value = "processId") String processId);

}
