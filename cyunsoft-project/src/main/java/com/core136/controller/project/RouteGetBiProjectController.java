package com.core136.controller.project;

import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.project.EchartsProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/echartsprojectget")
public class RouteGetBiProjectController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private EchartsProjectService echartsProjectService;

    @Autowired
    public void setEchartsProjectService(EchartsProjectService echartsProjectService) {
        this.echartsProjectService = echartsProjectService;
    }

    /**
     * 项目费用类型统计
     *
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getProCostTypePie", method = RequestMethod.POST)
    public RetDataBean getProCostTypePie(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsProjectService.getProCostTypePie(account, proId));
        } catch (Exception e) {
            //e.printStackTrace();
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getProCostTypeBar", method = RequestMethod.POST)
    public RetDataBean getProCostTypeBar(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsProjectService.getProCostTypeBar(account, proId));
        } catch (Exception e) {
            //e.printStackTrace();
            return RetDataTools.Error(e.getMessage());
        }
    }


}
