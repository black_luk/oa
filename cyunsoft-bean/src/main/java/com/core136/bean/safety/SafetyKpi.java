package com.core136.bean.safety;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name="safety_kpi")
public class SafetyKpi implements Serializable {
    private String kpiId;
    private String sortNo;
    private String entId;
    private String kpiPlanId;
    private Double score;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getKpiId() {
        return kpiId;
    }

    public void setKpiId(String kpiId) {
        this.kpiId = kpiId;
    }

    public String getSortNo() {
        return sortNo;
    }

    public void setSortNo(String sortNo) {
        this.sortNo = sortNo;
    }

    public String getEntId() {
        return entId;
    }

    public void setEntId(String entId) {
        this.entId = entId;
    }

    public String getKpiPlanId() {
        return kpiPlanId;
    }

    public void setKpiPlanId(String kpiPlanId) {
        this.kpiPlanId = kpiPlanId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
