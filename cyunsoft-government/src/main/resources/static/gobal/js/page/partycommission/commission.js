$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 200});
    $(".js-setcomm").unbind("click").click(function () {
        $("#setcommmodal").modal("show");
        $(".js-setcommmodal").unbind("click").click(function () {
            addComm();
        })
    })
    getPartyCommission();
    $(".js-appointment").unbind("click").click(function () {
        var recordId = $("#recordId").val();
        if (recordId == "") {
            layer.msg("当前届期信息不存在，不可任命！")
            return;
        } else {
            appointment();
            $("#setappointment").modal("show");
            $(".js-appointmentmodal").unbind("click").click(function () {
                $.ajax({
                    url: "/set/partycommissionset/updatePartyCommission",
                    type: "post",
                    dataType: "json",
                    data: {
                        recordId: recordId,
                        memberId: $("#appMemberId").attr("data-value"),
                        secretary: $("#appSecretary").attr("data-value"),
                        secretaryOther: $("#appSecretaryOther").attr("data-value")
                    },
                    success: function (data) {
                        if (data.status == "500") {
                            console.log(data.msg);
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            layer.msg(sysmsg[data.msg]);
                            $("#setappointment").modal("hide");
                        }
                    }
                })
            })
        }
    })
})

function appointment() {
    $.ajax({
        url: "/ret/partycommissionget/getPartyCommission",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $("#appMemberId").attr("data-value", recordInfo[id]);
                        $("#appMemberId").val(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretary") {
                        $("#appSecretary").attr("data-value", recordInfo[id]);
                        $("#appSecretary").val(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretaryOther") {
                        $("#appSecretaryOther").attr("data-value", recordInfo[id]);
                        $("#appSecretaryOther").val(getPartyMemberName(recordInfo[id]));
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}


function getPartyCommission() {
    $.ajax({
        url: "/ret/partycommissionget/getPartyCommission",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $("#spanMemberId").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretary") {
                        $("#spanSecretary").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretaryOther") {
                        $("#spanSecretaryOther").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "beginTime") {
                        $("#spanBeinTime").html(recordInfo.beginTime + "&nbsp;&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;&nbsp;" + recordInfo.endTime)
                    } else if (id == "deadline") {
                        $("#spanDeadline").html(recordInfo[id]);
                    } else if (id == "recordId") {
                        $("#recordId").val(recordInfo.recordId)
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}

function addComm() {
    $.ajax({
        url: "/set/partycommissionset/insertPartyCommission",
        type: "post",
        dataType: "json",
        data: {
            deadline: $("#deadline").val(),
            createType: $("#createType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            secretary: $("#secretary").attr("data-value"),
            secretaryOther: $("#secretaryOther").attr("data-value"),
            memberId: $("#memberId").attr("data-value"),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setcommmodal").modal("hide");
            }
        }
    })
}
