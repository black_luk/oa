$(function () {
    $(".js-create").unbind("click").click(function () {
        $("#createfilemodal").modal("show")
        $("#show_attach").empty();
        $("#attach").attr("data_value", "");
        $("#attach").val("");
        $("#version").val("");
        $(".js-filesave").unbind("click").click(function () {
            insertFileMapRecord();
        })
    })
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    getSmsConfig("msgType", "fileMap");
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $("#myTable").bootstrapTable({
        url: '/ret/filemapget/getFileRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'titleName',
                title: '文档分类',
                width: '100px'
            },
            {
                field: 'fileName',
                width: '150px',
                title: '文件名称'
            },
            {
                field: 'oldName',
                width: '150px',
                title: '附件名称'
            },
            {
                field: 'extName',
                title: '文档格式',
                width: '50px'
            },
            {
                field: 'version',
                title: '当前版本',
                width: '100px'
            },
            {
                field: 'clickCount',
                width: '50px',
                title: '查阅次数'
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'fileStatus',
                width: '50px',
                title: '当前状态',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "生效中";
                    } else if (value == "1") {
                        return "已作废";
                    }
                }
            },
            {
                field: 'createUserName',
                title: '创建人',
                width: '100px'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '200px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.extName, row.attachId, row.recordId, row.fileStatus);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
})

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        itemId: itemId,
        fileStatus: $("#fileStatus").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function createOptBtn(extName, attachId, recordId, fileStatus) {
    let html = "";
    if (fileManagePriv > 0 || fileCreatePriv > 0) {
        html = "<a href=\"javascript:void(0);editRecord('" + recordId + "')\" class=\"btn btn-magenta btn-xs\" >编辑</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);delRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    }
    if (fileStatus == "0") {
        html += "<a href=\"javascript:void(0);updateRecord('" + recordId + "','1')\" class=\"btn btn-darkorange btn-xs\" >作废</a>&nbsp;&nbsp;";
    } else {
        html += "<a href=\"javascript:void(0);updateRecord('" + recordId + "','0')\" class=\"btn btn-success btn-xs\" >生效</a>&nbsp;&nbsp;";
    }
    html += "<a href=\"javascript:void(0);openFileOnLine('" + extName + "','" + attachId + "','1')\" class=\"btn btn-sky btn-xs\" >查看</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachId + "');\" class=\"btn btn-sky btn-xs\">预览 </a>";
    return html;
}

function editRecord(recordId) {
    $("#show_attach").empty();
    $("#attach").attr("data_value", "");
    $("#attach").val("");
    $("#version").val("");
    $("#fileName").val("");
    $("#createfilemodal").modal("show");
    $.ajax({
        url: "/ret/filemapget/getFileMapRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attachId") {
                        $("#attach").attr("data_value", info[id]);
                        createAttach("attach", 4);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-filesave").unbind("click").click(function () {
                    updateFileMapRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updateFileMapRecord(recordId) {
    if ($("#fileName").val() == "") {
        layer.msg("文件名称不能为空!");
        return;
    }
    if ($("#version").val() == "") {
        layer.msg("文件版本不能为空!");
        return;
    }
    $.ajax({
        url: "/set/filemapset/updateFileMapRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            attachId: $("#attach").attr("data_value"),
            version: $("#version").val(),
            fileName:$("#fileName").val(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#createfilemodal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function insertFileMapRecord() {
    if ($("#fileName").val() == "") {
        layer.msg("文件名称不能为空!");
        return;
    }
    if ($("#version").val() == "") {
        layer.msg("文件版本不能为空!");
        return;
    }
    $.ajax({
        url: "/set/filemapset/insertFileMapRecord",
        type: "post",
        dataType: "json",
        data: {
            itemId: itemId,
            sortNo: $("#sortNo").val(),
            attachId: $("#attach").attr("data_value"),
            version: $("#version").val(),
            fileName:$("#fileName").val(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#createfilemodal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updateRecord(recordId, fileStatus) {
    $.ajax({
        url: "/set/filemapset/updateFileMapRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            fileStatus: fileStatus
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function delRecord(recordId) {
    $.ajax({
        url: "/set/filemapset/deleteFileMapRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
