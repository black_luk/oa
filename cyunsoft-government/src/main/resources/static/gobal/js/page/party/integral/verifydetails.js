$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyIntegralById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        getMemberInfo(recordInfo[id]);
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
})

function getMemberInfo(memberId) {
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberById",
        type: "post",
        dataType: "json",
        data: {memberId: memberId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "userName") {
                        if (recordInfo[id] == "") {
                            $("#" + id).html(getUserNameByStr(recordInfo.accountId));
                        } else {
                            $("#" + id).html(recordInfo[id]);
                        }
                    } else if (id == "post") {
                        $("#" + id).html(getUserLevelStr(recordInfo[id]));
                    } else if (id == "education") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("小学");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("初中");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("高中");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("中专");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("大专");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("大学");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("硕士");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("博士");
                        }
                    } else if (id == "partyStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("正式党员");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("预备党员");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("已出党");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("已停止党籍");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("已死亡");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).html(orgName);
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
