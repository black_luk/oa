/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrDepartment.java
 * @Package com.core136.bean.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月29日 上午9:17:32
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author HR模块部门
 *
 */
@Table(name = "hr_department")
public class HrDepartment implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String deptId;
    private Integer sortNo;
    private String deptName;
    private String orgLevelId;
    private String levelId;
    private String deptTel;
    private String deptEmail;
    private String deptFax;
    private String deptLead;
    private String deptFunction;
    private String orgId;

    /**
     * @return the deptId
     */
    public String getDeptId() {
        return deptId;
    }

    /**
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the deptName
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getOrgLevelId() {
        return orgLevelId;
    }

    public void setOrgLevelId(String orgLevelId) {
        this.orgLevelId = orgLevelId;
    }

    /**
     * @return the deptTel
     */
    public String getDeptTel() {
        return deptTel;
    }

    /**
     * @param deptTel the deptTel to set
     */
    public void setDeptTel(String deptTel) {
        this.deptTel = deptTel;
    }

    /**
     * @return the deptEmail
     */
    public String getDeptEmail() {
        return deptEmail;
    }

    /**
     * @param deptEmail the deptEmail to set
     */
    public void setDeptEmail(String deptEmail) {
        this.deptEmail = deptEmail;
    }

    /**
     * @return the deptFax
     */
    public String getDeptFax() {
        return deptFax;
    }

    /**
     * @param deptFax the deptFax to set
     */
    public void setDeptFax(String deptFax) {
        this.deptFax = deptFax;
    }

    /**
     * @return the deptLead
     */
    public String getDeptLead() {
        return deptLead;
    }

    /**
     * @param deptLead the deptLead to set
     */
    public void setDeptLead(String deptLead) {
        this.deptLead = deptLead;
    }

    /**
     * @return the deptFunction
     */
    public String getDeptFunction() {
        return deptFunction;
    }

    /**
     * @param deptFunction the deptFunction to set
     */
    public void setDeptFunction(String deptFunction) {
        this.deptFunction = deptFunction;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }
}
