$(function () {
    $.ajax({
        url: "/ret/budgetget/getBudgetProjectById",
        type: "post",
        dataType: "json",
        data: {projectId: projectId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "chargeUser" || id == "joinUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "remark") {
                        $("#remark").html(recordInfo[id]);
                    } else if (id == "budgetAccount") {
                        $("#" + id).html(getBudgetAccountName(recordInfo[id]));
                    } else if (id == "projectType") {
                        $("#" + id).html(getBudgetTypeName(recordInfo[id]));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}


function getBudgetTypeName(budgetTypeId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetTypeById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetTypeId: budgetTypeId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}
