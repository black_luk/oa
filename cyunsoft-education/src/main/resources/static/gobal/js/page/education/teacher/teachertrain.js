$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm",
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    });
    $('#remark').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        insertEduTeacherTrain();
    })
})

function insertEduTeacherTrain() {
    $.ajax({
        url: "/set/eduset/insertEduTeacherTrain",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            courseType: $("#courseType").val(),
            title: $("#title").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            sponsor: $("#sponsor").val(),
            address: $("#address").val(),
            institutionUser: $("#institutionUser").val(),
            institutionContact: $("#institutionContact").val(),
            courseName: $("#courseName").val(),
            courseTime: $("#courseTime").val(),
            joinUser:$("#joinUser").val(),
            attach:$("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}