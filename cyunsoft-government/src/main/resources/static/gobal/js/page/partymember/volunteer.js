$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    jeDate("#joinEndTime", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    $('#remark').summernote({height: 300});
    getCodeClass("volType", "vol_type");
    $(".js-add-save").unbind("click").click(function () {
        addVolunteer();
    })
})

function addVolunteer() {
    if ($("#title").val()== "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyVolunteer",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            volType: $("#volType").val(),
            title: $("#title").val(),
            sendUser: $("#sendUser").val(),
            sendOrg: $("#sendOrg").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinEndTime: $("#joinEndTime").val(),
            address: $("#address").val(),
            linkMan: $("#linkMan").val(),
            linkPhone: $("#linkPhone").val(),
            mainPic: $("#file").attr("data-value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
