$(function () {
    getMyCleanExposeListForPortal();
    getMyCleanMeritoriousListForPortal();
    getMyCleanReportListForPortal();
    getMyCleanSuggestionsListForPortal();
    getMyCleanComprehensionListForPortal();
    getMyCleanTipoffsListForPortal();
})

function readDetails(recordId, type) {
    window.open("/app/core/party/details?type=" + type + "&recordId=" + recordId)
}

function readDetailsList(type) {
    window.open("/app/core/party/detailslist?type=" + type)
}

function getMyCleanExposeListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanExposeListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html1 = "";
                var html2 = "<div class=\"new-day\"><ul class=\"\">";
                for (var i = 0; i < recordInfo.length; i++) {
                    if (i == 0) {
                        html1 = "<div class=\"new-video\" style=\"float: left;\">\n" +
                            "<a class=\"thumbnail x\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','1')\" title=\"" + recordInfo[i].title + "\">\n" +
                            "<img src=\"/sys/file/getStaticImg?r=0.7412651790518516&module=party&fileName=" + recordInfo[i].mainPic + "\" alt=\"" + recordInfo[i].title + "\"> <span><em>" + recordInfo[i].title + "</em></span>\n" +
                            "</a>\n" +
                            "</div>";
                    } else {
                        html2 += "<li class=\"media\">\n" +
                            "<div class=\"media-left\">\n" +
                            "<a class=\"thumbnail y\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','1')\"> <img src=\"/sys/file/getStaticImg?r=0.7412651790518516&module=party&fileName=" + recordInfo[i].mainPic + "\" alt=\"" + recordInfo[i].title + "\"></a>\n" +
                            "</div>\n" +
                            "<div class=\"media-body\">\n" +
                            "<a class=\"ft16\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','1')\" title=\"" + recordInfo[i].title + "\">" + recordInfo[i].title + "</a> <span class=\"media-bottom c9\">" + recordInfo[i].subheading + "</span>\n" +
                            "</div>\n" +
                            "</li>";
                    }
                }
                html2 += "</ul></div>";
                $("#exposeList").html(html1 + html2);
            }
        }
    });

}

function getMyCleanMeritoriousListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanMeritoriousListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html1 = "";
                var html2 = "<div class=\"new-day\"><ul class=\"\">";
                for (var i = 0; i < recordInfo.length; i++) {
                    if (i == 0) {
                        html1 = "<div class=\"new-video\">\n" +
                            "<a class=\"thumbnail x\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','2')\" title=\"" + recordInfo[i].title + "\">\n" +
                            "<img src=\"/sys/file/getStaticImg?r=0.7412651790518516&module=party&fileName=" + recordInfo[i].mainPic + "\" alt=\"" + recordInfo[i].title + "\"> <span><em>" + recordInfo[i].title + "</em></span>\n" +
                            "</a>\n" +
                            "</div>";
                    } else {
                        html2 += "<li class=\"media\">\n" +
                            "<div class=\"media-left\">\n" +
                            "<a class=\"thumbnail y\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','2')\"> <img src=\"/sys/file/getStaticImg?r=0.7412651790518516&module=party&fileName=" + recordInfo[i].mainPic + "\" alt=\"" + recordInfo[i].title + "\"></a>\n" +
                            "</div>\n" +
                            "<div class=\"media-body\">\n" +
                            "<a class=\"ft16\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','2')\" title=\"" + recordInfo[i].title + "\">" + recordInfo[i].title + "</a> <span class=\"media-bottom c9\">" + recordInfo[i].subheading + "</span>\n" +
                            "</div>\n" +
                            "</li>";
                    }
                }
                html2 += "</ul></div>";
                $("#meritoriousList").html(html1 + html2);
            }
        }
    });
}


function getMyCleanReportListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanReportListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html = "";
                for (var i = 0; i < recordInfo.length; i++) {
                    html += "<li class=\"media\">\n" +
                        "<div class=\"media-left\"> <a class=\"thumbnail x\" href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','3')\">\n" +
                        "<img class=\"delay\" src=\"/sys/file/getStaticImg?r=0.7412651790518516&module=party&fileName=" + recordInfo[i].mainPic + "\" alt=\"" + recordInfo[i].title + "\"> </a> </div>\n" +
                        "<div class=\"media-body\">\n" +
                        "<h2><a class=\"ft18\"  href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','3')\">" + recordInfo[i].title + "</a></h2>\n" +
                        "<div class=\"info ft12\"> <span class=\"info-icon date\"><i>" + recordInfo[i].reportTime + "</i></span> <span class=\"info-icon tags\">" + recordInfo[i].pUserName + "</span> </div>\n" +
                        "<span class=\"media-bottom ft14 c6\">" + recordInfo[i].subheading + "</span> </div>\n" +
                        "</li>";
                }
                $("#reportList").html(html);
            }
        }
    });
}

function getMyCleanSuggestionsListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanSuggestionsListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html = "";
                for (var i = 0; i < recordInfo.length; i++) {
                    html += "<li>\n" +
                        "<span class=\"listnum1\">NO." + (i + 1) + "</span> <a href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','4')\" title=\"" + recordInfo[i].title + "\"><b>" + recordInfo[i].title + "</b>\n" +
                        "<p class=\"c9\">" + recordInfo[i].subheading + "</p>\n" +
                        "</a>\n" +
                        "</li>";
                }
                $("#suggestionsList").html(html);
            }
        }
    });
}

function getMyCleanComprehensionListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanComprehensionListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html = "";
                for (var i = 0; i < recordInfo.length; i++) {
                    html += "<li>\n" +
                        "<span class=\"listnum1\">NO." + (i + 1) + "</span> <a href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','5')\" title=\"" + recordInfo[i].title + "\"><b>" + recordInfo[i].title + "</b>\n" +
                        "<p class=\"c9\">" + recordInfo[i].subheading + "</p>\n" +
                        "</a>\n" +
                        "</li>";
                }
                $("#comprehensionList").html(html);
            }
        }
    });
}


function getMyCleanTipoffsListForPortal() {
    $.ajax({
        url: "/ret/partyget/getMyCleanTipoffsListForPortal",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                var html = "";
                for (var i = 0; i < recordInfo.length; i++) {
                    html += "<li>\n" +
                        "<span class=\"listnum1\">NO." + (i + 1) + "</span> <a href=\"#\" onclick=\"readDetails('" + recordInfo[i].recordId + "','6')\" title=\"" + recordInfo[i].title + "\"><b>" + recordInfo[i].title + "</b>\n" +
                        "<p class=\"c9\">" + recordInfo[i].subheading + "</p>\n" +
                        "</a>\n" +
                        "</li>";
                }
                $("#tipoffsList").html(html);
            }
        }
    });
}



