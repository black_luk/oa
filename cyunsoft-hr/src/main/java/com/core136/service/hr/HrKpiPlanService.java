package com.core136.service.hr;

import com.alibaba.fastjson.JSONArray;
import com.core136.bean.account.Account;
import com.core136.bean.account.UnitDept;
import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrKpiPlan;
import com.core136.bean.hr.HrKpiPlanItem;
import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrKpiPlanMapper;
import com.core136.service.account.UnitDeptService;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrKpiPlanService {
    private HrKpiPlanMapper hrKpiPlanMapper;

    @Autowired
    public void setHrKpiPlanMapper(HrKpiPlanMapper hrKpiPlanMapper) {
        this.hrKpiPlanMapper = hrKpiPlanMapper;
    }

    private HrKpiPlanItemService hrKpiPlanItemService;

    @Autowired
    public void setHrKpiPlanItemService(HrKpiPlanItemService hrKpiPlanItemService) {
        this.hrKpiPlanItemService = hrKpiPlanItemService;
    }

    private HrKpiPlanRecordService hrKpiPlanRecordService;

    @Autowired
    public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
        this.hrKpiPlanRecordService = hrKpiPlanRecordService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private UnitDeptService unitDeptService;

    @Autowired
    public void setUnitDeptService(UnitDeptService unitDeptService) {
        this.unitDeptService = unitDeptService;
    }

    public int insertHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.insert(hrKpiPlan);
    }

    public int deleteHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.delete(hrKpiPlan);
    }

    /**
     * @param example
     * @param hrKpiPlan
     * @param itemList
     * @return int
     * @Title: updateHrKpiPlanAndItem
     * @Description:  更新考核计划与考核指标集
     */
    @Transactional(value = "generalTM")
    public int updateHrKpiPlanAndItem(Example example, HrKpiPlan hrKpiPlan, String itemList) {
        HrKpiPlanItem hrKpiPlanItemdel = new HrKpiPlanItem();
        hrKpiPlanItemdel.setPlanId(hrKpiPlan.getPlanId());
        hrKpiPlanItemdel.setOrgId(hrKpiPlan.getOrgId());
        hrKpiPlanItemService.deleteHrKpiPlanItem(hrKpiPlanItemdel);
        JSONArray jsonArray = JSONArray.parseArray(itemList);
        for (int i = 0; i < jsonArray.size(); i++) {
            HrKpiPlanItem hrKpiPlanItem = new HrKpiPlanItem();
            hrKpiPlanItem.setRecordId(SysTools.getGUID());
            hrKpiPlanItem.setSortNo(jsonArray.getJSONObject(i).getInteger("sortNo"));
            hrKpiPlanItem.setPlanId(hrKpiPlan.getPlanId());
            hrKpiPlanItem.setItemId(jsonArray.getJSONObject(i).getString("itemId"));
            hrKpiPlanItem.setCreateTime(hrKpiPlan.getCreateTime());
            hrKpiPlanItem.setCreateUser(hrKpiPlan.getCreateUser());
            hrKpiPlanItem.setOrgId(hrKpiPlan.getOrgId());
            hrKpiPlanItemService.insertHrKpiPlanItem(hrKpiPlanItem);
        }
        return hrKpiPlanMapper.updateByExampleSelective(hrKpiPlan, example);
    }

    public int updateHrKpiPlan(Example example, HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.updateByExampleSelective(hrKpiPlan, example);
    }


    public HrKpiPlan selectOneHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.selectOne(hrKpiPlan);
    }

    /**
     * @param hrKpiPlan
     * @param itemList
     * @return int
     * @Title: addHrKpiPlan
     * @Description:  添加考核计划与考核指标
     */
    @Transactional(value = "generalTM")
    public int addHrKpiPlan(HrKpiPlan hrKpiPlan, String itemList) {
        JSONArray jsonArray = JSONArray.parseArray(itemList);
        for (int i = 0; i < jsonArray.size(); i++) {
            HrKpiPlanItem hrKpiPlanItem = new HrKpiPlanItem();
            hrKpiPlanItem.setRecordId(SysTools.getGUID());
            hrKpiPlanItem.setSortNo(jsonArray.getJSONObject(i).getInteger("sortNo"));
            hrKpiPlanItem.setPlanId(hrKpiPlan.getPlanId());
            hrKpiPlanItem.setItemId(jsonArray.getJSONObject(i).getString("itemId"));
            hrKpiPlanItem.setCreateTime(hrKpiPlan.getCreateTime());
            hrKpiPlanItem.setCreateUser(hrKpiPlan.getCreateUser());
            hrKpiPlanItem.setOrgId(hrKpiPlan.getOrgId());
            hrKpiPlanItemService.insertHrKpiPlanItem(hrKpiPlanItem);
        }
        return hrKpiPlanMapper.insert(hrKpiPlan);
    }

    /**
     * @param orgId
     * @param status
     * @param kpiRule
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyHrKpiPlanList
     * @Description:   获取考核管理列表
     */
    public List<Map<String, String>> getMyHrKpiPlanList(String orgId, String status, String kpiRule, String beginTime, String endTime, String search) {
        return hrKpiPlanMapper.getMyHrKpiPlanList(orgId, status, kpiRule, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param kpiRule
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrKpiPlanList
     * @Description:  获取考核管理列表
     */
    public PageInfo<Map<String, String>> getMyHrKpiPlanList(PageParam pageParam, String status, String kpiRule, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrKpiPlanList(pageParam.getOrgId(), status, kpiRule, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForUserList
     * @Description: 获取人员考核列表
     */
    public List<Map<String, String>> getKpiPlanForUserList(String orgId, String accountId, String search) {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        return hrKpiPlanMapper.getKpiPlanForUserList(orgId, accountId, nowTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getKpiPlanForUserList
     * @Description:  获取人员考核列表
     */
    public PageInfo<Map<String, String>> getKpiPlanForUserList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getKpiPlanForUserList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyKpiPlanForList
     * @Description:  获取个人考核列表
     */
    public List<Map<String, String>> getMyKpiPlanForList(String orgId, String accountId) {
        return hrKpiPlanMapper.getMyKpiPlanForList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyKpiPlanForList
     * @Description:  获取个人考核列表
     */
    public PageInfo<Map<String, String>> getMyKpiPlanForList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyKpiPlanForList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param planId
     * @param chargeUser
     * @param accountId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForUserQueryList
     * @Description:  查询获取考核列表
     */
    public List<Map<String, String>> getKpiPlanForUserQueryList(String orgId, String planId, String chargeUser, String accountId, String search) {
        return hrKpiPlanMapper.getKpiPlanForUserQueryList(orgId, planId, chargeUser, accountId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param planId
     * @param chargeUser
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getKpiPlanForUserQueryList
     * @Description:  查询获取考核列表
     */
    public PageInfo<Map<String, String>> getKpiPlanForUserQueryList(PageParam pageParam, String planId, String chargeUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getKpiPlanForUserQueryList(pageParam.getOrgId(), planId, chargeUser, pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * @param orgId
     * @param planId
     * @param accountId
     * @param chargeUser
     * @return List<Map < String, String>>
     * @Title: getKipItemForUserById
     * @Description:  获取人员考核指标集
     */
    public List<Map<String, String>> getKipItemForUserById(String orgId, String planId, String accountId, String chargeUser) {
        return hrKpiPlanMapper.getKipItemForUserById(orgId, planId, accountId, chargeUser);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForListForSelect
     * @Description:  获取考核计划列表
     */
    public List<Map<String, String>> getKpiPlanForListForSelect(String orgId) {
        return hrKpiPlanMapper.getKpiPlanForListForSelect(orgId);
    }


    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: updatePlanEffect
     * @Description:  考核计划生效
     */
    @Transactional(value = "generalTM")
    public RetDataBean updatePlanEffect(Account account, HrKpiPlan hrKpiPlan) {
        String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        String chargeUser = "";
        hrKpiPlan = selectOneHrKpiPlan(hrKpiPlan);
        List<Map<String, String>> kpiUserList = userInfoService.getAccountIdInPriv(hrKpiPlan.getOrgId(), hrKpiPlan.getUserPriv(), hrKpiPlan.getDeptPriv(), hrKpiPlan.getLevelPriv());
        if (kpiUserList != null) {
            if (hrKpiPlan.getKpiRule().equals("1")) {
                chargeUser = hrKpiPlan.getChargeUser();
                if (StringUtils.isBlank(chargeUser)) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }
            }
            HrKpiPlanItem hrKpiPlanItem = new HrKpiPlanItem();
            hrKpiPlanItem.setOrgId(hrKpiPlan.getOrgId());
            hrKpiPlanItem.setPlanId(hrKpiPlanItem.getPlanId());
            List<HrKpiPlanItem> itemList = hrKpiPlanItemService.getHrKpiPlanItemList(hrKpiPlanItem);
            for (int k = 0; k < kpiUserList.size(); k++) {
                String accountId = kpiUserList.get(k).get("accountId");
                if (hrKpiPlan.getKpiRule().equals("2")) {
                    //部门主管考核
                    UserInfo userInfo = userInfoService.getUserInfoByAccountId(accountId, hrKpiPlan.getOrgId());
                    UnitDept unitDept = new UnitDept();
                    unitDept.setOrgId(userInfo.getOrgId());
                    unitDept.setDeptId(userInfo.getDeptId());
                    unitDept = unitDeptService.selectOneUnitDept(unitDept);
                    chargeUser = unitDept.getDeptLead();
                } else if (hrKpiPlan.getKpiRule().equals("3")) {
                    //逐级考核
                    UserInfo userInfo = userInfoService.getUserInfoByAccountId(accountId, hrKpiPlan.getOrgId());
                    chargeUser = userInfo.getLeadId();
                }
                for (int i = 0; i < itemList.size(); i++) {
                    if (StringUtils.isNotBlank(chargeUser)) {
                        HrKpiPlanRecord hrKpiPlanRecord1 = new HrKpiPlanRecord();
                        hrKpiPlanRecord1.setAccountId(accountId);
                        hrKpiPlanRecord1.setPlanId(hrKpiPlan.getPlanId());
                        hrKpiPlanRecord1.setItemId(itemList.get(i).getItemId());
                        hrKpiPlanRecord1.setOrgId(hrKpiPlan.getOrgId());
                        int count = hrKpiPlanRecordService.getHrKpiPlanRecordConnt(hrKpiPlanRecord1);
                        if (count == 0) {
                            HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
                            hrKpiPlanRecord.setRecordId(SysTools.getGUID());
                            hrKpiPlanRecord.setAccountId(accountId);
                            hrKpiPlanRecord.setPlanId(itemList.get(i).getPlanId());
                            hrKpiPlanRecord.setItemId(itemList.get(i).getItemId());
                            hrKpiPlanRecord.setChargeUser(chargeUser);
                            hrKpiPlanRecord.setCreateTime(createTime);
                            hrKpiPlanRecord.setStatus("0");
                            hrKpiPlanRecord.setCreateUser(account.getAccountId());
                            hrKpiPlanRecord.setOrgId(hrKpiPlan.getOrgId());
                            hrKpiPlanRecordService.insertHrKpiPlanRecord(hrKpiPlanRecord);
                        }
                    }
                }
            }
            HrKpiPlan hrKpiPlanTemp = new HrKpiPlan();
            hrKpiPlanTemp.setOrgId(hrKpiPlan.getOrgId());
            hrKpiPlanTemp.setPlanId(hrKpiPlan.getPlanId());
            hrKpiPlanTemp.setStatus("1");
            Example example = new Example(HrKpiPlan.class);
            example.createCriteria().andEqualTo("orgId", hrKpiPlanTemp.getOrgId()).andEqualTo("planId", hrKpiPlanTemp.getPlanId());
            updateHrKpiPlan(example, hrKpiPlanTemp);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } else {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }
    }

}
