$(function () {
    $.ajax({
        url: "/ret/budgetget/getBudgetCostApplyById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "approvalUser" || id == "applyUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "projectId") {
                        $.ajax({
                            url: "/ret/budgetget/getBudgetAdjustmentById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {projectId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    $("#projectId").html(res.list.title);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "status") {
                        if (data.list.status == "0") {
                            $("#" + id).html("审批中");
                        } else if (data.list.status == "1") {
                            $("#" + id).html("通过");
                        } else if (data.list.status == "2") {
                            $("#" + id).html("未通过");
                        }

                    } else if (id == "budgetAccount") {
                        $("#" + id).html(getBudgetAccountName(recordInfo[id]));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}
