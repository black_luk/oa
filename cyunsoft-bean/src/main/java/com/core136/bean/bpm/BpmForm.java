package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BpmForm
 * @Description: Bpm表单实体类
 * @author: 刘绍全
 * @date: 2018年12月11日 下午8:16:43
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bpm_form")
public class BpmForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private String formId;
    private Integer sortNo;
    private String formTitle;
    private String tableName;
    private String dataLogFlag;
    private String bpmTypeId;
    private String htmlCode;
    private String mobileHtmlCode;
    private String mobileStyle;
    private String mobileScript;
    private String printHtml;
    private String printStyle;
    private String printScript;
    private String style;
    private String script;
    private String version;
    private String formType;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getBpmTypeId() {
        return bpmTypeId;
    }

    public void setBpmTypeId(String bpmTypeId) {
        this.bpmTypeId = bpmTypeId;
    }

    public String getHtmlCode() {
        return htmlCode;
    }

    public void setHtmlCode(String htmlCode) {
        this.htmlCode = htmlCode;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getMobileHtmlCode() {
        return mobileHtmlCode;
    }

    public void setMobileHtmlCode(String mobileHtmlCode) {
        this.mobileHtmlCode = mobileHtmlCode;
    }

    public String getMobileStyle() {
        return mobileStyle;
    }

    public void setMobileStyle(String mobileStyle) {
        this.mobileStyle = mobileStyle;
    }

    public String getMobileScript() {
        return mobileScript;
    }

    public void setMobileScript(String mobileScript) {
        this.mobileScript = mobileScript;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getPrintHtml() {
        return printHtml;
    }

    public void setPrintHtml(String printHtml) {
        this.printHtml = printHtml;
    }

    public String getPrintStyle() {
        return printStyle;
    }

    public void setPrintStyle(String printStyle) {
        this.printStyle = printStyle;
    }

    public String getPrintScript() {
        return printScript;
    }

    public void setPrintScript(String printScript) {
        this.printScript = printScript;
    }

    public String getDataLogFlag() {
        return dataLogFlag;
    }

    public void setDataLogFlag(String dataLogFlag) {
        this.dataLogFlag = dataLogFlag;
    }
}
