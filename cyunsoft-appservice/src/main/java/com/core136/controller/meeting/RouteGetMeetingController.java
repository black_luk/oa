/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetMeetingController.java
 * @Package com.core136.controller.meeting
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月22日 上午10:11:20
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.meeting;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.meeting.Meeting;
import com.core136.bean.meeting.MeetingDevice;
import com.core136.bean.meeting.MeetingNotes;
import com.core136.bean.meeting.MeetingRoom;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.meeting.MeetingDeviceService;
import com.core136.service.meeting.MeetingNotesService;
import com.core136.service.meeting.MeetingRoomService;
import com.core136.service.meeting.MeetingService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/ret/meetingget")
public class RouteGetMeetingController {
    private MeetingService meetingService;

    @Autowired
    public void setMeetingService(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    private MeetingRoomService meetingRoomService;

    @Autowired
    public void setMeetingRoomService(MeetingRoomService meetingRoomService) {
        this.meetingRoomService = meetingRoomService;
    }

    private MeetingDeviceService meetingDeviceService;

    @Autowired
    public void setMeetingDeviceService(MeetingDeviceService meetingDeviceService) {
        this.meetingDeviceService = meetingDeviceService;
    }

    private MeetingNotesService meetingNotesService;

    @Autowired
    public void setMeetingNotesService(MeetingNotesService meetingNotesService) {
        this.meetingNotesService = meetingNotesService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getMyMeetingListForDesk
     * @Description:   获取桌面会议
     */
    @RequestMapping(value = "/getMyMeetingListForDesk", method = RequestMethod.POST)
    public RetDataBean getMyMeetingListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMyMeetingListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param neetingNotes
     * @return RetDataBean
     * @Title: getMeetingNotesInfo
     * @Description:  获取会议记要详情
     */
    @RequestMapping(value = "/getMeetingNotesInfo", method = RequestMethod.POST)
    public RetDataBean getMeetingNotesInfo(MeetingNotes neetingNotes) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingNotesService.getMeetingNotesInfo(account.getOrgId(), neetingNotes.getNotesId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingNotesList
     * @Description:   获取会议记要列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: roomId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingNotesList", method = RequestMethod.POST)
    public RetDataBean getMeetingNotesList(PageParam pageParam,
                                           String beginTime,
                                           String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = meetingNotesService.getMeetingNotesList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: queryMeetingNotesList
     * @Description:  会议记要查询
     */
    @RequestMapping(value = "/queryMeetingNotesList", method = RequestMethod.POST)
    public RetDataBean queryMeetingNotesList(PageParam pageParam,
                                             String beginTime,
                                             String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = meetingNotesService.queryMeetingNotesList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getNotNotesMeetingList
     * @Description:  获取没有会议纪要的会议列表
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getNotNotesMeetingList", method = RequestMethod.POST)
    public RetDataBean getNotNotesMeetingList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getNotNotesMeetingList(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingRoomById
     * @Description:  获取会议室详情
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingRoomById", method = RequestMethod.POST)
    public RetDataBean getMeetingRoomById(MeetingRoom meetingRoom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingRoom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingRoomService.selectOneMeetingRoom(meetingRoom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getCanUseMeetingRoomList
     * @Description:  获取当前用户可用的会议室
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getCanUseMeetingRoomList", method = RequestMethod.POST)
    public RetDataBean getCanUseMeetingRoomList(String search) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingRoomService.getCanUseMeetingRoomList(userInfo.getOrgId(), userInfo.getDeptId(), search));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingRoomList
     * @Description:  会议室列表
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingRoomList", method = RequestMethod.POST)
    public RetDataBean getMeetingRoomList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = meetingRoomService.getMeetingRoomList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingDeviceById
     * @Description:  获取设备详情
     * @param: meetingDevice
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingDeviceById", method = RequestMethod.POST)
    public RetDataBean getMeetingRoomDeviceById(MeetingDevice meetingDevice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingDevice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.selectOneMeetingDevice(meetingDevice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deviceIds
     * @return RetDataBean
     * @Title: getDeviceListName
     * @Description:  获取设备名称列表
     */
    @RequestMapping(value = "/getDeviceListName", method = RequestMethod.POST)
    public RetDataBean getDeviceListName(String deviceIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getDeviceListName(account.getOrgId(), deviceIds));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getCanUseDeviceList
     * @Description:  获取权限内可用的会议室设备
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getCanUseDeviceList", method = RequestMethod.POST)
    public RetDataBean getCanUseDeviceList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            if (account.getOpFlag().equals("1")) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getCanUseDeviceList(userInfo.getOrgId(), null));
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getCanUseDeviceList(userInfo.getOrgId(), userInfo.getDeptId()));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingDeviceList
     * @Description:  获取会议设置列表
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingDeviceList", method = RequestMethod.POST)
    public RetDataBean getMeetingDeviceList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = meetingDeviceService.getMeetingDeviceList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingById
     * @Description:  获取会议详情
     * @param: meeting
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingById", method = RequestMethod.POST)
    public RetDataBean getMeetingById(Meeting meeting) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            meeting.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.selectOneMeeting(meeting));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingList
     * @Description:  获取个人发起的会议申请
     * @param: pageParam
     * @param: roomId
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingList", method = RequestMethod.POST)
    public RetDataBean getMeetingList(PageParam pageParam,
                                      String roomId, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = meetingService.getMeetingList(pageParam, roomId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getApplyMeetingList
     * @Description:  获取待审批的会议列表
     */
    @RequestMapping(value = "/getApplyMeetingList", method = RequestMethod.POST)
    public RetDataBean getApplyMeetingList(PageParam pageParam, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = meetingService.getApplyMeetingList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyMeetingList
     * @Description:  获取当前用户待参加会议
     */
    @RequestMapping(value = "/getMyMeetingList", method = RequestMethod.POST)
    public RetDataBean getMyMeetingList(PageParam pageParam, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            PageInfo<Map<String, String>> pageInfo = meetingService.getMyMeetingList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyMeetingOldList
     * @Description:  获取个人以往参加的会议记录
     */
    @RequestMapping(value = "/getMyMeetingOldList", method = RequestMethod.POST)
    public RetDataBean getMyMeetingOldList(PageParam pageParam, String beginTime, String endTime) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            PageInfo<Map<String, String>> pageInfo = meetingService.getMyMeetingOldList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getOldApplyMeetingList
     * @Description:  获取历史审批记录
     */
    @RequestMapping(value = "/getOldApplyMeetingList", method = RequestMethod.POST)
    public RetDataBean getOldApplyMeetingList(PageParam pageParam, String status, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = meetingService.getOldApplyMeetingList(pageParam, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param roomId
     * @param beginTime
     * @param endTime
     * @param chair
     * @return RetDataBean
     * @Title: getMyApplyMeetingList
     * @Description:  获取个人历史会议申请记录
     */
    @RequestMapping(value = "/getMyApplyMeetingList", method = RequestMethod.POST)
    public RetDataBean getMyApplyMeetingList(PageParam pageParam,
                                             String roomId, String beginTime, String endTime, String chair
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = meetingService.getMyApplyMeetingList(pageParam, chair, roomId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMeetingNotesById
     * @Description:  获取会议记要详情
     * @param: request
     * @param: meetingNotes
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMeetingNotesById", method = RequestMethod.POST)
    public RetDataBean getMeetingNotesById(MeetingNotes meetingNotes) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingNotes.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingNotesService.selectOneMeetingNotes(meetingNotes));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dayStr
     * @return RetDataBean
     * @Title: getMeetingByDay
     * @Description:  获取禁用的会议时间段
     */
    @RequestMapping(value = "/getMeetingByDay", method = RequestMethod.POST)
    public RetDataBean getMeetingByDay(String dayStr) {
        try {
            if (StringUtils.isBlank(dayStr)) {
                dayStr = SysTools.getTime("yyyy-MM-dd");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMeetingByDay(userInfo.getOrgId(), userInfo.getDeptId(), dayStr));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
