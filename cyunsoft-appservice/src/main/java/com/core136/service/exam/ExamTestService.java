package com.core136.service.exam;

import com.core136.bean.exam.ExamTest;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.exam.ExamTestMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExamTestService {
    private ExamTestMapper examTestMapper;

    @Autowired
    public void setExamTestMapper(ExamTestMapper examTestMapper) {
        this.examTestMapper = examTestMapper;
    }

    public int insertExamTest(ExamTest examTest) {
        return examTestMapper.insert(examTest);
    }

    public int deleteExamTest(ExamTest examTest) {
        return examTestMapper.delete(examTest);
    }

    public int updateExamTest(Example example, ExamTest examTest) {
        return examTestMapper.updateByExampleSelective(examTest, example);
    }

    public ExamTest selectOneExamTest(ExamTest examTest) {
        return examTestMapper.selectOne(examTest);
    }

    public List<Map<String, String>> getExamTestListForSelect(String orgId) {
        return examTestMapper.getExamTestListForSelect(orgId);
    }

    /**
     * 获取试卷管理列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamTestManageList(String orgId, String beginTime, String endTime, String search) {
        return examTestMapper.getExamTestManageList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取试卷管理列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getExamTestManageList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamTestManageList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取个人考试表表
     *
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param nowTime
     * @return
     */
    public List<Map<String, String>> getMyExamTestList(String orgId, String accountId, String deptId, String levelId, String nowTime) {
        return examTestMapper.getMyExamTestList(orgId, accountId, deptId, levelId, nowTime);
    }

    /**
     * 获取个人考试表表
     *
     * @param pageParam
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMyExamTestList(PageParam pageParam) throws Exception {
        String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyExamTestList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), nowTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
