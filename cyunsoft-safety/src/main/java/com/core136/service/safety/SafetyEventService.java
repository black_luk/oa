package com.core136.service.safety;

import com.core136.bean.safety.SafetyEvent;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyEventMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyEventService {
    private SafetyEventMapper safetyEventMapper;

    @Autowired
    public void setSafetyEventMapper(SafetyEventMapper safetyEventMapper) {
        this.safetyEventMapper = safetyEventMapper;
    }

    public int insertSafetyEvent(SafetyEvent safetyEvent) {
        return safetyEventMapper.insert(safetyEvent);
    }

    public int deleteSafetyEvent(SafetyEvent safetyEvent) {
        return safetyEventMapper.delete(safetyEvent);
    }

    public SafetyEvent selectOneSafetyEvent(SafetyEvent safetyEvent) {
        return safetyEventMapper.selectOne(safetyEvent);
    }

    public int updateSafetyEvent(Example example, SafetyEvent safetyEvent) {
        return safetyEventMapper.updateByExampleSelective(safetyEvent, example);
    }

    /**
     * 获取重大事件列表
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyEventList(String orgId, String eventType, String beginTime, String endTime, String search)
    {
        return safetyEventMapper.getSafetyEventList(orgId,eventType,beginTime,endTime,"%"+search+"%");
    }

    /**
     *  获取重大事件列表
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyEventList(PageParam pageParam, String eventType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyEventList(pageParam.getOrgId(), eventType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
