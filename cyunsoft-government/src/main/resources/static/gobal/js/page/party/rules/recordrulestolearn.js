$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyRulesRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var rulesInfo = data.list;
                for (var id in rulesInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", rulesInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "userPriv") {
                        $("#" + id).html(getUserNameByStr(rulesInfo[id]));
                    } else if (id == "deptPriv") {
                        $("#" + id).html(getDeptNameByDeptIds(rulesInfo[id]));
                    } else if (id == "levelPriv") {
                        $("#" + id).html(getUserLevelStr(rulesInfo[id]));
                    } else if (id == "sortId") {
                        $.ajax({
                            url: "/ret/partyget/getPartyRulesSortById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: rulesInfo[id]
                            },
                            success: function (data) {
                                if (data.status == "200") {
                                    if (data.list) {
                                        $("#sortId").html(data.list.sortName);
                                    } else {
                                        $("#sortId").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "remark") {
                        $("#remark").html(rulesInfo[id]);
                    } else {
                        $("#" + id).html(rulesInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyRulesRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
    $(".js-addlearn").unbind("click").click(function () {
        $("#learnModal").modal("show");
        $(".js-save").unbind("click").click(function () {
            addLearnRecord();
        })
    })
    getMyLearnRecordList();
});

function addLearnRecord() {
    $.ajax({
        url: "/set/partyset/insertPartyRulesLearn",
        type: "post",
        dataType: "json",
        data: {
            rulesRecordId: recordId,
            attach: $("#attach1").attr("data_value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

function getMyLearnRecordList() {
    $.ajax({
        url: "/ret/partyget/getMyLearnRecordList",
        type: "post",
        dataType: "json",
        data: {
            rulesRecordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var infolist = data.list;
                for (var i = 0; i < infolist.length; i++) {
                    $("#learnRecord").append("<div id=\"attachlearn" + i + "\" data_value=\"" + infolist[i].attach + "\" class=\"well bordered-left bordered-themeprimary\"><p>" + infolist[i].remark +
                        "<span style=\"folat:right;\"><a href=\"javascript:void(0);deleteRecord('" + infolist[i].recordId + "');\" class=\"btn btn-link\">删除</a><a href=\"javascript:void(0);editRecord('" + infolist[i].recordId + "');\" class=\"btn btn-link\">编辑</a></span></p>" +
                        "<div id=\"show_attachlearn" + i + "\"></div>" +
                        "</div>");
                    createAttach("attachlearn" + i, 1);
                }
            }
        }
    })
}

function editRecord(learnRecordId) {
    $.ajax({
        url: "/ret/partyget/getPartyRulesLearnById",
        type: "post",
        dataType: "json",
        data: {
            recordId: learnRecordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
                for (var id in data.list) {
                    if (id == "attach") {
                        $("#attach1").attr("data_value", data.list.attach);
                        createAttach("attach1", 4);
                    } else if (id == "remark") {
                        $("#remark").val(data.list.remark);
                    }
                }
            }
        }
    })
    $("#learnModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        updateLearnRecord(learnRecordId)
    })
}

function updateLearnRecord(learnRecordId) {
    $.ajax({
        url: "/set/partyset/updatePartyRulesLearn",
        type: "post",
        dataType: "json",
        data: {
            recordId: learnRecordId,
            attach: $("#attach1").attr("data_value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}


function deleteRecord(recordId) {
    if (confirm("确定删除当前学习记录吗？")) {
        $.ajax({
            url: "/set/partyset/deletePartyRulesLearn",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    window.location.reload();
                    layer.msg(sysmsg[data.msg]);
                }
            }
        })
    }
}


