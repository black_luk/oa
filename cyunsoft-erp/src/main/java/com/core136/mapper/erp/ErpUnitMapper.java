package com.core136.mapper.erp;

import com.core136.bean.erp.ErpUnit;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ErpUnitMapper extends MyMapper<ErpUnit> {

    /**
     * @Title: getAllUnit
     * @Description:  获取所有的计量单位
     * @param: orgId
     * @param: @return
     * @return: List<ErpUnit>
     */
    public List<ErpUnit> getAllUnit(@Param(value = "orgId") String orgId);


}
