package com.core136.service.vehicle;

import com.core136.bean.sys.PageParam;
import com.core136.bean.vehicle.VehicleApply;
import com.core136.bean.vehicle.VehicleInfo;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.vehicle.VehicleApplyMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleApplyService {

    private VehicleApplyMapper vehicleApplyMapper;

    @Autowired
    public void setVehicleApplyMapper(VehicleApplyMapper vehicleApplyMapper) {
        this.vehicleApplyMapper = vehicleApplyMapper;
    }

    private VehicleInfoService vehicleInfoService;

    @Autowired
    public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
        this.vehicleInfoService = vehicleInfoService;
    }

    public int insertVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.insert(vehicleApply);
    }

    public int deleteVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.delete(vehicleApply);
    }

    public int updateVehicleApply(Example example, VehicleApply vehicleApply) {
        return vehicleApplyMapper.updateByExampleSelective(vehicleApply, example);
    }

    public VehicleApply selectOneVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.selectOne(vehicleApply);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleApplyList
     * @Description:  获取申请记录
     */
    public List<Map<String, String>> getVehicleApplyList(String orgId, String opFlag, String status, String accountId, String createUser, String beginTime, String endTime, String search) {
        return vehicleApplyMapper.getVehicleApplyList(orgId, opFlag, status, accountId, createUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleApprovedList
     * @Description:  获取待审批列表
     */
    public List<Map<String, String>> getVehicleApprovedList(String orgId, String opFlag, String accountId, String createUser, String beginTime, String endTime, String search) {
        return vehicleApplyMapper.getVehicleApprovedList(orgId, opFlag, accountId, createUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVehicleApplyList
     * @Description:  获取申请记录
     */
    public PageInfo<Map<String, String>> getVehicleApplyList(PageParam pageParam, String status, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleApplyList(pageParam.getOrgId(), pageParam.getOpFlag(), status, pageParam.getAccountId(), createUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVehicleApprovedList
     * @Description:  获取待审批列表
     */
    public PageInfo<Map<String, String>> getVehicleApprovedList(PageParam pageParam, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleApprovedList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), createUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleReturnList
     * @Description:  获取待还车记录
     */
    public List<Map<String, String>> getVehicleReturnList(String orgId, String opFlag, String accountId, String createUser, String beginTime, String endTime, String search) {
        return vehicleApplyMapper.getVehicleReturnList(orgId, opFlag, accountId, createUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVehicleReturnList
     * @Description:  获取待还车记录
     */
    public PageInfo<Map<String, String>> getVehicleReturnList(PageParam pageParam, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleReturnList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), createUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleReturnOldList
     * @Description:  历史还车记录
     */
    public List<Map<String, String>> getVehicleReturnOldList(String orgId, String opFlag, String accountId, String createUser, String beginTime, String endTime, String search) {
        return vehicleApplyMapper.getVehicleReturnOldList(orgId, opFlag, accountId, createUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVehicleReturnOldList
     * @Description:  历史还车记录
     */
    public PageInfo<Map<String, String>> getVehicleReturnOldList(PageParam pageParam, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleReturnOldList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), createUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: setApprovalStatus
     * @Description:  调度员审批
     */
    @Transactional(value = "generalTM")
    public RetDataBean setApprovalStatus(VehicleApply vehicleApply) {
        if (vehicleApply.getStatus().equals("1")) {
            Example example = new Example(VehicleInfo.class);
            example.createCriteria().andEqualTo("orgId", vehicleApply.getOrgId()).andEqualTo("vehicleId", vehicleApply.getVehicleId());
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.setStatus("1");
            vehicleInfoService.updateVehicleInfo(example, vehicleInfo);
        }
        Example example = new Example(VehicleApply.class);
        example.createCriteria().andEqualTo("orgId", vehicleApply.getOrgId()).andEqualTo("applyId", vehicleApply.getApplyId());
        updateVehicleApply(example, vehicleApply);
        return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS);
    }


    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: setReturnVehicle
     * @Description:  车辆归还
     */
    @Transactional(value = "generalTM")
    public RetDataBean setReturnVehicle(VehicleApply vehicleApply) {
        VehicleApply vehicleApply1 = new VehicleApply();
        vehicleApply1.setOrgId(vehicleApply.getOrgId());
        vehicleApply1.setApplyId(vehicleApply.getApplyId());
        vehicleApply1 = selectOneVehicleApply(vehicleApply1);
        Example example1 = new Example(VehicleInfo.class);
        example1.createCriteria().andEqualTo("orgId", vehicleApply.getOrgId()).andEqualTo("vehicleId", vehicleApply1.getVehicleId());
        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.setStatus("0");
        vehicleInfoService.updateVehicleInfo(example1, vehicleInfo);
        Example example = new Example(VehicleApply.class);
        example.createCriteria().andEqualTo("orgId", vehicleApply.getOrgId()).andEqualTo("applyId", vehicleApply.getApplyId());
        updateVehicleApply(example, vehicleApply);
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
