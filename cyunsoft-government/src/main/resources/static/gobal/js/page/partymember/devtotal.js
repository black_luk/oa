$(function () {
    getBarPartyMemberJoinByStatus();
    getPartyMemberJoinToPartyOrgPie();
    getPartyJoinByMonthLine();
    getPartyJoinByYearLine();
})

function getPartyJoinByMonthLine() {
    $.ajax({
        url: "/ret/partychartsget/getPartyJoinByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main3'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getPartyMemberJoinToPartyOrgPie() {
    $.ajax({
        url: "/ret/partychartsget/getPartyMemberJoinToPartyOrgPie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main2'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBarPartyMemberJoinByStatus() {
    $.ajax({
        url: "/ret/partychartsget/getBarPartyMemberJoinByStatus",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getPartyJoinByYearLine() {
    $.ajax({
        url: "/ret/partychartsget/getPartyJoinByYearLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main4'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}
