package com.core136.service.education;

import com.core136.bean.education.EduStudentOrg;
import com.core136.mapper.education.EduStudentOrgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class EduStudentOrgService {
    private EduStudentOrgMapper eduStudentOrgMapper;

    @Autowired
    public void setEduStudentOrgMapper(EduStudentOrgMapper eduStudentOrgMapper) {
        this.eduStudentOrgMapper = eduStudentOrgMapper;
    }

    public int insertEduStudentOrg(EduStudentOrg eduStudentOrg) {
        return eduStudentOrgMapper.insert(eduStudentOrg);
    }

    public int deleteEduStudentOrg(EduStudentOrg eduStudentOrg) {
        return eduStudentOrgMapper.delete(eduStudentOrg);
    }

    public EduStudentOrg selectOneEduStudentOrg(EduStudentOrg eduStudentOrg) {
        return eduStudentOrgMapper.selectOne(eduStudentOrg);
    }

    public int updateEduStudentOrg(Example example, EduStudentOrg eduStudentOrg) {
        return eduStudentOrgMapper.updateByExampleSelective(eduStudentOrg, example);
    }

}
