package com.core136.controller.dataupload;

import com.core136.bean.account.UserInfo;
import com.core136.service.account.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/dataupload")
public class PageDataController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param request
     * @param view
     * @return ModelAndView
     * @Title: goQueryInfo
     * @Description:  上报信息查询
     */
    @RequestMapping("/queryinfo")
    @RequiresPermissions("/app/core/dataupload/queryinfo")
    public ModelAndView goQueryInfo(HttpServletRequest request, String view) {
        try {
            return new ModelAndView("app/core/dataupload/queryinfo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goHandle
     * @Description:  上报信息处理
     */
    @RequestMapping("/handle")
    @RequiresPermissions("/app/core/dataupload/handle")
    public ModelAndView goHandle(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/dataupload/handle");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/dataupload/handlemanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goUploadInfo
     * @Description:  信息上报管理
     */
    @RequestMapping("/uploadinfo")
    @RequiresPermissions("/app/core/dataupload/uploadinfo")
    public ModelAndView goUploadInfo(HttpServletRequest request, String view) {
        UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/dataupload/datauploadinfomanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/dataupload/datauploadinfo");
                }
            }
            mv.addObject("userInfo", userInfo);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param view
     * @return ModelAndView
     * @Title: goUploadInfoDetails
     * @Description:  信息上报详情
     */
    @RequestMapping("/uploadinfodetails")
    @RequiresPermissions("/app/core/dataupload/uploadinfodetails")
    public ModelAndView goUploadInfoDetails(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            return new ModelAndView("app/core/dataupload/datauploadinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
