$(function () {
    getConfigInfo();
    $(".js-setbtn").unbind("click").click(function () {
        setLeaderMailbox();
    })
})

function getConfigInfo() {
    $.ajax({
        url: "/ret/oaget/getLeaderMailboxConfigById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var info = data.list;
                for (var id in info) {
                    if (id == "isAnonymous") {
                        $("input:checkbox[name='isAnonymous'][value='" + info[id] + "']").attr("checked", "checked");
                    } else if (id == "leader") {
                        $("#leader").attr("data-value", info[id]);
                        $("#leader").val(getUserNameByStr(info[id]));
                    } else if (id == "userPriv") {
                        $("#userPriv").attr("data-value", info[id]);
                        $("#userPriv").val(getUserNameByStr(info[id]));
                    } else if (id == "deptPriv") {
                        $("#deptPriv").attr("data-value", info[id]);
                        $("#deptPriv").val(getDeptNameByDeptIds(info[id]));
                    } else if (id == "levelPriv") {
                        $("#levelPriv").attr("data-value", info[id]);
                        $("#levelPriv").val(getUserLevelStr(info[id]));
                    }
                }
            }
        }
    })
}

function setLeaderMailbox() {
    $.ajax({
        url: "/set/oaset/setLeaderMailboxConfig",
        type: "post",
        dataType: "json",
        data: {
            isAnonymous: $('input[name="isAnonymous"]:checked').val(),
            leader: $("#leader").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
