package com.core136.bean.safety;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "safety_config")
public class SafetyConfig implements Serializable {
    private String configId;
    private String licRemindFlag;//证照到期提醒标记
    private String licRemind;//证照到期提醒
    private Double licRemindDay;//证照提前天数
    private String licRemidUser;//证照到期提醒人员

    private String eventRemindFlag;//开启重大事件提醒标记
    private String eventRemind;//重大事件提醒
    private String eventRemidUser;//重大事件提醒人员


    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getLicRemindFlag() {
        return licRemindFlag;
    }

    public void setLicRemindFlag(String licRemindFlag) {
        this.licRemindFlag = licRemindFlag;
    }

    public String getLicRemind() {
        return licRemind;
    }

    public void setLicRemind(String licRemind) {
        this.licRemind = licRemind;
    }

    public Double getLicRemindDay() {
        return licRemindDay;
    }

    public void setLicRemindDay(Double licRemindDay) {
        this.licRemindDay = licRemindDay;
    }

    public String getLicRemidUser() {
        return licRemidUser;
    }

    public void setLicRemidUser(String licRemidUser) {
        this.licRemidUser = licRemidUser;
    }

    public String getEventRemindFlag() {
        return eventRemindFlag;
    }

    public void setEventRemindFlag(String eventRemindFlag) {
        this.eventRemindFlag = eventRemindFlag;
    }

    public String getEventRemind() {
        return eventRemind;
    }

    public void setEventRemind(String eventRemind) {
        this.eventRemind = eventRemind;
    }

    public String getEventRemidUser() {
        return eventRemidUser;
    }

    public void setEventRemidUser(String eventRemidUser) {
        this.eventRemidUser = eventRemidUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
