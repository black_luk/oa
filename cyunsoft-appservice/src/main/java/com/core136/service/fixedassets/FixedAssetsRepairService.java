package com.core136.service.fixedassets;

import com.core136.bean.fixedassets.FixedAssets;
import com.core136.bean.fixedassets.FixedAssetsRepair;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.fixedassets.FixedAssetsRepairMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class FixedAssetsRepairService {
    private FixedAssetsRepairMapper fixedAssetsRepairMapper;

    @Autowired
    public void setFixedAssetsRepairMapper(FixedAssetsRepairMapper fixedAssetsRepairMapper) {
        this.fixedAssetsRepairMapper = fixedAssetsRepairMapper;
    }

    private FixedAssetsService fixedAssetsService;

    @Autowired
    public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
        this.fixedAssetsService = fixedAssetsService;
    }

    public int insertFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.insert(fixedAssetsRepair);
    }

    public int deleteFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.delete(fixedAssetsRepair);
    }

    public int updateFixedAssetsRepair(Example example, FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.updateByExampleSelective(fixedAssetsRepair, example);
    }

    public FixedAssetsRepair selectOneFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.selectOne(fixedAssetsRepair);
    }

    /**
     * @param fixedAssetsRepair
     * @return RetDataBean
     * @Title: addFixedAssetsRepair
     * @Description:  添加修改记录
     */
    public RetDataBean addFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        FixedAssets fixedAssets = new FixedAssets();
        fixedAssets.setAssetsCode(fixedAssetsRepair.getAssetsCode());
        fixedAssets.setOrgId(fixedAssetsRepair.getOrgId());
        try {
            fixedAssets = fixedAssetsService.selectOneFixedAssets(fixedAssets);
        } catch (Exception e) {
            return RetDataTools.NotOk(MessageCode.MSG_00032);
        }
        if (fixedAssets == null) {
            return RetDataTools.NotOk(MessageCode.MSG_00032);
        } else {
            fixedAssetsRepair.setAssetsId(fixedAssets.getAssetsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertFixedAssetsRepair(fixedAssetsRepair));
        }
    }

    /**
     * @Title: getFixedAssetsRepairList
     * @Description:  获取维修列表
     * @param: orgId
     * @param: beginTime
     * @param: endTime
     * @param: assetsName
     * @param: assetsSortId
     * @param: status
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getFixedAssetsRepairList(String orgId, String accountId, String beginTime, String endTime, String opFlag, String assetsSortId, String status, String search) {
        return fixedAssetsRepairMapper.getFixedAssetsRepairList(orgId, accountId, beginTime, endTime, opFlag, assetsSortId, status, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getFixedAssetsRepairList
     * @Description:  获取维修列表
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: assetsName
     * @param: assetsSortId
     * @param: status
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getFixedAssetsRepairList(PageParam pageParam, String beginTime, String endTime, String assetsSortId, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsRepairList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, pageParam.getOpFlag(), assetsSortId, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
