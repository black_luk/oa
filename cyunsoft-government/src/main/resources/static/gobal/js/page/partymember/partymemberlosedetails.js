$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberLoseById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: info[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    }
                                }
                            }
                        });
                    } else if (id == "loseType") {
                        if (info[id] == "1") {
                            $("#" + id).html("6个月未能联络");
                        } else if (info[id] == "2") {
                            $("#" + id).html("不参加组织生活");
                        } else if (info[id] == "3") {
                            $("#" + id).html("不交纳党费");
                        } else if (info[id] == "4") {
                            $("#" + id).html("组织关系所在党组织失去联系");
                        }
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
