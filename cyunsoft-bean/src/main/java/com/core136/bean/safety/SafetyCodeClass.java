package com.core136.bean.safety;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "safety_code_class")
public class SafetyCodeClass implements Serializable {
    private static final long serialVersionUID = 1L;
    private String codeClassId;
    private Integer sortNo;
    private String codeValue;
    private String codeName;
    private String module;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getCodeClassId() {
        return codeClassId;
    }

    public void setCodeClassId(String codeClassId) {
        this.codeClassId = codeClassId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
