package com.core136.service.education;

import com.core136.bean.education.EduClassCode;
import com.core136.mapper.education.EduClassCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduClassCodeService {
    private EduClassCodeMapper eduClassCodeMapper;

    @Autowired
    public void setEduClassCodeMapper(EduClassCodeMapper eduClassCodeMapper) {
        this.eduClassCodeMapper = eduClassCodeMapper;
    }

    public int insertEduClassCode(EduClassCode eduClassCode) {
        return eduClassCodeMapper.insert(eduClassCode);
    }

    public int deleteEduClassCode(EduClassCode eduClassCode) {
        return eduClassCodeMapper.delete(eduClassCode);
    }

    public EduClassCode selectOneEduClassCode(EduClassCode eduClassCode) {
        return eduClassCodeMapper.selectOne(eduClassCode);
    }

    public int updateEduClassCode(Example example, EduClassCode eduClassCode) {
        return eduClassCodeMapper.updateByExampleSelective(eduClassCode, example);
    }


    /**
     * @param orgId
     * @param module
     * @return List<Map < String, String>>
     * @Title: getCodeListByModule
     * @Description:   获取下拉列表
     */
    public List<Map<String, String>> getCodeListByModule(String orgId, String module) {
        return eduClassCodeMapper.getCodeListByModule(orgId, module);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAllParentCodeList
     * @Description:  获取所有的主分类
     */
    public List<Map<String, String>> getAllParentCodeList(String orgId) {
        return eduClassCodeMapper.getAllParentCodeList(orgId);
    }

    /**
     * @param orgId
     * @param module
     * @param codeValue
     * @return List<Map < String, String>>
     * @Title: getHrClassCodeName
     * @Description:  获取分类码名称
     */
    public List<Map<String, String>> getEduClassCodeName(String orgId, String module, String codeValue) {
        return eduClassCodeMapper.getEduClassCodeName(orgId, module, codeValue);
    }

}
