$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM",
    });
    jeDate("#endTime", {
        format: "YYYY-MM"
    });
    $('#remark').summernote({height: 300});
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    });
    $(".js-add-save").unbind("click").click(function () {
        createTeachingExp();
    });
    $("#teacherId").select2({
        theme: "bootstrap",
        allowClear: true,
        placeholder: "请输入教师的姓名、手机号、身份证号",
        query: function (query) {
            var url = "/ret/eduget/getTeacherListForSelect2";
            var param = {search: query.term}; // 查询参数，query.term为用户在select2中的输入内容.
            var type = "json";
            var data = {results: []};
            $.post(
                url,
                param,
                function (datas) {
                    var datalist = datas.list;
                    for (var i = 0, len = datalist.length; i < len; i++) {
                        var info = datalist[i];
                        var option = {
                            "id": info.teacherId,
                            "text": info.userName
                        };
                        data.results.push(option);
                    }
                    query.callback(data);
                }, type);

        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        formatResult: function (data) {
            return '<div class="select2-user-result">' + data.text + '</div>'
        },
        formatSelection: function (data) {
            return data.text;
        },
        initSelection: function (data, cb) {
            cb(data);
        }
    });
})

function createTeachingExp() {
    $.ajax({
        url: "/set/eduset/insertEduTeacherExperience",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            teacherId: $("#teacherId").val(),
            courseType: $("#courseType").val(),
            grade: $("#grade").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            teachingSchool: $("#teachingSchool").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
