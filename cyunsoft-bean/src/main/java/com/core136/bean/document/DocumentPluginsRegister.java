/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmPluginsRegister.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2020年1月7日 上午11:19:32
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "document_plugins_register")
public class DocumentPluginsRegister implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String pluginsId;
    private String title;
    private Integer sortNo;
    private String packName;
    private String className;
    private String method;
    private String status;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the pluginsId
     */
    public String getPluginsId() {
        return pluginsId;
    }

    /**
     * @param pluginsId the pluginsId to set
     */
    public void setPluginsId(String pluginsId) {
        this.pluginsId = pluginsId;
    }

    /**
     * @return the packName
     */
    public String getPackName() {
        return packName;
    }

    /**
     * @param packName the packName to set
     */
    public void setPackName(String packName) {
        this.packName = packName;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }


}
