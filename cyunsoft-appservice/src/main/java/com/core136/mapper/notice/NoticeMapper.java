package com.core136.mapper.notice;

import com.core136.bean.notice.Notice;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface NoticeMapper extends MyMapper<Notice> {
    /**
     * @param @param  orgId
     * @param @param  opFlag
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getNoticeManageList
     * @Description:  获取通知公告列表
     */
    public List<Map<String, Object>> getNoticeManageList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "search") String search,
            @Param(value = "noticeType") String noticeType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime
    );

    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getNoticeApproverList
     * @Description:  获取审批列表
     */
    public List<Map<String, Object>> getNoticeApproverList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "search") String search
    );

    /**
     * @param @param  orgId
     * @param @param  noticeId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: setOnClickCount
     * @Description:  设置查看次数
     */
    public int setOnClickCount(@Param(value = "orgId") String orgId, @Param(value = "noticeId") String noticeId);

    /**
     * @param @param  orgId
     * @param @param  noticeId
     * @param @param  endTime
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isNoSendStatus
     * @Description:  判断是否有效
     */
    public int isNoSendStatus(@Param(value = "orgId") String orgId,
                              @Param(value = "noticeId") String noticeId,
                              @Param(value = "endTime") String endTime);

    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  deptId
     * @param @param  levelId
     * @param @param  noticeType
     * @param @param  beginTime
     * @param @param  endTime
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, String>> 返回类型
     * @Title: getMyNoticeList
     * @Description:  获取对应个人的通知公告
     */
    public List<Map<String, String>> getMyNoticeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId,
            @Param(value = "noticeType") String noticeType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "readStatus") String readStatus,
            @Param(value = "search") String search
    );

    /**
     * @Title: getMyNoticeListForDesk
     * @Description:  获取桌面的通知消息
     * @param: orgId
     * @param: endTime
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMyNoticeListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "endTime") String endTime,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId);

    /**
     * @Title: getMobileMyNoticeList
     * @Description:  移动端下滑加载更多
     * @param: orgId
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMobileMyNoticeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId,
            @Param(value = "page") Integer page);

    /***
     * 获取通知公告人员查看状态
     * @param orgId
     * @param newsId
     * @param list
     * @return
     */
    public List<Map<String, String>> getNoticeReadStatus(@Param(value = "orgId") String orgId, @Param(value = "newsId") String newsId, @Param(value = "list") List<String> list);

}
