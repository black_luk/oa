package com.core136.controller.partyrelation;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/partyrelation")
public class PagePartyRelationController {
    /**
     * @param view
     * @return ModelAndView
     * @Title: goInFlow 党员流入管理
     * @Description:
     */
    @RequestMapping("/inflow")
    @RequiresPermissions("/app/core/partyrelation/inflow")
    public ModelAndView goInFlow(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/inflowmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/inflow");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOutFlow   党员流出管理
     * @Description:
     */
    @RequestMapping("/outflow")
    @RequiresPermissions("/app/core/partyrelation/outflow")
    public ModelAndView goOutFlow(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/outflowmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/outflow");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRelIinDetails
     * @Description: 党员转入记录详情
     */
    @RequestMapping("/relindetails")
    @RequiresPermissions("/app/core/partyrelation/relindetails")
    public ModelAndView goRelIinDetails(String view) {
        try {
            return new ModelAndView("app/core/partyrelation/relindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goIntroduce
     * @Description:  介绍信
     */
    @RequestMapping("/introduce")
    @RequiresPermissions("/app/core/partyrelation/introduce")
    public ModelAndView goIntroduce(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/introducemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/introduce");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goIntroduceDetails
     * @Description:  介绍信打印预览
     */
    @RequestMapping("/introducedetails")
    @RequiresPermissions("/app/core/partyrelation/introducedetails")
    public ModelAndView goIntroduceDetails(String view) {
        try {
            return new ModelAndView("app/core/partyrelation/introducedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRelBetween
     * @Description:  支部间调转
     */
    @RequestMapping("/relbetween")
    @RequiresPermissions("/app/core/partyrelation/relbetween")
    public ModelAndView goRelBetween(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/relbetweenmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/relbetween");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRelIn
     * @Description:  关系转入
     */
    @RequestMapping("/relin")
    @RequiresPermissions("/app/core/partyrelation/relin")
    public ModelAndView goRelIn(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/relinmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/relin");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goRelOut
     * @Description:  组织关系转出
     */
    @RequestMapping("/relout")
    @RequiresPermissions("/app/core/partyrelation/relout")
    public ModelAndView goRelOut(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/reloutmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/relout");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRelAllOut
     * @Description:  整建制关系转出
     */
    @RequestMapping("/relallout")
    @RequiresPermissions("/app/core/partyrelation/relallout")
    public ModelAndView goRelAllOut(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyrelation/relalloutmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partyrelation/relallout");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }
}
