$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyCleanTipoffsById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                console.log(recordInfo);
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "tipoffsType") {
                        $("#tipoffsType").html(getCodeClassName(recordInfo[id], "party_tipoffs"));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
