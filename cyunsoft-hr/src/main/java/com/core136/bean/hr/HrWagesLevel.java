/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrWorkType.java
 * @Package com.core136.bean.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2020年1月6日 下午3:22:16
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *	工资级别
 */
@Table(name = "hr_wages_level")
public class HrWagesLevel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String wagesId;
    private Integer sortNo;
    private String title;
    private Double wages;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getWagesId() {
        return wagesId;
    }

    public void setWagesId(String wagesId) {
        this.wagesId = wagesId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getWages() {
        return wages;
    }

    public void setWages(Double wages) {
        this.wages = wages;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
