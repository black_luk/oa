package com.core136.bean.oa;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "diary_priv")
public class DiaryPriv implements Serializable {
    private static final long serialVersionUID = 1L;
    private String diaryPrivId;
    private Integer lockDay;
    private Integer shareStatus;
    private Integer commStatus;
    private String template;
    private String orgId;
    private String createTime;
    private String createUser;

    public String getDiaryPrivId() {
        return diaryPrivId;
    }

    public void setDiaryPrivId(String diaryPrivId) {
        this.diaryPrivId = diaryPrivId;
    }

    public Integer getLockDay() {
        return lockDay;
    }

    public void setLockDay(Integer lockDay) {
        this.lockDay = lockDay;
    }

    public Integer getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(Integer shareStatus) {
        this.shareStatus = shareStatus;
    }

    public Integer getCommStatus() {
        return commStatus;
    }

    public void setCommStatus(Integer commStatus) {
        this.commStatus = commStatus;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

}
