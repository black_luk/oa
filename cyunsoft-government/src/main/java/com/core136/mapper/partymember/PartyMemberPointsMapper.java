package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberPoints;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyMemberPointsMapper extends MyMapper<PartyMemberPoints> {
    /**
     * 获取党员汇总积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getMemberPointsTotalList(@Param(value = "orgId") String orgId, @Param(value = "pointsYear") String pointsYear, @Param(value = "memberId") String memberId);

    /**
     * 获取党组织积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getMemberPointsList(@Param(value = "orgId") String orgId, @Param(value = "pointsYear") String pointsYear, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param pointsYear
     * @param memberId
     * @return Map<String, Object>
     * @Title: getMyPointsByYear
     * @Description:  按年度获取党员个人积分总和
     */
    public Map<String, Object> getMyPointsByYear(@Param(value = "orgId") String orgId, @Param(value = "pointsYear") String pointsYear, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param memberId
     * @return Map<String, Object>
     * @Title: getMyPointsByAllYear
     * @Description:  获取党员历史积分总和
     */
    public Map<String, Object> getMyPointsByAllYear(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);
}
