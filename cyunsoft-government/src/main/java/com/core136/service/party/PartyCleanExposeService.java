package com.core136.service.party;

import com.core136.bean.party.PartyCleanExpose;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanExposeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanExposeService {


    private PartyCleanExposeMapper partyCleanExposeMapper;

    @Autowired
    public void setPartyCleanExposeMapper(PartyCleanExposeMapper partyCleanExposeMapper) {
        this.partyCleanExposeMapper = partyCleanExposeMapper;
    }

    public int insertPartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        return partyCleanExposeMapper.insert(partyCleanExpose);
    }

    public int deletePartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        return partyCleanExposeMapper.delete(partyCleanExpose);
    }

    public int updatePartyCleanExpose(Example example, PartyCleanExpose partyCleanExpose) {
        return partyCleanExposeMapper.updateByExampleSelective(partyCleanExpose, example);
    }

    public PartyCleanExpose selectOnePartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        return partyCleanExposeMapper.selectOne(partyCleanExpose);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param exposeType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanExposeList
     * @Description:  获取曝光台列表
     */
    public List<Map<String, String>> getCleanExposeList(String orgId, String opFlag, String accountId, String exposeType, String status, String beginTime, String endTime, String search) {
        return partyCleanExposeMapper.getCleanExposeList(orgId, opFlag, accountId, exposeType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanExposeList
     * @Description:  获取曝光台列表
     */
    public PageInfo<Map<String, String>> getCleanExposeList(PageParam pageParam, String exposeType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanExposeList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), exposeType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyCleanExposeListForPortal
     * @Description:  获取门户曝光记录列表
     */
    public List<Map<String, String>> getMyCleanExposeListForPortal(String orgId, String memberId) {
        return partyCleanExposeMapper.getMyCleanExposeListForPortal(orgId, memberId);
    }

    /**
     * @param orgId
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanExposeList
     * @Description:  获取更多曝光记录
     */
    public List<Map<String, String>> getMyCleanExposeList(String orgId, String memberId, String search) {
        return partyCleanExposeMapper.getMyCleanExposeList(orgId, memberId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanExposeList
     * @Description:  获取更多曝光记录
     */
    public PageInfo<Map<String, String>> getMyCleanExposeList(PageParam pageParam, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanExposeList(pageParam.getOrgId(), memberId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
