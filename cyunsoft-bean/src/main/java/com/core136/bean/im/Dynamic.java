/**
 * All rights Reserved, Designed By www.cyunsoft.com
 * @Title:  Dynamic.java
 * @Package com.core136.bean.im
 * @Description:    (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date:   2019年6月20日 上午8:42:06
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.im;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName:  Dynamic
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date:   2019年6月20日 上午8:42:06
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name="dynamic")
public class Dynamic implements Serializable{
	/**
	 * 人员动态
	 */
	private static final long serialVersionUID = 1L;
	private String dynamicId;
	private String accountId;
	private String attach;
	private String content;
	private String createTime;
	private String orgId;
	public String getDynamicId() {
		return dynamicId;
	}
	public void setDynamicId(String dynamicId) {
		this.dynamicId = dynamicId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


}
