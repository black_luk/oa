package com.core136.bean.calendar;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "calendar_type")
public class CalendarType implements Serializable {
    /**
     * 日程类型
     */
    private static final long serialVersionUID = 1L;
    private String calendarTypeId;
    private Integer sortNo;
    private String accountId;
    private String typeName;
    private String module;
    private String color;
    private String orgId;

    public String getCalendarTypeId() {
        return calendarTypeId;
    }

    public void setCalendarTypeId(String calendarTypeId) {
        this.calendarTypeId = calendarTypeId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

}
