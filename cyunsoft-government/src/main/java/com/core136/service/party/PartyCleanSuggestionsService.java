package com.core136.service.party;

import com.core136.bean.party.PartyCleanSuggestions;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanSuggestionsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanSuggestionsService {

    private PartyCleanSuggestionsMapper partyCleanSuggestionsMapper;

    @Autowired
    public void setPartyCleanSuggestionsMapper(PartyCleanSuggestionsMapper partyCleanSuggestionsMapper) {
        this.partyCleanSuggestionsMapper = partyCleanSuggestionsMapper;
    }

    public int insertPartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        return partyCleanSuggestionsMapper.insert(partyCleanSuggestions);
    }

    public int deletePartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        return partyCleanSuggestionsMapper.delete(partyCleanSuggestions);
    }

    public int updatePartyCleanSuggestions(Example example, PartyCleanSuggestions partyCleanSuggestions) {
        return partyCleanSuggestionsMapper.updateByExampleSelective(partyCleanSuggestions, example);
    }

    public PartyCleanSuggestions selectOnePartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        return partyCleanSuggestionsMapper.selectOne(partyCleanSuggestions);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param suggestionsType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanSuggestionsList
     * @Description:  获取计策列表
     */
    public List<Map<String, String>> getCleanSuggestionsList(String orgId, String opFlag, String accountId, String suggestionsType, String status, String beginTime, String endTime, String search) {
        return partyCleanSuggestionsMapper.getCleanSuggestionsList(orgId, opFlag, accountId, suggestionsType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param meritoriousType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanSuggestionsList
     * @Description:  获取计策列表
     */
    public PageInfo<Map<String, String>> getCleanSuggestionsList(PageParam pageParam, String suggestionsType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanSuggestionsList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), suggestionsType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanSuggestionsListForPortal
     * @Description:  获取门户计策列表
     */
    public List<Map<String, String>> getMyCleanSuggestionsListForPortal(String orgId) {
        return partyCleanSuggestionsMapper.getMyCleanSuggestionsListForPortal(orgId);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanSuggestionsList
     * @Description:  获取更多计策记录
     */
    public List<Map<String, String>> getMyCleanSuggestionsList(String orgId, String search) {
        return partyCleanSuggestionsMapper.getMyCleanSuggestionsList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanSuggestionsList
     * @Description:  获取更多计策记录
     */
    public PageInfo<Map<String, String>> getMyCleanSuggestionsList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanSuggestionsList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
