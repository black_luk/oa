$(function () {
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
    });
    jeDate("#workTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        addTeacherInfo();
    })
    getBaseNation();
    getAllBaseEducation();
})

function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}

function addTeacherInfo() {
    $.ajax({
        url: "/set/eduset/insertEduTeacher",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            headImg: $("#file").attr("data-value"),
            userName: $("#userName").val(),
            accountId: $("#accountId").attr("data-value"),
            sex: $("#sex").val(),
            cardId: $("#cardId").val(),
            birthday: $("#birthday").val(),
            nation: $("#nation").val(),
            mobile: $("#mobile").val(),
            major: $("#major").val(),
            teacherType: $("#teacherType").val(),
            workTime: $("#workTime").val(),
            graduateSchool: $("#graduateSchool").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getBaseNation() {
    $.ajax({
        url: "/ret/baseinfoget/getAllBaseNation",
        type: "post",
        dataType: "json",
        success: function (data) {
            $("#nation").append("<option value=''>请选择</option>");
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#nation").append("<option value='" + data.list[i].nationId + "'>" + data.list[i].nationName + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getAllBaseEducation() {
    $.ajax({
        url: "/ret/baseinfoget/getAllBaseEducation",
        type: "post",
        dataType: "json",
        success: function (data) {
            $("#education").append("<option value=''>请选择</option>");
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#education").append("<option value='" + data.list[i].educationId + "'>" + data.list[i].educationName + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
