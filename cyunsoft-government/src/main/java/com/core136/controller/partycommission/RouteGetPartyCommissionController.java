package com.core136.controller.partycommission;


import com.core136.bean.account.Account;
import com.core136.bean.partycommission.PartyBigEvent;
import com.core136.bean.partycommission.PartyBuildClean;
import com.core136.bean.partycommission.PartyCommission;
import com.core136.bean.partycommission.PartyEfficiency;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.partycommission.PartyBigEventService;
import com.core136.service.partycommission.PartyBuildCleanService;
import com.core136.service.partycommission.PartyCommissionService;
import com.core136.service.partycommission.PartyEfficiencyService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/ret/partycommissionget")
public class RouteGetPartyCommissionController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyCommissionService partyCommissionService;

    @Autowired
    public void setPartyCommissionService(PartyCommissionService partyCommissionService) {
        this.partyCommissionService = partyCommissionService;
    }

    private PartyBigEventService partyBigEventService;

    @Autowired
    public void setPartyBigEventService(PartyBigEventService partyBigEventService) {
        this.partyBigEventService = partyBigEventService;
    }

    private PartyEfficiencyService partyEfficiencyService;

    @Autowired
    public void setPartyEfficiencyService(PartyEfficiencyService partyEfficiencyService) {
        this.partyEfficiencyService = partyEfficiencyService;
    }

    private PartyBuildCleanService partyBuildCleanService;

    @Autowired
    public void setPartyBuildCleanService(PartyBuildCleanService partyBuildCleanService) {
        this.partyBuildCleanService = partyBuildCleanService;
    }

    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyBuildClean
     * @Description:  获取党风廉政记录列表
     */
    @RequestMapping(value = "/getPartyBuildCleanList", method = RequestMethod.POST)
    public RetDataBean getPartyBuildCleanList(PageParam pageParam, String eventType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.happen_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyBuildCleanService.getPartyBuildCleanList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyEfficiency
     * @Description:  效能监察工作记录列表
     */
    @RequestMapping(value = "/getPartyEfficiencyList", method = RequestMethod.POST)
    public RetDataBean getPartyEfficiencyList(PageParam pageParam, String eventType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.happen_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyEfficiencyService.getPartyEfficiencyList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyBigEventList
     * @Description:  获取重大事件记录列表
     */
    @RequestMapping(value = "/getPartyBigEventList", method = RequestMethod.POST)
    public RetDataBean getPartyBigEventList(PageParam pageParam, String eventType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.happen_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyBigEventService.getPartyBigEventList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyEfficiency
     * @return RetDataBean
     * @Title: getPartyEfficiencyById
     * @Description:  效能监察工作记录详情
     */
    @RequestMapping(value = "/getPartyEfficiencyById", method = RequestMethod.POST)
    public RetDataBean getPartyEfficiencyById(PartyEfficiency partyEfficiency) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEfficiency.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyEfficiencyService.selectOnePartyEfficiency(partyEfficiency));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBuildClean
     * @return RetDataBean
     * @Title: getPartyBuildCleanById
     * @Description:  党风廉政建设记录详情
     */
    @RequestMapping(value = "/getPartyBuildCleanById", method = RequestMethod.POST)
    public RetDataBean getPartyBuildCleanById(PartyBuildClean partyBuildClean) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBuildClean.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyBuildCleanService.selectOnePartyBuildClean(partyBuildClean));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getAllCommissionRecordList
     * @Description:  获取历史届次信息
     */
    @RequestMapping(value = "/getAllCommissionRecordList", method = RequestMethod.POST)
    public RetDataBean getAllCommissionRecordList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<PartyCommission> pageInfo = partyCommissionService.getAllCommissionRecordList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyCommission
     * @Description:  获取当前届次信息
     */
    @RequestMapping(value = "/getPartyCommission", method = RequestMethod.POST)
    public RetDataBean getPartyCommission() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCommissionService.getPartyCommission(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommission
     * @return RetDataBean
     * @Title: getPartyCommissionById
     * @Description:  获取纪委届次详情
     */
    @RequestMapping(value = "/getPartyCommissionById", method = RequestMethod.POST)
    public RetDataBean getPartyCommissionById(PartyCommission partyCommission) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommission.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCommissionService.selectOnePartyCommission(partyCommission));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBigEvent
     * @return RetDataBean
     * @Title: getPartyBigEventById
     * @Description:  获取重大事件详情
     */
    @RequestMapping(value = "/getPartyBigEventById", method = RequestMethod.POST)
    public RetDataBean getPartyBigEventById(PartyBigEvent partyBigEvent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBigEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyBigEventService.selectOnePartyBigEvent(partyBigEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
