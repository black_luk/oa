package com.core136.service.hr;

import com.core136.bean.hr.HrLeaveRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLeaveRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrLeaveRecordService {
    private HrLeaveRecordMapper hrLeaveRecordMapper;

    @Autowired
    public void setHrLeaveRecordMapper(HrLeaveRecordMapper hrLeaveRecordMapper) {
        this.hrLeaveRecordMapper = hrLeaveRecordMapper;
    }

    public int insertHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.insert(hrLevelRecord);
    }

    public int deleteHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.delete(hrLevelRecord);
    }

    public int updateHrLeaveRecord(Example example, HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.updateByExample(hrLevelRecord, example);
    }

    public HrLeaveRecord selectOneHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.selectOne(hrLevelRecord);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param levelType
     * @return List<Map < String, String>>
     * @Title: getHrLeaveRecordList
     * @Description:  获取离职人员列表
     */
    public List<Map<String, String>> getHrLeaveRecordList(String orgId, String userId, String beginTime, String endTime, String levelType) {
        return hrLeaveRecordMapper.getHrLeaveRecordList(orgId, userId, beginTime, endTime, levelType);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param levelType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrLeaveRecordList
     * @Description:  获取离职人员列表
     */
    public PageInfo<Map<String, String>> getHrLeaveRecordList(PageParam pageParam, String userId, String beginTime, String endTime, String levelType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrLeaveRecordList(pageParam.getOrgId(), userId, beginTime, endTime, levelType);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
