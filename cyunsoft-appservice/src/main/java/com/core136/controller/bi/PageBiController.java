package com.core136.controller.bi;

import com.core136.bean.account.Account;
import com.core136.bean.bi.BiTemplate;
import com.core136.service.account.AccountService;
import com.core136.service.bi.JasperreportsUnit;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/app/core")
public class PageBiController {
    private JasperreportsUnit jasperreportsUnit;

    @Autowired
    public void setJasperreportsUnit(JasperreportsUnit jasperreportsUnit) {
        this.jasperreportsUnit = jasperreportsUnit;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param biTemplate
     * @return ModelAndView
     * @Title: goGetreportpage
     * @Description:  查看报表
     */
    @RequestMapping("/getreportpage")
    @RequiresPermissions("/app/core/getreportpage")
    public ModelAndView goGetreportpage(BiTemplate biTemplate) {
        try {
            ModelAndView mv = new ModelAndView("app/core/bi/report/reportpage");
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setOrgId(account.getOrgId());
            mv.addObject("jascontent", jasperreportsUnit.getRepHtml(biTemplate));
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title gobisort
     * @Description  跳转报表BI分类设置
     */
    @RequestMapping("/bisort")
    @RequiresPermissions("/app/core/bisort")
    public ModelAndView gobisort() {
        try {
            return new ModelAndView("app/core/bi/set/bisort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 跳转报表BI设置
     *
     * @return ModelAndView
     * @Title gocreatebi
     * @Description (这里用一句话描述这个方法的作用)
     */
    @RequestMapping("/createbi")
    @RequiresPermissions("/app/core/createbi")
    public ModelAndView gocreatebi() {
        try {
            return new ModelAndView("app/core/bi/set/createbi");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBiread
     * @Description  报表查看
     */
    @RequestMapping("/biread")
    @RequiresPermissions("/app/core/biread")
    public ModelAndView goBiread() {
        try {
            return new ModelAndView("app/core/bi/report/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


}
