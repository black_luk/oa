package com.core136.controller.safety;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/safety")
public class PageSafetyController {

    /**
     * 监管机构管理
     * @param view
     * @return
     */
    @RequestMapping("/regulator")
    public ModelAndView goRegulator(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/manage/regulatormanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/manage/regulator");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 管理目标管理
     * @param view
     * @return
     */
    @RequestMapping("/target")
    public ModelAndView goTarget(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/manage/targetmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/manage/target");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 管理目标详情
     * @return
     */
    @RequestMapping("/targetdetails")
    public ModelAndView goTargetDetails() {
        try {
            return new ModelAndView("/app/core/safety/manage/targetdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 设置管理分类
     * @return
     */
    @RequestMapping("/targetsort")
    public ModelAndView goTargetSort() {
        try {
            return new ModelAndView("/app/core/safety/param/targetsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 应急物资管理
     * @param view
     * @return
     */
    @RequestMapping("/material")
    public ModelAndView goMaterial(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/emergency/materialmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/emergency/material");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 应急物资详情
     * @return
     */
    @RequestMapping("/materialdetails")
    public ModelAndView goMaterialDetails() {
        try {
            return new ModelAndView("/app/core/safety/emergency/materialdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 日常安全演练
     * @param view
     * @return
     */
    @RequestMapping("/drill")
    public ModelAndView goDrill(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/emergency/drillmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/emergency/drill");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 日常安全演练详情
     * @return
     */
    @RequestMapping("/drilldetails")
    public ModelAndView goDrillDetails() {
        try {
            return new ModelAndView("/app/core/safety/emergency/drilldetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 日常安全培训
     * @param view
     * @return
     */
    @RequestMapping("/train")
    public ModelAndView goTrain(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/event/trainmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/event/train");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 培训详情
     * @return
     */
    @RequestMapping("/traildetails")
    public ModelAndView goTrailDetails() {
        try {
            return new ModelAndView("/app/core/safety/event/traindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 日常安全日志
     *
     * @param view
     * @return
     */
    @RequestMapping("/eventdiary")
    public ModelAndView goEventDiary(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/event/eventdiarymanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/event/eventdiary");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 安全日志详情
     * @return
     */
    @RequestMapping("/eventdiarydetails")
    public ModelAndView goEventDiaryDetails() {
        try {
            return new ModelAndView("/app/core/safety/event/eventdiarydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 重大事件
     * @param view
     * @return
     */
    @RequestMapping("/entevent")
    public ModelAndView goEntEvent(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/event/eventmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/event/event");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 重大事件详情
     * @return
     */
    @RequestMapping("/enteventdetails")
    public ModelAndView goEntEventDetails() {
        try {
            return new ModelAndView("/app/core/safety/event/eventdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 应急小组成员
     * @param view
     * @return
     */
    @RequestMapping("/emergencyteam")
    public ModelAndView goeMergencyTeam(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/emergency/teammanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/emergency/team");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 急应小组详情
     * @return
     */
    @RequestMapping("/teamdetails")
    public ModelAndView goTeamDetails() {
        try {
            return new ModelAndView("/app/core/safety/emergency/teamdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
    /**
     * 应急方案管理
     * @param view
     * @return
     */
    @RequestMapping("/emergencyplan")
    public ModelAndView goeMergencyPlan(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/emergency/planmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/emergency/plan");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 应急方案详情
     * @return
     */
    @RequestMapping("/plandetails")
    public ModelAndView goPlanDeails() {
        try {
            return new ModelAndView("/app/core/safety/emergency/plandetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业人员管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/entmember")
    public ModelAndView goEntMember(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/ent/entmembermange");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/ent/entmember");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业人员管理详情
     *
     * @return
     */
    @RequestMapping("/entmemberdetails")
    public ModelAndView goEntMemberDetails() {
        try {
            return new ModelAndView("/app/core/safety/ent/entmemberdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业信息详情
     *
     * @return
     */
    @RequestMapping("/entinfodetails")
    public ModelAndView goEntInfoDetails() {
        try {
            return new ModelAndView("/app/core/safety/ent/baseinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 分类码设置
     *
     * @return
     */
    @RequestMapping("/codeclass")
    public ModelAndView goCodeclass() {
        try {
            return new ModelAndView("/app/core/safety/param/codeclass");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 企业信息档案
     *
     * @param view
     * @return
     */
    @RequestMapping("/entbaseinfo")
    public ModelAndView goEntBaseInfo(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/ent/baseinfomanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/ent/baseinfo");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业证照管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/entlicence")
    public ModelAndView goEntLicence(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/safety/ent/licencemanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/safety/ent/licence");
                } else {
                    return new ModelAndView("titps");
                }
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业证照详情
     *
     * @return
     */
    @RequestMapping("/licencedetails")
    public ModelAndView goLicenceDetails() {
        try {
            return new ModelAndView("/app/core/safety/ent/licencedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 企业档案详情
     *
     * @return
     */
    @RequestMapping("/ent/baseinfodeatils")
    public ModelAndView goBaseInfoDeatils() {
        try {
            return new ModelAndView("/app/core/safety/ent/baseinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
