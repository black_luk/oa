package com.core136.mapper.education;

import com.core136.bean.education.EduClassCode;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduClassCodeMapper extends MyMapper<EduClassCode> {
    /**
     * @param orgId
     * @param module
     * @return List<Map < String, String>>
     * @Title: getCodeListByModule
     * @Description:  获取下拉列表
     */
    public List<Map<String, String>> getCodeListByModule(@Param(value = "orgId") String orgId, @Param(value = "module") String module);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAllParentCodeList
     * @Description:  获取所有的主分类
     */
    public List<Map<String, String>> getAllParentCodeList(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param module
     * @param codeValue
     * @return List<Map < String, String>>
     * @Title: getHrClassCodeName
     * @Description:  获取分类码名称
     */
    public List<Map<String, String>> getEduClassCodeName(@Param(value = "orgId") String orgId, @Param(value = "module") String module, @Param(value = "codeValue") String codeValue);

}
