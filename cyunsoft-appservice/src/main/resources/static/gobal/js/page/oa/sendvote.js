let ue=UE.getEditor('remark');
$(function () {
    getCodeClass("voteType", "vote");
    getSmsConfig("msgType", "vote");
    jeDate("#startTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    $(".js-add-save").unbind("click").click(function () {
        sendVote();
    })
})

function sendVote() {
    if ($("#title").val() == "") {
        layer.msg("投票标题不能为空！");
        return;
    }
    if ($("#startTime").val() == "") {
        layer.msg("投票开始时间不能为空！");
        return;
    }
    if ($("#endTime").val() == "") {
        layer.msg("投票结束时间不能为空！");
        return;
    }
    $.ajax({
        url: "/set/oaset/insertVote",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            voteType: $("#voteType").val(),
            isAnonymous: $("#isAnonymous").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            remark: ue.getContent(),
            startTime: $("#startTime").val(),
            endTime: $("#endTime").val(),
            attach: $("#attach").attr("data_value"),
            isTop: $("input:radio[name='isTop']:checked").val(),
            readRes: $("input:radio[name='readRes']:checked").val(),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
