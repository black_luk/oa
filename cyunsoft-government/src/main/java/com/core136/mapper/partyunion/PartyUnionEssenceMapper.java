package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionEssence;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionEssenceMapper extends MyMapper<PartyUnionEssence> {
    /**
     * 获取职场精英列表
     *
     * @param orgId
     * @param essenceType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionEssenceList(@Param(value = "orgId") String orgId, @Param(value = "essenceType") String essenceType,
                                                              @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
