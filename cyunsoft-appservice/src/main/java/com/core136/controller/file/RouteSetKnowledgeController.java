package com.core136.controller.file;


import com.core136.bean.account.Account;
import com.core136.bean.file.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.file.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/knowledgeset")
public class RouteSetKnowledgeController {
    private KnowledgeSortService knowledgeSortService;

    @Autowired
    public void setKnowledgeSortService(KnowledgeSortService knowledgeSortService) {
        this.knowledgeSortService = knowledgeSortService;
    }

    private KnowledgeService knowledgeService;

    @Autowired
    public void setKnowledgeService(KnowledgeService knowledgeService) {
        this.knowledgeService = knowledgeService;
    }

    private KnowledgeLearnService knowledgeLearnService;

    @Autowired
    public void setKnowledgeLearnService(KnowledgeLearnService knowledgeLearnService) {
        this.knowledgeLearnService = knowledgeLearnService;
    }

    private KnowledgeSearchService knowledgeSearchService;

    @Autowired
    public void setKnowledgeSearchService(KnowledgeSearchService knowledgeSearchService) {
        this.knowledgeSearchService = knowledgeSearchService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private KnowledgeCommentService knowledgeCommentService;

    @Autowired
    public void setKnowledgeCommentService(KnowledgeCommentService knowledgeCommentService) {
        this.knowledgeCommentService = knowledgeCommentService;
    }

    /**
     * 删除查询记录
     *
     * @param knowledgeSearch
     * @return
     */
    @RequestMapping(value = "/deleteKnowledgeSearch", method = RequestMethod.POST)
    public RetDataBean deleteKnowledgeSearch(KnowledgeSearch knowledgeSearch) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(knowledgeSearch.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            knowledgeSearch.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, knowledgeSearchService.deleteKnowledgeSearch(knowledgeSearch));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledgeComment
     * @return RetDataBean
     * @Title: insertKnowledgeComment
     * @Description:  添加评论
     */
    @RequestMapping(value = "/insertKnowledgeComment", method = RequestMethod.POST)
    public RetDataBean insertKnowledgeComment(KnowledgeComment knowledgeComment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledgeComment.setCommentId(SysTools.getGUID());
            knowledgeComment.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            knowledgeComment.setCreateUser(account.getAccountId());
            knowledgeComment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, knowledgeCommentService.addKnowledgeComment(knowledgeComment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledgeComment
     * @return RetDataBean
     * @Title: deleteKnowledgeComment
     * @Description:  删除评论
     */
    @RequestMapping(value = "/deleteKnowledgeComment", method = RequestMethod.POST)
    public RetDataBean deleteKnowledgeComment(KnowledgeComment knowledgeComment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(knowledgeComment.getCommentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            knowledgeComment.setCreateUser(account.getAccountId());
            knowledgeComment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, knowledgeCommentService.deleteKnowledgeComment(knowledgeComment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledgeComment
     * @return RetDataBean
     * @Title: updateKnowledgeComment
     * @Description: 更新评论
     */
    @RequestMapping(value = "/updateKnowledgeComment", method = RequestMethod.POST)
    public RetDataBean updateKnowledgeComment(KnowledgeComment knowledgeComment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(knowledgeComment.getCommentId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(KnowledgeComment.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("commentId", knowledgeComment.getCommentId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, knowledgeCommentService.updateKnowledgeComment(example, knowledgeComment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertKnowledgeLearn
     * @Description:  创建学习记录
     * @param: request
     * @param: knowledgeLearn
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertKnowledgeLearn", method = RequestMethod.POST)
    public RetDataBean insertKnowledgeLearn(KnowledgeLearn knowledgeLearn) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            knowledgeLearn.setLearnId(SysTools.getGUID());
            knowledgeLearn.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            knowledgeLearn.setCreateUser(account.getAccountId());
            knowledgeLearn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, knowledgeLearnService.insertKnowledgeLearn(knowledgeLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertKnowledgeSort
     * @Description:  添加知识分类
     * @param: request
     * @param: knowledgeSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertKnowledgeSort", method = RequestMethod.POST)
    public RetDataBean insertKnowledgeSort(KnowledgeSort knowledgeSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(knowledgeSort.getSortLevel())) {
                knowledgeSort.setSortLevel("0");
            }
            knowledgeSort.setSortId(SysTools.getGUID());
            knowledgeSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            knowledgeSort.setCreateUser(account.getAccountId());
            knowledgeSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, knowledgeSortService.insertKnowledgeSort(knowledgeSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteKnowledgeSort
     * @Description:  删除知识分类
     * @param: request
     * @param: knowledgeSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteKnowledgeSort", method = RequestMethod.POST)
    public RetDataBean deleteKnowledgeSort(KnowledgeSort knowledgeSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(knowledgeSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            knowledgeSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, knowledgeSortService.deleteKnowledgeSort(knowledgeSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateKnowledgeSort
     * @Description:  更新知识分类
     * @param: request
     * @param: knowledgeSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateKnowledgeSort", method = RequestMethod.POST)
    public RetDataBean updateKnowledgeSort(KnowledgeSort knowledgeSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(knowledgeSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (knowledgeSort.getSortId().equals(knowledgeSort.getSortLevel())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            Example example = new Example(KnowledgeSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", knowledgeSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, knowledgeSortService.updateKnowledgeSort(example, knowledgeSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledge
     * @return RetDataBean
     * @Title: resetKnowledgeindex
     * @Description:   重建知识文档索引
     */
    @RequestMapping(value = "/resetKnowledgeindex", method = RequestMethod.POST)
    public RetDataBean resetKnowledgeindex(Knowledge knowledge) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledge.setOrgId(account.getOrgId());
            knowledgeService.resetKnowledgeindex(knowledge);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertKnowledge
     * @Description:  添加知识
     * @param: request
     * @param: knowledge
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertKnowledge", method = RequestMethod.POST)
    public RetDataBean insertKnowledge(Knowledge knowledge) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledge.setKnowledgeId(SysTools.getGUID());
            knowledge.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            knowledge.setCreateUser(account.getAccountId());
            knowledge.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, knowledgeService.addKnowledge(knowledge));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledge
     * @return RetDataBean
     * @Title: updateKnowledge
     * @Description:  更新知识
     */
    @RequestMapping(value = "/updateKnowledge", method = RequestMethod.POST)
    public RetDataBean updateKnowledge(Knowledge knowledge) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(knowledge.getKnowledgeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(Knowledge.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("knowledgeId", knowledge.getKnowledgeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, knowledgeService.updateKnowledge(example, knowledge));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledge
     * @return RetDataBean
     * @Title: deleteIndex
     * @Description:  删除知识文档的索引
     */
    @RequestMapping(value = "/deleteKnowledgeindex", method = RequestMethod.POST)
    public RetDataBean deleteKnowledgeindex(Knowledge knowledge) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                knowledge.setCreateUser(account.getAccountId());
            }
            if (StringUtils.isBlank(knowledge.getKnowledgeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            knowledge.setOrgId(account.getOrgId());
            knowledgeService.deleteIndexByAttachId(knowledge);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteKnowledge
     * @Description:  删除知识文档
     * @param: request
     * @param: knowledge
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteKnowledge", method = RequestMethod.POST)
    public RetDataBean deleteKnowledge(Knowledge knowledge) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                knowledge.setCreateUser(account.getAccountId());
            }
            if (StringUtils.isBlank(knowledge.getKnowledgeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            knowledge.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, knowledgeService.deleteKnowledgeAndIndex(knowledge));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
