package com.core136.mapper.project;

import com.core136.bean.project.ProTaskProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProTaskProcessMapper extends MyMapper<ProTaskProcess> {
    /**
     * 按任务ID获取处理过程列表
     *
     * @param orgId
     * @param taskId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProTaskProcessList(@Param(value = "orgId") String orgId, @Param(value = "taskId") String taskId, @Param(value = "search") String search);
}
