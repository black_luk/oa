$(function () {
    $('#content').summernote({height: 200});
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionActivity();
    })
    getCodeClass("activityType", "partyunion_activity");
})

function insertPartyUnionActivity() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionActivity",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            activityType: $("#activityType").val(),
            title: $("#title").val(),
            mainImg: $("#file").attr("data-value"),
            organizer: $("#organizer").val(),
            linkMan: $("#linkMan").val(),
            linkTel: $("#linkTel").val(),
            address: $("#address").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinUser: $("#joinUser").attr("data-value"),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
