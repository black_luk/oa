$(function () {
    getAlbumVideoForHomeTop();
    getSendVideoUserList();
    getVideoType();
    getSubscribeUserList();
})

function getVideoType() {

    $(".js-typelist").each(function () {
        var albumTypeId = $(this).attr("id");
        getAlbumVideoForHome(albumTypeId);
    })

}

function getAlbumVideoForHome(albumTypeId) {
    $.ajax({
        url: "/ret/partyget/getAlbumVideoForHome",
        type: "post",
        dataType: "json",
        data: {albumType: albumTypeId},
        success: function (data) {
            if (data.status == "200") {
                var topVideo = data.list;
                for (var i = 0; i < topVideo.length; i++) {
                    var tempHtml = ['<li>',
                        '                            <a href="/app/core/party/album/single-video?attachId=' + topVideo[i].attach + '&videoId=' + topVideo[i].videoId + '" class="video-post">',
                        '                                <!-- Blog Post Thumbnail -->',
                        '                                <div class="video-post-thumbnail">',
                        '                                    <span class="video-post-count">' + topVideo[i].fileSize + 'M</span>',
                        '                                    <span class="video-post-time">' + topVideo[i].videoDuration + '</span>',
                        '                                    <span class="play-btn-trigger"></span>',
                        '                                    <img src="/sys/file/getVideoPic?attachId=' + topVideo[i].attach + '&fileName=' + topVideo[i].pics + '" alt="">',
                        '                                </div>',
                        '                                <!-- Blog Post Content -->',
                        '                                <div class="video-post-content">',
                        '                                    <h3 style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"> ' + topVideo[i].title + '</h3>',
                        '                                    <img src="/sys/file/getOtherHeadImg?headImg=' + topVideo[i].headImg + '" alt="">',
                        '                                    <span class="video-post-user">' + topVideo[i].createUserName + '</span>',
                        '                                    <span class="video-post-views">播放:' + topVideo[i].clickCount + '次</span>',
                        '                                    <span class="video-post-date">' + getshowtime(topVideo[i].sendTime) + ' </span>',
                        '                                </div>',
                        '                            </a>',
                        '                        </li>'].join("");

                    $("#" + albumTypeId).append(tempHtml);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getSubscribeUserList() {
    $.ajax({
        url: "/ret/partyget/getSubscribeUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var userList = data.list;
                if (userList) {
                    for (var i = 0; i < userList.length; i++) {
                        var tempLate = ['<li> <a href="#"> <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                            '                                    <span>' + userList[i].userName + ' </span> <span class="dot-notiv"></span></a></li>'].join("");
                        $("#mySubscribeUserList").append(tempLate);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getAlbumVideoForHomeTop() {
    $.ajax({
        url: "/ret/partyget/getAlbumVideoForHomeTop",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var topVideo = data.list;
                for (var i = 0; i < topVideo.length; i++) {
                    var tempHtml = ['<li>',
                        '                            <a href="/app/core/party/album/single-video?attachId=' + topVideo[i].attach + '&videoId=' + topVideo[i].videoId + '" class="video-post">',
                        '                                <!-- Blog Post Thumbnail -->',
                        '                                <div class="video-post-thumbnail">',
                        '                                    <span class="video-post-count">' + topVideo[i].fileSize + 'M</span>',
                        '                                    <span class="video-post-time">' + topVideo[i].videoDuration + '</span>',
                        '                                    <span class="play-btn-trigger"></span>',
                        '                                    <img src="/sys/file/getVideoPic?attachId=' + topVideo[i].attach + '&fileName=' + topVideo[i].pics + '" alt="">',
                        '                                </div>',
                        '                                <!-- Blog Post Content -->',
                        '                                <div class="video-post-content">',
                        '                                    <h3 style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"> ' + topVideo[i].title + '</h3>',
                        '                                    <img src="/sys/file/getOtherHeadImg?headImg=' + topVideo[i].headImg + '" alt="">',
                        '                                    <span class="video-post-user">' + topVideo[i].createUserName + '</span>',
                        '                                    <span class="video-post-views">播放:' + topVideo[i].clickCount + '次</span>',
                        '                                    <span class="video-post-date">' + getshowtime(topVideo[i].sendTime) + ' </span>',
                        '                                </div>',
                        '                            </a>',
                        '                        </li>'].join("");

                    $("#topvedio").append(tempHtml);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getSendVideoUserList() {
    $.ajax({
        url: "/ret/partyget/getSendVideoUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var userList = data.list;

                for (var i = 0; i < userList.length; i++) {
                    var template = ['<li tabindex="-1" class="uk-active">',
                        '                                            <a href="#">',
                        '                                                </a><div class="single-channal"><a href="single-channal.html">',
                        '                                                    <div class="single-channal-creator">',
                        '                                                        <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                        '                                                    </div>',
                        '                                                    </a><div class="single-channal-body"><a href="single-channal.html">',
                        '                                                        <h4>' + userList[i].createUserName + '</h4>',
                        '                                                        <p> 发布视频数：' + userList[i].total + '次 </p>',
                        '                                                        </a><a href="javascript:void(0);" class="button primary small circle" style="background-color: white;" onclick="setSubscribeUser(\'' + userList[i].createUser + '\');"> 关注 </a>',
                        '                                                    </div>',
                        '                                                </div>',
                        '                                            ',
                        '                                        </li>'].join("");
                    $("#userList").append(template);
                }

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function setSubscribeUser(createUser) {
    $.ajax({
        url: "/set/partyset/setSubscribeUser",
        type: "post",
        dataType: "json",
        data: {subscribeUser: createUser},
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
