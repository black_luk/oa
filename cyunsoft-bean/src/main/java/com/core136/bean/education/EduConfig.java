package com.core136.bean.education;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 基本参数设置
 */
@Table(name="edu_config")
public class EduConfig implements Serializable {
    private String configId;
    private String eduType;//0:全部1:普教2:高职
    private String autoUp;//0:手动升学1:自动升学
    private String stuFlagWebSite;//0:关闭学生门户1:开启学生门户
    private String parFlagWebSite;//0:关闭家长门户1:开启家长门户
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getEduType() {
        return eduType;
    }

    public void setEduType(String eduType) {
        this.eduType = eduType;
    }

    public String getAutoUp() {
        return autoUp;
    }

    public void setAutoUp(String autoUp) {
        this.autoUp = autoUp;
    }

    public String getStuFlagWebSite() {
        return stuFlagWebSite;
    }

    public void setStuFlagWebSite(String stuFlagWebSite) {
        this.stuFlagWebSite = stuFlagWebSite;
    }

    public String getParFlagWebSite() {
        return parFlagWebSite;
    }

    public void setParFlagWebSite(String parFlagWebSite) {
        this.parFlagWebSite = parFlagWebSite;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
