/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: OaPageController.java
 * @Package com.core136.controller.oa
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午4:12:08
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.oa;

import com.core136.bean.account.Account;
import com.core136.bean.oa.LeaderMailbox;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.service.account.AccountService;
import com.core136.service.oa.LeaderMailboxService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lsq
 */
@Controller
@RequestMapping("/app/core")
public class PageOaController {
    private LeaderMailboxService leaderMailboxConfig;

    @Autowired
    public void setLeaderMailboxService(LeaderMailboxService leaderMailboxConfig) {
        this.leaderMailboxConfig = leaderMailboxConfig;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/oa/leadermailboxdetails")
    @RequiresPermissions("/app/core/oa/leadermailboxdetails")
    public ModelAndView getMyLeaderMailboxDetails(HttpServletRequest request, String recordId, String flag) {
        try {
            if (StringUtils.isNotBlank(flag)) {
                Account account = accountService.getRedisAUserInfoToAccount();
                LeaderMailbox leaderMailbox = new LeaderMailbox();
                leaderMailbox.setStatus("1");
                Example example = new Example(LeaderMailbox.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", recordId);
                leaderMailboxConfig.updateLeaderMailbox(example, leaderMailbox);
            }
            return new ModelAndView("app/core/leadermailbox/leadermailboxdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: getMyLeaderMailbox
     * @Description:  意见反馈
     */
    @RequestMapping("/oa/myleadermailbox")
    @RequiresPermissions("/app/core/oa/myleadermailbox")
    public ModelAndView getMyLeaderMailbox(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/leadermailbox/myleadermailbox");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: getLeaderMailbox
     * @Description:  领导信箱
     */
    @RequestMapping("/oa/leadermailbox")
    @RequiresPermissions("/app/core/oa/leadermailbox")
    public ModelAndView getLeaderMailbox(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/leadermailbox/leadermailbox");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: getLeaderMailboxConfig
     * @Description:  领导信箱设置
     */
    @RequestMapping("/oa/leadermailboxconfig")
    @RequiresPermissions("/app/core/oa/leadermailboxconfig")
    public ModelAndView getLeaderMailboxConfig(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/leadermailbox/leadermailboxconfig");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goAddressBook
     * @Description:  通讯录
     */
    @RequestMapping("/oa/addressbook")
    @RequiresPermissions("/app/core/oa/addressbook")
    public ModelAndView goAddressBook(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/addressbook/index");
            } else {
                if (view.equals("left")) {
                    mv = new ModelAndView("app/core/addressbook/left");
                } else if (view.equals("right")) {
                    mv = new ModelAndView("app/core/addressbook/right");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goBigStoryDetails
     * @Description:  大纪事详情
     */
    @RequestMapping("/oa/bigstorydetails")
    @RequiresPermissions("/app/core/oa/bigstorydetails")
    public ModelAndView goBigStoryDetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/bigstory/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goReadBigstory
     * @Description:  浏览大纪事
     */
    @RequestMapping("/oa/readbigsorty")
    @RequiresPermissions("/app/core/oa/readbigsorty")
    public ModelAndView goReadBigstory(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/bigstory/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param view
     * @return ModelAndView
     * @Title: goBigStory
     * @Description:  大记事管理
     */
    @RequestMapping("/oa/bigstory")
    @RequiresPermissions("/app/core/oa/bigstory")
    public ModelAndView goBigStory(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/bigstory/create");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/bigstory/manage");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("/app/core/bigstory/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param view
     * @return ModelAndView
     * @Title: goMyVote  我的投票
     * @Description:
     */
    @RequestMapping("/vote/myvote")
    @RequiresPermissions("/app/core/vote/myvote")
    public ModelAndView goMyVote(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/vote/myvote");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/vote/myvotemanage");
                } else if (view.equals("dovote")) {
                    mv = new ModelAndView("/app/core/vote/dovote");
                } else if (view.equals("readres")) {
                    mv = new ModelAndView("/app/core/vote/readresvote");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goSendVote  发布投票
     * @Description:
     */
    @RequestMapping("/vote/sendvote")
    @RequiresPermissions("/app/core/vote/sendvote")
    public ModelAndView goSendVote(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/vote/sendvote");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/vote/sendvotemanage");
                } else if (view.equals("item")) {
                    mv = new ModelAndView("/app/core/vote/voteitem");
                } else if (view.equals("manageitem")) {
                    mv = new ModelAndView("/app/core/vote/manageitem");
                } else if (view.equals("details")) {
                    mv = new ModelAndView("/app/core/vote/details");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goSetAttendConfig
     * @Description:  跳转到考勤设置
     * @param: request
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/attend/setconfig")
    @RequiresPermissions("/app/core/attend/setconfig")
    public ModelAndView goSetAttendConfig(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/attend/config");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goStatistics
     * @Description:  考勤汇总统计
     * @param: request
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/attend/statistics")
    @RequiresPermissions("/app/core/attend/statistics")
    public ModelAndView goStatistics(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/attend/statistics");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goMyattend
     * @Description:  我的考勤
     * @param: request
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/attend/myattend")
    @RequiresPermissions("/app/core/attend/myattend")
    public ModelAndView goMyattend(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/attend/myattend");
            } else {
                if (view.equals("mattend")) {
                    mv = new ModelAndView("app/core/attend/mattend");
                } else if (view.equals("outattend")) {
                    mv = new ModelAndView("app/core/attend/outattend");
                } else if (view.equals("leaveattend")) {
                    mv = new ModelAndView("app/core/attend/leaveattend");
                } else if (view.equals("travelattend")) {
                    mv = new ModelAndView("app/core/attend/travelattend");
                } else if (view.equals("travelattend")) {
                    mv = new ModelAndView("app/core/attend/travelattend");
                } else if (view.equals("overtimeattend")) {
                    mv = new ModelAndView("app/core/attend/overtimeattend");
                } else if (view.equals("dutyattend")) {
                    mv = new ModelAndView("app/core/attend/dutyattend");
                } else if (view.equals("queryattend")) {
                    mv = new ModelAndView("app/core/attend/queryattend");
                }
            }

            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
