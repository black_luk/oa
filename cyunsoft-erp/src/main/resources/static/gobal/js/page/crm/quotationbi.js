$(function () {
    getBiQuotationByDeptPie();
    getBiQuotationByProductPie();
    getBiQuotationByAccountPie();
    getBiQuotationByMonthLine();
});

function getBiQuotationByDeptPie() {
    $.ajax({
        url: "/ret/echartscrmget/getBiQuotationByDeptPie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBiQuotationByProductPie() {
    $.ajax({
        url: "/ret/echartscrmget/getBiQuotationByProductPie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main2'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBiQuotationByAccountPie() {
    $.ajax({
        url: "/ret/echartscrmget/getBiQuotationByAccountPie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main3'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBiQuotationByMonthLine() {
    $.ajax({
        url: "/ret/echartscrmget/getBiQuotationByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main4'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}
