$(function () {
    getAllBpmFlowListByManage();
    $(".js-setbtn").unbind("click").click(function () {
        setBudgetConfig();
    });
    initConfig();
})

function initConfig() {
    $.ajax({
        url: "/ret/budgetget/getBudgetConfigById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var config = data.list;
                for (var id in config) {
                    if (id == "costUpdateType") {
                        $("input:radio[name='costUpdateType'][value='" + config[id] + "']").prop("checked", "checked");
                    } else if (id == "costApplayType") {
                        $("input:radio[name='costApplayType'][value='" + config[id] + "']").prop("checked", "checked");
                    } else if (id == "adjustmentType") {
                        $("input:radio[name='adjustmentType'][value='" + config[id] + "']").prop("checked", "checked");
                    } else if (id == "costApprovalUser") {
                        $("#" + id).attr("data-value", config[id]);
                        $("#" + id).val(getUserNameByStr(config[id]));
                    } else if (id == "adjustmentApprovalUser") {
                        $("#" + id).attr("data-value", config[id]);
                        $("#" + id).val(getUserNameByStr(config[id]));
                    } else {
                        $("#" + id).val(config[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function setBudgetConfig() {
    $.ajax({
        url: "/set/budgetset/setBudgetConfig",
        type: "post",
        dataType: "json",
        data: {
            costUpdateType: $("input:radio[name='costUpdateType']:checked").val(),
            flowIdUpdate: $("#flowIdUpdate").val(),
            costApplayType: $("input:radio[name='costApplayType']:checked").val(),
            flowIdCost: $("#flowIdCost").val(),
            costApprovalUser: $("#costApprovalUser").attr("data-value"),
            adjustmentType: $("input:radio[name='adjustmentType']:checked").val(),
            adjustmentApprovalUser: $("#adjustmentApprovalUser").attr("data-value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getAllBpmFlowListByManage() {
    $.ajax({
        url: "/ret/bpmget/getAllBpmFlowListByManage",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                var html = "<option value=''>全部</option>"
                for (var i = 0; i < data.list.length; i++) {

                    html += "<option value='" + data.list[i].flowId + "'>" + data.list[i].flowName + "</option>"
                }
                $("#flowIdUpdate").html(html);
                $("#flowIdCost").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
