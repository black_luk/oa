package com.core136.mapper.project;

import com.core136.bean.project.ProTask;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProTaskMapper extends MyMapper<ProTask> {
    /**
     * 获取项目子任务列表
     *
     * @param orgId
     * @param proId
     * @return
     */
    public List<Map<String, String>> getProTaskList(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);

    /**
     * 获取个人待办任务列表
     *
     * @param orgId
     * @param accountId
     * @param proSort
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getMyTaskWorkList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "proSort") String proSort,
                                                       @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    public List<Map<String, String>> getTaskByProForSelect(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);

}
