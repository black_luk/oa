package com.core136.controller.echarts;

import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.echarts.EchartsFinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/echartsfinanceget")
public class RouteGetBiFinanceController {
    private EchartsFinanceService echartsFinanceService;

    @Autowired
    public void setEchartsFinanceService(EchartsFinanceService echartsFinanceService) {
        this.echartsFinanceService = echartsFinanceService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getARAPOptionConfig
     * @Description:  获取财务门户的收支
     */
    @RequestMapping(value = "/getARAPOptionConfig", method = RequestMethod.POST)
    public RetDataBean getARAPOptionConfig() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsFinanceService.getARAPOptionConfig(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getPayReceivTotalData
     * @Description:  获取应收应付总数
     */
    @RequestMapping(value = "/getPayReceivTotalData", method = RequestMethod.POST)
    public RetDataBean getPayReceivTotalData() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsFinanceService.getPayReceivTotalData(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
