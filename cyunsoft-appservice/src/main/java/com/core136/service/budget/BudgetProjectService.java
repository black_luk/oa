package com.core136.service.budget;

import com.core136.bean.budget.BudgetProject;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.budget.BudgetProjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetProjectService {
    private BudgetProjectMapper budgetProjectMapper;

    @Autowired
    public void setBudgetProjectMapper(BudgetProjectMapper budgetProjectMapper) {
        this.budgetProjectMapper = budgetProjectMapper;
    }

    public int insertBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.insert(budgetProject);
    }

    public int deleteBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.delete(budgetProject);
    }

    public int updateBudgetProject(Example example, BudgetProject budgetProject) {
        return budgetProjectMapper.updateByExampleSelective(budgetProject, example);
    }

    public BudgetProject selectOneBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.selectOne(budgetProject);
    }

    public int getCountChild(String orgId, String parentId) {
        BudgetProject budgetProject = new BudgetProject();
        budgetProject.setOrgId(orgId);
        budgetProject.setParentId(parentId);
        return budgetProjectMapper.selectCount(budgetProject);
    }

    public RetDataBean deleteAndCheckBudgetProject(BudgetProject budgetProject) {
        if (getCountChild(budgetProject.getOrgId(), budgetProject.getProjectId()) > 1) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
        } else {
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetProjectMapper.delete(budgetProject));
        }
    }


    /**
     * @param example
     * @param budgetProject
     * @return int
     * @Title: updateBudgetProjectAndParentId
     * @Description:  更新预算项目信息
     */
    public int updateBudgetProjectAndParentId(Example example, BudgetProject budgetProject) {
        if (budgetProject.getLevelId().equals("0")) {
            budgetProject.setParentId(budgetProject.getProjectId());
        } else {
            BudgetProject budgetProject1 = new BudgetProject();
            budgetProject1.setProjectId(budgetProject.getLevelId());
            budgetProject1.setOrgId(budgetProject.getOrgId());
            budgetProject1 = selectOneBudgetProject(budgetProject1);
            budgetProject.setParentId(budgetProject1.getParentId() + "/" + budgetProject.getProjectId());
        }
        return updateBudgetProject(example, budgetProject);
    }


    /**
     * @param budgetProject
     * @return int
     * @Title: addBudgetProject
     * @Description:  创建预算项目
     */
    public int addBudgetProject(BudgetProject budgetProject) {
        if (budgetProject.getLevelId().equals("0")) {
            budgetProject.setParentId(budgetProject.getProjectId());
        } else {
            BudgetProject budgetProject1 = new BudgetProject();
            budgetProject1.setProjectId(budgetProject.getLevelId());
            budgetProject1.setOrgId(budgetProject.getOrgId());
            budgetProject1 = selectOneBudgetProject(budgetProject1);
            budgetProject.setParentId(budgetProject1.getParentId() + "/" + budgetProject.getProjectId());
        }
        return insertBudgetProject(budgetProject);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getProjectTreeList
     * @Description:  获取项目列表
     */
    public List<Map<String, Object>> getProjectTreeList(String orgId, String levelId) {
        return budgetProjectMapper.getProjectTreeList(orgId, levelId);
    }

    /**
     * @param orgId
     * @param projectType
     * @param chargeUser
     * @param budgetAccount
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetProjectList
     * @Description:  获取项目列表
     */
    public List<Map<String, String>> getBudgetProjectList(String orgId, String projectType, String chargeUser, String budgetAccount, String opFlag, String accountId, String search) {
        return budgetProjectMapper.getBudgetProjectList(orgId, projectType, chargeUser, budgetAccount, opFlag, accountId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectType
     * @param chargeUser
     * @param budgetAccount
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetProjectList
     * @Description:  获取项目列表
     */
    public PageInfo<Map<String, String>> getBudgetProjectList(PageParam pageParam, String projectType, String chargeUser, String budgetAccount) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetProjectList(pageParam.getOrgId(), projectType, chargeUser, budgetAccount, pageParam.getOpFlag(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param parentId
     * @return List<Map < String, String>>
     * @Title: getBudgetChildProjectList
     * @Description:  获取子项目列表
     */
    public List<Map<String, String>> getBudgetChildProjectList(String orgId, String parentId) {
        return budgetProjectMapper.getBudgetChildProjectList(orgId, "%" + parentId + "%");
    }

    /**
     * @param pageParam
     * @param parendtId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetChildProjectList
     * @Description:  获取子项目列表
     */
    public PageInfo<Map<String, String>> getBudgetChildProjectList(PageParam pageParam, String parendtId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetChildProjectList(pageParam.getOrgId(), parendtId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getBudgetProjectListForProject
     * @Description:  获取主项目下拉列表
     */
    public List<Map<String, String>> getBudgetProjectListForProject(String orgId, String opFlag, String accountId) {
        return budgetProjectMapper.getBudgetProjectListForProject(orgId, opFlag, accountId);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param parentId
     * @return List<Map < String, String>>
     * @Title: getBudgetChildProjectListForProject
     * @Description:  获取子项目列表
     */
    public List<Map<String, String>> getBudgetChildProjectListForProject(String orgId, String opFlag, String accountId, String parentId) {
        return budgetProjectMapper.getBudgetChildProjectListForProject(orgId, opFlag, accountId, "%" + parentId + "%");
    }

}
