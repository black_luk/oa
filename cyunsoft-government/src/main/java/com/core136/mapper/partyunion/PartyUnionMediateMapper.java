package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionMediate;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionMediateMapper extends MyMapper<PartyUnionMediate> {

    /**
     * 获取调解事件列表
     *
     * @param orgId
     * @param mediateType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionMediateList(@Param(value = "orgId") String orgId, @Param(value = "mediateType") String mediateType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
