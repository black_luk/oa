package com.core136.service.exam;

import com.core136.bean.exam.ExamQuestions;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.exam.ExamQuestionsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ExamQuestionsService {


    private ExamQuestionsMapper examQuestionsMapper;

    @Autowired
    public void setExamQuestionsMapper(ExamQuestionsMapper examQuestionsMapper) {
        this.examQuestionsMapper = examQuestionsMapper;
    }

    public int insertExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.insert(examQuestions);
    }

    public int deleteExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.delete(examQuestions);
    }

    public int updateExamQuestions(Example example, ExamQuestions examQuestions) {
        return examQuestionsMapper.updateByExampleSelective(examQuestions, example);
    }

    public ExamQuestions selectOneExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.selectOne(examQuestions);
    }

    /**
     * 按分类获取试题分类
     *
     * @param orgId
     * @param sortId
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamQuestionsListBySortId(String orgId, String sortId, String search) {
        return examQuestionsMapper.getExamQuestionsListBySortId(orgId, sortId, "%" + search + "%");
    }

    /**
     * 按分类获取试题分类
     *
     * @param pageParam
     * @param sortId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getExamQuestionsListBySortId(PageParam pageParam, String sortId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamQuestionsListBySortId(pageParam.getOrgId(), sortId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 按分类获取试题列表
     *
     * @param orgId
     * @param sortId
     * @return
     */
    public List<Map<String, String>> getExamQuestListForSelectBySortId(String orgId, String sortId) {
        return examQuestionsMapper.getExamQuestListForSelectBySortId(orgId, sortId);
    }

    public List<ExamQuestions> getExamQuestionListByIds(String orgId, String recordIds) {
        if (StringUtils.isNotBlank(recordIds)) {
            String[] arr = recordIds.split(",");
            List<String> list = Arrays.asList(arr);
            Example example = new Example(ExamQuestions.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            List<ExamQuestions> examQuestionsList = examQuestionsMapper.selectByExample(example);
            List<ExamQuestions> retList = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < examQuestionsList.size(); j++) {
                    if (examQuestionsList.get(j).getRecordId().equals(arr[i])) {
                        retList.add(examQuestionsList.get(j));
                    }
                }
            }
            return retList;

        } else {
            return null;
        }
    }


}
