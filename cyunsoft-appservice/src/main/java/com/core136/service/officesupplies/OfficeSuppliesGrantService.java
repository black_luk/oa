package com.core136.service.officesupplies;

import com.core136.bean.officesupplies.OfficeSuppliesGrant;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.officesupplies.OfficeSuppliesGrantMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: OfficeSuppliesGrantService
 * @Description: 办公用品发放
 * @author: 稠云技术
 * @date: 2019年12月1日 下午9:29:21
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class OfficeSuppliesGrantService {
    private OfficeSuppliesGrantMapper officeSuppliesGrantMapper;

    @Autowired
    public void setOfficeSuppliesGrantMapper(OfficeSuppliesGrantMapper officeSuppliesGrantMapper) {
        this.officeSuppliesGrantMapper = officeSuppliesGrantMapper;
    }

    public int insertOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.insert(officeSuppliesGrant);
    }

    public int deleteOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.delete(officeSuppliesGrant);
    }

    public int updateOfficeSuppliesGrant(Example example, OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.updateByExampleSelective(officeSuppliesGrant, example);
    }

    public OfficeSuppliesGrant selectOneOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.selectOne(officeSuppliesGrant);
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getGrantOfficeList
     * @Description:  获取办公用品发放列表
     */
    public List<Map<String, String>> getGrantOfficeList(String orgId, String accountId, String beginTime, String endTime) {
        return officeSuppliesGrantMapper.getGrantOfficeList(orgId, accountId, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getGrantOfficeList
     * @Description:  获取办公用品发放列表
     */
    public PageInfo<Map<String, String>> getGrantOfficeList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getGrantOfficeList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getGrantCount
     * @Description:  统计已发放了多少办公用品
     * @param: orgId
     * @param: applyId
     * @param: @return
     * @return: int
     */
    public int getGrantCount(String orgId, String applyId) {
        try {
            return officeSuppliesGrantMapper.getGrantCount(orgId, applyId);
        } catch (Exception e) {
            return 0;
        }
    }

}
