package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionTraining;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionTrainingMapper extends MyMapper<PartyUnionTraining> {

    /**
     * 获取就业培训记录列表
     *
     * @param orgId
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionTrainingList(@Param(value = "orgId") String orgId, @Param(value = "trainType") String trainType,
                                                               @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                               @Param(value = "search") String search);


}
