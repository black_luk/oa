package com.core136.mapper.partyrelation;

import com.core136.bean.partyrelation.PartyRelOut;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRelOutMapper extends MyMapper<PartyRelOut> {

    /**
     * @param orgId
     * @param isInSys
     * @param outType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getRelOutList
     * @Description:  获取党员党组织关系转出记录
     */
    public List<Map<String, String>> getRelOutList(@Param(value = "orgId") String orgId, @Param(value = "isInSys") String isInSys,
                                                   @Param(value = "outType") String outType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                   @Param(value = "oldPartyOrgId") String oldPartyOrgId, @Param(value = "outPartyOrgId") String outPartyOrgId, @Param(value = "search") String search);

}
