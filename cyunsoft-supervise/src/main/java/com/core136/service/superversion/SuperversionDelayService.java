/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionDelayService.java
 * @Package com.core136.service.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月19日 下午5:43:19
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionDelay;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionDelayMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SuperversionDelayService
 * @Description: 督查事件延期申请
 * @author: 稠云技术
 * @date: 2020年4月9日 下午7:27:48
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class SuperversionDelayService {
    private SuperversionDelayMapper superversionDelayMapper;

    @Autowired
    public void setSuperversionDelayMapper(SuperversionDelayMapper superversionDelayMapper) {
        this.superversionDelayMapper = superversionDelayMapper;
    }

    public int insertSuperversionDelay(SuperversionDelay superversionDelay) {
        return superversionDelayMapper.insert(superversionDelay);
    }


    public int deleteSuperversionDelay(SuperversionDelay superversionDelay) {
        return superversionDelayMapper.delete(superversionDelay);
    }

    public int updateSuperversionDelay(Example example, SuperversionDelay superversionDelay) {
        return superversionDelayMapper.updateByExampleSelective(superversionDelay, example);
    }

    public SuperversionDelay selectOneSuperversionDelay(SuperversionDelay superversionDelay) {
        return superversionDelayMapper.selectOne(superversionDelay);
    }

    /**
     * @Title: getDelayApplyList
     * @Description:  获取延期审批列表
     * @param: orgId
     * @param: accountId
     * @param: beginTime
     * @param: endTime
     * @param: createUser
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getDelayApplyList(String orgId, String accountId, String beginTime, String endTime, String createUser, String search) {
        return superversionDelayMapper.getDelayApplyList(orgId, accountId, beginTime, endTime, createUser, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getDelayApplyList
     * @Description:  获取延期审批列表
     * @param: pageParam
     * @param: type
     * @param: beginTime
     * @param: endTime
     * @param: createUser
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getDelayApplyList(PageParam pageParam, String beginTime, String endTime, String createUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDelayApplyList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, createUser, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 延期审批历史记录
     *
     * @param orgId
     * @param accountId
     * @param status
     * @param beginTime
     * @param endTime
     * @param createUser
     * @param search
     * @return
     */
    public List<Map<String, String>> getDelayOldApplyList(String orgId, String accountId, String status, String beginTime, String endTime, String createUser, String search) {
        return superversionDelayMapper.getDelayOldApplyList(orgId, accountId, status, beginTime, endTime, createUser, "%" + search + "%");
    }

    /**
     * 延期审批历史记录
     *
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @param createUser
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getDelayOldApplyList(PageParam pageParam, String status, String beginTime, String endTime, String createUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDelayOldApplyList(pageParam.getOrgId(), pageParam.getAccountId(), status, beginTime, endTime, createUser, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
