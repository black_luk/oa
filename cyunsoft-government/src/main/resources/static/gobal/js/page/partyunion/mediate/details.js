$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionMediateById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "mediateModel") {
                        if (info[id] == "1") {
                            $("#" + id).html("约谈");
                        } else if (info[id] == "2") {
                            $("#" + id).html("双方协商");
                        } else if (info[id] == "3") {
                            $("#" + id).html("法院调解");
                        } else if (info[id] == "4") {
                            $("#" + id).html("人民调解");
                        } else if (info[id] == "5") {
                            $("#" + id).html("行政调解");
                        } else if (info[id] == "6") {
                            $("#" + id).html("仲裁调解");
                        }
                    } else if (id == "mediateType") {
                        $("#" + id).html(getCodeClassName(info[id], "partyunion_mediate"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
