package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyUnitBase;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyUnitBaseMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class PartyUnitBaseService {
    private PartyUnitBaseMapper partyUnitBaseMapper;

    @Autowired
    public void setPartyTemporgMapper(PartyUnitBaseMapper partyUnitBaseMapper) {
        this.partyUnitBaseMapper = partyUnitBaseMapper;
    }

    public int insertPartyUnitBase(PartyUnitBase partyUnitBase) {
        return partyUnitBaseMapper.insert(partyUnitBase);
    }

    public int deletePartyUnitBase(PartyUnitBase partyUnitBase) {
        return partyUnitBaseMapper.delete(partyUnitBase);
    }

    public int updatePartyUnitBase(Example example, PartyUnitBase partyUnitBase) {
        return partyUnitBaseMapper.updateByExampleSelective(partyUnitBase, example);
    }

    public PartyUnitBase selectOnePartyUnitBase(PartyUnitBase partyUnitBase) {
        return partyUnitBaseMapper.selectOne(partyUnitBase);
    }

    /**
     * @param orgId
     * @param unitIds
     * @return List<PartyUnitBase>
     * @Title: getPartyUnitBaseListByIds
     * @Description:  获取多个Id的单位信息
     */
    public List<PartyUnitBase> getPartyUnitBaseListByIds(String orgId, String unitIds) {
        if (StringUtils.isNotBlank(unitIds)) {
            String[] unitsArr = unitIds.split(",");
            List<String> list = Arrays.asList(unitsArr);
            Example example = new Example(PartyUnitBase.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("unitId", list);
            return partyUnitBaseMapper.selectByExample(example);
        } else {
            return null;
        }
    }


    /**
     * @param orgId
     * @param partyStatus
     * @param unitAffiliation
     * @param serviceIndustry
     * @param search
     * @return List<Map < String, String>>
     * @Title: getUnitBaseInfoList
     * @Description:  获取单位基本信息列表
     */
    public List<Map<String, String>> getUnitBaseInfoList(String orgId, String partyStatus, String unitAffiliation, String serviceIndustry, String search) {
        return partyUnitBaseMapper.getUnitBaseInfoList(orgId, partyStatus, unitAffiliation, serviceIndustry, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyStatus
     * @param unitAffiliation
     * @param serviceIndustry
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getUnitBaseInfoList
     * @Description:  获取单位基本信息列表
     */
    public PageInfo<Map<String, String>> getUnitBaseInfoList(PageParam pageParam, String partyStatus, String unitAffiliation, String serviceIndustry) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getUnitBaseInfoList(pageParam.getOrgId(), partyStatus, unitAffiliation, serviceIndustry, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getSelect2UnitBaseList
     * @Description:  获取select2下拉列表
     */
    public List<Map<String, String>> getSelect2UnitBaseList(String orgId, String search) {
        return partyUnitBaseMapper.getSelect2UnitBaseList(orgId, "%" + search + "%");
    }

}
