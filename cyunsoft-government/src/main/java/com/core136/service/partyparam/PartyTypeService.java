package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyType;
import com.core136.mapper.partyparam.PartyTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTypeService {
    private PartyTypeMapper partyTypeMapper;

    @Autowired
    public void setPartyTypeMapper(PartyTypeMapper partyTypeMapper) {
        this.partyTypeMapper = partyTypeMapper;
    }

    public int insertPartyType(PartyType partyType) {
        return partyTypeMapper.insert(partyType);
    }

    public int deletePartyType(PartyType partyType) {
        return partyTypeMapper.delete(partyType);
    }

    public int updatePartyType(Example example, PartyType partyType) {
        return partyTypeMapper.updateByExampleSelective(partyType, example);
    }

    public PartyType selectOnePartyType(PartyType partyType) {
        return partyTypeMapper.selectOne(partyType);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPartyTypeTree
     * @Description:  获取党组织类别树
     */
    public List<Map<String, String>> getPartyTypeTree(String orgId, String levelId) {
        return partyTypeMapper.getPartyTypeTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyTypeMapper.isExistChild(orgId, sortId);
    }

}
