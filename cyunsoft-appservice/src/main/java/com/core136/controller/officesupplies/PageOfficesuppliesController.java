package com.core136.controller.officesupplies;


import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/officesupplies")
public class PageOfficesuppliesController {

    /**
     * @return ModelAndView
     * @Title goSetSort
     * @Description  跳转办公用品分类管理页面
     */
    @RequestMapping("/setofficesupplies")
    @RequiresPermissions("/app/core/officesupplies/setofficesupplies")
    public ModelAndView goSetSort() {
        try {
            return new ModelAndView("app/core/officesupplies/sortofficesupplies");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goAddofficesupplies
     * @Description:  添加办公用品
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/addofficesupplies")
    @RequiresPermissions("/app/core/officesupplies/addofficesupplies")
    public ModelAndView goAddofficesupplies() {
        try {
            return new ModelAndView("app/core/officesupplies/addofficesupplies.html");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: setOfficesppliesunit
     * @Description: 设置办公用品的计量单位
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/setofficesppliesunit")
    @RequiresPermissions("/app/core/officesupplies/setofficesppliesunit")
    public ModelAndView setOfficesppliesunit() {
        try {
            return new ModelAndView("app/core/officesupplies/officesppliesunit");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goApplyofficesupplies
     * @Description:  办公领用申请
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/applyofficesupplies")
    @RequiresPermissions("/app/core/officesupplies/applyofficesupplies")
    public ModelAndView goApplyofficesupplies(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/officesupplies/applyofficesupplies");

            } else if (view.equals("myapplyofficesupplies")) {
                mv = new ModelAndView("app/core/officesupplies/myapplyofficesupplies");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goOfficesuppliesdetails
     * @Description:  办公用品详情
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/officesuppliesdetails")
    @RequiresPermissions("/app/core/officesupplies/officesuppliesdetails")
    public ModelAndView goOfficesuppliesdetails() {
        try {
            return new ModelAndView("app/core/officesupplies/officesuppliesdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @Title: goApprovalofficesupplies
     * @Description:  办公用品领用审批
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/approvalofficesupplies")
    @RequiresPermissions("/app/core/officesupplies/approvalofficesupplies")
    public ModelAndView goApprovalofficesupplies() {
        try {
            return new ModelAndView("app/core/officesupplies/approvalofficesupplies");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goManageofficesupplies
     * @Description:  办公用品统计
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/manageofficesupplies")
    public ModelAndView goManageofficesupplies() {
        try {
            return new ModelAndView("app/core/officesupplies/manageofficesupplies");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @Title: goGetofficesupplies
     * @Description:  办公用品发放
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/grantofficesupplies")
    @RequiresPermissions("/app/core/officesupplies/grantofficesupplies")
    public ModelAndView goGrantofficesupplies() {
        try {
            return new ModelAndView("app/core/officesupplies/grantofficesupplies");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
