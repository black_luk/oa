package com.core136.service.partycommission;

import com.core136.bean.partycommission.PartyBuildClean;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partycommission.PartyBuildCleanMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyBuildCleanService {
    private PartyBuildCleanMapper partyBuildCleanMapper;

    @Autowired
    public void setPartyBuildCleanMapper(PartyBuildCleanMapper partyBuildCleanMapper) {
        this.partyBuildCleanMapper = partyBuildCleanMapper;
    }

    public int insertPartyBuildClean(PartyBuildClean partyBuildClean) {
        return partyBuildCleanMapper.insert(partyBuildClean);
    }

    public int deletePartyBuildClean(PartyBuildClean partyBuildClean) {
        return partyBuildCleanMapper.delete(partyBuildClean);
    }

    public int updatePartyBuildClean(Example example, PartyBuildClean partyBuildClean) {
        return partyBuildCleanMapper.updateByExampleSelective(partyBuildClean, example);
    }

    public PartyBuildClean selectOnePartyBuildClean(PartyBuildClean partyBuildClean) {
        return partyBuildCleanMapper.selectOne(partyBuildClean);
    }

    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBuildCleanList
     * @Description:  获取党风廉政记录列表
     */
    public List<Map<String, String>> getPartyBuildCleanList(String orgId, String eventType, String beginTime, String endTime, String search) {
        return partyBuildCleanMapper.getPartyBuildCleanList(orgId, eventType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyBuildCleanList
     * @Description:  获取党风廉政记录列表
     */
    public PageInfo<Map<String, String>> getPartyBuildCleanList(PageParam pageParam, String eventType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyBuildCleanList(pageParam.getOrgId(), eventType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
