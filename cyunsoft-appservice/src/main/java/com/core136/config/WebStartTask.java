package com.core136.config;


import com.core136.service.sys.WebStartTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


/**
 * @ClassName: WebStartTask
 * @Description: 系统启动后初始化工作
 * @author: 稠云技术
 * @date: 2019年10月30日 下午10:29:48
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Component
public class WebStartTask implements CommandLineRunner {
    private WebStartTaskService webStartTaskService;

    @Autowired
    public void setWebStartTaskService(WebStartTaskService webStartTaskService) {
        this.webStartTaskService = webStartTaskService;
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            webStartTaskService.initializationApp();
            //new ImService().startup();
            System.out.println("智能办公系统已启动,相关初始化工作已完成,欢迎您对本系统的支持！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
