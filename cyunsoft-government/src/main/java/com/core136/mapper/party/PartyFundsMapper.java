package com.core136.mapper.party;

import com.core136.bean.party.PartyFunds;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyFundsMapper extends MyMapper<PartyFunds> {

    /**
     * @param orgId
     * @param fundsType
     * @param approvalUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getGovFundsList
     * @Description:  获取经费申请例表
     */
    public List<Map<String, String>> getGovFundsList(@Param(value = "orgId") String orgId, @Param(value = "status") String status, @Param(value = "fundsType") String fundsType,
                                                     @Param(value = "approvalUser") String approvalUser, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getApprovalFundsList
     * @Description:  获取审批列表
     */
    public List<Map<String, String>> getApprovalFundsList(@Param(value = "orgId") String orgId, @Param(value = "fundsType") String fundsType,
                                                          @Param(value = "applyUser") String applyUser, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    public List<Map<String, String>> getApprovalFundsOldList(@Param(value = "orgId") String orgId, @Param(value = "status") String status, @Param(value = "fundsType") String fundsType,
                                                             @Param(value = "applyUser") String applyUser, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


}
