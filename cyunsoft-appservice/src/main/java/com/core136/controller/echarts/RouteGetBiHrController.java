package com.core136.controller.echarts;

import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.echarts.EchartsHrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/echartshrget")
public class RouteGetBiHrController {
    private EchartsHrService echartsHrService;

    @Autowired
    public void setEchartsHrService(EchartsHrService echartsHrService) {
        this.echartsHrService = echartsHrService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getCareTableForAnalysis
     * @Description:  人员关怀复职分析tabale
     */
    @RequestMapping(value = "/getCareTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getCareTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getCareTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getCareBarForAnalysis
     * @Description:  人员关怀复职柱状图分析
     */
    @RequestMapping(value = "/getCareBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getCareBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getCareBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getCarePieForAnalysis
     * @Description:  人员关怀复饼状图分析
     */
    @RequestMapping(value = "/getCarePieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getCarePieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getCarePieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getReinstatTableForAnalysis
     * @Description:  人员复职分析tabale
     */
    @RequestMapping(value = "/getReinstatTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getReinstatTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getReinstatTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getReinstatBarForAnalysis
     * @Description:  人员复职柱状图分析
     */
    @RequestMapping(value = "/getReinstatBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getReinstatBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getReinstatBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getReinstatPieForAnalysis
     * @Description:  人员复饼状图分析
     */
    @RequestMapping(value = "/getReinstatPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getReinstatPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getReinstatPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getEvaluationTableForAnalysis
     * @Description:  职称评定分析tabale
     */
    @RequestMapping(value = "/getEvaluationTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getEvaluationTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getEvaluationTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getEvaluationBarForAnalysis
     * @Description:  职称评定柱状图分析
     */
    @RequestMapping(value = "/getEvaluationBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getEvaluationBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getEvaluationBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getEvaluationPieForAnalysis
     * @Description:  职称评定饼状图分析
     */
    @RequestMapping(value = "/getEvaluationPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getEvaluationPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getEvaluationPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLevelTableForAnalysis
     * @Description:  人员离职分析tabale
     */
    @RequestMapping(value = "/getLevelTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLevelTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLevelTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLevelBarForAnalysis
     * @Description:  人员离职柱状图分析
     */
    @RequestMapping(value = "/getLevelBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLevelBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLevelBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLevelPieForAnalysis
     * @Description:  人员离职饼状图分析
     */
    @RequestMapping(value = "/getLevelPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLevelPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLevelPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getTransferTableForAnalysis
     * @Description:  调动类型分析tabale
     */
    @RequestMapping(value = "/getTransferTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getTransferTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getTransferTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getTransferBarForAnalysis
     * @Description:  调动类型柱状图分析
     */
    @RequestMapping(value = "/getTransferBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getTransferBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getTransferBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getTransferPieForAnalysis
     * @Description:  调动类型饼状图分析
     */
    @RequestMapping(value = "/getTransferPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getTransferPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getTransferPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getSkillsTableForAnalysis
     * @Description:  劳动技能分析tabale
     */
    @RequestMapping(value = "/getSkillsTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getSkillsTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getSkillsTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getSkillsBarForAnalysis
     * @Description: 劳动技能柱状图分析
     */
    @RequestMapping(value = "/getSkillsBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getSkillsBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getSkillsBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getSkillsPieForAnalysis
     * @Description:  劳动技能饼状图分析
     */
    @RequestMapping(value = "/getSkillsPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getSkillsPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getSkillsPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLearnTableForAnalysis
     * @Description:  学习经功分析tabale
     */
    @RequestMapping(value = "/getLearnTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLearnTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLearnTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLearnBarForAnalysis
     * @Description:  学习经历柱状图分析
     */
    @RequestMapping(value = "/getLearnBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLearnBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLearnBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLearnPieForAnalysis
     * @Description:  学习经历饼状图分析
     */
    @RequestMapping(value = "/getLearnPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLearnPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLearnPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLicenceTableForAnalysis
     * @Description:  证照分析tabale
     */
    @RequestMapping(value = "/getLicenceTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLicenceTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLicenceTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLicenceBarForAnalysis
     * @Description:  证照柱状图分析
     */
    @RequestMapping(value = "/getLicenceBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLicenceBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLicenceBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getLicencePieForAnalysis
     * @Description:  证照饼状图分析
     */
    @RequestMapping(value = "/getLicencePieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getLicencePieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getLicencePieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getIncentiveTableForAnalysis
     * @Description:  奖惩分析tabale
     */
    @RequestMapping(value = "/getIncentiveTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getIncentiveTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getIncentiveTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getIncentiveBarForAnalysis
     * @Description:  奖惩柱状图分析
     */
    @RequestMapping(value = "/getIncentiveBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getIncentiveBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getIncentiveBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getIncentivePieForAnalysis
     * @Description:  奖惩饼状图分析
     */
    @RequestMapping(value = "/getIncentivePieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getIncentivePieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getIncentivePieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getContractTableForAnalysis
     * @Description:  合同分析tabale
     */
    @RequestMapping(value = "/getContractTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getContractTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getContractTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getContractBarForAnalysis
     * @Description:  合同柱状图分析
     */
    @RequestMapping(value = "/getContractBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getContractBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getContractBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getContractPieForAnalysis
     * @Description:  合同饼状图分析
     */
    @RequestMapping(value = "/getContractPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getContractPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getContractPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getBaseInfoTableForAnalysis
     * @Description:  人事档案分析Table
     */
    @RequestMapping(value = "/getBaseInfoTableForAnalysis", method = RequestMethod.POST)
    public RetDataBean getBaseInfoTableForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getBaseInfoTableForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getBaseInfoBarForAnalysis
     * @Description:  柱状图
     */
    @RequestMapping(value = "/getBaseInfoBarForAnalysis", method = RequestMethod.POST)
    public RetDataBean getBaseInfoBarForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getBaseInfoBarForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param deptId
     * @param dataType
     * @return RetDataBean
     * @Title: getBaseInfoPieForAnalysisPlacePie
     * @Description:  人事档案分析饼状图
     */
    @RequestMapping(value = "/getBaseInfoPieForAnalysis", method = RequestMethod.POST)
    public RetDataBean getBaseInfoPieForAnalysis(String deptId, String dataType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getBaseInfoPieForAnalysis(account.getOrgId(), deptId, dataType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getNativePlacePie
     * @Description:  HR人员籍贯占比
     */
    @RequestMapping(value = "/getNativePlacePie", method = RequestMethod.POST)
    public RetDataBean getNativePlacePie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getNativePlacePie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getWorkTypeBar
     * @Description:   HR人员工种对比
     */
    @RequestMapping(value = "/getWorkTypeBar", method = RequestMethod.POST)
    public RetDataBean getWorkTypeBar() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getWorkTypeBar(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getHighsetShoolPie
     * @Description:  获取HR学历占比
     */
    @RequestMapping(value = "/getHighsetShoolPie", method = RequestMethod.POST)
    public RetDataBean getHighsetShoolPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsHrService.getHighsetShoolPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
