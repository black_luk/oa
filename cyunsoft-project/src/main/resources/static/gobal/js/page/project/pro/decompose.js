$(function () {
    getProRecordById();
    $(".js-clear").unbind("click").click(function () {
        clearAll();
    })
    gantt.config.xml_date = "%Y-%m-%d %H:%i";
    gantt.config.lightbox.sections = [
        {name: "description", height: 38, map_to: "text", type: "textarea", focus: true},
        {name: "mainperson", height: 22, map_to: "accountId", type: "select", options: getTaskUsersList()},
        {name: "time", type: "duration", map_to: "auto"}
    ];
    gantt.addMarker({
        start_date: new Date(),
        css: "today",
        text: "今日",
        title: "日期:" + getSysDate()
    });
    //gantt.config.readonly = true;
    gantt.init("gantt_here");
    gantt.attachEvent("onGanttReady", function () {
        var tooltips = gantt.ext.tooltips;
        tooltips.tooltip.setViewport(gantt.$task_data);
    });
    gantt.parse(getProTaskInfo());
    gantt.attachEvent("onAfterTaskAdd", function (id, item) {
        gantt.render();
        insertProTask(item);
    });
    gantt.attachEvent("onAfterTaskUpdate", function (id, item) {
        updateProTask(item);
    });
    gantt.attachEvent("onAfterLinkAdd", function (id, item) {
        insertProTaskLink(item)
    });

    gantt.attachEvent("onAfterTaskDelete", function (id, item) {
        deleteProTask(item);
    });

    gantt.attachEvent("onAfterLinkDelete", function (id, item) {
        deleteTaskLink(item);
    });

});

function clearAll() {
    gantt.clearAll();
}

function getProTaskInfo() {
    var returnData;
    $.ajax({
        url: "/ret/proget/getProTaskInfo",
        type: "POST",
        dataType: "json",
        async: false,
        data: {
            proId: proId
        },
        success: function (data) {
            if (data.status == "200") {
                returnData = data.list;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
    return returnData;
}

function insertProTask(item) {
    var date = new Date(item.start_date);
    var date_value = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    console.log(date_value);
    $.ajax({
        url: "/set/proset/insertProTask",
        type: "POST",
        dataType: "json",
        data: {
            proId: proId,
            text: item.text,
            parent: item.parent,
            startDate: date_value,
            duration: item.duration,
            progress: item.progress,
            accountId: item.accountId,
            open: true
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function updateProTask(item) {
    var date = new Date(item.start_date);
    var date_value = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    $.ajax({
        url: "/set/proset/updateProTask",
        type: "POST",
        dataType: "json",
        data: {
            taskId: item.id,
            proId: proId,
            text: item.text,
            startDate: date_value,
            duration: item.duration,
            accountId: item.accountId,
            progress: item.progress,
            open: item.open
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function deleteProTask(item) {
    $.ajax({
        url: "/set/proset/deleteProTask",
        type: "POST",
        dataType: "json",
        data: {
            taskId: item.id,
            proId: proId
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function insertProTaskLink(item) {
    $.ajax({
        url: "/set/proset/insertProTaskLink",
        type: "POST",
        dataType: "json",
        data: {
            proId: proId,
            source: item.source,
            target: item.target,
            type: item.type
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function deleteTaskLink(item) {
    $.ajax({
        url: "/set/proset/deleteProTaskLink",
        type: "POST",
        dataType: "json",
        data: {
            taskLinkId: item.id,
            proId: proId
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function getProRecordById() {
    $.ajax({
        url: "/ret/proget/getProRecordById",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#title").html(data.list.title);
                $("#manager").html("项目经理：【" + getUserNameByStr(data.list.manager) + "】");
                $("#prodate").html("开始日期：" + data.list.beginTime + "&nbsp;&nbsp;结束日期：" + data.list.endTime + "")
            }
        }
    });
}


function getTaskUsersList() {
    var returnValue;
    $.ajax({
        url: "/ret/proget/getTaskUsersList",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        async: false,
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnValue = data.list;
            }
        }
    });
    return returnValue;
}
