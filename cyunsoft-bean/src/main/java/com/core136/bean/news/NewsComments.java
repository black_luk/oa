package com.core136.bean.news;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "news_comments")
public class NewsComments implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String commId;
    private String commPid;
    private String newsId;
    private String commType;
    private String commContent;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getCommId() {
        return commId;
    }

    public void setCommId(String commId) {
        this.commId = commId;
    }

    public String getCommPid() {
        return commPid;
    }

    public void setCommPid(String commPid) {
        this.commPid = commPid;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCommType() {
        return commType;
    }

    public void setCommType(String commType) {
        this.commType = commType;
    }

    public String getCommContent() {
        return commContent;
    }

    public void setCommContent(String commContent) {
        this.commContent = commContent;
    }

}
