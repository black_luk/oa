package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionTip;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionTipMapper extends MyMapper<PartyUnionTip> {
    /**
     * 获取举报记录列表
     *
     * @param orgId
     * @param tipType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionTipList(@Param(value = "orgId") String orgId, @Param(value = "tipType") String tipType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
