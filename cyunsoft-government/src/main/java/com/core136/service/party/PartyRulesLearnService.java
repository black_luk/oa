package com.core136.service.party;

import com.core136.bean.party.PartyRulesLearn;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyRulesLearnMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRulesLearnService {
    private PartyRulesLearnMapper partyRulesLearnMapper;

    @Autowired
    public void setPartyRulesLearnMapper(PartyRulesLearnMapper partyRulesLearnMapper) {
        this.partyRulesLearnMapper = partyRulesLearnMapper;
    }

    public int insertPartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        return partyRulesLearnMapper.insert(partyRulesLearn);
    }

    public int deletePartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        return partyRulesLearnMapper.delete(partyRulesLearn);
    }

    public int updatePartyRulesLearn(Example example, PartyRulesLearn partyRulesLearn) {
        return partyRulesLearnMapper.updateByExampleSelective(partyRulesLearn, example);
    }

    public PartyRulesLearn selectOnePartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        return partyRulesLearnMapper.selectOne(partyRulesLearn);
    }

    /**
     * @param orgId
     * @param accountId
     * @param rulesRecordId
     * @return List<Map < String, String>>
     * @Title: getMyLearnRecordList
     * @Description:  获取当前用户的历史学习记录
     */
    public List<Map<String, String>> getMyLearnRecordList(String orgId, String accountId, String rulesRecordId) {
        return partyRulesLearnMapper.getMyLearnRecordList(orgId, accountId, rulesRecordId);
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLearnRecordList
     * @Description:  查询学习记录
     */
    public List<Map<String, String>> getLearnRecordList(String orgId, String accountId, String beginTime, String endTime, String search) {
        return partyRulesLearnMapper.getLearnRecordList(orgId, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getLearnRecordList
     * @Description:  查询学习记录
     */
    public PageInfo<Map<String, String>> getLearnRecordList(PageParam pageParam, String accountId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLearnRecordList(pageParam.getOrgId(), accountId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
