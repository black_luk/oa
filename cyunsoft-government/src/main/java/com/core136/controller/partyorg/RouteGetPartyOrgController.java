package com.core136.controller.partyorg;

import com.core136.bean.account.Account;
import com.core136.bean.partyorg.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyorg.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/ret/partyorgget")
public class RouteGetPartyOrgController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyOrgService partyOrgService;

    @Autowired
    public void setPartyOrgService(PartyOrgService partyOrgService) {
        this.partyOrgService = partyOrgService;
    }

    private PartyUnitBaseService partyUnitBaseService;

    @Autowired
    public void setPartyUnitBaseService(PartyUnitBaseService partyUnitBaseService) {
        this.partyUnitBaseService = partyUnitBaseService;
    }

    private PartyCommitteeService partyCommitteeService;

    @Autowired
    public void setPartyCommitteeService(PartyCommitteeService partyCommitteeService) {
        this.partyCommitteeService = partyCommitteeService;
    }

    private PartyElectionService partyElectionService;

    @Autowired
    public void setPartyElectionService(PartyElectionService partyElectionService) {
        this.partyElectionService = partyElectionService;
    }

    private PartyRevokeService partyRevokeService;

    @Autowired
    public void setPartyRevokeService(PartyRevokeService partyRevokeService) {
        this.partyRevokeService = partyRevokeService;
    }

    private PartyRebuildService partyRebuildService;

    @Autowired
    public void setPartyRebuildService(PartyRebuildService partyRebuildService) {
        this.partyRebuildService = partyRebuildService;
    }

    private PartyRenameService partyRenameService;

    @Autowired
    public void setPartyRenameService(PartyRenameService partyRenameService) {
        this.partyRenameService = partyRenameService;
    }

    private PartyTemporgService partyTemporgService;

    @Autowired
    public void setPartyTemporgService(PartyTemporgService partyTemporgService) {
        this.partyTemporgService = partyTemporgService;
    }

    private PartyTalkService partyTalkService;

    @Autowired
    public void setPartyTalkService(PartyTalkService partyTalkService) {
        this.partyTalkService = partyTalkService;
    }

    private PartyLesMeetService partyLesMeetService;

    @Autowired
    public void setPartyLesMeetService(PartyLesMeetService partyLesMeetService) {
        this.partyLesMeetService = partyLesMeetService;
    }

    private PartyCheckMeetService partyCheckMeetService;

    @Autowired
    public void setPartyCheckMeetService(PartyCheckMeetService partyCheckMeetService) {
        this.partyCheckMeetService = partyCheckMeetService;
    }

    private PartyMzMeetService partyMzMeetService;

    @Autowired
    public void setPartyMzMeetService(PartyMzMeetService partyMzMeetService) {
        this.partyMzMeetService = partyMzMeetService;
    }

    private PartyLifeMeetService partyLifeMeetService;

    @Autowired
    public void setPartyLifeMeetService(PartyLifeMeetService partyLifeMeetService) {
        this.partyLifeMeetService = partyLifeMeetService;
    }

    private PartyPrimaryMeetService partyPrimaryMeetService;

    @Autowired
    public void setPartyPrimaryMeetService(PartyPrimaryMeetService partyPrimaryMeetService) {
        this.partyPrimaryMeetService = partyPrimaryMeetService;
    }

    private PartyDuesOrgService partyDuesOrgService;

    @Autowired
    public void setPartyDuesOrgService(PartyDuesOrgService partyDuesOrgService) {
        this.partyDuesOrgService = partyDuesOrgService;
    }

    private PartyHistoryRecordService partyHistoryRecordService;

    @Autowired
    public void setPartyHistoryRecordService(PartyHistoryRecordService partyHistoryRecordService) {
        this.partyHistoryRecordService = partyHistoryRecordService;
    }

    /**
     * 获取历史党组织详情
     * @param partyHistoryRecord
     * @return
     */
    @RequestMapping(value = "/getPartyHistoryRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyHistoryRecordById(PartyHistoryRecord partyHistoryRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyHistoryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyHistoryRecordService.selectOnePartyHistoryRecord(partyHistoryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyOrgLngAndLat
     * @Description:  获取党组织坐标
     */
    @RequestMapping(value = "/getPartyOrgLngAndLat", method = RequestMethod.POST)
    public RetDataBean getPartyOrgLngAndLat() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyOrgService.getPartyOrgLngAndLat(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getPartyOrgListById
     * @Description:  获取党组织下组织单位列表
     */
    @RequestMapping(value = "/getPartyOrgListById", method = RequestMethod.POST)
    public RetDataBean getPartyOrgListById(PageParam pageParam, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.sort_no");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyOrgService.getPartyOrgListById(pageParam, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党员缴费列表
     * @param pageParam
     * @param onTime
     * @param partyOrgId
     * @param year
     * @return
     */
    @RequestMapping(value = "/getPartyDeusOrgList", method = RequestMethod.POST)
    public RetDataBean getPartyDeusOrgList(PageParam pageParam, String onTime, String partyOrgId, String year) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("j.create_time");
            } else {
                pageParam.setSort("j." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyDuesOrgService.getPartyDeusOrgList(pageParam, onTime, partyOrgId, year);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDuesOrg
     * @return RetDataBean
     * @Title: getPartyDuesOrgById
     * @Description:  获取党组织党费记录
     */
    @RequestMapping(value = "/getPartyDuesOrgById", method = RequestMethod.POST)
    public RetDataBean getPartyDuesOrgById(PartyDuesOrg partyDuesOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDuesOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyDuesOrgService.selectOnePartyDuesOrg(partyDuesOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @return RetDataBean
     * @Title: getPrimaryMeetList
     * @Description:  获取主题党日活动列表
     */
    @RequestMapping(value = "/getPrimaryMeetList", method = RequestMethod.POST)
    public RetDataBean getPrimaryMeetList(PageParam pageParam, String partyOrgId, String beginTime, String endTime, String meetType) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyPrimaryMeetService.getPrimaryMeetList(pageParam, beginTime, endTime, meetType, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPrimaryMeet
     * @return RetDataBean
     * @Title: getPartyPrimaryMeetById
     * @Description:  获取主题党日详情
     */
    @RequestMapping(value = "/getPartyPrimaryMeetById", method = RequestMethod.POST)
    public RetDataBean getPartyPrimaryMeetById(PartyPrimaryMeet partyPrimaryMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPrimaryMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPrimaryMeetService.selectOnePartyPrimaryMeet(partyPrimaryMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param meetType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getMzMeetList
     * @Description:  获取民主生活会记录
     */
    @RequestMapping(value = "/getMzMeetList", method = RequestMethod.POST)
    public RetDataBean getMzMeetList(PageParam pageParam, String meetType, String meetYear, String beginTime, String endTime, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMzMeetService.getMzMeetList(pageParam, meetType, meetYear, beginTime, endTime, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMzMeet
     * @return RetDataBean
     * @Title: getPartyMzMeetById
     * @Description:  获取民主生活会详情
     */
    @RequestMapping(value = "/getPartyMzMeetById", method = RequestMethod.POST)
    public RetDataBean getPartyMzMeetById(PartyMzMeet partyMzMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMzMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMzMeetService.selectOnePartyMzMeet(partyMzMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param checkYear
     * @param checkStatus
     * @param checkLevel
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getCheckMeetList
     * @Description:  获取述评考记录列表
     */
    @RequestMapping(value = "/getCheckMeetList", method = RequestMethod.POST)
    public RetDataBean getCheckMeetList(PageParam pageParam, String checkYear, String checkStatus, String checkLevel, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.create_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCheckMeetService.getCheckMeetList(pageParam, checkYear, checkStatus, checkLevel, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCheckMeet
     * @return RetDataBean
     * @Title: getPartyCheckMeetById
     * @Description:  获取述评考详情
     */
    @RequestMapping(value = "/getPartyCheckMeetById", method = RequestMethod.POST)
    public RetDataBean getPartyCheckMeetById(PartyCheckMeet partyCheckMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCheckMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCheckMeetService.selectOnePartyCheckMeet(partyCheckMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 党组织生活会详情
     * @param partyLifeMeet
     * @return
     */
    @RequestMapping(value = "/getPartyLifeMeetById", method = RequestMethod.POST)
    public RetDataBean getPartyLifeMeetById(PartyLifeMeet partyLifeMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyLifeMeetService.selectOnePartyLifeMeet(partyLifeMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党组织生活会记录
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @return
     */
    @RequestMapping(value = "/getLifeMeetList", method = RequestMethod.POST)
    public RetDataBean getLifeMeetList(PageParam pageParam, String partyOrgId, String beginTime, String endTime, String meetType) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyLifeMeetService.getLifeMeetList(pageParam, beginTime, endTime, meetType, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param meetModel
     * @return RetDataBean
     * @Title: getLesMeetList
     * @Description:  获取三会一课记录列表
     */
    @RequestMapping(value = "/getLesMeetList", method = RequestMethod.POST)
    public RetDataBean getLesMeetList(PageParam pageParam, String beginTime, String endTime, String meetType, String meetModel) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyLesMeetService.getLesMeetList(pageParam, beginTime, endTime, meetType, meetModel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyLesMeet
     * @return RetDataBean
     * @Title: getPartyLesMeetById
     * @Description:  获取三会一课详情
     */
    @RequestMapping(value = "/getPartyLesMeetById", method = RequestMethod.POST)
    public RetDataBean getPartyLesMeetById(PartyLesMeet partyLesMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLesMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyLesMeetService.selectOnePartyLesMeet(partyLesMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyTalkList
     * @Description:  获取书记谈话记录
     */
    @RequestMapping(value = "/getPartyTalkList", method = RequestMethod.POST)
    public RetDataBean getPartyTalkList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.talk_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyTalkService.getPartyTalkList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTalk
     * @return RetDataBean
     * @Title: getPartyTalkById
     * @Description:  书记谈话详情
     */
    @RequestMapping(value = "/getPartyTalkById", method = RequestMethod.POST)
    public RetDataBean getPartyTalkById(PartyTalk partyTalk) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTalk.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTalkService.selectOnePartyTalk(partyTalk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyTemporgList
     * @Description:  获取临时党组织列表
     */
    @RequestMapping(value = "/getPartyTemporgList", method = RequestMethod.POST)
    public RetDataBean getPartyTemporgList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.sort_no");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyTemporgService.getPartyTemporgList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyRebuildList
     * @Description:  获取党组织改建记录列表
     */
    @RequestMapping(value = "/getPartyRebuildList", method = RequestMethod.POST)
    public RetDataBean getPartyRebuildList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.approval_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRebuildService.getPartyRebuildList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyRevokeList
     * @Description:  获取党组织撤销记录
     */
    @RequestMapping(value = "/getPartyRevokeList", method = RequestMethod.POST)
    public RetDataBean getPartyRevokeList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.approval_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRevokeService.getPartyRevokeList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRevoke
     * @return RetDataBean
     * @Title: getSelectPartyMemberByPartyId
     * @Description:  获取撤销的党组记录详情
     */
    @RequestMapping(value = "/getPartyRevokeById", method = RequestMethod.POST)
    public RetDataBean getPartyRevokeById(PartyRevoke partyRevoke) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRevoke.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRevokeService.selectOnePartyRevoke(partyRevoke));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRename
     * @return RetDataBean
     * @Title: getPartyRenameById
     * @Description:  获取党组织更名记录详情
     */
    @RequestMapping(value = "/getPartyRenameById", method = RequestMethod.POST)
    public RetDataBean getPartyRenameById(PartyRename partyRename) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRename.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRenameService.selectOnePartyRename(partyRename));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyRenameList
     * @Description:  获取党组织更名历史记录
     */
    @RequestMapping(value = "/getPartyRenameList", method = RequestMethod.POST)
    public RetDataBean getPartyRenameList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.approval_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRenameService.getPartyRenameList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyRebuild
     * @return RetDataBean
     * @Title: getPartyRebuildById
     * @Description:  获取党组织改建记录详情
     */
    @RequestMapping(value = "/getPartyRebuildById", method = RequestMethod.POST)
    public RetDataBean getPartyRebuildById(PartyRebuild partyRebuild) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRebuild.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRebuildService.selectOnePartyRebuild(partyRebuild));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTemporg
     * @return RetDataBean
     * @Title: getPartyTemporgById
     * @Description:  获取临时党组织详情
     */
    @RequestMapping(value = "/getPartyTemporgById", method = RequestMethod.POST)
    public RetDataBean getPartyTemporgById(PartyTemporg partyTemporg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTemporg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTemporgService.selectOnePartyTemporg(partyTemporg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: electionIsExist
     * @Description:  当前时间是否存在届次
     */
    @RequestMapping(value = "/electionIsExist", method = RequestMethod.POST)
    public RetDataBean electionIsExist(String partyOrgId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyElectionService.electionIsExist(account.getOrgId(), partyOrgId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyElection
     * @return RetDataBean
     * @Title: getPartyElectionById
     * @Description:  获取届次详情
     */
    @RequestMapping(value = "/getPartyElectionById", method = RequestMethod.POST)
    public RetDataBean getPartyElectionById(PartyElection partyElection) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyElection.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyElectionService.selectOnePartyElection(partyElection));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getNowCommitteeList
     * @Description:  获取当前届次委员列表
     */
    @RequestMapping(value = "/getNowCommitteeList", method = RequestMethod.POST)
    public RetDataBean getNowCommitteeList(PageParam pageParam, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCommitteeService.getNowCommitteeList(pageParam, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param electionRecordId
     * @return RetDataBean
     * @Title: getCommitteeList
     * @Description:  获取当前届期委员列表
     */
    @RequestMapping(value = "/getCommitteeList", method = RequestMethod.POST)
    public RetDataBean getCommitteeList(PageParam pageParam, String electionRecordId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCommitteeService.getCommitteeList(pageParam, electionRecordId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommittee
     * @return RetDataBean
     * @Title: getPartyCommitteeById
     * @Description:  获取委员详情
     */
    @RequestMapping(value = "/getPartyCommitteeById", method = RequestMethod.POST)
    public RetDataBean getPartyCommitteeById(PartyCommittee partyCommittee) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommittee.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCommitteeService.selectOnePartyCommittee(partyCommittee));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param search
     * @return RetDataBean
     * @Title: getSelect2UnitBaseList
     * @Description:  获取select2下拉列表
     */
    @RequestMapping(value = "/getSelect2UnitBaseList", method = RequestMethod.POST)
    public RetDataBean getSelect2UnitBaseList(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitBaseService.getSelect2UnitBaseList(account.getOrgId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitBase
     * @return RetDataBean
     * @Title: getPartyUnitBaseById
     * @Description:  获取单位信息详情
     */
    @RequestMapping(value = "/getPartyUnitBaseById", method = RequestMethod.POST)
    public RetDataBean getPartyUnitBaseById(PartyUnitBase partyUnitBase) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitBase.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitBaseService.selectOnePartyUnitBase(partyUnitBase));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param unitIds
     * @return RetDataBean
     * @Title: getPartyUnitBaseListByIds
     * @Description:
     */
    @RequestMapping(value = "/getPartyUnitBaseListByIds", method = RequestMethod.POST)
    public RetDataBean getPartyUnitBaseListByIds(String unitIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitBaseService.getPartyUnitBaseListByIds(account.getOrgId(), unitIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param partyStatus
     * @param unitAffiliation
     * @param serviceIndustry
     * @return RetDataBean
     * @Title: getUnitBaseInfoList
     * @Description:  获取单位基本信息列表
     */
    @RequestMapping(value = "/getUnitBaseInfoList", method = RequestMethod.POST)
    public RetDataBean getUnitBaseInfoList(PageParam pageParam, String partyStatus, String unitAffiliation, String serviceIndustry) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("u.create_time");
            } else {
                pageParam.setSort("u." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnitBaseService.getUnitBaseInfoList(pageParam, partyStatus, unitAffiliation, serviceIndustry);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrg
     * @return RetDataBean
     * @Title: getPartyOrgById
     * @Description:  获取党组织详情
     */
    @RequestMapping(value = "/getPartyOrgById", method = RequestMethod.POST)
    public RetDataBean getPartyOrgById(PartyOrg partyOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyOrgService.selectOnePartyOrg(partyOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrgId
     * @return List<Map < String, String>>
     * @Title: getPartyOrgTree
     * @Description:  获取党组织树形结构
     */
    @RequestMapping(value = "/getPartyOrgTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartyOrgTree(String partyOrgId) {
        try {
            String orgLevelId = "0";
            if (StringUtils.isNotBlank(partyOrgId)) {
                orgLevelId = partyOrgId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyOrgService.getPartyOrgTree(account.getOrgId(), orgLevelId);
        } catch (Exception e) {


            return null;
        }
    }

    @RequestMapping(value = "/getPartyOrgAllParentTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartyOrgAllParentTree(String partyOrgId) {
        try {
            String orgLevelId = "0";
            if (StringUtils.isNotBlank(partyOrgId)) {
                orgLevelId = partyOrgId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyOrgService.getPartyOrgAllParentTree(account.getOrgId(), orgLevelId);
        } catch (Exception e) {


            return null;
        }
    }


}
