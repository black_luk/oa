package com.core136.mapper.fixedassets;

import com.core136.bean.fixedassets.FixedAssetsAllocation;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface FixedAssetsAllocationMapper extends MyMapper<FixedAssetsAllocation> {

    /**
     * @param orgId
     * @param deptId
     * @param sortId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getFixedAssetsAllocationOldList
     * @Description:  历史调拨记录
     */
    public List<Map<String, String>> getFixedAssetsAllocationOldList(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId, @Param(value = "sortId") String sortId, @Param(value = "search") String search);
}
