package com.core136.service.mobile;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MobileNetService {
    @Value("${app.baidu.map.ak}")
    private String ak;

    /**
     * @param lat
     * @param lng
     * @return JSONObject
     * @Title: getLocation
     * @Description:  获取地理位置
     */
    public JSONObject getLocation(String lat, String lng) {
        RestTemplate restTemplate = new RestTemplate();
        String latlon = lat + "," + lng;
        String url = "http://api.map.baidu.com/reverse_geocoding/v3/?ak=" + ak + "&output=json&coordtype=wgs84ll&location=" + latlon;
        ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);
        String body = res.getBody();
        JSONObject jsonObject = JSONObject.parseObject(body);
        return jsonObject;
    }
}
