$(function () {
    $(".js-add-save").unbind("click").click(function () {
        addduty();
    })
    jeDate("#dirthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
    });
    $('#remark').summernote({height: 300});
})

function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}

function addduty() {
    $.ajax({
        url: "/set/eduset/insertEduStudent",
        type: "post",
        dataType: "json",
        data: {
            name: $("#name").val(),
            headImg: $("#file").attr("data-value"),
            sex: $("#sex").val(),
            cardId: $("#cardId").val(),
            dirthday: $("#dirthday").val(),
            nation: $("#nation").val(),
            mobile: $("#mobile").val(),
            major: $("#major").val(),
            score: $("#score").val(),
            lastScore: $("#lastScore").val(),
            homeAddress: $("#homeAddress").val(),
            graduateSchool: $("#graduateSchool").val(),
            inType: $("#inType").val(),
            isStay: $("#isStay").val(),
            registerTime: $("#registerTime").val(),
            introducers: $("#introducers").val(),
            father: $("#father").val(),
            fatherMobile: $("#fatherMobile").val(),
            mother: $("#mother").val(),
            motherMobile: $("#motherMobile").val(),
            remark: $("#remark").code(),
            studentNo: $("#studentNo").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });

}
