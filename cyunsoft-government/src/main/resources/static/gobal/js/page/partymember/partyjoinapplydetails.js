$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberJoinById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "statusFlag") {
                        for (var i = 0; i <= recordInfo[id]; i++) {
                            var eid = i + 1;
                            console.log(eid);
                            $("#simplewizardstep" + eid).addClass("active");
                        }
                    } else if (id == "isAdm1") {
                        $("input:checkbox[name='isAdm1'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "isAdm2") {
                        $("input:checkbox[name='isAdm2'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "isAdm3") {
                        $("input:checkbox[name='isAdm3'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "trainUser1") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "trainUser2") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "linkMan1") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "linkMan2") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "maturity") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("入党申请");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("发展对象");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("入党积极份子");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "checkSitu") {
                        if (recordInfo[id] == "0") {
                            $("#" + id).html("未政审");
                        } else if (recordInfo[id] == "1") {
                            $("#" + id).html("政审通过");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("政审中问题待查");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "trainResult") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("合格");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("不合格");
                        } else {
                            $("#" + id).html("未知");
                        }

                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
