package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrWorkRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWorkRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrWorkRecordService {
    private HrWorkRecordMapper hrWorkRecordMapper;

    @Autowired
    public void setHrWorkRecordMapper(HrWorkRecordMapper hrWorkRecordMapper) {
        this.hrWorkRecordMapper = hrWorkRecordMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrWorkRecord(HrWorkRecord hrWorkRecord) {
        return hrWorkRecordMapper.insert(hrWorkRecord);
    }

    public int deleteHrWorkRecord(HrWorkRecord hrWorkRecord) {
        return hrWorkRecordMapper.delete(hrWorkRecord);
    }

    public int updateHrWorkRecord(Example example, HrWorkRecord hrWorkRecord) {
        return hrWorkRecordMapper.updateByExampleSelective(hrWorkRecord, example);
    }

    public HrWorkRecord selectOneHrWorkRecord(HrWorkRecord hrWorkRecord) {
        return hrWorkRecordMapper.selectOne(hrWorkRecord);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrWorkRecordList
     * @Description:  获取工作经历列表
     */
    public List<Map<String, String>> getHrWorkRecordList(String orgId, String userId, String beginTime, String endTime, String nature, String search) {
        return hrWorkRecordMapper.getHrWorkRecordList(orgId, userId, beginTime, endTime, nature, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrWorkRecordList
     * @Description:  查询个人工作经历
     */
    public List<Map<String, String>> getMyHrWorkRecordList(String orgId, String accountId) {
        return hrWorkRecordMapper.getMyHrWorkRecordList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrWorkRecordList
     * @Description:  获取工作经历列表
     */
    public PageInfo<Map<String, String>> getHrWorkRecordList(PageParam pageParam, String userId, String beginTime, String endTime, String nature) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrWorkRecordList(pageParam.getOrgId(), userId, beginTime, endTime, nature, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrWorkRecordList
     * @Description:  查询个人工作经历
     */
    public PageInfo<Map<String, String>> getMyHrWorkRecordList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrWorkRecordList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrWorkRecord
     * @Description:  工作经历导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrWorkRecord(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("关联档案人员", "user_id");
        fieldMap.put("公司名称", "comp_name");
        fieldMap.put("所在部门", "dept_name");
        fieldMap.put("所属行业", "industry");
        fieldMap.put("公司类型", "nature");
        fieldMap.put("担任职务", "post");
        fieldMap.put("证明人", "cerifier");
        fieldMap.put("入职日期", "begin_time");
        fieldMap.put("离职日期", "end_time");
        fieldMap.put("工作内容", "job_content");
        fieldMap.put("工作成就", "achievement");
        fieldMap.put("离职原因", "reason_for_leave");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("关联档案人员")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_work_record(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
