package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyHistoryRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartyHistoryRecordMapper extends MyMapper<PartyHistoryRecord> {
}
