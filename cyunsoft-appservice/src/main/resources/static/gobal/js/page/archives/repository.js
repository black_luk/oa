$(function () {
    $(".js-add-save").unbind("click").click(function () {
        addRepository();
    })
})

function addRepository() {
    if ($("#title").val() == "") {
        layer.msg(archivesMsg['REPOSITORY_MANAGE_TITLE_CANNOT_EMPTY']);
        return;
    }
    $.ajax({
        url: "/set/archivesset/insertArchivesRepository",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            manageUser: $("#manageUser").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
