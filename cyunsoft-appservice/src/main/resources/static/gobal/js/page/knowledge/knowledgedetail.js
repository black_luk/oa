$(function () {
    $.ajax({
        url: "/ret/knowledgeget/getKnowledgeById",
        type: "post",
        dataType: "json",
        data: {knowledgeId: knowledgeId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    if (name == "attach") {
                        $("#attach").attr("data_value", data.list.attach);
                        createAttach("attach", "1");
                    } else if (name == "sortId") {
                        $("#" + name).html(getSortName(data.list.sortId));
                    } else if (name == "createUser") {
                        $("#" + name).html(getUserNameByStr(data.list.createUser));
                    } else {
                        $("#" + name).html(data.list[name]);
                    }
                }
            }
        }
    });
    $("#onclickCount").html(getLearnCount() + "次");
    setTimeout(function () {
        addKnowledgeLearn();
    }, 1000 * 30);
    $(".js-send").unbind("click").click(function () {
        addKnowledgeComment();
    })

    getCommlist();
});

function addKnowledgeComment() {
    $.ajax({
        url: "/set/knowledgeset/insertKnowledgeComment",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            knowledgeId: knowledgeId,
            commType: $("input:radio[name='commType']:checked").val(),
            remark: $("#remark").val(),
            levelStar: $("#input-21b").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                getCommlist();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}

function addKnowledgeLearn() {
    $.ajax({
        url: "/set/knowledgeset/insertKnowledgeLearn",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            knowledgeId: knowledgeId
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}

function deleteComm(commentId) {
    $.ajax({
        url: "/set/knowledgeset/deleteKnowledgeComment",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            commentId: commentId
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                getCommlist();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}


function getSortName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/knowledgeget/getKnowledgeSortById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (data) {
            if (data.status == "200") {
                returnStr = data.list.sortName;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });

    return returnStr;
}


function getCommlist() {
    $("#comment").empty();
    $.ajax({
        url: "/ret/knowledgeget/getCommentsList",
        type: "post",
        dataType: "json",
        data: {
            knowledgeId: knowledgeId
        },
        success: function (data) {
            if (data.status == "200") {
                for (var i = 0; i < data.list.length; i++) {
                    var html = '<div class="comment">';
                    if (data.list[i].commType == '1') {
                        html += '<img src="/sys/file/getOtherHeadImg?headImg=' + data.list[i].headImg + '" alt="" class="comment-avatar" onerror="this.onerror=null;this.src=\'/assets/img/avatars/adam-jansen.jpg\'">';
                    } else {
                        html += '<img src="/assets/img/avatars/adam-jansen.jpg" alt="" class="comment-avatar" onerror="this.onerror=null;this.src=\'/assets/img/avatars/adam-jansen.jpg\'">';
                    }
                    html += '<div class="comment-body">'
                        + '<div class="comment-text">'
                        + '<div class="comment-header">';
                    if (data.list[i].commType == '1') {
                        html += '<a href="#" title="">' + data.list[i].createUserName + '</a>';
                    } else {
                        html += '<a href="#" title="">匿名</a>';
                    }
                    html += '<span>' + data.list[i].createTime + '</span>';
                    if (data.list[i].createUser == accountId) {
                        html += "<a style='margin-right: 20px;float:right;cursor: pointer;' onclick='deleteComm(\"" + data.list[i].commentId + "\");'>删除</a>";
                    }
                    html += '</div>' + data.list[i].remark + '</div>' + '</div>' + '</div>';
                    $("#comment").append(html);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}


function getLearnCount() {
    var returnStr = "";
    $.ajax({
        url: "/ret/knowledgeget/getLearnCount",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            knowledgeId: knowledgeId
        },
        success: function (data) {
            if (data.status == "200") {
                returnStr = data.list;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });

    return returnStr;
}
