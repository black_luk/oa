package com.core136.service.safety;

import com.core136.bean.safety.SafetyEntScore;
import com.core136.mapper.safety.SafetyEntScoreMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SafetyEntScoreService {
    private SafetyEntScoreMapper safetyEntScoreMapper;

    @Autowired
    public void setSafetyEntScoreMapper(SafetyEntScoreMapper safetyEntScoreMapper) {
        this.safetyEntScoreMapper = safetyEntScoreMapper;
    }

    public int insertSafetyEntScore(SafetyEntScore safetyEntScore) {
        return safetyEntScoreMapper.insert(safetyEntScore);
    }

    public int deleteSafetyEntScore(SafetyEntScore safetyEntScore) {
        return safetyEntScoreMapper.delete(safetyEntScore);
    }

    public SafetyEntScore selectOneSafetyEntScore(SafetyEntScore safetyEntScore) {
        return safetyEntScoreMapper.selectOne(safetyEntScore);
    }

    public int updateSafetyEntScore(Example example, SafetyEntScore safetyEntScore) {
        return safetyEntScoreMapper.updateByExampleSelective(safetyEntScore, example);
    }

}
