$(function () {
    getSupversionBpmRecordList();
    query();
    $.ajax({
        url: "/ret/superversionget/getSuperversionProcessById",
        type: "post",
        dataType: "json",
        data: {processId: processId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    if (name == "attach") {
                        $("#zsuperversionattach").attr("data_value", data.list.attach);
                        createAttach("zsuperversionattach", "1");
                    } else if (name == "holder" || name == "operator") {
                        $("#z" + name).html(getUserNameByStr(data.list[name]));
                    } else {
                        $("#z" + name).html(data.list[name]);
                    }
                }
            }
        }
    })
});


function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getSuperversionResultList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'superversionId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'content',
                title: '处理结果',
                sortable: true,
                width: '300px'
            },
            {
                field: 'attach',
                width: '200px',
                title: '相关附件',
                formatter: function (value, row, index) {
                    return createTableAttach(value);
                }
            },
            {
                field: 'finishTime',
                width: '100px',
                title: '完成时间'
            },
            {
                field: 'createUser',
                width: '100px',
                title: '处理人'
            }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        processId: processId,
    };
    return temp;
};

function getSupversionBpmRecordList() {
    $.ajax({
        url: "/ret/superversionget/getSupversionBpmRecordList",
        type: "post",
        dataType: "json",
        data: {processId: processId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let bpmList = data.list;
                $("#flowCount").html("(" + bpmList.length + ")条");
                for (let i = 0; i < bpmList.length; i++) {
                    $("#flowlist").append("<a style='margin: 10px;' href=\"javascript:void(0);readBpm('" + bpmList[i].runId + "')\" class=\"btn btn-link\">" + bpmList[i].flowTitle + "</a>");
                }
            }
        }
    })
}

function readBpm(runId) {
    window.open("/app/core/bpm/bpmread?runId=" + runId);
}
