package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.FileMapItem;
import com.core136.bean.file.FileMapRecord;
import com.core136.bean.file.FileMapSort;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.file.FileMapItemService;
import com.core136.service.file.FileMapLogService;
import com.core136.service.file.FileMapRecordService;
import com.core136.service.file.FileMapSortService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/filemapget")
public class RouteGetFileMapController {

    private FileMapSortService fileMapSortService;

    @Autowired
    public void setFileMapSortService(FileMapSortService fileMapSortService) {
        this.fileMapSortService = fileMapSortService;
    }

    private FileMapLogService fileMapLogService;

    @Autowired
    public void setFileMapLogService(FileMapLogService fileMapLogService) {
        this.fileMapLogService = fileMapLogService;
    }

    private FileMapItemService fileMapItemService;

    @Autowired
    public void setFileMapItemService(FileMapItemService fileMapItemService) {
        this.fileMapItemService = fileMapItemService;
    }

    private FileMapRecordService fileMapRecordService;

    @Autowired
    public void setFileMapRecordService(FileMapRecordService fileMapRecordService) {
        this.fileMapRecordService = fileMapRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
    /**
     * 获取桌面个人内网文件
     * @param request
     * @param readStatus
     * @return
     */
    @RequestMapping(value = "/getMyFileMapForDesk", method = RequestMethod.POST)
    public RetDataBean getMyFileMapForDesk(HttpServletRequest request,String readStatus) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            String myIp = SysTools.getIpAddress(request);
            if (SysTools.isInnerIP(myIp)) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapRecordService.getMyInFileMapFileListForDesk(userInfo.getOrgId(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getLeadLevel(),readStatus));
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapRecordService.getMyOutFileMapFileListForDesk(userInfo.getOrgId(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getLeadLevel(),readStatus));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 查询获取权限内的文档
     *
     * @param request
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/searchMyFileMapFileList", method = RequestMethod.POST)
    public RetDataBean searchMyFileMapFileList(HttpServletRequest request, PageParam pageParam, String itemId, String beginTime, String endTime,String readStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            String myIp = SysTools.getIpAddress(request);
            if (SysTools.isInnerIP(myIp)) {
                PageInfo<Map<String, String>> pageInfo = fileMapRecordService.searchMyInFileMapFileList(pageParam, itemId, beginTime, endTime,readStatus);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
            } else {
                PageInfo<Map<String, String>> pageInfo = fileMapRecordService.searchMyOutFileMapFileList(pageParam, itemId, beginTime, endTime,readStatus);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
    @RequestMapping(value = "/getFileMapRecordById", method = RequestMethod.POST)
    public RetDataBean getFileMapRecordById(FileMapRecord fileMapRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fileMapRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapRecordService.selectOneFileMapRecord(fileMapRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取文档管理文件目录
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param itemId
     * @return
     */
    @RequestMapping(value = "/getFileRecordList", method = RequestMethod.POST)
    public RetDataBean getFileRecordList(PageParam pageParam, String beginTime, String endTime, String itemId, String fileStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fileMapRecordService.getFileRecordList(pageParam, itemId, fileStatus, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getMyMangeFileItem", method = RequestMethod.POST)
    public RetDataBean getMyMangeFileItem(HttpServletRequest request, String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapItemService.getMyMangeFileItem(account.getOrgId(), account.getAccountId(), sortId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getAllFileItem", method = RequestMethod.POST)
    public RetDataBean getAllFileItem(HttpServletRequest request, String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapItemService.getAllFileItem(account.getOrgId(), sortId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按访问来源判断文件目录
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getFileItemList", method = RequestMethod.POST)
    public RetDataBean getFileItemList(HttpServletRequest request, String sortId) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapItemService.getFileItemList(request, userInfo.getOrgId(), sortId, userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取文档分类
     *
     * @param fileMapItem
     * @return
     */
    @RequestMapping(value = "/getFileMapItemById", method = RequestMethod.POST)
    public RetDataBean getFileMapItemById(FileMapItem fileMapItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fileMapItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapItemService.selectOneFileMapItem(fileMapItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取目录结构
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getFileMapSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getFileMapSortTree(String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            return fileMapSortService.getFileMapSortTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取目录详情
     *
     * @param fileMapSort
     * @return
     */
    @RequestMapping(value = "/getFileMapSortById", method = RequestMethod.POST)
    public RetDataBean getFileMapSortById(FileMapSort fileMapSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fileMapSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fileMapSortService.selectOneFileMapSort(fileMapSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
