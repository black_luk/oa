$(function () {
    query();
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#finishTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysTime()
    });
    jeDate("#delayTime", {
        format: "YYYY-MM-DD"
    });
    getsuperversiontype()
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
})

function getsuperversiontype() {
    $.ajax({
        url: "/ret/superversionget/getAllSuperversionConfigList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option data-value='" + data.list[i].leadId + "' value='" + data.list[i].configId + "'>" + data.list[i].typeName + "</option>"
                }
                $("#typeQuery").html(html);
            }
        }
    })
}

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getSupperversionPorcessList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'processId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '任务标题',
                width: '150px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readdetails('" + row.processId + "','" + row.superversionId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'typeName',
                width: '50px',
                title: '事件类型'
            },
            {
                field: 'leadUserName',
                title: '督查领导',
                width: '50px'
            },

            {
                field: 'beginTime',
                width: '50px',
                sortable: true,
                title: '开始时间'
            },
            {
                field: 'endTime',
                width: '50px',
                sortable: true,
                title: '结束时间'
            },
            {
                field: 'operator',
                width: '100px',
                title: '经办人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            },
            {
                field: 'status',
                width: '30px',
                title: '状态',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "<a href='javascript:void(0);' class='btn btn-info shiny btn-xs'>进行中</a>";
                    } else if (value == "1") {
                        return "<a href='javascript:void(0);' class='btn btn-warning shiny btn-xs'>延期中</a>";
                    } else if (value == "2") {
                        return "<a href='javascript:void(0);' class='btn btn-success shiny btn-xs'>已完成</a>";
                    }
                }

            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'opt',
                width: '150px',
                align: 'center',
                title: '操作',
                formatter: function (value, row, index) {
                    return createOptBtn(row.processId, row.superversionId, row.status, row.type);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        type: $("#typeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(processId, superversionId, status, type) {
    var html = "<a href=\"javascript:void(0);changeUser('" + processId + "','" + superversionId + "');\" class=\"btn btn-sky btn-xs\" >转办</a>";
    html += "&nbsp;&nbsp;<a href=\"javascript:void(0);delayappl('" + processId + "','" + superversionId + "')\" class=\"btn btn-magenta btn-xs\" >延期</a>"
    html += "&nbsp;&nbsp;<a href=\"javascript:void(0);setbpm('" + processId + "','" + type + "')\" class=\"btn btn-success btn-xs\" >流程</a>"
    if (status != "2") {
        html += "&nbsp;&nbsp;<a href=\"javascript:void(0);dowork('" + processId + "')\" class=\"btn btn-magenta btn-xs\" >处理</a>"
        html += "&nbsp;&nbsp;<a href=\"javascript:void(0);getworkprocess('" + processId + "')\" class=\"btn btn-success btn-xs\" >过程</a>"
    }
    return html;
}

function setbpm(processId, type) {
    $("#title").val("");
    $("#setbpmmodal").modal("show");
    $.ajax({
        url: "/ret/superversionget/getSuperversionBpmConfigById",
        type: "post",
        dataType: "json",
        data: {configId: type},
        success: function (data) {
            if (data.status == "200") {
                getFlowNameByFlowIds(data.list.flowIds);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    });
    $(".js-add-bpm").unbind("click").click(function () {
        startBpm(processId);
    });
}

function startBpm(processId) {
    $.ajax({
        url: "/set/superversionset/startSuperversionBpm",
        type: "post",
        dataType: "json",
        data: {
            processId: processId,
            flowId: $("#flowId").val(),
            title: $("#title").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setbpmmodal").modal("hide");
                parent.openNewTabs(data.redirect, "流程待办");
            }
        }
    })
}

function getFlowNameByFlowIds(flowIds) {
    $.ajax({
        url: "/ret/bpmget/getFlowNameByFlowIds",
        type: "post",
        dataType: "json",
        data: {flowIds: flowIds},
        success: function (data) {
            if (data.status == "200") {
                let infoList = data.list;
                let html = "";
                for (let i = 0; i < infoList.length; i++) {
                    html += "<option value='" + infoList[i].flowId + "'>" + infoList[i].flowName + "</option>";
                }
                $("#flowId").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function delayappl(processId, superversionId) {
    $("#delayapplymodal").modal("show");
    document.getElementById("form2").reset();
    $(".js-save-delay").unbind("click").click(function () {
        $.ajax({
            url: "/set/superversionset/insertSuperversionDelay",
            type: "post",
            dataType: "json",
            data: {
                superversionId: superversionId,
                processId: processId,
                content: $("#delaycontent").val(),
                attach: $("#attach").attr("data_value"),
                delayTime: $("#delayTime").val(),
                status: "0"
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#delayapplymodal").modal("hide");
                }
            }
        })
    })
}

function dowork(processId) {
    document.getElementById("form3").reset();
    $("#superversionattach").attr("data_value");
    $("#dowordmodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        addResult(processId);
    })
}

function addResult(processId) {
    $.ajax({
        url: "/set/superversionset/insertSuperversionResult",
        type: "post",
        dataType: "json",
        data: {
            processId: processId,
            content: $("#content").val(),
            attach: $("#superversionattach").attr("data_value"),
            status: $("#status").val(),
            prcsValue: $("#prcsValue").val(),
            finishTime: $("#finishTime").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#dowordmodal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            }
        }
    })
}


function getsuperversiontype() {
    $.ajax({
        url: "/ret/superversionget/getAllSuperversionConfigList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option data-value='" + data.list[i].leadId + "' value='" + data.list[i].configId + "'>" + data.list[i].typeName + "</option>"
                }
                $("#typeQuery").html(html);
            }
        }
    })
}

function readdetails(processId, superversionId) {
    window.open("/app/core/superversion/processdetails?processId=" + processId + "&superversionId=" + superversionId);
}

function getworkprocess(processId) {
    window.open("/app/core/superversion/processresultdetails?processId=" + processId);
}

function changeUser(processId, superversionId) {
    $("#changeUserModal").modal("show");
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/superversionget/getSuperversionById",
        type: "post",
        dataType: "json",
        data: {
            superversionId: superversionId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                createChangeUserList(data.list.joinUser);
            }
        }
    })
    $(".js-add-change").unbind("click").click(function () {
        $.ajax({
            url: "/set/superversionset/setChangeUserByProcess",
            type: "post",
            dataType: "json",
            data: {
                processId: processId,
                changeUser: $("#changeUserList").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#changeUserModal").modal("hide");
                }
            }
        })
    })
}


function createChangeUserList(joinUser) {
    $.ajax({
        url: "/ret/unitget/getAllUserInfoByAccountList",
        type: "post",
        dataType: "json",
        data: {accountStrs: joinUser},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var html = "";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>"
                }
                $("#changeUserList").html(html);
            }
        }
    })
}
