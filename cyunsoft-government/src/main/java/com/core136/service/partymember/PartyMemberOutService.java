package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMemberOut;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberOutMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberOutService {
    private PartyMemberOutMapper partyMemberOutMapper;

    @Autowired
    public void setPartyMemberOutMapper(PartyMemberOutMapper partyMemberOutMapper) {
        this.partyMemberOutMapper = partyMemberOutMapper;
    }

    public int insertPartyMemberOut(PartyMemberOut partyMemberOut) {
        return partyMemberOutMapper.insert(partyMemberOut);
    }

    public int deletePartyMemberOut(PartyMemberOut partyMemberOut) {
        return partyMemberOutMapper.delete(partyMemberOut);
    }

    public int updatePartyMemberOut(Example example, PartyMemberOut partyMemberOut) {
        return partyMemberOutMapper.updateByExampleSelective(partyMemberOut, example);
    }

    public PartyMemberOut selectOnePartyMemberOut(PartyMemberOut partyMemberOut) {
        return partyMemberOutMapper.selectOne(partyMemberOut);
    }

    /**
     * @param orgId
     * @param outType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberOutList
     * @Description:  获取出党人员列表
     */
    public List<Map<String, String>> getMemberOutList(String orgId, String outType, String beginTime, String endTime, String search) {
        return partyMemberOutMapper.getMemberOutList(orgId, outType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param outType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberOutList
     * @Description:  获取出党人员列表
     */
    public PageInfo<Map<String, String>> getMemberOutList(PageParam pageParam, String outType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberOutList(pageParam.getOrgId(), outType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
