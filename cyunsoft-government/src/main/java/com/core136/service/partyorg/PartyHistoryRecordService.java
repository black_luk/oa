package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyHistoryRecord;
import com.core136.mapper.partyorg.PartyHistoryRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyHistoryRecordService {
    private PartyHistoryRecordMapper partyHistoryRecordMapper;

    @Autowired
    public void setPartyHistoryRecordMapper(PartyHistoryRecordMapper partyHistoryRecordMapper) {
        this.partyHistoryRecordMapper = partyHistoryRecordMapper;
    }


    public int insertPartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        return partyHistoryRecordMapper.insert(partyHistoryRecord);
    }

    public int deletePartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        return partyHistoryRecordMapper.delete(partyHistoryRecord);
    }

    public int updatePartyHistoryRecord(Example example, PartyHistoryRecord partyHistoryRecord) {
        return partyHistoryRecordMapper.updateByExampleSelective(partyHistoryRecord, example);
    }

    public PartyHistoryRecord selectOnePartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        return partyHistoryRecordMapper.selectOne(partyHistoryRecord);
    }
}
