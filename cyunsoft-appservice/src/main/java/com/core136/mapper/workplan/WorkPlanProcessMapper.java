package com.core136.mapper.workplan;

import com.core136.bean.workplan.WorkPlanProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkPlanProcessMapper extends MyMapper<WorkPlanProcess> {

}
