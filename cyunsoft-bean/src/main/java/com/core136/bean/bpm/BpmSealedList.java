package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "bpm_sealed_list")
public class BpmSealedList implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sealedListId;
    private Integer oldId;
    private String sealedYear;
    private String runId;
    private String flowId;
    private String flowTitle;
    private String endTime;
    private String delFlag;
    private String attach;
    private String opUserStr;
    private String opUserName;
    private String follow;
    private String urgency;
    private String status;
    private String parentRunId;
    private String parentProcessId;
    private String isEmpty;
    private String formVersion;

    public String getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(String parentProcessId) {
        this.parentProcessId = parentProcessId;
    }

    private String createUser;
    private String createUserName;
    private String createTime;
    private String sealedTime;
    private String sealedUser;
    private String orgId;

    public String getSealedYear() {
        return sealedYear;
    }

    public void setSealedYear(String sealedYear) {
        this.sealedYear = sealedYear;
    }

    public String getRunId() {
        return runId;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getFlowTitle() {
        return flowTitle;
    }

    public void setFlowTitle(String flowTitle) {
        this.flowTitle = flowTitle;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOpUserStr() {
        return opUserStr;
    }

    public void setOpUserStr(String opUserStr) {
        this.opUserStr = opUserStr;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getParentRunId() {
        return parentRunId;
    }

    public void setParentRunId(String parentRunId) {
        this.parentRunId = parentRunId;
    }

    public String getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(String isEmpty) {
        this.isEmpty = isEmpty;
    }

    public String getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(String formVersion) {
        this.formVersion = formVersion;
    }

    public String getSealedListId() {
        return sealedListId;
    }

    public void setSealedListId(String sealedListId) {
        this.sealedListId = sealedListId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getSealedTime() {
        return sealedTime;
    }

    public void setSealedTime(String sealedTime) {
        this.sealedTime = sealedTime;
    }

    public String getSealedUser() {
        return sealedUser;
    }

    public void setSealedUser(String sealedUser) {
        this.sealedUser = sealedUser;
    }

    public Integer getOldId() {
        return oldId;
    }

    public void setOldId(Integer oldId) {
        this.oldId = oldId;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }
}
