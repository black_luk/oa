package com.core136.bean.crm;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "crm_priv")
public class CrmPriv implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String privId;
    private String manager;
    private String sale;
    private String sender;
    private String orgId;

    public String getPrivId() {
        return privId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public void setPrivId(String privId) {
        this.privId = privId;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
