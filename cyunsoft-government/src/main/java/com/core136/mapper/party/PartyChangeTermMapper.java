package com.core136.mapper.party;

import com.core136.bean.party.PartyChangeTerm;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PartyChangeTermMapper extends MyMapper<PartyChangeTerm> {

}
