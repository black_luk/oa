package com.core136.mapper.hr;

import com.core136.bean.hr.HrContract;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrContractMapper extends MyMapper<HrContract> {

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param enterpries
     * @param contractType
     * @return List<Map < String, String>>
     * @Title: getHrContractList
     * @Description:  获取合同列表
     */
    public List<Map<String, String>> getHrContractList(@Param(value = "orgId") String orgId,
                                                       @Param(value = "userId") String userId,
                                                       @Param(value = "beginTime") String beginTime,
                                                       @Param(value = "endTime") String endTime,
                                                       @Param(value = "enterpries") String enterpries,
                                                       @Param(value = "contractType") String contractType
    );

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getDeskHrContractList
     * @Description:  获取快到期的合同列表
     */
    public List<Map<String, String>> getDeskHrContractList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrContractList
     * @Description:  查询自己的合同列表
     */
    public List<Map<String, String>> getMyHrContractList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
