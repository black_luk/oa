$(function () {
    jeDate("#applyTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#joinTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#joinTime1", {
        format: "YYYY-MM-DD"
    });
    jeDate("#trainTime", {
        format: "YYYY-MM-DD"
    });
    $('#trainRemark').summernote({height: 300});
    $('#applyRemark').summernote({height: 300});
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $(".js-add-save").unbind("click").click(function () {
        joinApplay();
    })
})

function joinApplay() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    if($("#userName").val()=="")
    {
        layer.msg("姓名不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyMemberJoin",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            userName: $("#userName").val(),
            applyTime: $("#applyTime").val(),
            userSex: $("input[name='userSex']:checked").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            isAdm1: getCheckBoxValue("isAdm1"),
            isAdm2: getCheckBoxValue("isAdm2"),
            isAdm3: getCheckBoxValue("isAdm3"),
            cardId: $("#cardId").val(),
            tel: $("#tel").val(),
            maturity: $("#maturity").val(),
            checkSitu: $("#checkSitu").val(),
            joinTime: $("#joinTime").val(),
            joinTime1: $("#joinTime1").val(),
            trainUser1: $("#trainUser1").attr("data-value"),
            trainUser2: $("#trainUser2").attr("data-value"),
            linkMan1: $("#linkMan1").attr("data-value"),
            linkMan2: $("#linkMan2").attr("data-value"),
            trainTime: $("#trainTime").val(),
            trainResult: $("#trainResult").val(),
            trainRemark: $("#trainRemark").code(),
            applyRemark: $("#applyRemark").code(),
            statusFlag: "0",
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
