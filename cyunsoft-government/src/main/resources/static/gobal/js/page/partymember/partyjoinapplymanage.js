$(function () {
    jeDate("#applyTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#joinTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#joinTime1", {
        format: "YYYY-MM-DD"
    });
    jeDate("#trainTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $('#trainRemark').summernote({height: 300});
    $('#applyRemark').summernote({height: 300});
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdQueryTree"), partyOrgIdQuerysetting, newTreeNodes);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#partyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).offset().left
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberJoinApplyList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '申请标题',
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        },
            {
                field: 'cardId',
                width: '100px',
                title: '身份证号'
            }, {
                field: 'userName',
                width: '100px',
                title: '申请人姓名'
            }, {
                field: 'tel',
                width: '100px',
                title: '联系电话'
            }, {
                field: 'applyTime',
                width: '100px',
                title: '申请时间'
            }, {
                field: 'partyOrgName',
                width: '100px',
                title: '接受党支部名称'
            }, {
                field: 'trainUser1',
                width: '100px',
                title: '培养人',
                formatter: function (value, row, index) {
                    var temp = [];
                    if (value) {
                        temp.push(value);
                    }
                    if (row.linkMan2) {
                        temp.push(row.trainUser2);
                    }
                    return getPartyMemberName(temp.join(","));
                }
            }, {
                field: 'linkMan1',
                width: '100px',
                title: '介绍人',
                formatter: function (value, row, index) {
                    var temp = [];
                    if (value) {
                        temp.push(value);
                    }
                    if (row.linkMan2) {
                        temp.push(row.linkMan2);
                    }
                    return getPartyMemberName(temp.join(","));
                }
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '120px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId, row.statusFlag);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        statusFlag: "0",
        partyOrgId: $("#partyOrgIdQuery").attr("data-value"),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId, statusFlag) {
    var html = "";
    if (statusFlag < 4) {
        html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    }
    return html;
}

function edit(recordId) {
    $("#applylistdiv").hide();
    $("#applydiv").show();
    $("#attach").attr("data_value", "");
    $("#trainUser1").attr("data-value", "");
    $("#trainUser2").attr("data-value", "");
    $("#linkMan1").attr("data-value", "");
    $("#linkMan2").attr("data-value", "");
    $("#show_attach").empty();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberJoinById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "userSex") {
                        $("input[name='userSex'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "trainRemark") {
                        $("#trainRemark").code(recordInfo[id]);
                    } else if (id == "applyRemark") {
                        $("#applyRemark").code(recordInfo[id]);
                    } else if (id == "isAdm1") {
                        $("input:checkbox[name='isAdm1'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "isAdm2") {
                        $("input:checkbox[name='isAdm2'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "isAdm3") {
                        $("input:checkbox[name='isAdm3'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else if (id == "trainUser1") {
                        $("#trainUser1").attr("data-value", recordInfo[id]);
                        if (recordInfo[id] != "") {
                            $.ajax({
                                url: "/ret/partymemberget/getPartyMemberById",
                                type: "post",
                                dataType: "json",
                                async: false,
                                data: {memberId: recordInfo[id]},
                                success: function (res) {
                                    if (res.status == "200") {
                                        if (res.list) {
                                            $("#" + id).val(res.list.userName);
                                        } else {
                                            $("#" + id).val("");
                                        }
                                    }
                                }
                            });
                        }
                    } else if (id == "trainUser2") {
                        $("#trainUser2").attr("data-value", recordInfo[id]);
                        if (recordInfo[id] != "") {
                            $.ajax({
                                url: "/ret/partymemberget/getPartyMemberById",
                                type: "post",
                                dataType: "json",
                                async: false,
                                data: {memberId: recordInfo[id]},
                                success: function (res) {
                                    if (res.status == "200") {
                                        if (res.list) {
                                            $("#" + id).val(res.list.userName);
                                        } else {
                                            $("#" + id).val("");
                                        }
                                    }
                                }
                            });
                        }
                    } else if (id == "linkName1") {
                        $("#linkName1").attr("data-value", recordInfo[id]);
                        if (recordInfo[id] != "") {
                            $.ajax({
                                url: "/ret/partymemberget/getPartyMemberById",
                                type: "post",
                                dataType: "json",
                                async: false,
                                data: {memberId: recordInfo[id]},
                                success: function (res) {
                                    if (res.status == "200") {
                                        if (res.list) {
                                            $("#" + id).val(res.list.userName);
                                        } else {
                                            $("#" + id).val("");
                                        }
                                    }
                                }
                            });
                        }
                    } else if (id == "linkName2") {
                        $("#linkName2").attr("data-value", recordInfo[id]);
                        if (recordInfo[id] != "") {
                            $.ajax({
                                url: "/ret/partymemberget/getPartyMemberById",
                                type: "post",
                                dataType: "json",
                                async: false,
                                data: {memberId: recordInfo[id]},
                                success: function (res) {
                                    if (res.status == "200") {
                                        if (res.list) {
                                            $("#" + id).val(res.list.userName);
                                        } else {
                                            $("#" + id).val("");
                                        }
                                    }
                                }
                            });
                        }
                    } else if (id == "partyOrgId") {
                        $("#partyOrgId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateApplay(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyMemberJoin",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function updateApplay(recordId) {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    if($("#userName").val()=="")
    {
        layer.msg("姓名不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyMemberJoin",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            title: $("#title").val(),
            userName: $("#userName").val(),
            userSex: $("input[name='userSex']:checked").val(),
            applyTime: $("#applyTime").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            isAdm1: getCheckBoxValue("isAdm1"),
            isAdm2: getCheckBoxValue("isAdm2"),
            isAdm3: getCheckBoxValue("isAdm3"),
            cardId: $("#cardId").val(),
            tel: $("#tel").val(),
            maturity: $("#maturity").val(),
            checkSitu: $("#checkSitu").val(),
            joinTime: $("#joinTime").val(),
            joinTime1: $("#joinTime1").val(),
            trainUser1: $("#trainUser1").attr("data-value"),
            trainUser2: $("#trainUser2").attr("data-value"),
            linkMan1: $("#linkMan1").attr("data-value"),
            linkMan2: $("#linkMan2").attr("data-value"),
            trainTime: $("#trainTime").val(),
            trainResult: $("#trainResult").val(),
            trainRemark: $("#trainRemark").code(),
            applyRemark: $("#applyRemark").code(),
            statusFlag: "0",
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

function goback() {
    $("#applydiv").hide();
    $("#applylistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/partyjoinapplydetails?recordId=" + recordId);
}

var partyOrgIdQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
