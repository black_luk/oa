let ue=UE.getEditor('remark');
$(function () {
    jeDate("#happenTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
        isinitVal: true
    });
    getCodeClass("type", "bigstory");
    $(".js-add-save").unbind("click").click(function () {
        insertBigStory();
    })
})

function insertBigStory() {
    $.ajax({
        url: "/set/oaset/insertBigStory",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            type: $("#type").val(),
            mainPic: $("#file").attr("data-value"),
            happenTime: $("#happenTime").val(),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
