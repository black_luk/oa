package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberJoinMapper extends MyMapper<PartyMemberJoin> {


    /**
     * @param orgId
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberJoinApplyList
     * @Description:  获取入党申请列表
     */
    public List<Map<String, String>> getMemberJoinApplyList(@Param(value = "orgId") String orgId, @Param(value = "statusFlag") String statusFlag, @Param(value = "partyOrgId") String partyOrgId,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getActivistsMemberIdList
     * @Description:  获取积极份子列表
     */
    public List<Map<String, String>> getActivistsMemberIdList(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getTargetMemberIdList
     * @Description:  获取发展对象人员列表
     */
    public List<Map<String, String>> getTargetMemberIdList(@Param(value = "orgId") String orgId);
}
