package com.core136.mapper.project;

import com.core136.bean.project.ProSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProSortMapper extends MyMapper<ProSort> {
    /**
     * 获取项目分类树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProSortTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  sortId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);

    /**
     * 获取项目分类树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProSortForProTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

}
