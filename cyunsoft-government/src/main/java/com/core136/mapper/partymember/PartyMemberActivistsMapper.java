package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberActivists;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberActivistsMapper extends MyMapper<PartyMemberActivists> {
    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getActivistsRecordList
     * @Description:  获取积极份子审批记录
     */
    public List<Map<String, String>> getActivistsRecordList(@Param(value = "orgId") String orgId, @Param(value = "status") String status,
                                                            @Param(value = "partyOrgId") String partyOrgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                            @Param(value = "search") String search);


}
