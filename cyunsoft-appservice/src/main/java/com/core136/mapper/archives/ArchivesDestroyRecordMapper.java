package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesDestroyRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesDestroyRecordMapper extends MyMapper<ArchivesDestroyRecord> {
    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesDestoryFileList
     * @Description:  获取销毁记录
     */
    public List<Map<String, String>> getArchivesDestoryFileList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesDestoryVolumeList
     * @Description:  获取销毁记录
     */
    public List<Map<String, String>> getArchivesDestoryVolumeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

}
