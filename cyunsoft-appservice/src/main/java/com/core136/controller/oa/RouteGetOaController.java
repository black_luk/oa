/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetOaController.java
 * @Package com.core136.controller.oa
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:55:10
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.oa;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.attend.AttendConfig;
import com.core136.bean.oa.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.attend.AttendConfigService;
import com.core136.service.attend.AttendService;
import com.core136.service.oa.*;
import com.core136.service.sys.MobileSmsService;
import com.core136.service.sys.SmsService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @ClassName: RoutGetOaController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:55:10
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/oaget")
public class RouteGetOaController {
    private SmsService smsService;

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    private AttendConfigService attendConfigService;

    @Autowired
    public void setAttendConfigService(AttendConfigService attendConfigService) {
        this.attendConfigService = attendConfigService;
    }

    private AttendService attendService;

    @Autowired
    public void setAttendService(AttendService attendService) {
        this.attendService = attendService;
    }

    private MobileSmsService mobileSmsService;

    @Autowired
    public void setMobileSmsService(MobileSmsService mobileSmsService) {
        this.mobileSmsService = mobileSmsService;
    }

    private LeadActivityService leadActivityService;

    @Autowired
    public void setLeadActivityService(LeadActivityService leadActivityService) {
        this.leadActivityService = leadActivityService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private VoteService voteService;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    private VoteItemService voteItemService;

    @Autowired
    public void setVoteItemService(VoteItemService voteItemService) {
        this.voteItemService = voteItemService;
    }

    private VoteResultService voteResultService;

    @Autowired
    public void setVoteResultService(VoteResultService voteResultService) {
        this.voteResultService = voteResultService;
    }

    private BigStoryService bigStoryService;

    @Autowired
    public void setBigStoryService(BigStoryService bigStoryService) {
        this.bigStoryService = bigStoryService;
    }

    private AddressBookService addressBookService;

    @Autowired
    public void setAddressBookService(AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }

    private LeaderMailboxConfigService leaderMailboxConfigService;

    @Autowired
    public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
        this.leaderMailboxConfigService = leaderMailboxConfigService;
    }

    private LeaderMailboxService leaderMailboxService;

    @Autowired
    public void setLeaderMailboxService(LeaderMailboxService leaderMailboxService) {
        this.leaderMailboxService = leaderMailboxService;
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getLeaderMailBoxList
     * @Description:  获取领导信箱记录列表
     */
    @RequestMapping(value = "/getLeaderMailBoxList", method = RequestMethod.POST)
    public RetDataBean getLeaderMailBoxList(PageParam pageParam) {
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isPermitted("leadermailbox:manage")) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
        }
        Account account = accountService.getRedisAUserInfoToAccount();
        LeaderMailboxConfig leaderMailboxConfig = new LeaderMailboxConfig();
        leaderMailboxConfig.setOrgId(account.getOrgId());
        leaderMailboxConfig = leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig);
        if(leaderMailboxConfig==null)
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
        }
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            if (leaderMailboxConfig.getLeader().equals(account.getAccountId())) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leaderMailboxService.getLeaderMailBoxList(pageParam));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyLeaderMailBoxList
     * @Description:  获取领导信箱信息列表
     */
    @RequestMapping(value = "/getMyLeaderMailBoxList", method = RequestMethod.POST)
    public RetDataBean getMyLeaderMailBoxList(PageParam pageParam) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadermailbox:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = leaderMailboxService.getMyLeaderMailBoxList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param leaderMailboxConfig
     * @return RetDataBean
     * @Title: getLeaderMailboxConfigById
     * @Description:  领导信箱设置祥情
     */
    @RequestMapping(value = "/getLeaderMailboxConfigById", method = RequestMethod.POST)
    public RetDataBean getLeaderMailboxConfigById(LeaderMailboxConfig leaderMailboxConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            leaderMailboxConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param leaderMailbox
     * @return RetDataBean
     * @Title: getLeaderMailboxById
     * @Description:  获取领导信箱详情
     */
    @RequestMapping(value = "/getLeaderMailboxById", method = RequestMethod.POST)
    public RetDataBean getLeaderMailboxById(LeaderMailbox leaderMailbox) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadermailbox:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            leaderMailbox.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leaderMailboxService.selectOneLeaderMailbox(leaderMailbox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getLeadActivityLsitForDesk
     * @Description:
     */
    @RequestMapping(value = "/getLeadActivityLsitForDesk", method = RequestMethod.POST)
    public RetDataBean getLeadActivityLsitForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leadActivityService.getLeadActivityLsitForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param search
     * @return RetDataBean
     * @Title: getQueryAddressBookList
     * @Description:  查询通讯录人员
     */
    @RequestMapping(value = "/getQueryAddressBookList", method = RequestMethod.POST)
    public RetDataBean getQueryAddressBookList(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getQueryAddressBookList(account.getOrgId(), account.getAccountId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param accountId
     * @return RetDataBean
     * @Title: getMyFriendsInfo
     * @Description:  获取同事联系方式
     */
    @RequestMapping(value = "/getMyFriendsInfo", method = RequestMethod.POST)
    public RetDataBean getMyFriendsInfo(String accountId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getMyFriendsInfo(account.getOrgId(), accountId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param bookType
     * @return RetDataBean
     * @Title: getMyShareAddressBookList
     * @Description:  获取他人共享的人员列表
     */
    @RequestMapping(value = "/getMyShareAddressBookList", method = RequestMethod.POST)
    public RetDataBean getMyShareAddressBookList(String bookType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getMyShareAddressBookList(account.getOrgId(), account.getAccountId(), bookType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param levelId
     * @return RetDataBean
     * @Title: getMyFriendsByLevel
     * @Description:  获取同事列表
     */
    @RequestMapping(value = "/getMyFriendsByLevel", method = RequestMethod.POST)
    public RetDataBean getMyFriendsByLevel(String levelId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getMyFriendsByLevel(account.getOrgId(), levelId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param addressBook
     * @return RetDataBean
     * @Title: getMyAddressBookList
     * @Description:  获取个人通讯录列表
     */
    @RequestMapping(value = "/getMyAddressBookList", method = RequestMethod.POST)
    public RetDataBean getMyAddressBookList(AddressBook addressBook) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            addressBook.setCreateUser(account.getAccountId());
            addressBook.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getAddressBook(addressBook));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param addressBook
     * @return
     */
    @RequestMapping(value = "/getAddressBookById", method = RequestMethod.POST)
    public RetDataBean getAddressBookById(AddressBook addressBook) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            addressBook.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.selectOneAddressBook(addressBook));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBigStoryList
     * @Description:  获取所有大记事
     */
    @RequestMapping(value = "/getBigStoryList", method = RequestMethod.POST)
    public RetDataBean getBigStoryList() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, bigStoryService.getBigStoryList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param type
     * @return RetDataBean
     * @Title: getBigStoryListForManage
     * @Description:  获取大纪事管理列表
     */
    @RequestMapping(value = "/getBigStoryListForManage", method = RequestMethod.POST)
    public RetDataBean getBigStoryListForManage(PageParam pageParam, String beginTime, String endTime, String type) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("happen_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = bigStoryService.getBigStoryListForManage(pageParam, type, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param bigStory
     * @return RetDataBean
     * @Title: getBigStoryById
     * @Description:  获取大纪事详情
     */
    @RequestMapping(value = "/getBigStoryById", method = RequestMethod.POST)
    public RetDataBean getBigStoryById(BigStory bigStory) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            bigStory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, bigStoryService.selectOneBigStory(bigStory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param voteId
     * @return RetDataBean
     * @Title: voteDetailsForUser
     * @Description:  获取投票人员列表
     */
    @RequestMapping(value = "/voteDetailsForUser", method = RequestMethod.POST)
    public RetDataBean voteDetailsForUser(String voteId, String recordId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteResultService.voteDetailsForUser(account.getOrgId(), voteId, recordId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param voteId
     * @return RetDataBean
     * @Title: getVoteResult
     * @Description:  获取投票结果
     */
    @RequestMapping(value = "/getVoteResult", method = RequestMethod.POST)
    public RetDataBean getVoteResult(String voteId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteResultService.getVoteResult(account.getOrgId(), voteId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @return RetDataBean
     * @Title: getMyOldVoteListForVote
     * @Description:  获取历史投票列表
     */
    @RequestMapping(value = "/getMyOldVoteListForVote", method = RequestMethod.POST)
    public RetDataBean getMyOldVoteListForVote(PageParam pageParam,
                                               String voteType,
                                               String beginTime,
                                               String endTime,
                                               String status
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = voteService.getMyOldVoteListForVote(pageParam, voteType, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param voteResult
     * @return RetDataBean
     * @Title: getVoteResultCount
     * @Description:  判断是否已经投过票
     */
    @RequestMapping(value = "/getVoteResultCount", method = RequestMethod.POST)
    public RetDataBean getVoteResultCount(VoteResult voteResult) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            voteResult.setCreateUser(account.getAccountId());
            voteResult.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteResultService.getVoteResultCount(voteResult));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param voteId
     * @param recordId
     * @return RetDataBean
     * @Title: getVoteChildItemsList
     * @Description:  获取投票明细项列表
     */
    @RequestMapping(value = "/getVoteChildItemsList", method = RequestMethod.POST)
    public RetDataBean getVoteChildItemsList(String voteId, String recordId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteItemService.getVoteChildItemsList(account.getOrgId(), voteId, recordId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param voteId
     * @return RetDataBean
     * @Title: getVoteItemsList
     * @Description:  获取投票项
     */
    @RequestMapping(value = "/getVoteItemsList", method = RequestMethod.POST)
    public RetDataBean getVoteItemsList(String voteId) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            String nowTime = SysTools.getTime("yyyy-MM-dd");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteItemService.getVoteItemsList(userInfo.getOrgId(), nowTime, userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), voteId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyVoteListForVote
     * @Description:   获取待我投票列表
     */
    @RequestMapping(value = "/getMyVoteListForVote", method = RequestMethod.POST)
    public RetDataBean getMyVoteListForVote(PageParam pageParam) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            } else {
                pageParam.setSort("v." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(userInfo.getOrgId());
            PageInfo<Map<String, String>> pageInfo = voteService.getMyVoteListForVote(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param voteId
     * @return RetDataBean
     * @Title: getVoteItemListForManage
     * @Description:  获取投票管理列表
     */
    @RequestMapping(value = "/getVoteItemListForManage", method = RequestMethod.POST)
    public RetDataBean getVoteItemListForManage(PageParam pageParam,
                                                String voteId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.sort_no");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = voteItemService.getVoteItemListForManage(pageParam, voteId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @return RetDataBean
     * @Title: getVoteListForManage
     * @Description:  获取投票管理列表
     */
    @RequestMapping(value = "/getVoteListForManage", method = RequestMethod.POST)
    public RetDataBean getVoteListForManage(PageParam pageParam,
                                            String voteType,
                                            String beginTime,
                                            String endTime,
                                            String status
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            } else {
                pageParam.setSort("v." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = voteService.getVoteListForManage(pageParam, voteType, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param leader
     * @return RetDataBean
     * @Title: getLeadActivityLsit
     * @Description:  获取领导行程列表
     */
    @RequestMapping(value = "/getLeadActivityLsit", method = RequestMethod.POST)
    public RetDataBean getLeadActivityLsit(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String leader
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = leadActivityService.getLeadActivityLsit(pageParam, beginTime, endTime, leader);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param leader
     * @return RetDataBean
     * @Title: getLeadActivityQueryLsit
     * @Description:  查询程领导日程
     */
    @RequestMapping(value = "/getLeadActivityQueryLsit", method = RequestMethod.POST)
    public RetDataBean getLeadActivityQueryLsit(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String leader
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(userInfo.getOrgId());
            PageInfo<Map<String, String>> pageInfo = leadActivityService.getLeadActivityQueryLsit(pageParam, beginTime, endTime, leader);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vote
     * @return RetDataBean
     * @Title: getVoteById
     * @Description:  获取投票详情
     */
    @RequestMapping(value = "/getVoteById", method = RequestMethod.POST)
    public RetDataBean getVoteById(Vote vote) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            vote.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.selectOneVote(vote));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param voteItem
     * @return RetDataBean
     * @Title: getVoteItemById
     * @Description:  获取投票项详情
     */
    @RequestMapping(value = "/getVoteItemById", method = RequestMethod.POST)
    public RetDataBean getVoteItemById(VoteItem voteItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            voteItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteItemService.selectOneVoteItem(voteItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param leadActivity
     * @return RetDataBean
     * @Title: getLeadActivityById
     * @Description:  获取领导行程详情
     */
    @RequestMapping(value = "/getLeadActivityById", method = RequestMethod.POST)
    public RetDataBean getLeadActivityById(LeadActivity leadActivity) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            leadActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leadActivityService.selectOneLeadActivity(leadActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getTotalAttendList
     * @Description:  获取考勤列表
     * @param: request
     * @param: pageParam
     * @param: type
     * @param: beginTime
     * @param: endTime
     * @param: deptId
     * @param: createUser
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getTotalAttendList", method = RequestMethod.POST)
    public RetDataBean getTotalAttendList(
            PageParam pageParam,
            String type,
            String beginTime,
            String endTime,
            String deptId,
            String createUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getTotalAttendList(pageParam, type, beginTime, endTime, deptId, createUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyTravelList
     * @Description:  获取出差列表
     */
    @RequestMapping(value = "/getMyTravelList", method = RequestMethod.POST)
    public RetDataBean getMyTravelList(
            PageParam pageParam,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getMyTravelList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyOutattendList
     * @Description:  获取外出列表
     */
    @RequestMapping(value = "/getMyOutattendList", method = RequestMethod.POST)
    public RetDataBean getMyOutattendList(
            PageParam pageParam,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getMyOutattendList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyOverTimeList
     * @Description:  获取加班列表
     */
    @RequestMapping(value = "/getMyOverTimeList", method = RequestMethod.POST)
    public RetDataBean getMyOverTimeList(
            PageParam pageParam,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getMyOverTimeList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyDutyList
     * @Description:  获取值班列表
     */
    @RequestMapping(value = "/getMyDutyList", method = RequestMethod.POST)
    public RetDataBean getMyDutyList(
            PageParam pageParam,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getMyDutyList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyLevelList
     * @Description:  获取人员请假列表
     */
    @RequestMapping(value = "/getMyLevelList", method = RequestMethod.POST)
    public RetDataBean getMyLevelList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String type
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = attendService.getMyLevelList(pageParam, beginTime, endTime, type);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAttendYearList
     * @Description:  获取年份列表
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAttendYearList", method = RequestMethod.POST)
    public RetDataBean getAttendYearList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getAttendYearList(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMonthList
     * @Description:  获取月份
     * @param: request
     * @param: year
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMonthList", method = RequestMethod.POST)
    public RetDataBean getMonthList(String year) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getMonthList(account.getOrgId(), year, account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyAllAttendList
     * @Description:  获取个人一年内的考勤记录
     * @param: request
     * @param: year
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyAllAttendList", method = RequestMethod.POST)
    public RetDataBean getMyAllAttendList(HttpServletRequest request, String year, String type) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getMyAllAttendList(account.getOrgId(), year, account.getAccountId(), type));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getMySms
     * @Description:  获取个人消息列表
     * @param: request
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMySms", method = RequestMethod.POST)
    public RetDataBean getMySms(
            PageParam pageParam,
            String type,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sms_send_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = smsService.getMySms(pageParam, type, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMySendMoblieSms
     * @Description: (描述这个方法的作用)
     * @param: request
     * @param: pageParam
     * @param: module
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMySendMobileSms", method = RequestMethod.POST)
    public RetDataBean getMySendMobileSms(
            PageParam pageParam,
            String module,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.send_time");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = mobileSmsService.getMySendMobileSms(pageParam, beginTime, endTime, module);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getAttendConfigById
     * @Description:  获取考勤配置详情
     * @param: request
     * @param: attendConfig
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAttendConfigById", method = RequestMethod.POST)
    public RetDataBean getAttendConfigById(AttendConfig attendConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            attendConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendConfigService.selectOneAttendConfig(attendConfig));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAllAttendConfigList
     * @Description:  获取考勤列表
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllAttendConfigList", method = RequestMethod.POST)
    public RetDataBean getAllAttendConfigList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendConfigService.getAllAttendConfigList(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyAttendConfigList
     * @Description:  获取所有的考勤记录
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyAttendConfigList", method = RequestMethod.POST)
    public RetDataBean getMyAttendConfigList() {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendConfigService.getMyAttendConfigList(userInfo.getOrgId(), userInfo.getAttendConfigId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAttendConfigList
     * @Description:  获取考勤规则列表
     * @param: request
     * @param: sortId
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAttendConfigList", method = RequestMethod.POST)
    public RetDataBean getAttendConfigList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            AttendConfig attendConfig = new AttendConfig();
            attendConfig.setOrgId(account.getOrgId());
            PageInfo<AttendConfig> pageInfo = attendConfigService.getAttendConfigList(pageParam, attendConfig);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
