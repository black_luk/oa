let ue=UE.getEditor("remark");
$(function () {
    getCodeClass("dataType", "dataType");
    getSmsConfig("msgType", "datainfo");
    $("#createbut").unbind("click").click(function () {
        sendNews();
    });
    jeDate("#sendTime", {
        format: "YYYY-MM-DD hh:mm:ss",
        isinitVal: true
    });
    $(".js-add-save").unbind("click").click(function () {
        uploadinfo();
    })
});

function uploadinfo() {
    if ($("#title").val() == "") {
        layer.msg("信息标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/datauploadset/insertDataUploadInfo",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            dataType: $("#dataType").val(),
            deptId: $("#deptId").attr("data-value"),
            fromAccountId: $("#fromAccountId").attr("data-value"),
            approvedType: $("#approvedType").val(),
            approvedUser: $("#approvedUser").attr("data-value"),
            sendTime: $("#sendTime").val(),
            toUser: $("#toUser").attr("data-value"),
            attach: $("#hrattach").attr("data_value"),
            msgType: getCheckBoxValue("msgType"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
