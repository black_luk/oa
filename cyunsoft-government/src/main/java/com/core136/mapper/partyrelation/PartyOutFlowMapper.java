package com.core136.mapper.partyrelation;

import com.core136.bean.partyrelation.PartyOutFlow;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyOutFlowMapper extends MyMapper<PartyOutFlow> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyOutFlowList
     * @Description:  党员流出管理
     */
    public List<Map<String, String>> getPartyOutFlowList(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "beginTime") String beginTime, @Param("endTime") String endTime,
                                                         @Param(value = "search") String search);
}
