package com.core136.controller.meeting;


import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/app/core/meeting")
public class PageMeetingController {
    /**
     * @return ModelAndView
     * @Title: goMeetingDetails
     * @Description:  会议详情
     */
    @RequestMapping("/meetingdetails")
    @RequiresPermissions("/app/core/meeting/meetingdetails")
    public ModelAndView goMeetingDetails() {
        try {
            return new ModelAndView("/app/core/meeting/meetingdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: summarydetails
     * @Description:  会议记要详情
     */
    @RequestMapping("/summarydetails")
    @RequiresPermissions("/app/core/meeting/summarydetails")
    public ModelAndView summarydetails() {
        try {
            return new ModelAndView("/app/core/meeting/summarydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goSummary
     * @Description:  会议记要
     * @param: view
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/summary")
    @RequiresPermissions("/app/core/meeting/summary")
    public ModelAndView goSummary(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/meeting/summary");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/meeting/summarymanage");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("/app/core/meeting/summaryedit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSummaryQuery
     * @Description:  会议纪要查询
     */
    @RequestMapping("/summaryquery")
    @RequiresPermissions("/app/core/meeting/summaryquery")
    public ModelAndView goSummaryQuery() {
        try {
            return new ModelAndView("/app/core/meeting/summaryquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMyMeeting
     * @Description:  我的会议列表
     */
    @RequestMapping("/mymeeting")
    @RequiresPermissions("/app/core/meeting/mymeeting")
    public ModelAndView goMyMeeting(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/meeting/mymeeting");
            } else if (view.equals("manage")) {
                mv = new ModelAndView("/app/core/meeting/mymeetingmanage");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBommange
     * @Description  添加会议室
     */
    @RequestMapping("/addmeetingroom")
    @RequiresPermissions("/app/core/meeting/addmeetingroom")
    public ModelAndView addMeetingroom() {
        try {
            return new ModelAndView("/app/core/meeting/addroom");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: setmeetingdevice
     * @Description:  设置会议室设备
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/setmeetingdevice")
    @RequiresPermissions("/app/core/meeting/setmeetingdevice")
    public ModelAndView setmeetingdevice() {
        try {
            return new ModelAndView("/app/core/meeting/meetingdevice");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goApplymeeting
     * @Description:  申请会议
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/applymeeting")
    @RequiresPermissions("/app/core/meeting/applymeeting")
    public ModelAndView goApplymeeting(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/meeting/applymeeting");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/meeting/applymeetingmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: manage
     * @Description:  会议管理
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/manage")
    @RequiresPermissions("/app/core/meeting/manage")
    public ModelAndView manage(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/meeting/manage");
            } else {
                if (view.equals("old")) {
                    mv = new ModelAndView("/app/core/meeting/manageold");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
