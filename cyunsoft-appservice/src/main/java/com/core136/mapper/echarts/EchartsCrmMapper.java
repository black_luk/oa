package com.core136.mapper.echarts;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EchartsCrmMapper {
    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiCustomerIndustryPie
     * @Description:  获取CRM的行业占比
     */
    public List<Map<String, String>> getBiCustomerIndustryPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiCustomerKeepUserPie
     * @Description:  获取CRM销售人员的占比
     */
    public List<Map<String, String>> getBiCustomerKeepUserPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiCustomerAreaPie
     * @Description:  获取CRM地区占比
     */
    public List<Map<String, String>> getBiCustomerAreaPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiCustomerLevelPie
     * @Description:  获取CRM客户等级占比
     */
    public List<Map<String, String>> getBiCustomerLevelPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiInquiryByAccountPie
     * @Description:  获取询价单人员占比
     */
    public List<Map<String, String>> getBiInquiryByAccountPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getBiInquiryByMonthLine
     * @Description:  按月份统计工作量
     */
    public List<Map<String, Object>> getBiInquiryByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiInquiryByProductPie
     * @Description:  获取产品占比
     */
    public List<Map<String, String>> getBiInquiryByProductPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiInquiryByDeptPie
     * @Description:  部门询价单占比前10的占比
     */
    public List<Map<String, String>> getBiInquiryByDeptPie(@Param(value = "orgId") String orgId);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiQuotationByAccountPie
     * @Description:  获取报价单人员占比
     */
    public List<Map<String, String>> getBiQuotationByAccountPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getBiQuotationByMonthLine
     * @Description:  按月份统计报价单
     */
    public List<Map<String, Object>> getBiQuotationByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiQuotationByProductPie
     * @Description:  获取产品占比
     */
    public List<Map<String, String>> getBiQuotationByProductPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiQuotationByDeptPie
     * @Description:  部门报价单占比前10的占比
     */
    public List<Map<String, String>> getBiQuotationByDeptPie(@Param(value = "orgId") String orgId);
}
