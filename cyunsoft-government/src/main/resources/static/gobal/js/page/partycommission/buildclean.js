$(function () {
    jeDate("#happenTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getCodeClass("eventType", "build_clean_type");
    getCodeClass("eventTypeQuery", "build_clean_type");
    $('#remark').summernote({height: 200});
    $(".js-create").unbind("click").click(function () {
        $("#listdiv").hide();
        $("#creatediv").show();
        document.getElementById("form1").reset();
        $("#remark").code("");
        $("#show_attach").empty();
        $("#attach").attr("data_value", "");
        $(".js-update-save").unbind("click").click(function () {
            addBuildCleanRecord()
        })
    })
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partycommissionget/getPartyBuildCleanList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '事件标题',
            width: '100px'
        }, {
            field: 'subheading',
            width: '200px',
            title: '副标题'
        }, {
            field: 'eventType',
            title: '事件类型',
            width: '50px',
            formatter: function (value, row, index) {
                return getCodeClassName(value, "build_clean_type");
            }
        }, {
            field: 'happenTime',
            title: '事件时间',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '80px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        eventType: $("#eventTypeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    return html;
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partycommissionset/deletePartyBuildClean",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#listdiv").hide();
    $("#creatediv").show();
    document.getElementById("form1").reset();
    $("#remark").code("");
    $("#show_attach").empty();
    $("#attach").attr("data-value");
    $.ajax({
        url: "/ret/partycommissionget/getPartyBuildCleanById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateBuildCleanRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

}

function goback() {
    $("#creatediv").hide();
    $("#listdiv").show();
}

function addBuildCleanRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partycommissionset/insertPartyBuildClean",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            happenTime: $("#happenTime").val(),
            eventType: $("#eventType").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            console.log(data);
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                $("#creatediv").hide();
                $("#listdiv").show();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
}

function updateBuildCleanRecord(recordId) {
    $.ajax({
        url: "/set/partycommissionset/updatePartyBuildClean",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            title: $("#title").val(),
            happenTime: $("#happenTime").val(),
            eventType: $("#eventType").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            console.log(data);
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                $("#creatediv").hide();
                $("#listdiv").show();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
}
