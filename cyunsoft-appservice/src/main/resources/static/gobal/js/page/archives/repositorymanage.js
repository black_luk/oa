$(function () {
    $.ajax({
        url: "/ret/archivesget/getArchivesRepositoryList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordList = data.list;
                var html = "";
                for (var i = 0; i < recordList.length; i++) {
                    html += "<tr><td>" + (i + 1) + "</td><td><a href=\"javascript:void(0);details('" + recordList[i].repositoryId + "')\" style='cursor: pointer;'>" + recordList[i].title + "</a></td><td>" + getUserNameByStr(recordList[i].manageUser) + "</td>" +
                        "<td>" + recordList[i].createTime + "</td><td>" + recordList[i].createUserName + "</td><td><a href=\"javascript:void(0);edit('" + recordList[i].repositoryId + "')\" class=\"btn btn-primary btn-xs\">" + sysmsg['OPT_EDIT'] + "</a>&nbsp;&nbsp;" +
                        "<a href=\"javascript:void(0);deleteRepository('" + recordList[i].repositoryId + "')\" class=\"btn btn-darkorange btn-xs\">" + sysmsg['OPT_DELETE'] + "</a>"+
                        "</td></tr>";
                }
                $("#repositorylist").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
})

function details(repositoryId) {
    window.open("/app/core/archives/repositorydetails?repositoryId=" + repositoryId);
}

function deleteRepository(repositoryId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/archivesset/deleteArchivesRepository",
            type: "post",
            dataType: "json",
            data: {repositoryId: repositoryId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}
function goback() {
    $("#repositorydiv").hide();
    $("#repositorylist").show();
}
function edit(repositoryId) {
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#userPriv").attr("data-value","");
    $("#deptPriv").attr("data-value", "");
    $("#levelPriv").attr("data-value","");
    $("#manageUser").attr("data-value","");
    $("#repositorylistdiv").hide();
    $("#repositorydiv").show();
    $.ajax({
        url: "/ret/archivesget/getArchivesRepositoryById",
        type: "post",
        dataType: "json",
        data: {
            repositoryId: repositoryId,
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "userPriv") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getUserNameByStr(recordInfo[id]));
                    } else if (id == "deptPriv") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getDeptNameByDeptIdsrecordInfo[id]);
                    } else if (id == "levelPriv") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getUserLevelStr(recordInfo[id]));
                    } else if (id == "manageUser") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getUserNameByStr(recordInfo[id]));
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateRepository(repositoryId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function updateRepository(repositoryId) {
    if ($("#title").val() == "") {
        layer.msg(archivesMsg['REPOSITORY_MANAGE_TITLE_CANNOT_EMPTY']);
        return;
    }
    $.ajax({
        url: "/set/archivesset/updateArchivesRepository",
        type: "post",
        dataType: "json",
        data: {
            repositoryId: repositoryId,
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            manageUser: $("#manageUser").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                //location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function goback() {
    $("#repositorydiv").hide();
    $("#repositorylistdiv").show();
}

