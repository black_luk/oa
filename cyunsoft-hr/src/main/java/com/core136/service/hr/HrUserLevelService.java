/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserLevelServic.java
 * @Package com.core136.service.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月30日 上午10:42:13
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.hr;

import com.core136.bean.hr.HrUserLevel;
import com.core136.mapper.hr.HrUserLevelMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @author lsq
 */
@Service
public class HrUserLevelService {
    private HrUserLevelMapper hrUserLevelMapper;

    @Autowired
    public void setHrUserLevelMapper(HrUserLevelMapper hrUserLevelMapper) {
        this.hrUserLevelMapper = hrUserLevelMapper;
    }

    public int insertHrUserLevel(HrUserLevel hrUserLevel) {
        return hrUserLevelMapper.insert(hrUserLevel);
    }

    public int deleteHrUserLevel(HrUserLevel hrUserLevel) {
        return hrUserLevelMapper.delete(hrUserLevel);
    }

    public int updateHrUserLevel(Example example, HrUserLevel hrUserLevel) {
        return hrUserLevelMapper.updateByExampleSelective(hrUserLevel, example);
    }

    public HrUserLevel selectOneHrUserLevel(HrUserLevel hrUserLevel) {
        return hrUserLevelMapper.selectOne(hrUserLevel);
    }

    public List<HrUserLevel> selectByExample(Example example) {
        return hrUserLevelMapper.selectByExample(example);
    }

    /**
     * @Title: getHrUserLevelChart
     * @Description:  获取行政级别CHART数据
     * @param: orgId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    public List<Map<String, Object>> getHrUserLevelChart(String orgId, String levelId) {
        //  Auto-generated method stub
        return hrUserLevelMapper.getHrUserLevelChart(orgId, levelId);
    }

    /**
     * @Title: getAllHrUserLevelChart
     * @Description:  获取行政级别CHART数据
     * @param: orgId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    public List<Map<String, Object>> getAllHrUserLevelChart(String orgId, String levelId) {
        List<Map<String, Object>> listMapper = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> userLevelList = getHrUserLevelChart(orgId, levelId);
        for (int i = 0; i < userLevelList.size(); i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map = userLevelList.get(i);
            map.put("children", getAllHrUserLevelChart(orgId, userLevelList.get(i).get("id").toString()));
            listMapper.add(map);
        }
        return listMapper;
    }

    /**
     * @param orgId
     * @param levelIds
     * @return List<Map < String, String>>
     * @Title: getHrUserLevelByStr
     * @Description:  获取HR的行政级别名称
     */
    public List<Map<String, String>> getHrUserLevelByStr(String orgId, String levelIds) {
        if (StringUtils.isNotBlank(levelIds)) {
            String[] levelIdArr = levelIds.split(",");
            List<String> list = Arrays.asList(levelIdArr);
            return hrUserLevelMapper.getHrUserLevelByStr(orgId, list);
        } else {
            return null;
        }
    }

}
