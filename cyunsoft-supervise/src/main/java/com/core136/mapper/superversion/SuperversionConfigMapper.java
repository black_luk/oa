package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuperversionConfigMapper extends MyMapper<SuperversionConfig> {

    /**
     * 获取分类树结构
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getSuperverionConfigTree(@Param(value = "orgId") String orgId);

    /**
     * @Title: getAllSuperversionConfigList
     * @Description:  获取类型与领导列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getAllSuperversionConfigList(@Param(value = "orgId") String orgId);

    /**
     * @Title: getMySuperversionConfigList
     * @Description:  与我有关的分类列表
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMySuperversionConfigList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * @Title: getQuerySuperversionForType
     * @Description:  按类型汇总
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getQuerySuperversionForType(@Param(value = "orgId") String orgId);
}
