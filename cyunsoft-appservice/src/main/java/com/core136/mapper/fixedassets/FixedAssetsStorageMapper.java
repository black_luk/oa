/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsStorageMapper.java
 * @Package com.core136.mapper.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月17日 上午10:12:21
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.fixedassets;

import com.core136.bean.fixedassets.FixedAssetsStorage;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface FixedAssetsStorageMapper extends MyMapper<FixedAssetsStorage> {

    /**
     *
     * @Title: getFixedAssetsStorageList
     * @Description:  获取仓库列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getFixedAssetsStorageList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     *
     * @Title: getAllFixedAssetsStorageList
     * @Description:  获取仓库列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getAllFixedAssetsStorageList(@Param(value = "orgId") String orgId);

}
