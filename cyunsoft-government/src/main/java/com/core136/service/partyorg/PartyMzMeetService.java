package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyMzMeet;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyMzMeetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMzMeetService {
    private PartyMzMeetMapper partyMzMeetMapper;

    @Autowired
    public void setPartyMzMeetMapper(PartyMzMeetMapper partyMzMeetMapper) {
        this.partyMzMeetMapper = partyMzMeetMapper;
    }

    public int insertPartyMzMeet(PartyMzMeet partyMzMeet) {
        return partyMzMeetMapper.insert(partyMzMeet);
    }

    public int deletePartyMzMeet(PartyMzMeet partyMzMeet) {
        return partyMzMeetMapper.delete(partyMzMeet);
    }

    public int updatePartyMzMeet(Example example, PartyMzMeet partyMzMeet) {
        return partyMzMeetMapper.updateByExampleSelective(partyMzMeet, example);
    }

    public PartyMzMeet selectOnePartyMzMeet(PartyMzMeet partyMzMeet) {
        return partyMzMeetMapper.selectOne(partyMzMeet);
    }

    /**
     * @param orgId
     * @param meetType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMzMeetList
     * @Description:  获取民主生活会记录
     */
    public List<Map<String, String>> getMzMeetList(String orgId, String meetType, String meetYear, String beginTime, String endTime, String partyOrgId, String search) {
        return partyMzMeetMapper.getMzMeetList(orgId, meetType, meetYear, beginTime, endTime, partyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param meetType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMzMeetList
     * @Description:  获取民主生活会记录
     */
    public PageInfo<Map<String, String>> getMzMeetList(PageParam pageParam, String meetType, String meetYear, String beginTime, String endTime, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMzMeetList(pageParam.getOrgId(), meetType, meetYear, beginTime, endTime, partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
