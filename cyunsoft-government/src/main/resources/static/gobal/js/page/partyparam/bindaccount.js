var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};
$(function () {
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    $("#butalldel").unbind("click").click(function () {
        var records = $("#myTable").bootstrapTable('getSelections');
        if (records.length == 0) {
            layer.msg("请先选择解绑党员！");
            return;
        } else {
            var memberArr = [];
            for (var i = 0; i < records.length; i++) {
                memberArr.push(records[i].memberId);
            }
            unbindaccount(memberArr.join(","));
        }
    })
    query("");
});

function zTreeOnClick(event, treeId, treeNode) {
    $("#myTable").bootstrapTable('destroy');
    if (treeNode.partyOrgId == "0") {
        query("");
    } else {
        query(treeNode.partyOrgId);
    }
}

function query(partyOrgId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getPartyMemberAccountListByPartyOrgId?partyOrgId=' + partyOrgId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'memberId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名'
        }, {
            field: 'accountId',
            title: '账号状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value != "" && value != null) {
                    return value;
                } else {
                    return '<a href="javascript:void(0);" class="btn btn-danger shiny btn-xs">未绑定</a>';
                }
            }
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '所属党组织'
        }, {
            field: 'userType',
            title: '党员类型',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                }
            }
        }, {
            field: 'postStatus',
            title: '党籍状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                } else if (value == "3") {
                    return "已出党";
                } else if (value == "4") {
                    return "已停止党籍";
                } else if (value == "5") {
                    return "已死亡";
                }
            }
        }, {
            field: 'mobileNo',
            title: '联系电话',
            width: '100px',
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '180px',
            formatter: function (value, row, index) {
                return createOptBtn(row.memberId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(memberId) {
    var html = "<a href=\"javascript:void(0);bindaccount('" + memberId + "')\" class=\"btn btn-success btn-xs\">绑定账号</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);unbindaccount('" + memberId + "')\" class=\"btn btn-primary btn-xs\">解绑账号</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function bindaccount(memberId) {
    $("#accountId").attr("data-value", "");
    $("#accountId").val("");
    $("#bindaccountmodal").modal("show");
    $(".js-bangding").unbind("click").click(function () {
        var accountId = $("#accountId").attr("data-value");
        $.ajax({
            url: "/set/partymemberset/setMemberAccount",
            type: "post",
            dataType: "json",
            data: {
                memberId: memberId,
                accountId: accountId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                    $("#bindaccountmodal").modal("hide");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    })
}

function unbindaccount(memberId) {
    if (confirm("确定解绑该党员的账号吗？")) {
        $.ajax({
            url: "/set/partymemberset/setUnbindAccount",
            type: "post",
            dataType: "json",
            data: {
                memberIds: memberId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


