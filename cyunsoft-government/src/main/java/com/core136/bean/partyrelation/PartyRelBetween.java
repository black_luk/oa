package com.core136.bean.partyrelation;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyRelBetween
 * @Description: 支部间调转记录
 * @author: 稠云技术
 * @date: 2021年3月2日 上午11:07:32
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_rel_between")
public class PartyRelBetween implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String oldPartyOrgId;
    private String relPartyOrgId;
    private String memberId;
    private String relType;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getOldPartyOrgId() {
        return oldPartyOrgId;
    }

    public void setOldPartyOrgId(String oldPartyOrgId) {
        this.oldPartyOrgId = oldPartyOrgId;
    }

    public String getRelPartyOrgId() {
        return relPartyOrgId;
    }

    public void setRelPartyOrgId(String relPartyOrgId) {
        this.relPartyOrgId = relPartyOrgId;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }


}
