package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMember;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberMapper extends MyMapper<PartyMember> {

    /**
     * @param orgId
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyMemberListByPartyOrgId
     * @Description:  按所属党组织获取党员列表
     */
    public List<Map<String, String>> getPartyMemberListByPartyOrgId(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyMemberAccountListByPartyOrgId
     * @Description:  获取账号绑定党员列表
     */
    public List<Map<String, String>> getPartyMemberAccountListByPartyOrgId(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);


    /**
     * @param orgId
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberInFlowList
     * @Description:  获取流入党员列表
     */
    public List<Map<String, String>> getMemberInFlowList(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId,
                                                         @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param partyOrgId
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyVerifyList
     * @Description:  获取考核党员列表
     */
    public List<Map<String, String>> getPartyVerifyList(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "year") String year, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param partyOrgId
     * @return List<Map < String, String>>
     * @Title: getSelectPartyMemberByPartyId
     * @Description:  按党支部获取党员列表
     */
    public List<Map<String, String>> getSelectPartyMemberByPartyId(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId);

    /**
     * @param orgId
     * @param searchuser
     * @return List<Map < String, String>>
     * @Title: getPartyMemberBySearchuser
     * @Description:  党员选择时查询
     */
    public List<Map<String, String>> getPartyMemberBySearchuser(@Param(value = "orgId") String orgId, @Param(value = "searchuser") String searchuser);

    /**
     * @param orgId
     * @param list
     * @return List<Map < String, String>>
     * @Title: getPartyMemeberByIds
     * @Description:  获取党员姓名列表
     */
    public List<Map<String, String>> getPartyMemeberByIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

    /**
     * @param orgId
     * @param memberId
     * @return Map<String, String>
     * @Title: getMyMemberInfoByMemberId
     * @Description:  获取个人党员信息
     */
    public Map<String, String> getMyMemberInfoByMemberId(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);
}
