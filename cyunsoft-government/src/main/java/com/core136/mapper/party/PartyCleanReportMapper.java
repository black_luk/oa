package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanReport;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanReportMapper extends MyMapper<PartyCleanReport> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param reportType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanReportList
     * @Description:  获取报告列表
     */
    public List<Map<String, String>> getCleanReportList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId, @Param(value = "reportType") String reportType,
                                                        @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanReportListForPortal
     * @Description:  获取门户领导报告列表
     */
    public List<Map<String, String>> getMyCleanReportListForPortal(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanReportList
     * @Description:  获取更多领导报告列表
     */
    public List<Map<String, String>> getMyCleanReportList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
