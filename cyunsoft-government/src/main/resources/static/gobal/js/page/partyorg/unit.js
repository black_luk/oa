var unittypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("unitTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#unitType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var unitAffiliationsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitSuboTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("unitAffiliationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#unitAffiliation");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var industrysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitIndustryTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("industryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#industry");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var serviceIndustrysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitServiceTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("serviceIndustryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#serviceIndustry");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/partyparamget/getUnitTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#unitTypeTree"), unittypesetting, data);
        }
    });
    $("#unitType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#unitTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getUnitSuboTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#unitAffiliationTree"), unitAffiliationsetting, data);
        }
    });
    $("#unitAffiliation").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#unitAffiliationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getUnitIndustryTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#industryTree"), industrysetting, data);
        }
    });
    $("#industry").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#industryContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getUnitServiceTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#serviceIndustryTree"), serviceIndustrysetting, data);
        }
    });
    $("#serviceIndustry").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#serviceIndustryContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });

    $("#createbut").unbind("click").click(function () {
        addPartyUnitBase();
    })
    var select2 = $("#subordinateUnit").select2({
        ajax: {
            url: "/ret/partyorgget/getSelect2UnitBaseList",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (datas) {
                var datares = {
                    results: []
                };
                var datalist = datas.list;
                datares.results.push({
                    id: "0",
                    text: "无"
                });
                for (var i = 0; i < datalist.length; i++) {
                    var record = datalist[i];
                    var name = record.unitName != '' ? record.unitName : record.shortName;
                    var option = {
                        "id": record.unitId,
                        "text": name
                    };
                    datares.results.push(option);
                }
                return datares;
            },
            cache: true
        },
        language: 'zh-CN',
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        placeholder: "请输入单位名称",
    })
})

function addPartyUnitBase() {
    if ($("#unitName").val()== "") {
        layer.msg("单位名称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyUnitBase",
        type: "post",
        dataType: "json",
        data: {
            unitName: $("#unitName").val(),
            shortName: $("#shortName").val(),
            unitType: $("#unitType").attr("data-value"),
            partyStatus: $("#partyStatus").val(),
            creditCode: $("#creditCode").val(),
            unitAffiliation: $("#unitAffiliation").attr("data-value"),
            industry: $("#industry").attr("data-value"),
            serviceIndustry: $("#serviceIndustry").attr("data-value"),
            address: $("#address").val(),
            legalPerson: $("#legalPerson").val(),
            linkTel: $("#linkTel").val(),
            unitType1: getCheckBoxValue("unitType1"),
            unitType2: getCheckBoxValue("unitType2"),
            unitType3: getCheckBoxValue("unitType3"),
            subordinateUnit: $("#subordinateUnit").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}
