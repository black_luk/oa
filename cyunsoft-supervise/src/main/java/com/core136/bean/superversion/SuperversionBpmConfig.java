package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: SuperversionBpmConfig
 * @Description: 督查督办流程设置
 * @author: 稠云技术
 * @date: Jun 8, 2021 11:13:16 AM
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "superversion_bpm_config")
public class SuperversionBpmConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String configId;
    private String flowIds;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getFlowIds() {
        return flowIds;
    }

    public void setFlowIds(String flowIds) {
        this.flowIds = flowIds;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
