package com.core136.service.vehicle;

import com.core136.bean.sys.PageParam;
import com.core136.bean.vehicle.VehicleDriver;
import com.core136.common.utils.SysTools;
import com.core136.mapper.vehicle.VehicleDriverMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class VehicleDriverService {
    private VehicleDriverMapper vehicleDriverMapper;

    @Autowired
    public void setVehicleDriverMapper(VehicleDriverMapper vehicleDriverMapper) {
        this.vehicleDriverMapper = vehicleDriverMapper;
    }

    public int insertVehicleDriver(VehicleDriver vehicleDriver) {
        return vehicleDriverMapper.insert(vehicleDriver);
    }

    public int deleteVehicleDriver(VehicleDriver vehicleDriver) {
        return vehicleDriverMapper.delete(vehicleDriver);
    }

    public int updateVehicleDriver(Example example, VehicleDriver vehicleDriver) {
        return vehicleDriverMapper.updateByExampleSelective(vehicleDriver, example);
    }

    public VehicleDriver selectOneVehicleDriver(VehicleDriver vehicleDriver) {
        return vehicleDriverMapper.selectOne(vehicleDriver);
    }


    public List<Map<String, String>> getVehicleDriverListForSelect(String orgId) {
        return vehicleDriverMapper.getVehicleDriverListForSelect(orgId);
    }

    public List<Map<String, String>> getVehicleDriverList(String orgId) {
        return vehicleDriverMapper.getVehicleDriverList(orgId);
    }

    public PageInfo<Map<String, String>> getVehicleDriverList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleDriverList(pageParam.getOrgId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getNextDayList() throws ParseException {
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < 15; i++) {
            Map<String, String> map = new HashMap<String, String>();
            String dateStr = SysTools.getFetureDate(i);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
            Date date = simpleDateFormat.parse(dateStr);
            int weekInt = SysTools.getDayOfWeek(date);
            if (weekInt == 1) {
                map.put("week", "周一");
            } else if (weekInt == 2) {
                map.put("week", "周二");
            } else if (weekInt == 3) {
                map.put("week", "周三");
            } else if (weekInt == 4) {
                map.put("week", "周四");
            } else if (weekInt == 5) {
                map.put("week", "周五");
            } else if (weekInt == 6) {
                map.put("week", "周六");
            } else if (weekInt == 7) {
                map.put("week", "周日");
            }
            map.put("date", dateStr);
            retList.add(map);
        }
        return retList;
    }


}
