package com.core136.mapper.project;

import com.core136.bean.project.ProRole;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProRoleMapper extends MyMapper<ProRole> {
}
