package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyTemporg;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyTemporgMapper extends MyMapper<PartyTemporg> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyTemporgList
     * @Description:  获取临时党组织列表
     */
    public List<Map<String, String>> getPartyTemporgList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
