/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetContractController.java
 * @Package com.core136.controller.im
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年6月20日 下午2:36:34
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.mobile;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.news.News;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.email.EmailService;
import com.core136.service.im.UserFriendsService;
import com.core136.service.meeting.MeetingService;
import com.core136.service.mobile.MobileLoginService;
import com.core136.service.mobile.MobileNetService;
import com.core136.service.mobile.MobileOrgService;
import com.core136.service.news.NewsService;
import com.core136.service.notice.NoticeService;
import com.core136.service.oa.DiaryService;
import com.core136.service.oa.LeadActivityService;
import com.core136.service.task.TaskGanttDataService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 * @ClassName: RoutGetContractController
 * @Description: 移动端API接口
 * @author: 稠云信息
 * @date: 2019年6月20日 下午2:36:34
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/mobile/mobileget")
public class RouteGetMobileController {
    private NoticeService noticeService;

    @Autowired
    public void setNoticeService(NoticeService noticeService) {
        this.noticeService = noticeService;
    }

    private NewsService newsService;

    @Autowired
    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    private EmailService emailService;

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    private MobileLoginService mobileLoginService;

    @Autowired
    public void setMobileLoginService(MobileLoginService mobileLoginService) {
        this.mobileLoginService = mobileLoginService;
    }

    private MeetingService meetingService;

    @Autowired
    public void setMeetingService(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private MobileOrgService mobileOrgService;

    @Autowired
    public void setMobileOrgService(MobileOrgService mobileOrgService) {
        this.mobileOrgService = mobileOrgService;
    }

    private DiaryService diaryService;

    @Autowired
    public void setDiaryService(DiaryService diaryService) {
        this.diaryService = diaryService;
    }

    private MobileNetService mobileNetService;

    @Autowired
    public void setMobileNetService(MobileNetService mobileNetService) {
        this.mobileNetService = mobileNetService;
    }

    private TaskGanttDataService taskGanttDataService;

    @Autowired
    public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
        this.taskGanttDataService = taskGanttDataService;
    }

    private LeadActivityService leadActivityService;

    @Autowired
    public void setLeadActivityService(LeadActivityService leadActivityService) {
        this.leadActivityService = leadActivityService;
    }

    private UserFriendsService userFriendsService;
    @Autowired
    public void setUserFriendsService(UserFriendsService userFriendsService)
    {
        this.userFriendsService = userFriendsService;
    }


    /**
     * @param orgId
     * @param deptId
     * @return RetDataBean
     * @Title: getDeptAndUserInfoForMobile
     * @Description:  APP获取组织机构
     */
    @RequestMapping(value = "/getDeptAndUserInfoForMobile", method = RequestMethod.POST)
    public RetDataBean getDeptAndUserInfoForMobile(String orgId, String deptId) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, mobileOrgService.getDeptAndUserInfoForMobile(orgId, deptId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取好友列表
     * @param orgId
     * @param accountId
     * @return
     */
    @RequestMapping(value = "/getMyFriendsList", method = RequestMethod.POST)
    public RetDataBean getMyFriendsList(String orgId, String accountId) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userFriendsService.getMyFriendsList(orgId,accountId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param lng
     * @param lat
     * @return RetDataBean
     * @Title: getLocation
     * @Description:  获取地理位置
     */
    @RequestMapping(value = "/getLocation", method = RequestMethod.POST)
    public RetDataBean getLocation(String lat, String lng) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, mobileNetService.getLocation(lat, lng));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param request
     * @param code
     * @return RetDataBean
     * @Title: doMobileLogin
     * @Description:  移动端登陆
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public RetDataBean doMobileLogin(HttpServletRequest request, String code) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account == null) {
                code = SysTools.decode(code);
                if (StringUtils.isNotBlank(code)) {
                    String[] codeArr = code.split("&");
                    String accountId = codeArr[0];
                    String passWord = codeArr[2];
                    Account tempAccount = null;
                    try {
                        tempAccount = accountService.login(accountId);
                    } catch (Exception e) {
                        return RetDataTools.NotOk(MessageCode.MSG_00018);
                    }
                    UsernamePasswordToken userToken = new UsernamePasswordToken(tempAccount.getAccountId(), passWord);
                    Subject subject = SecurityUtils.getSubject();
                    try {
                        SysTools.reloadAuthorizing(accountId);
                        subject.login(userToken);
                        return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS);
                    } catch (Exception e) {
                        return RetDataTools.NotOk(MessageCode.MESSAGE_SYSTEM_LOGIN_FAIL);
                    }
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_SYSTEM_LOGIN_FAIL);
                }
            }else
            {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param page
     * @return RetDataBean
     * @Title: getMyEmailAllForMobile
     * @Description:  移动端内部邮件
     */
    @RequestMapping(value = "/getMyEmailAllForMobile", method = RequestMethod.POST)
    public RetDataBean getMyEmailAllForMobile(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = emailService.getMyEmailAllForMobile(userInfo.getOrgId(), userInfo.getAccountId(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getMobileMyNewsList
     * @Description:  获取移动端企业新闻的列表
     * @param: request
     * @param: page
     * @param: dingUserId
     * @param: dingDeviceId
     * @param: orgId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMobileMyNewsList", method = RequestMethod.POST)
    public RetDataBean getMobileMyNewsList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = newsService.getMobileMyNewsList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 移动端任务管理
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/getMobileMyTaskList", method = RequestMethod.POST)
    public RetDataBean getMobileMyTaskList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = taskGanttDataService.getMobileMyTaskList(userInfo.getOrgId(), userInfo.getAccountId(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param page
     * @return
     */
    @RequestMapping(value = "/getMobileMyLeadActivity", method = RequestMethod.POST)
    public RetDataBean getMobileMyLeadActivity(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = leadActivityService.getMobileMyLeadActivity(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param page
     * @return RetDataBean
     * @Title: getMobileMyDiaryList
     * @Description:  获取个人工作日志列表
     */
    @RequestMapping(value = "/getMobileMyDiaryList", method = RequestMethod.POST)
    public RetDataBean getMobileMyDiaryList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = diaryService.getMobileMyDiaryList(userInfo.getOrgId(), userInfo.getAccountId(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMobileMyNoticeList
     * @Description:  移动端下滑加载更多
     * @param: request
     * @param: page
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMobileMyNoticeList", method = RequestMethod.POST)
    public RetDataBean getMobileMyNoticeList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = noticeService.getMobileMyNoticeList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param page
     * @return RetDataBean
     * @Title: getMobileMyMeetingList
     * @Description:   获取移动端待办会议
     */
    @RequestMapping(value = "/getMobileMyMeetingList", method = RequestMethod.POST)
    public RetDataBean getMobileMyMeetingList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = meetingService.getMobileMyMeetingList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  tabsType
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getNewsByTabsType
     * @Description:  获取新闻列表
     */
    @RequestMapping(value = "/getNewsByTabsType", method = RequestMethod.POST)
    public RetDataBean getNewsByTabsType(String tabsType) {
        try {
            Example example = new Example(News.class);
            example.createCriteria().andEqualTo("newsType", tabsType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.selectNewsList(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
