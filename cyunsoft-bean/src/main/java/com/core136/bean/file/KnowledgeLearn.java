/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: KnowledgeLearn.java
 * @Package com.core136.bean.file
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月30日 上午10:46:38
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "knowledge_learn")
public class KnowledgeLearn implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String learnId;
    private Integer sortNo;
    /**
     * 文件类型
     */
    private String type;
    /**
     * 文件Id
     */
    private String knowledgeId;
    //留言评论
    private String comment;
    //关联回复内容
    private String commentId;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the learnId
     */
    public String getLearnId() {
        return learnId;
    }

    /**
     * @param learnId the learnId to set
     */
    public void setLearnId(String learnId) {
        this.learnId = learnId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }


    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the knowledgeId
     */
    public String getKnowledgeId() {
        return knowledgeId;
    }

    /**
     * @param knowledgeId the knowledgeId to set
     */
    public void setKnowledgeId(String knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the commentId
     */
    public String getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
