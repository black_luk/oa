$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getCodeClass("fundsTypeQuery", "gov_funds_type");
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getApprovalFundsOldList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '200px',
            title: '活动标题',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>"
            }
        }, {
            field: 'fundsType',
            title: '经费类型',
            width: '50px',
            formatter: function (value, row, index) {
                return getCodeClassName(value, "gov_funds_type");
            }
        }, {
            field: 'applyUser',
            title: '申请人',
            width: '100px',
            formatter: function (value, row, index) {
                return getPartyMemberName(value);
            }
        }, {
            field: 'createTime',
            title: '申请时间',
            width: '100px'
        }, {
            field: 'funds',
            width: '100px',
            title: '费用金额'
        }, {
            field: 'needTime',
            title: '需要日期',
            width: '100px'
        }, {
            field: 'approvalUser',
            width: '100px',
            title: '审批人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'status',
            title: '当前状态',
            width: '80px',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "审批中";
                } else if (value == "1") {
                    return "审批通过";
                } else if (value == "2") {
                    return "审批未通过";
                } else {
                    return "未知";
                }
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        status: $("#statusQuery").val(),
        fundsType: $("#fundsTypeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        applyUser: $("#applyUser").attr("data-value")
    };
    return temp;
};

function details(recordId) {
    window.open("/app/core/partyorg/fundsdetails?recordId=" + recordId);
}

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
