package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "document_send_to")
public class DocumentSendTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sendToId;
    private String runId;
    private String status;
    private String accountId;
    private String revTime;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getSendToId() {
        return sendToId;
    }

    public void setSendToId(String sendToId) {
        this.sendToId = sendToId;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRevTime() {
        return revTime;
    }

    public void setRevTime(String revTime) {
        this.revTime = revTime;
    }

}
