$(function () {
    $('#remark').summernote({height: 300});
    getApprovedUserList();
    jeDate("#colsingTime", {
        format: "YYYY-MM-DD hh:mm:ss",
        minDate: getSysDate(),
        isinitVal: true
    });
    getCrmInquiryListForSelect();
    getQuotationInfo();
    $(".js-add-update").unbind("click").click(function () {
        updateQuotation();
    })
    query();
    $("#inquiryId").unbind("change").change(function () {
        $("#myTable").bootstrapTable('destroy');
        query();
    })
});

function getQuotationInfo() {
    $.ajax({
        url: "/ret/crmget/getCrmQuotationById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            quotationId: quotationId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var id in data.list) {
                    if (id == "remark") {
                        $("#remark").code(data.list[id]);
                    } else if (id == "attach") {
                        $("#crmattach").attr("data_value", data.list[id]);
                        createAttach("crmattach", 4);
                    } else {
                        $("#" + id).val(data.list[id]);
                    }
                }
            }
        }
    })
}


function getCrmInquiryListForSelect() {
    $.ajax({
        url: "/ret/crmget/getCrmInquiryListForSelect",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                var recordList = data.list;
                var html = "";
                for (var i = 0; i < recordList.length; i++) {
                    html += "<option value=\"" + recordList[i].inquiryId + "\">" + recordList[i].title + "</option>";
                }
                $("#inquiryId").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function updateQuotation() {
    $.ajax({
        url: "/set/crmset/updateCrmQuotation",
        type: "post",
        dataType: "json",
        data: {
            quotationId: quotationId,
            title: $("#title").val(),
            quotationCode: $("#quotationCode").val(),
            inquiryId: $('#inquiryId').val(),
            tax: $("#tax").val(),
            totalPrice: $("#totalPrice").val(),
            colsingTime: $("#colsingTime").val(),
            approvedUser: $("#approvedUser").attr("data-value"),
            attach: $("#crmattach").attr("data_value"),
            remark: $("#remark").code(),
            detail: JSON.stringify($('#myTable').bootstrapTable('getData'))
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}


function query() {
    var inquiryId = $("#inquiryId").val()
    $("#myTable").bootstrapTable({
        url: '/ret/crmget/getCrmInquiryDetailListForQuotation?inquiryId=' + inquiryId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'detailId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'productId',
            title: '产品编码',
            width: '250px'
        }, {
            field: 'materielCode',
            title: '物料编码',
            visible: true,
            width: '200px'
        }, {
            field: 'productName',
            title: '产品名称',
            width: '150px'
        }, {
            field: 'model',
            title: '产品型号',
            width: '50px'
        }, {
            field: 'count',
            width: '50px',
            title: '数量'
        }, {
            field: 'unit',
            width: '50px',
            title: '单位'
        }, {
            field: 'delivery',
            width: '100px',
            sortable: true,
            title: '交货期'
        }, {
            field: 'price',
            width: '100px',
            title: '单价',
            formatter: function (value, row, index) {
                return "<a id='price" + index + "' index=" + index + " class='price' style='cursor: pointer;'>" + value + "</a>";
            }
        }, {
            field: 'remark',
            width: '100px',
            visible: false,
            title: '备注'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                if (value != '<a onclick="saveok(' + index + ')" class="btn btn-magenta btn-xs" >确认</a>') {
                    return createOptBtn(index);
                } else {
                    return value;
                }
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        onLoadSuccess: function (aa) {
            $("#myTable .price").editable().on('save', function (e, params) {
                updatePric(e, params);
            });
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(index) {
    return html = "<a onclick=\"delrows(" + index + ")\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
}

function updatePric(e, params) {
    var newValue = params.newValue;
    var index = $(e.currentTarget.outerHTML).attr("index");
    var allRow = $('#myTable').bootstrapTable('getData');
    var tempRow = allRow[index];
    tempRow.price = newValue;
    allRow[index] = tempRow;
    $("#myTable").bootstrapTable('load', allRow);
    $("#myTable .price").editable().on('save', function (e, params) {
        updatePric(e, params);
    });
}

function addRow() {
    var count = $('#myTable').bootstrapTable('getData').length;
    if ($("#productId" + (count - 1)).length < 1) {
        if (count != 0 && $("#productId" + (count - 1)).length > 0) {
            var row = {};
            row.num = count - 1;
            row.productId = $("#productId" + (count - 1)).val();
            row.materielCode = $("#materielCode" + (count - 1)).text();
            row.productName = $("#productName" + (count - 1)).text();
            row.model = $("#model" + (count - 1)).text();
            row.unit = $("#unit" + (count - 1)).text();
            row.count = $("#count" + (count - 1)).val();
            row.delivery = $("#delivery" + (count - 1)).val();
            row.remark = $("#remark" + (count - 1)).val();
            row.price = $("#price" + (count - 1)).text();
            row.opt = createOptBtn(count - 1);
            tableData.push(row);
            $("#myTable").bootstrapTable('load', tableData);
        }
        var _data = {
            num: count,
            materielCode: "<div id='materielCode" + count + "' name='materielCode'></div>",
            productId: "<div id='productId" + count + "' name='productId'></div>",
            productName: "<input type='hidden' style='width:100%' id='productName" + count + "' name='productName'/>",
            model: "<div id='model" + count + "' name='model'></div>",
            count: "<input type='text' class='form-control' id='count" + count + "' name='count'/>",
            unit: "<div id='unit" + count + "' name='unit'></div>",
            delivery: "<input type='text' class='form-control' id='delivery" + count + "' name='delivery' readonly='readonly'/>",
            price: 0,
            remark: "<input type='text' class='form-control' id='remark" + count + "' name='remark'/>",
            opt: "<a onclick=\"saveok(" + count + ")\" class=\"btn btn-magenta btn-xs\" >确认</a>"
        }
        $("#myTable").bootstrapTable('append', _data)
        jeDate("#delivery" + count, {
            format: "YYYY-MM-DD"
        });
        $("#productName" + count).select2({
            theme: "bootstrap",
            allowClear: true,
            placeholder: "请输入产品名称或型号",
            query: function (query) {
                var url = "/ret/erpget/getProductSelect2";
                var param = {
                    search: query.term
                }; // 查询参数，query.term为用户在select2中的输入内容.
                var type = "json";
                var data = {
                    results: []
                };
                $.post(url, param, function (datas) {
                    var datalist = datas.list;
                    for (var i = 0, len = datalist.length; i < len; i++) {
                        var land = datalist[i];
                        var option = {
                            "id": land.productId,
                            "text": land.productName,
                            "model": land.model,
                            "materielCode": land.materielCode,
                            "unit": land.unit
                        };
                        data.results.push(option);
                    }
                    query.callback(data);
                }, type);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
            formatResult: function (data) {
                return '<div class="select2-user-result">' + data.text + '</div>'
            },
            formatSelection: function (data) {
                $("#productId" + count).html(data.id);
                $("#materielCode" + count).html(data.materielCode);
                $("#productName" + count).html(data.text);
                $("#model" + count).html(data.model);
                $("#unit" + count).html(getUnitName(data.unit));
                return data.text;
            },
            initSelection: function (data, cb) {
                console.log(data);
                cb(data);
            }
        });
    } else {
        layer.msg("请选确认当前添加的产品信息!产品不能为空!")
        return;
    }
    $("#myTable .price").editable().on('save', function (e, params) {
        tableData = $('#myTable').bootstrapTable('getData');
        let popped = tableData.pop();
        var row = {};
        row.num = count;
        row.productId = $("#productId" + count).text();
        row.materielCode = $("#materielCode" + count).text();
        row.productName = $("#productName" + count).text();
        row.model = $("#model" + count).text();
        row.unit = $("#unit" + count).text();
        row.count = $("#count" + count).val();
        row.delivery = $("#delivery" + count).val();
        row.remark = $("#remark" + count).val();
        row.price = $("#price" + count).text();
        row.opt = createOptBtn(count);
        tableData.push(row);
        $("#myTable").bootstrapTable('load', tableData);
        updatePric(e, params);
    });
}

function saveok(count) {
    tableData = $('#myTable').bootstrapTable('getData');
    let popped = tableData.pop();
    var row = {};
    row.num = count;
    row.productId = $("#productId" + count).text();
    row.materielCode = $("#materielCode" + count).text();
    row.productName = $("#productName" + count).text();
    row.model = $("#model" + count).text();
    row.unit = $("#unit" + count).text();
    row.count = $("#count" + count).val();
    row.delivery = $("#delivery" + count).val();
    row.remark = $("#remark" + count).val();
    row.price = $("#price" + count).text();
    row.opt = createOptBtn(count);
    tableData.push(row);
    $("#myTable").bootstrapTable('load', tableData);
    $("#myTable .price").editable().on('save', function (e, params) {
        updatePric(e, params);
    });

}

function delrows(index) {
    $('#myTable').bootstrapTable('remove', {
        field: 'num',
        values: [index]
    });
    var newData = $('#myTable').bootstrapTable('getData');
    var newTableData = [];
    for (var i = 0; i < newData.length; i++) {
        var json = newData[i];
        json.num = i;
        json.opt = createOptBtn(i);
        newTableData.push(json);
    }
    $("#myTable").bootstrapTable('load', newTableData);
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        quotationId: quotationId
    };
    return temp;
};

function getApprovedUserList() {
    $.ajax({
        url: "/ret/crmget/getCrmApprovedUserList",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                var recordList = data.list;
                var html = "";
                for (var i = 0; i < recordList.length; i++) {
                    html += "<option value=\"" + recordList[i].accountId + "\">" + recordList[i].userName + "</option>";
                }
                $("#approvedUser").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}
