/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: ContractPageController.java
 * @Package com.core136.controller.sys
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月23日 上午11:03:55
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.contract;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: ContractPageController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月23日 上午11:03:55
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Controller
@RequestMapping("/app/core")
public class PageContractController {

    /**
     * 合同汇总
     * @return
     */
    @RequestMapping("/contract/contracttotal")
    @RequiresPermissions("/app/core/contract/contracttotal")
    public ModelAndView goContracttotal() {
        try {
            return new ModelAndView("app/core/contract/contracttotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goReceivablesdetails
     * @Description:  收款详情
     * @param request
     * @return
     * ModelAndView

     */
    @RequestMapping("/contract/receivablesdetails")
    @RequiresPermissions("/app/core/contract/receivablesdetails")
    public ModelAndView goReceivablesdetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/receivablesdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goPayablesdetails
     * @Description:  付款详情
     * @param request
     * @return
     * ModelAndView

     */
    @RequestMapping("/contract/payablesdetails")
    @RequiresPermissions("/app/core/contract/payablesdetails")
    public ModelAndView goPayablesdetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/payablesdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goBilldetails
     * @Description:  票据详情
     * @param request
     * @return
     * ModelAndView

     */
    @RequestMapping("/contract/billdetails")
    @RequiresPermissions("/app/core/contract/billdetails")
    public ModelAndView goBilldetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/billdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goContractBill
     * @Description:  票据管理
     * @param: request
     * @param: view
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/contract/bill")
    @RequiresPermissions("/app/core/contract/bill")
    public ModelAndView goContractBill(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/contract/bill");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/contract/billmanage");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/contract/billedit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goContractPayable
     * @Description:  合同付款
     * @param: request
     * @param: view
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/contract/payable")
    @RequiresPermissions("/app/core/contract/payable")
    public ModelAndView goContractPayable(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/contract/payable");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/contract/payablemanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSendGoods
     * @Description:  合同的发货管理
     * @param request
     * @param view
     * @return
     * ModelAndView

     */
    @RequestMapping("/contract/sendgoods")
    @RequiresPermissions("/app/core/contract/sendgoods")
    public ModelAndView goSendGoods(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/contract/sendgoods");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/contract/sendgoodsmanage");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/contract/sendgoodsedit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goContractReceviables
     * @Description:  应收款列表
     * @param: request
     * @param: view
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/contract/receivables")
    @RequiresPermissions("/app/core/contract/receivables")
    public ModelAndView goContractReceviables(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/contract/receivables");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/contract/receivablesmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goContractdetails
     * @Description:  合同详情
     * @param: request
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/contract/contractdetails")
    @RequiresPermissions("/app/core/contract/contractdetails")
    public ModelAndView goContractdetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/contractdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     *
     * @Title: goQuery
     * @Description:  合同查询
     * @param: request
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/contract/query")
    @RequiresPermissions("/app/core/contract/query")
    public ModelAndView goQuery(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/query");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSendGoodsDetails
     * @Description:  发货详情
     * @param request
     * @return
     * ModelAndView

     */
    @RequestMapping("/contract/sendgoodsdetails")
    @RequiresPermissions("/app/core/contract/sendgoodsdetails")
    public ModelAndView goSendGoodsDetails(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/sendgoodsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSetpriv
     * @Description: (这里用一句话描述这个方法的作用)
     * @param @param request
     * @param @return 设定文件
     * @return ModelAndView 返回类型

     */
    @RequestMapping("/contract/setpriv")
    @RequiresPermissions("/app/core/contract/setpriv")
    public ModelAndView goSetpriv(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/seting/setpriv");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSetsort
     * @Description:  合同分类设置
     * @param @param request
     * @param @return 设定文件
     * @return ModelAndView 返回类型

     */
    @RequestMapping("/contract/sort")
    @RequiresPermissions("/app/core/contract/sort")
    public ModelAndView goSetsort(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/contract/seting/sort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goCreateContract
     * @Description:  创建合同
     * @param @param request
     * @param @return 设定文件
     * @return ModelAndView 返回类型

     */
    @RequestMapping("/contract/create")
    @RequiresPermissions("/app/core/contract/create")
    public ModelAndView goCreateContract(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/contract/createcontract");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/contract/contractmanage");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/contract/editcontract");
                }
            }

            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
