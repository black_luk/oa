package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "bpm_list")
public class BpmList implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String runId;
    private String flowId;
    private String flowTitle;
    private String endTime;
    private String delFlag;
    private String attach;
    private String opUserStr;
    private String follow;
    private String urgency;
    private String status;
    private String parentRunId;
    private String parentProcessId;
    private String isEmpty;
    private String endRunProcessId;
    private String formVersion;
    private String createUser;
    private String createTime;
    private String orgId;
    public String getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(String parentProcessId) {
        this.parentProcessId = parentProcessId;
    }
    public String getRunId() {
        return runId;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getFlowTitle() {
        return flowTitle;
    }

    public void setFlowTitle(String flowTitle) {
        this.flowTitle = flowTitle;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOpUserStr() {
        return opUserStr;
    }

    public void setOpUserStr(String opUserStr) {
        this.opUserStr = opUserStr;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getParentRunId() {
        return parentRunId;
    }

    public void setParentRunId(String parentRunId) {
        this.parentRunId = parentRunId;
    }

    public String getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(String isEmpty) {
        this.isEmpty = isEmpty;
    }

    public String getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(String formVersion) {
        this.formVersion = formVersion;
    }

    public String getEndRunProcessId() {
        return endRunProcessId;
    }

    public void setEndRunProcessId(String endRunProcessId) {
        this.endRunProcessId = endRunProcessId;
    }
}
