package com.core136.mapper.education;

import com.core136.bean.education.EduTeacherExperience;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduTeacherExperienceMapper extends MyMapper<EduTeacherExperience> {
    /**
     * 获取任教经历列表
     *
     * @param orgId
     * @param courseType
     * @param grade
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getEduTeacherExperienceList(@Param(value = "orgId") String orgId, @Param(value = "courseType") String courseType, @Param(value = "grade") String grade,
                                                                 @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
