package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BpmSort
 * @Description: 工作流分类实体类
 * @author: 刘绍全
 * @date: 2018年12月11日 下午8:15:36
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bpm_sort")
public class BpmSort implements Serializable {
    private static final long serialVersionUID = 1L;
    private String bpmSortId;
    private Integer sortNo;
    private String bpmSortName;
    private String manageAccountId;
    private String levelId;
    private String sysFlag;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getBpmSortId() {
        return bpmSortId;
    }

    public void setBpmSortId(String bpmSortId) {
        this.bpmSortId = bpmSortId;
    }

    public String getBpmSortName() {
        return bpmSortName;
    }

    public void setBpmSortName(String bpmSortName) {
        this.bpmSortName = bpmSortName;
    }

    public String getManageAccountId() {
        return manageAccountId;
    }

    public void setManageAccountId(String manageAccountId) {
        this.manageAccountId = manageAccountId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getSysFlag() {
        return sysFlag;
    }

    public void setSysFlag(String sysFlag) {
        this.sysFlag = sysFlag;
    }

}
