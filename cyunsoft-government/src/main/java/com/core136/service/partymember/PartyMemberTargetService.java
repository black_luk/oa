package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.bean.partymember.PartyMemberTarget;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberTargetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberTargetService {
    private PartyMemberTargetMapper partyMemberTargetMapper;

    @Autowired
    public void setPartyMemberTargetMapper(PartyMemberTargetMapper partyMemberTargetMapper) {
        this.partyMemberTargetMapper = partyMemberTargetMapper;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    public int insertPartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        return partyMemberTargetMapper.insert(partyMemberTarget);
    }

    public int deletePartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        return partyMemberTargetMapper.delete(partyMemberTarget);
    }

    public int updatePartyMemberTarget(Example example, PartyMemberTarget partyMemberTarget) {
        return partyMemberTargetMapper.updateByExampleSelective(partyMemberTarget, example);
    }

    public PartyMemberTarget selectOnePartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        return partyMemberTargetMapper.selectOne(partyMemberTarget);
    }

    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getTargetRecordList
     * @Description:  获取发展对象审批列表
     */
    public List<Map<String, String>> getTargetRecordList(String orgId, String status, String partyOrgId, String beginTime, String endTime, String search) {
        return partyMemberTargetMapper.getTargetRecordList(orgId, status, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getTargetRecordList
     * @Description:  获取发展对象审批列表
     */
    public PageInfo<Map<String, String>> getTargetRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTargetRecordList(pageParam.getOrgId(), status, partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: setPartyMemberTarget
     * @Description:  设置入党申请人为发展对象
     */
    @Transactional(value = "generalTM")
    public RetDataBean setPartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        if (StringUtils.isBlank(partyMemberTarget.getJoinRecordId())) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            if (partyMemberTarget.getStatus().equals("1")) {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime1(partyMemberTarget.getJoinTime());
                partyMemberJoin.setStatusFlag(2);
                partyMemberJoin.setLinkMan1(partyMemberTarget.getLinkMan1());
                partyMemberJoin.setLinkMan2(partyMemberTarget.getLinkMan2());
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberTarget.getOrgId()).andEqualTo("recordId", partyMemberTarget.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberTarget(partyMemberTarget));
            } else {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime1(partyMemberTarget.getJoinTime());
                partyMemberJoin.setStatusFlag(100);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberTarget.getOrgId()).andEqualTo("recordId", partyMemberTarget.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberTarget(partyMemberTarget));
            }
        }
    }

}
