$(function () {
    $.ajax({
        url: "/ret/crmget/getCrmInquiry",
        type: "post",
        dataType: "json",
        data: {
            inquiryId: inquiryId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var id in data.list) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", data.list[id]);
                        createAttach("attach", 1);
                    } else if (id == "cashType") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("RMB 人民币");
                        } else if (data.list[id] == "2") {
                            $("#" + id).html("US 美元");
                        }

                    } else if (id == "payType") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("进度付款");
                        } else if (data.list[id] == "2") {
                            $("#" + id).html("滚动付款");
                        } else if (data.list[id] == "3") {
                            $("#" + id).html("款到发货");
                        } else if (data.list[id] == "4") {
                            $("#" + id).html("货到付款");
                        }
                    } else if (id == "customerType") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("终端用户 ");
                        } else if (data.list[id] == "2") {
                            $("#" + id).html("合作伙伴 ");
                        }
                    } else if (id == "taxFlag") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("含税 ");
                        } else if (data.list[id] == "0") {
                            $("#" + id).html("不含税 ");
                        }
                    } else if (id == "packCharges") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("需要包装 ");
                        } else if (data.list[id] == "0") {
                            $("#" + id).html("不需要包装 ");
                        }
                    } else if (id == "invoiceFlag") {
                        if (data.list[id] == "1") {
                            $("#" + id).html("不开票 ");
                        } else if (data.list[id] == "0") {
                            $("#" + id).html("开票 ");
                        }
                    } else {
                        $("#" + id).html(data.list[id]);
                    }
                }
            }
        }
    });
    query();
})


function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/crmget/getCrmInquiryDetailList?inquiryId=' + inquiryId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'detailId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'productId',
            title: '产品编码',
            visible: false,
            width: '200px'
        }, {
            field: 'materielCode',
            title: '物料编码',
            visible: true,
            width: '200px'
        }, {
            field: 'productName',
            title: '产品名称',
            width: '200px'
        }, {
            field: 'model',
            title: '产品型号',
            width: '100px'
        }, {
            field: 'count',
            width: '50px',
            title: '数量'
        }, {
            field: 'unit',
            width: '50px',
            title: '单位'
        }, {
            field: 'delivery',
            width: '100px',
            sortable: true,
            title: '交货期'
        }, {
            field: 'remark',
            width: '100px',
            visible: false,
            title: '备注'
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};
