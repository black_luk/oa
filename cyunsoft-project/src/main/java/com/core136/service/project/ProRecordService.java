package com.core136.service.project;

import com.core136.bean.project.ProRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProRecordMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProRecordService {
    private ProRecordMapper proRecordMapper;

    @Autowired
    public void setProRecordMapper(ProRecordMapper proRecordMapper) {
        this.proRecordMapper = proRecordMapper;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public int insertProRecord(ProRecord proRecord) {
        return proRecordMapper.insert(proRecord);
    }

    public int deleteProRecord(ProRecord proRecord) {
        return proRecordMapper.delete(proRecord);
    }

    public int updateProRecord(Example example, ProRecord proRecord) {
        return proRecordMapper.updateByExampleSelective(proRecord, example);
    }

    public ProRecord selectOneProRecord(ProRecord proRecord) {
        return proRecordMapper.selectOne(proRecord);
    }

    /**
     * 获取子任务负责人列表
     *
     * @param orgId
     * @param proId
     * @return
     */
    public List<Map<String, String>> getTaskUsersList(String orgId, String proId) {
        ProRecord proRecord = new ProRecord();
        proRecord.setOrgId(orgId);
        proRecord.setProId(proId);
        proRecord = selectOneProRecord(proRecord);
        String taslUsers = proRecord.getTaskUsers();
        List<Map<String, String>> tMaps = userInfoService.getAllUserInfoByAccountList(proRecord.getOrgId(), taslUsers);
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < tMaps.size(); i++) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("key", tMaps.get(i).get("accountId"));
            map.put("label", tMaps.get(i).get("userName"));
            returnList.add(map);
        }
        return returnList;
    }


    public Map<String, String> getProRecordStatusCountList(String orgId) {
        return proRecordMapper.getProRecordStatusCountList(orgId);
    }

    /**
     * 按状态获取项目列表
     *
     * @param orgId
     * @param status
     * @param search
     * @return
     */
    public List<Map<String, String>> getProRecordListByStatus(String orgId, String proLevel, String beginTime, String endTime, String status, String search) {
        return proRecordMapper.getProRecordListByStatus(orgId, proLevel, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * 按状态获取项目列表
     *
     * @param pageParam
     * @param status
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProRecordListByStatus(PageParam pageParam, String proLevel, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListByStatus(pageParam.getOrgId(), proLevel, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 项目记录查询
     *
     * @param orgId
     * @param proSort
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return
     */
    public List<Map<String, String>> getProRecordListByQuery(String orgId, String proSort, String proLevel, String beginTime, String endTime, String status, String search) {
        return proRecordMapper.getProRecordListByQuery(orgId, proSort, proLevel, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * 项目记录查询
     *
     * @param pageParam
     * @param proSort
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @param status
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProRecordListByQuery(PageParam pageParam, String proSort, String proLevel, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListByQuery(pageParam.getOrgId(), proSort, proLevel, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getProReocrdTree(String orgId, String levelId) {
        return proRecordMapper.getProReocrdTree(orgId, levelId);
    }

    public List<Map<String, String>> getProRecordListForApproval(String orgId, String proLevel, String beginTime, String endTime, String search) {
        return proRecordMapper.getProRecordListForApproval(orgId, proLevel, beginTime, endTime, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getProRecordListForApproval(PageParam pageParam, String proLevel, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListForApproval(pageParam.getOrgId(), proLevel, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
