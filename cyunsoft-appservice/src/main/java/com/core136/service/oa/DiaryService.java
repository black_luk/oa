package com.core136.service.oa;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.oa.Diary;
import com.core136.bean.oa.DiaryPriv;
import com.core136.bean.sys.MsgBody;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.GobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.DiaryMapper;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.file.AttachService;
import com.core136.service.sys.MessageUnitService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DiaryService
 * @Description: 工作日志操作类
 * @author: 稠云技术
 * @date: 2019年7月6日 下午11:38:45
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class DiaryService {
    private DiaryMapper diaryMapper;

    @Autowired
    public void setDiaryMapper(DiaryMapper diaryMapper) {
        this.diaryMapper = diaryMapper;
    }

    private DiaryCommentsService diaryCommentsService;

    @Autowired
    public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
        this.diaryCommentsService = diaryCommentsService;
    }

    private MessageUnitService messageUnitService;

    @Autowired
    public void setMessageUnitService(MessageUnitService messageUnitService) {
        this.messageUnitService = messageUnitService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    /**
     * @Title: insertDiary
     * @Description:  创建工作日志
     * @param: diary
     * @param: @return
     * @return: int
     */
    public int insertDiary(Diary diary) {
        return diaryMapper.insert(diary);
    }

    public int createDiary(Account account, UserInfo userInfo, Diary diary, DiaryPriv diaryPriv) {
        if (diaryPriv != null) {
            if (diaryPriv.getShareStatus() == 0) {
                diary.setUserPriv("@all");
                diary.setDeptPriv("");
                diary.setLevelPriv("");
            } else if (diaryPriv.getShareStatus() == 1) {
                diary.setUserPriv("");
                diary.setDeptPriv(userInfo.getDeptId());
                diary.setLevelPriv("");
            } else if (diaryPriv.getShareStatus() == 3) {
                diary.setUserPriv("");
                diary.setDeptPriv("");
                diary.setLevelPriv("");
            }
        } else {
            diary.setUserPriv("");
            diary.setDeptPriv("");
            diary.setLevelPriv("");
        }
        if (StringUtils.isNotBlank(diary.getMsgType())) {
            List<Map<String, String>> userList = userInfoService.getAccountIdInPriv(diary.getOrgId(), diary.getUserPriv(), diary.getDeptPriv(), diary.getLevelPriv());
            List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
            for (int i = 0; i < userList.size(); i++) {
                Account account2 = new Account();
                account2.setAccountId(userList.get(i).get("accountId"));
                account2.setOrgId(account.getOrgId());
                account2 = accountService.selectOneAccount(account2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("工作日志分享提醒");
                msgBody.setContent("标题为：" + diary.getTitle() + "的工作日志分享提醒！");
                msgBody.setSendTime(diary.getCreateTime());
                msgBody.setAccount(account2);
                msgBody.setFromAccountId(account.getAccountId());
                msgBody.setFormUserName(userInfo.getUserName());
                msgBody.setRedirectUrl("/app/core/diary/readdiary?diaryId=" + diary.getDiaryId());
                msgBody.setOrgId(account.getOrgId());
                msgBodyList.add(msgBody);
            }
            String smsStatus = diary.getMsgType();
            messageUnitService.sendMessage(smsStatus, GobalConstant.MSG_TYPE_DIARY, msgBodyList);
        }
        return insertDiary(diary);

    }

    /**
     * @Title: delDiary
     * @Description:  更新工作日志
     * @param: diary
     * @param: @return
     * @return: int
     */
    public int delDiary(Diary diary) {
        return diaryMapper.delete(diary);
    }

    public Diary selectOneDiary(Diary diary) {
        return diaryMapper.selectOne(diary);
    }

    public int updateDiary(Account account, UserInfo userInfo, Example example, Diary diary, DiaryPriv diaryPriv) {
        if (diaryPriv != null) {
            if (diaryPriv.getShareStatus() == 0) {
                diary.setUserPriv("@all");
                diary.setDeptPriv("");
                diary.setLevelPriv("");
            } else if (diaryPriv.getShareStatus() == 1) {
                diary.setUserPriv("");
                diary.setDeptPriv(userInfo.getDeptId());
                diary.setLevelPriv("");
            } else if (diaryPriv.getShareStatus() == 3) {
                diary.setUserPriv("");
                diary.setDeptPriv("");
                diary.setLevelPriv("");
            }
        } else {
            diary.setUserPriv("");
            diary.setDeptPriv("");
            diary.setLevelPriv("");
        }
        if (StringUtils.isNotBlank(diary.getMsgType())) {
            List<Map<String, String>> userList = userInfoService.getAccountIdInPriv(diary.getOrgId(), diary.getUserPriv(), diary.getDeptPriv(), diary.getLevelPriv());
            List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
            for (int i = 0; i < userList.size(); i++) {
                Account account2 = new Account();
                account2.setAccountId(userList.get(i).get("accountId"));
                account2.setOrgId(account.getOrgId());
                account2 = accountService.selectOneAccount(account2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("工作日志分享提醒");
                msgBody.setContent("标题为：" + diary.getTitle() + "的工作日志分享提醒！");
                msgBody.setSendTime(diary.getCreateTime());
                msgBody.setAccount(account2);
                msgBody.setFromAccountId(account.getAccountId());
                msgBody.setFormUserName(userInfo.getUserName());
                msgBody.setRedirectUrl("/app/core/diary/readdiary?diaryId=" + diary.getDiaryId());
                msgBody.setOrgId(account.getOrgId());
                msgBodyList.add(msgBody);
            }
            String smsStatus = diary.getMsgType();
            messageUnitService.sendMessage(smsStatus, GobalConstant.MSG_TYPE_DIARY, msgBodyList);
        }
        return diaryMapper.updateByExampleSelective(diary, example);
    }

    /**
     * @Title: getMyDiaryList
     * @Description:   获取当前用户历史工作日志
     * @param: orgId
     * @param: accountId
     * @param: search
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    public List<Map<String, Object>> getMyDiaryList(String orgId, String accountId, String search, String diaryDay) {
        return diaryMapper.getMyDiaryList(orgId, accountId, "%" + search + "%", diaryDay);
    }

    /**
     * @param orgId
     * @param accountId
     * @param page
     * @return List<Map < String, String>>
     * @Title: getMobileMyDiaryList
     * @Description:  移动端获取个人工作日志列表
     */
    public List<Map<String, String>> getMobileMyDiaryList(String orgId, String accountId, int page) {
        return diaryMapper.getMobileMyDiaryList(orgId, accountId, page);
    }

    /**
     * @throws Exception
     * @Title: getMyDiaryList
     * @Description:  获取当前用户历史工作日志
     * @param: pageParam
     * @param: createTime
     * @param: @return
     * @return: PageInfo<Map < String, Object>>
     */
    public PageInfo<Map<String, Object>> getMyDiaryList(PageParam pageParam, String diaryDay) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getMyDiaryList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch(), diaryDay);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

    /**
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  orderBy
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  beginTime
     * @param @param  endTime
     * @param @param  search
     * @param @return 设定文件
     * @return PageInfo<Map < String, Object>> 返回类型
     * @Title: getOtherDiaryList
     * @Description:  获取他人的工作日志
     */
    public PageInfo<Map<String, Object>> getOtherDiaryList(int pageNumber, int pageSize, String orderBy, String orgId, String accountId, String beginTime, String endTime, String search) {
        PageHelper.startPage(pageNumber, pageSize, orderBy);
        List<Map<String, Object>> datalist = diaryMapper.getOtherDiaryList(orgId, accountId, search, beginTime, endTime);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getMySubordinatesDiaryList
     * @Description:  获取我下属的工作晶志列表
     * @param: orgId
     * @param: accountId
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMySubordinatesDiaryList(String orgId, List<String> list, String beginTime, String endTime, String search) {
        return diaryMapper.getMySubordinatesDiaryList(orgId, list, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getMySubordinatesDiaryList
     * @Description:  获取我下属的工作晶志列表
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getMySubordinatesDiaryList(PageParam pageParam, List<String> list, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = diaryMapper.getMySubordinatesDiaryList(pageParam.getOrgId(), list, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getMyDiaryCount
     * @Description:  获取个人日志总数
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: int
     */
    public int getMyDiaryCount(String orgId, String accountId) {
        return diaryMapper.getMyDiaryCount(orgId, accountId);
    }

    /**
     * @Title: getDiaryCommentCount
     * @Description:  被他人评论数
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: Integer
     */
    public int getDiaryCommentCount(String orgId, String accountId) {
        return diaryMapper.getDiaryCommentCount(orgId, accountId);
    }

    /**
     * @Title: getMyDiaryInfo
     * @Description:  获取个人日志信息
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: Map<String, Object>
     */
    public Map<String, Object> getMyDiaryInfo(String orgId, String accountId) {
        Map<String, Object> tempMap = new HashMap<String, Object>();
        tempMap.put("diaryCount", getMyDiaryCount(orgId, accountId));
        tempMap.put("commToMeCount", getDiaryCommentCount(orgId, accountId));
        tempMap.put("commCount", diaryCommentsService.getMyDiaryCommentsCount(orgId, accountId));
        return tempMap;
    }

    /**
     * @Title: getShowDiaryList
     * @Description:  获取他人分享的工作日志
     * @param: orgId
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getShowDiaryList(String orgId, String accountId, String deptId, String levelId, String beginTime, String endTime, String search) {
        return diaryMapper.getShowDiaryList(orgId, accountId, deptId, levelId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getShowDiaryList
     * @Description:  获取他人分享的工作日志
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getShowDiaryList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getShowDiaryList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getHomePageByAccountId
     * @Description:  获取通讯录的中的个人主页信息
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: JSONObject
     */
    public JSONObject getHomePageByAccountId(String orgId, String accountId) {

        JSONObject returnJson = new JSONObject();
        Map<String, String> userInfo = userInfoService.getAccountAndUserInfo(accountId, orgId);
        returnJson.put("userInfo", userInfo);
        returnJson.put("attachTotal", attachService.getMyAttachCount(orgId, accountId));
        returnJson.put("diaryTotal", getMyDiaryCount(orgId, accountId));
        return returnJson;
    }


}
