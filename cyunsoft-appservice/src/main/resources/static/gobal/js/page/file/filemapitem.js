var vm = new Vue({
    el: '#app',  //实例化对象
    data: {
        itemList: []  //要存放的数据
    },
    methods: {
        //存放实例方法
    }
})
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/filemapget/getFileMapSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    view: {
        addHoverDom: addHoverDom,
        selectedMulti: false
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    getItemList("");
    $.ajax({
        url: "/ret/filemapget/getFileMapSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
})

function getItemList(sortId) {
    $.ajax({
        url: "/ret/filemapget/getAllFileItem",
        type: "post",
        dataType: "json",
        data: {
            sortId: sortId,
        },
        success: function (data) {
            if (data.status == "200") {
                vm.itemList = data.list;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function zTreeOnClick(event, treeId, treeNode) {
    getItemList(treeNode.sortId);
}

function addHoverDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span").parent();
    $(".js-child-pro").remove();
    if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
    var addStr = "<a style='right: 5px;position: absolute;width: 80px;color: #F5F5F5;line-height: 15px;' class='js-child-pro btn btn-success btn-xs' id='addBtn_" + treeNode.tId + "' onfocus='this.blur();'>创建分类</a>";
    sObj.after(addStr);
    var btn = $("#addBtn_" + treeNode.tId);
    if (btn) btn.bind("click", function () {
        document.getElementById("form1").reset();
        $("#inUserPriv").attr("data-value", "");
        $("#inDeptPriv").attr("data-value", "");
        $("#inLevelPriv").attr("data-value", "");
        $("#outUserPriv").attr("data-value", "");
        $("#outDeptPriv").attr("data-value", "");
        $("#outLevelPriv").attr("data-value", "");
        $("#excInUser").attr("data-value", "");
        $("#excOutUser").attr("data-value", "");
        $("#fileManageUser").attr("data-value", "");
        $("#fileCreateUser").attr("data-value", "");
        $("#itemmodal").modal("show");
        $(".js-save").unbind("click").click(function () {
            if ($("#titleName").val() == "") {
                layer.msg(sysmsg['MSG_FILE_SORT_EMPTY']);
                return;
            }
            $.ajax({
                url: "/set/filemapset/insertFileMapItem",
                type: "post",
                dataType: "json",
                data: {
                    sortId: treeNode.sortId,
                    sortNo: $("#sortNo").val(),
                    titleName: $("#titleName").val(),
                    inUserPriv: $("#inUserPriv").attr("data-value"),
                    inDeptPriv: $("#inDeptPriv").attr("data-value"),
                    inLevelPriv: $("#inLevelPriv").attr("data-value"),
                    outUserPriv: $("#outUserPriv").attr("data-value"),
                    outDeptPriv: $("#outDeptPriv").attr("data-value"),
                    outLevelPriv: $("#outLevelPriv").attr("data-value"),
                    excInUser: $("#excInUser").attr("data-value"),
                    excOutUser: $("#excOutUser").attr("data-value"),
                    fileManageUser: $("#fileManageUser").attr("data-value"),
                    fileCreateUser: $("#fileCreateUser").attr("data-value")
                },
                success: function (data) {
                    if (data.status == "200") {
                        layer.msg(sysmsg[data.msg]);
                        location.reload();
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        })
        return false;
    });
};

function updateItem(obj) {
    let itemId = $(obj).attr("value");
    document.getElementById("form1").reset();
    $("#inUserPriv").attr("data-value", "");
    $("#inDeptPriv").attr("data-value", "");
    $("#inLevelPriv").attr("data-value", "");
    $("#outUserPriv").attr("data-value", "");
    $("#outDeptPriv").attr("data-value", "");
    $("#outLevelPriv").attr("data-value", "");
    $("#excInUser").attr("data-value", "");
    $("#excOutUser").attr("data-value", "");
    $("#fileManageUser").attr("data-value", "");
    $("#fileCreateUser").attr("data-value", "");
    $("#itemmodal").modal("show");
    $.ajax({
        url: "/ret/filemapget/getFileMapItemById",
        type: "post",
        dataType: "json",
        data: {
            itemId: itemId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (id in info) {
                    if (id == "sortNo" || id == "titleName") {
                        $("#" + id).val(info[id])
                    } else if (id == "inUserPriv" || id == "outUserPriv" || id == "fileManageUser" || id == "fileCreateUser"||id=="excOutUser"||id=="excInUser") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserNameByStr(info[id]));
                    } else if (id == "inDeptPriv" || id == "outDeptPriv") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getDeptNameByDeptIds(info[id]));
                    } else if (id == "inLevelPriv" || id == "outLevelPriv") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserLevelStr(info[id]));
                    }
                }
                $(".js-save").unbind("click").click(function () {
                    if ($("#titleName").val() == "") {
                        layer.msg(sysmsg['MSG_FILE_SORT_EMPTY']);
                        return;
                    }
                    $.ajax({
                        url: "/set/filemapset/updateFileMapItem",
                        type: "post",
                        dataType: "json",
                        data: {
                            itemId: itemId,
                            sortNo: $("#sortNo").val(),
                            titleName: $("#titleName").val(),
                            inUserPriv: $("#inUserPriv").attr("data-value"),
                            inDeptPriv: $("#inDeptPriv").attr("data-value"),
                            inLevelPriv: $("#inLevelPriv").attr("data-value"),
                            outUserPriv: $("#outUserPriv").attr("data-value"),
                            outDeptPriv: $("#outDeptPriv").attr("data-value"),
                            outLevelPriv: $("#outLevelPriv").attr("data-value"),
                            excInUser: $("#excInUser").attr("data-value"),
                            excOutUser: $("#excOutUser").attr("data-value"),
                            fileManageUser: $("#fileManageUser").attr("data-value"),
                            fileCreateUser: $("#fileCreateUser").attr("data-value")
                        },
                        success: function (data) {
                            if (data.status == "200") {
                                layer.msg(sysmsg[data.msg]);
                                location.reload();
                            } else if (data.status == "100") {
                                layer.msg(sysmsg[data.msg]);
                            } else {
                                console.log(data.msg);
                            }
                        }
                    });
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function deleteItem(obj) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        let itemId = $(obj).attr("value");
        $.ajax({
            url: "/set/filemapset/deleteFileMapItem",
            type: "post",
            dataType: "json",
            data: {itemId: itemId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}
