package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionBpmList;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuperversionBpmListMapper extends MyMapper<SuperversionBpmList> {
    /**
     * 督查督办历史流程
     *
     * @param orgId
     * @param processId
     * @return
     */
    public List<Map<String, String>> getSupversionBpmRecordList(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId);

}
