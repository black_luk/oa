/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetOfficesuppliesController.java
 * @Package com.core136.controller.officesupplies
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月31日 下午2:22:30
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.officesupplies;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.officesupplies.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.officesupplies.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/ret/officesuppliesget")
public class RouteGetOfficesuppliesController {
    private OfficeSuppliesSortService officeSuppliesSortService;

    @Autowired
    public void setOfficeSuppliesSortService(OfficeSuppliesSortService officeSuppliesSortService) {
        this.officeSuppliesSortService = officeSuppliesSortService;
    }

    private OfficeSuppliesService officeSuppliesService;

    @Autowired
    public void setOfficeSuppliesService(OfficeSuppliesService officeSuppliesService) {
        this.officeSuppliesService = officeSuppliesService;
    }

    private OfficeSuppliesUnitService officeSuppliesUnitService;

    @Autowired
    public void setOfficeSuppliesUnitService(OfficeSuppliesUnitService officeSuppliesUnitService) {
        this.officeSuppliesUnitService = officeSuppliesUnitService;
    }

    private OfficeSuppliesApprovalService officeSuppliesApprovalService;

    @Autowired
    public void setOfficeSuppliesApprovalService(OfficeSuppliesApprovalService officeSuppliesApprovalService) {
        this.officeSuppliesApprovalService = officeSuppliesApprovalService;
    }

    private OfficeSuppliesApplyService officeSuppliesApplyService;

    @Autowired
    public void setOfficeSuppliesApplyService(OfficeSuppliesApplyService officeSuppliesApplyService) {
        this.officeSuppliesApplyService = officeSuppliesApplyService;
    }

    private OfficeSuppliesGrantService officeSuppliesGrantService;

    @Autowired
    public void setOfficeSuppliesGrantService(OfficeSuppliesGrantService officeSuppliesGrantService) {
        this.officeSuppliesGrantService = officeSuppliesGrantService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param begTime
     * @param endTime
     * @param getUser
     * @param pageParam
     * @return RetDataBean
     * @Title: getGrantOfficeList
     * @Description:  获取办公用品发放列表
     */
    @RequestMapping(value = "/getGrantOfficeList", method = RequestMethod.POST)
    public RetDataBean getGrantOfficeList(
            String begTime,
            String endTime,
            String getUser,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("g.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(getUser);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesGrantService.getGrantOfficeList(pageParam, begTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getApplyOfficeSuppliesList
     * @Description:  获取审批列表
     * @param: request
     * @param: begTime
     * @param: endTime
     * @param: status
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getApplyOfficeSuppliesList", method = RequestMethod.POST)
    public RetDataBean getApplyOfficeSuppliesList(
            String begTime,
            String endTime,
            String status,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getApplyOfficeSuppliesList(pageParam, begTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getGrantOfficeSuppliesList
     * @Description:  待发放用品列表
     * @param: request
     * @param: begTime
     * @param: endTime
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getGrantOfficeSuppliesList", method = RequestMethod.POST)
    public RetDataBean getGrantOfficeSuppliesList(
            String begTime,
            String endTime,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getGrantOfficeSuppliesList(pageParam, begTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyApplyOfficeSuppliesList
     * @Description:  获取个人历史申请记录
     * @param: request
     * @param: begTime
     * @param: endTime
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyApplyOfficeSuppliesList", method = RequestMethod.POST)
    public RetDataBean getMyApplyOfficeSuppliesList(
            String begTime,
            String endTime,
            String status,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getMyApplyOfficeSuppliesList(pageParam, begTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getOfficeSuppliesApplyServiceById
     * @Description:  获取申请详情
     * @param: request
     * @param: officeSuppliesApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOfficeSuppliesApplyServiceById", method = RequestMethod.POST)
    public RetDataBean getOfficeSuppliesApplyServiceById(OfficeSuppliesApply officeSuppliesApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesApplyService.selectOneOfficeSuppliesApply(officeSuppliesApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getOfficeSuppliesApprovalById
     * @Description:  获取审批详情
     * @param: request
     * @param: officeSuppliesApproval
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOfficeSuppliesApprovalById", method = RequestMethod.POST)
    public RetDataBean getOfficeSuppliesApprovalById(OfficeSuppliesApproval officeSuppliesApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesApprovalService.selectOneOfficeSuppliesApproval(officeSuppliesApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getApplyOfficeSupplieslist
     * @Description:  获取可以领用的办公用品列表
     * @param: request
     * @param: sortId
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getApplyOfficeSupplieslist", method = RequestMethod.POST)
    public RetDataBean getApplyOfficeSupplieslist(
            String sortId,
            PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesService.getApplyOfficeSupplieslist(pageParam, sortId, userInfo.getDeptId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getOfficeSupplieslistBySortId
     * @Description:  获取办公用品列表
     * @param: request
     * @param: sortId
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOfficeSupplieslistBySortId", method = RequestMethod.POST)
    public RetDataBean getOfficeSupplieslistBySortId(
            String sortId,
            PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesService.getOfficeSupplieslistBySortId(pageParam, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getOfficeSuppliesUnitList
     * @Description:  获取办公用品计量单位列表
     * @param: request
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOfficeSuppliesUnitList", method = RequestMethod.POST)
    public RetDataBean getOfficeSuppliesUnitList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = officeSuppliesUnitService.getOfficeSuppliesUnitList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getofficeSuppliesUnitById
     * @Description:  获取办公用品计量单位详情
     * @param: request
     * @param: officeSuppliesUnit
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getofficeSuppliesUnitById", method = RequestMethod.POST)
    public RetDataBean getofficeSuppliesUnitById(OfficeSuppliesUnit officeSuppliesUnit) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesUnit.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesUnitService.selectOneOfficeSuppliesUnit(officeSuppliesUnit));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAllUnit
     * @Description:  获取办公用品所有的计量单位
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllUnit", method = RequestMethod.POST)
    public RetDataBean getAllUnit() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesUnitService.getAllUnit(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getOfficeSuppliesSortTree
     * @Description: 获取办公用品分类
     * @param: request
     * @param: sortId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    @RequestMapping(value = "/getOfficeSuppliesSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getOfficeSuppliesSortTree(String sortId) {
        try {
            String parentId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                parentId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return officeSuppliesSortService.getOfficeSuppliesSortTree(account.getOrgId(), parentId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getofficeSuppliesById
     * @Description:  获取办公用品详情
     * @param: request
     * @param: officeSupplies
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getofficeSuppliesById", method = RequestMethod.POST)
    public RetDataBean getofficeSuppliesById(OfficeSupplies officeSupplies) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("officesupplies:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSupplies.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesService.selectOneOfficeSupplies(officeSupplies));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getofficeSuppliesSortById
     * @Description:  获取分类详情
     * @param: request
     * @param: officeSuppliesSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getofficeSuppliesSortById", method = RequestMethod.POST)
    public RetDataBean getofficeSuppliesSortById(OfficeSuppliesSort officeSuppliesSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            officeSuppliesSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, officeSuppliesSortService.selectOneOfficeSuppliesSort(officeSuppliesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
