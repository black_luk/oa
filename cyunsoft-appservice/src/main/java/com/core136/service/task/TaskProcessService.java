package com.core136.service.task;

import com.core136.bean.sys.PageParam;
import com.core136.bean.task.TaskProcess;
import com.core136.common.utils.SysTools;
import com.core136.mapper.task.TaskProcessMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class TaskProcessService {

    private TaskProcessMapper taskProcessMapper;

    @Autowired
    public void setTaskProcessMapper(TaskProcessMapper taskProcessMapper) {
        this.taskProcessMapper = taskProcessMapper;
    }

    public int insertTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.insert(taskProcess);
    }

    public int deleteTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.delete(taskProcess);
    }

    public int updateTaskProcess(Example example, TaskProcess taskProcess) {
        return taskProcessMapper.updateByExampleSelective(taskProcess, example);
    }

    public TaskProcess selectOneTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.selectOne(taskProcess);
    }

    /**
     * @param orgId
     * @param accountId
     * @param taskType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyTaskProcessList
     * @Description:  获取子任务的处理过程列表
     */
    public List<Map<String, String>> getMyTaskProcessList(String orgId, String accountId, String createUser, String taskType, String beginTime, String endTime, String search) {
        return taskProcessMapper.getMyTaskProcessList(orgId, accountId, createUser, taskType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param createUser
     * @param taskType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyTaskProcessList
     * @Description:  获取子任务的处理过程列表
     */
    public PageInfo<Map<String, String>> getMyTaskProcessList(PageParam pageParam, String createUser, String taskType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTaskProcessList(pageParam.getOrgId(), pageParam.getAccountId(), createUser, taskType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param processId
     * @return Map<String, String>
     * @Title: getProcessInfo
     * @Description:  获取处理事件详情
     */
    public Map<String, String> getProcessInfo(String orgId, String processId, String accountId) {
        return taskProcessMapper.getProcessInfo(orgId, processId, accountId);
    }
}
