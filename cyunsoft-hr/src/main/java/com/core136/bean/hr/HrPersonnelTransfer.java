package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: HrPersonnelTransfer
 * @Description: HR人事调动
 * @author: 稠云技术
 * @date: 2020年4月27日 下午11:31:48
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "hr_personnel_transfer")
public class HrPersonnelTransfer implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String transferId;
    private String title;
    private String userId;
    private Integer sortNo;
    private String transferType;
    private String transferTime;
    private String startTime;
    private String compName;
    private String transferComp;
    private String levelName;
    private String transferLevel;
    private String deptName;
    private String transferDept;
    private String transferCondition;
    private String attach;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(String transferTime) {
        this.transferTime = transferTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTransferComp() {
        return transferComp;
    }

    public void setTransferComp(String transferComp) {
        this.transferComp = transferComp;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getTransferLevel() {
        return transferLevel;
    }

    public void setTransferLevel(String transferLevel) {
        this.transferLevel = transferLevel;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getTransferDept() {
        return transferDept;
    }

    public void setTransferDept(String transferDept) {
        this.transferDept = transferDept;
    }

    public String getTransferCondition() {
        return transferCondition;
    }

    public void setTransferCondition(String transferCondition) {
        this.transferCondition = transferCondition;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
