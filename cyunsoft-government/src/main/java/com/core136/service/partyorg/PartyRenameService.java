package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyRename;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyRenameMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRenameService {
    private PartyRenameMapper partyRenameMapper;

    @Autowired
    public void setPartyRenameMapper(PartyRenameMapper partyRenameMapper) {
        this.partyRenameMapper = partyRenameMapper;
    }

    public int insertPartyRename(PartyRename partyRename) {
        return partyRenameMapper.insert(partyRename);
    }

    public int deletePartyRename(PartyRename partyRename) {
        return partyRenameMapper.delete(partyRename);
    }

    public int updatePartyRename(Example example, PartyRename partyRename) {
        return partyRenameMapper.updateByExampleSelective(partyRename, example);
    }

    public PartyRename selectOnePartyRename(PartyRename partyRename) {
        return partyRenameMapper.selectOne(partyRename);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRenameList
     * @Description:  获取党组织更名历史记录
     */
    public List<Map<String, String>> getPartyRenameList(String orgId, String beginTime, String endTime, String search) {
        return partyRenameMapper.getPartyRenameList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyRenameList
     * @Description:  获取党组织更名历史记录
     */
    public PageInfo<Map<String, String>> getPartyRenameList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyRenameList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
