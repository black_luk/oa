package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: DocumentFlow
 * @Description:收发流程
 * @author: 稠云技术
 * @date: 2021年4月11日 上午12:07:30
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "document_flow")
public class DocumentFlow implements Serializable {

    private static final long serialVersionUID = 1L;

    private String flowId;
    private Integer sortNo;
    private String flowName;
    private String formId;
    private String documentType;
    private String freeToOther;
    private String flowLock;
    private String printField;
    private String readFlag;
    private String docNumRule;
    private String flowCache;
    private String autoStyle;
    private String beginDocNum;
    private String endToSend;
    private String remark;
    private String queryPriv;
    private String managePriv;
    private String printFlag;
    private String attachPriv;
    private String status;
    private String imgFlag;
    private String tempOrgId;
    private String userLevelFlag;
    private String printTemplate;
    private String waterMark;
    private String prefixWaterMark;
    private String expPriv;
    private String flowType;
    private String ideaTextFlag;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getPrintField() {
        return printField;
    }

    public void setPrintField(String printField) {
        this.printField = printField;
    }

    public String getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(String readFlag) {
        this.readFlag = readFlag;
    }

    public String getDocNumRule() {
        return docNumRule;
    }

    public void setDocNumRule(String docNumRule) {
        this.docNumRule = docNumRule;
    }

    public String getFlowCache() {
        return flowCache;
    }

    public void setFlowCache(String flowCache) {
        this.flowCache = flowCache;
    }

    public String getAutoStyle() {
        return autoStyle;
    }

    public void setAutoStyle(String autoStyle) {
        this.autoStyle = autoStyle;
    }

    public String getBeginDocNum() {
        return beginDocNum;
    }

    public void setBeginDocNum(String beginDocNum) {
        this.beginDocNum = beginDocNum;
    }

    public String getEndToSend() {
        return endToSend;
    }

    public void setEndToSend(String endToSend) {
        this.endToSend = endToSend;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getQueryPriv() {
        return queryPriv;
    }

    public void setQueryPriv(String queryPriv) {
        this.queryPriv = queryPriv;
    }

    public String getManagePriv() {
        return managePriv;
    }

    public void setManagePriv(String managePriv) {
        this.managePriv = managePriv;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getFreeToOther() {
        return freeToOther;
    }

    public void setFreeToOther(String freeToOther) {
        this.freeToOther = freeToOther;
    }

    public String getFlowLock() {
        return flowLock;
    }

    public void setFlowLock(String flowLock) {
        this.flowLock = flowLock;
    }

    public String getPrintFlag() {
        return printFlag;
    }

    public void setPrintFlag(String printFlag) {
        this.printFlag = printFlag;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getPrintTemplate() {
        return printTemplate;
    }

    public void setPrintTemplate(String printTemplate) {
        this.printTemplate = printTemplate;
    }

    public String getTempOrgId() {
        return tempOrgId;
    }

    public void setTempOrgId(String tempOrgId) {
        this.tempOrgId = tempOrgId;
    }

    public String getUserLevelFlag() {
        return userLevelFlag;
    }

    public void setUserLevelFlag(String userLevelFlag) {
        this.userLevelFlag = userLevelFlag;
    }

    public String getWaterMark() {
        return waterMark;
    }

    public void setWaterMark(String waterMark) {
        this.waterMark = waterMark;
    }

    public String getPrefixWaterMark() {
        return prefixWaterMark;
    }

    public void setPrefixWaterMark(String prefixWaterMark) {
        this.prefixWaterMark = prefixWaterMark;
    }

    public String getExpPriv() {
        return expPriv;
    }

    public void setExpPriv(String expPriv) {
        this.expPriv = expPriv;
    }

    public String getFlowType() {
        return flowType;
    }

    public void setFlowType(String flowType) {
        this.flowType = flowType;
    }

    public String getIdeaTextFlag() {
        return ideaTextFlag;
    }

    public void setIdeaTextFlag(String ideaTextFlag) {
        this.ideaTextFlag = ideaTextFlag;
    }
}
