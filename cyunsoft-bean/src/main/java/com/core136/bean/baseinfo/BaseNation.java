package com.core136.bean.baseinfo;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "base_nation")
public class BaseNation implements Serializable {
    private String nationId;
    private Integer sortNo;
    private String nationName;

    public String getNationId() {
        return nationId;
    }

    public void setNationId(String nationId) {
        this.nationId = nationId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getNationName() {
        return nationName;
    }

    public void setNationName(String nationName) {
        this.nationName = nationName;
    }
}
