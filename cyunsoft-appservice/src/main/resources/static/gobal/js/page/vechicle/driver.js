$(function () {
    jeDate("#issueYear", {
        format: "YYYY-MM-DD",
    });
    $(".js-add-save").unbind("click").click(function () {
        insertVehicleDriver();
    })
})


function insertVehicleDriver() {
    $.ajax({
        url: "/set/vehicleset/insertVehicleDriver",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            accountId: $("#accountId").attr("data-value"),
            licenceType: $("#licenceType").val(),
            issueYear: $("#issueYear").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
