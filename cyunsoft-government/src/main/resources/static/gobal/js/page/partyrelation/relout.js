$(function () {

    $("input:radio[name='isInSys']").change(function () {
        var value = $("input:radio[name='isInSys']:checked").val();
        if (value == "1") {
            $("#otherPartyOrgId").val("");
            $("#outTr").hide();
            $("#inTr").show();
        } else if (value == "2") {
            $("#outPartyOrgId").val("");
            $("#outPartyOrgId").attr("data-value", "");
            $("#inTr").hide();
            $("#outTr").show();
        }
    })

    getCodeClass("outType", "party_out_type");
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    query("");
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#outPartyOrgIdTree"), outPartyOrgIdsetting, data);
        }
    });
    $("#outPartyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#outPartyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $("#doall").unbind("click").click(function () {
        var records = $("#myTable").bootstrapTable('getSelections');
        if (records.length == 0) {
            layer.msg("请先选择党员！");
            return;
        }
        $("#relmodal").modal("show");
        $(".js-save").unbind("click").click(function () {
            for (var i = 0; i < records.length; i++) {
                $.ajax({
                    url: "/set/partyrelationset/setRelMemberToOtherOrg",
                    type: "post",
                    dataType: "json",
                    async: false,
                    data: {
                        oldPartyOrgId: records[i].partyOrgId,
                        memberId: records[i].memberId,
                        outPartyOrgId: $("#outPartyOrgId").attr("data-value"),
                        outType: $("#outType").val(),
                        remark: $("#remark").val()
                    },
                    success: function (data) {
                        if (data.status == "500") {
                            console.log(data.msg);
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            layer.msg(sysmsg[data.msg]);
                        }
                    }
                })
            }
            $("#myTable").bootstrapTable("refresh");
            $("#relmodal").modal("hide");

        })
    })
});


function query(partyOrgId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getPartyMemberListByPartyOrgId?partyOrgId=' + partyOrgId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'memberId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名'
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '所属党组织'
        }, {
            field: 'userType',
            title: '党员类型',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                }
            }
        }, {
            field: 'postStatus',
            title: '党籍状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                } else if (value == "3") {
                    return "已出党";
                } else if (value == "4") {
                    return "已停止党籍";
                } else if (value == "5") {
                    return "已死亡";
                }
            }
        }, {
            field: 'joinTime',
            title: '入党时间',
            width: '100px'
        }, {
            field: 'mobileNo',
            title: '联系电话',
            width: '100px',
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.partyOrgId, row.memberId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(oldPartyOrgId, memberId) {
    var html = "<a href=\"javascript:void(0);doRelBetween('" + oldPartyOrgId + "','" + memberId + "')\" class=\"btn btn-primary btn-xs\">党组织转出</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function doRelBetween(oldPartyOrgId, memberId) {
    $("#relmodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/partyrelationset/setRelMemberToOutOtherOrg",
            type: "post",
            dataType: "json",
            data: {
                oldPartyOrgId: oldPartyOrgId,
                memberId: memberId,
                outPartyOrgId: $("#outPartyOrgId").attr("data-value"),
                isInSys: $("input:radio[name='isInSys']:checked").val(),
                otherPartyOrg: $("#otherPartyOrg").val(),
                outType: $("#outType").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    $("#relmodal").modal("hide");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        })
    })
}

var outPartyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("outPartyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#outPartyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    $("#myTable").bootstrapTable('destroy');
    if (treeNode.partyOrgId == "0") {
        query("");
    } else {
        query(treeNode.partyOrgId);
    }
}
