package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyCommittee;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCommitteeMapper extends MyMapper<PartyCommittee> {

    /**
     * @param orgId
     * @param electRecordId
     * @return List<Map < String, String>>
     * @Title: getCommitteeList
     * @Description:  获取当前届期委员列表
     */
    public List<Map<String, String>> getCommitteeList(@Param(value = "orgId") String orgId, @Param(value = "electionRecordId") String electionRecordId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param partyOrgId
     * @param nowTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getNowCommitteeList
     * @Description:  获取当前届次委员列表
     */
    public List<Map<String, String>> getNowCommitteeList(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "nowTime") String nowTime, @Param(value = "search") String search);


}
