package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanTipoffs;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanTipoffsMapper extends MyMapper<PartyCleanTipoffs> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param tipoffsType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanTipoffsList
     * @Description:  获取举报列表
     */
    public List<Map<String, String>> getCleanTipoffsList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId,
                                                         @Param(value = "tipoffsType") String tipoffsType, @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                         @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanTipoffsListForPortal
     * @Description:  获取门户举报列表
     */
    public List<Map<String, String>> getMyCleanTipoffsListForPortal(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanTipoffsList
     * @Description:  获取更多举报记录
     */
    public List<Map<String, String>> getMyCleanTipoffsList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
