package com.core136.service.partymember;

import com.core136.bean.partymember.PartyFamilyRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyFamilyRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyFamilyRecordService {

    private PartyFamilyRecordMapper partyFamilyRecordMapper;

    @Autowired
    public void setPartyFamilyRecordMapper(PartyFamilyRecordMapper partyFamilyRecordMapper) {
        this.partyFamilyRecordMapper = partyFamilyRecordMapper;
    }

    public int insertPartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        return partyFamilyRecordMapper.insert(partyFamilyRecord);
    }

    public int deletePartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        return partyFamilyRecordMapper.delete(partyFamilyRecord);
    }

    public int updatePartyFamilyRecord(Example example, PartyFamilyRecord partyFamilyRecord) {
        return partyFamilyRecordMapper.updateByExampleSelective(partyFamilyRecord, example);
    }

    public PartyFamilyRecord selectOnePartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        return partyFamilyRecordMapper.selectOne(partyFamilyRecord);
    }

    public List<Map<String, String>> getMemberFamilyList(String orgId, String userName, String memberId) {
        return partyFamilyRecordMapper.getMemberFamilyList(orgId, "%" + userName + "%", memberId);
    }

    public PageInfo<Map<String, String>> getMemberFamilyList(PageParam pageParam, String userName, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberFamilyList(pageParam.getOrgId(), userName, memberId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberFamilyList
     * @Description:  获取个人家庭成员记录
     */
    public List<Map<String, String>> getMyMemberFamilyList(String orgId, String memberId) {
        return partyFamilyRecordMapper.getMyMemberFamilyList(orgId, memberId);
    }


}
