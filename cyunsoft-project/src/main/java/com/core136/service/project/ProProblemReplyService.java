package com.core136.service.project;

import com.core136.bean.project.ProProblemReply;
import com.core136.mapper.project.ProProblemReplyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class ProProblemReplyService {
    private ProProblemReplyMapper proProblemReplyMapper;

    @Autowired
    public void setProProblemReplyMapper(ProProblemReplyMapper proProblemReplyMapper) {
        this.proProblemReplyMapper = proProblemReplyMapper;
    }

    public int insertProProblemReply(ProProblemReply proProblemReply) {
        return proProblemReplyMapper.insert(proProblemReply);
    }

    public int deleteProProblemReply(ProProblemReply proProblemReply) {
        return proProblemReplyMapper.delete(proProblemReply);
    }

    public int updateProProblemReply(Example example, ProProblemReply proProblemReply) {
        return proProblemReplyMapper.updateByExampleSelective(proProblemReply, example);
    }

    public ProProblemReply selectOneProProblemReply(ProProblemReply proProblemReply) {
        return proProblemReplyMapper.selectOne(proProblemReply);
    }

    /**
     * 更新任务问题列表
     *
     * @param orgId
     * @param problemId
     * @return
     */
    public List<Map<String, String>> getProblemReplyList(String orgId, String problemId) {
        return proProblemReplyMapper.getProblemReplyList(orgId, problemId);
    }

}
