var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/proget/getProReocrdTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/proget/getProSortForProTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query("");
});

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        getTaskByProForSelect(treeNode.sortId);
        $("#myTable").bootstrapTable('destroy');
        query(treeNode.sortId);
    } else {

    }
}

function query(proId) {
    $("#myTable").bootstrapTable({
        url: '/ret/proget/getProProblemListByPro?proId=' + proId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'taskId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '150px',
            title: '问题标题',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'problemType',
            width: '50px',
            title: '问题类型',
            formatter: function (value, row, index) {
                return getCodeClassName(value, "pro_problem_type");
            }
        }, {
            field: 'remark',
            width: '250px',
            title: '问题概要'
        }, {
            field: 'attach',
            width: '50px',
            title: '相关附件',
            formatter: function (value, row, index) {
                return createTableAttach(value);
            }
        }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        taskId: $("#taskIdQuery").val()
    };
    return temp;
};

function details(recordId) {
    window.open("/app/core/project/problemdetails?recordId=" + recordId);
}

function getTaskByProForSelect(proId) {
    $.ajax({
        url: "/ret/proget/getTaskByProForSelect",
        type: "post",
        dataType: "json",
        data: {
            proId: proId
        },
        success: function (data) {
            if (data.status == "200") {
                let html = "<option value=''>全部</option>";
                for (let i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].taskId + "'>" + data.list[i].text + "</option>";
                }
                $("#taskIdQuery").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
