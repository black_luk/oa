$(function () {
    $.ajax({
        url: "/ret/examget/getExamQuestionsById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (let id in data.list) {
                    if (id === "sortId") {
                        $.ajax({
                            url: "/ret/examget/getExamSortById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                sortId: data.list[id]
                            },
                            success: function (res) {
                                console.log(res);
                                if (res.status == "200") {
                                    $("#sortId").html(res.list.sortName);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id === "examType") {
                        if (data.list.examType === "0") {
                            $("#examType").html("单选");
                        } else if (data.list.examType == "1") {
                            $("#examType").html("多选");
                        } else if (data.list.examType == "2") {
                            $("#examType").html("简述");
                        }
                    } else {
                        if (id != 'content') {
                            $("#" + id).html(data.list[id]);
                        } else {
                            if (data.list.examType != "2") {
                                $("#content").show();
                                let childItemArr = JSON.parse(data.list[id]);
                                createChildItem(childItemArr);
                            } else {
                                $("#content").hide();
                            }
                        }

                    }
                }
            }
        }
    })
})

function createChildItem(childItemArr) {
    $("#child-item-table").find("tr").next().remove();
    for (let i = 0; i < childItemArr.length; i++) {
        $("#child-item-table").append("<tr><td>" + childItemArr[i].optValue + "</td>" +
            "<td>" + childItemArr[i].childTitle + "</td></tr>");

    }
}
