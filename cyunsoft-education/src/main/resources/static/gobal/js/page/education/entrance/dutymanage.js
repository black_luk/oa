$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

});

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getDutyStudentList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'studentId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'name',
            title: '学生姓名',
            width: '100px'

        }, {
            field: 'sex',
            width: '50px',
            title: '性别',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "男";
                } else if (value == "0") {
                    return "女";
                } else {
                    return "其他";
                }

            }
        }, {
            field: 'cardId',
            width: '150px',
            title: '身份证号'
        }, {
            field: 'nation',
            width: '50px',
            title: '民族'
        }, {
            field: 'major',
            width: '100px',
            title: '专业'
        }, {
            field: 'registerTime',
            width: '50px',
            title: '报到时间'
        }, {
            field: 'status',
            width: '50px',
            title: '是否分班',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "未分班";
                } else if (value == "1") {
                    return "已分班"
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'graduateSchool',
            title: '毕业学校',
            width: '100px'
        }, {
            field: 'mobile',
            title: '联系电话',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.studentId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        semesterId: $("#semesterIdQuery").attr("data-value"),
        major: $("#majorQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        status: $("#statusQuery").val()
    };
    return temp;
};

function createOptBtn(studentId) {
    var html = "<a href=\"javascript:void(0);edit('" + studentId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteLicence('" + studentId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);details('" + studentId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}

function updateEduStudent(studentId) {
    $.ajax({
        url: "/set/eduset/updateEduStudent",
        type: "post",
        dataType: "json",
        data: {
            studentId: studentId,
            name: $("#name").val(),
            headImg: $("#file").attr("data-value"),
            sex: $("#sex").val(),
            cardId: $("#cardId").val(),
            dirthday: $("#dirthday").val(),
            nation: $("#nation").val(),
            mobile: $("#mobile").val(),
            major: $("#major").val(),
            score: $("#score").val(),
            lastScore: $("#lastScore").val(),
            homeAddress: $("#homeAddress").val(),
            graduateSchool: $("#graduateSchool").val(),
            inType: $("#inType").val(),
            isStay: $("#isStay").val(),
            registerTime: $("#registerTime").val(),
            introducers: $("#introducers").val(),
            father: $("#father").val(),
            fatherMobile: $("#fatherMobile").val(),
            mother: $("#mother").val(),
            motherMobile: $("#motherMobile").val(),
            remark: $("#remark").code(),
            studentNo: $("#studentNo").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });

}
