$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#punishTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#loseTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberPunishList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '50px',
            title: '党员姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '组织名称'
        }, {
            field: 'title',
            title: '惩戒名称',
            width: '100px',
            formatter: function (value, row, index) {
                if (row.otherTitle != "") {
                    return value + "/" + row.otherTitle;
                } else {
                    return value;
                }
            }
        }, {
            field: 'unitName',
            width: '100px',
            title: '惩戒单位'
        }, {
            field: 'punishTime',
            width: '100px',
            title: '惩戒日期'
        }, {
            field: 'punishReasons',
            width: '100px',
            title: '惩戒原因'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        punishLevel: $("#punishLevelQuery").val(),
        memberId: $("#takeTypeQuery").attr("data-value"),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(recordId) {
    $("#rewardlistdiv").hide();
    $("#rewarddiv").show();
    $("#memberId").attr("data_value", "");
    document.getElementById("form1").reset();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyPunishRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "partyType") {
                        $("#" + id).attr("data-value", recordInfo[id])
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTypeById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {sortId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.sortName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyPunishRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partymemberset/deletePartyPunishRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function goback() {
    $("#rewarddiv").hide();
    $("#rewardlistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/punishdetails?recordId=" + recordId);
}


function updatePartyPunishRecord(recordId) {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    if ($("#title").val() == "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyPunishRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            title: $("#title").val(),
            otherTitle: $("#otherTitle").val(),
            punishTime: $("#punishTime").val(),
            unitName: $("#unitName").val(),
            punishLevel: $("#punishLevel").val(),
            punishReasons: $("#punishReasons").val(),
            partyType: $("#partyType").attr("data-value"),
            loseTime: $("#loseTime").val(),
            loseResons: $("#loseResons").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
