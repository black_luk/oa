$(function () {
    getCodeClass("fileType", "volume_file_type");
    jeDate("#sendTime", {
        format: "YYYY-MM-DD",
        isinitVal: true
    });
    getVolumeList();
    $(".js-add-save").unbind("click").click(function () {
        addFile();
    })
})

function addFile() {
    if ($("#title").val() == "" || $("#fileCode").val() == "") {
        layer.msg("文件号或文件标题不能为空！")
        return;
    }
    $.ajax({
        url: "/set/archivesset/insertArchivesFile",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            fileCode: $("#fileCode").val(),
            title: $("#title").val(),
            sendOrg: $("#sendOrg").val(),
            subject: $("#subject").val(),
            subheading: $("#subheading").val(),
            fileType: $("#fileType").val(),
            secretLevel: $("#secretLevel").val(),
            sendTime: $("#sendTime").val(),
            volumeId: $("#volumeId").val(),
            pageTotal: $("#pageTotal").val(),
            printTotal: $("#printTotal").val(),
            attach: $("#archivesattach").attr("data_value"),
            isaudit: $("input:radio[name='isaudit']:checked").val(),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getVolumeList() {
    $.ajax({
        url: "/ret/archivesget/getArchivesVolumeListForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordList = data.list;
                for (var i = 0; i < recordList.length; i++) {
                    $("#volumeId").append("<option value=\"" + recordList[i].volumeId + "\">" + recordList[i].volumeTitle + "</option>")
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}
