$(function () {
    getMyMemberPointsList();
    getMemberInfo();
    getMyPointsInfo();
    getMyPointsByMonthLine();
})

function getMemberInfo() {
    $.ajax({
        url: "/ret/partymemberget/getMyMemberInfoByMemberId",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                if(data.list)
                {
                    $(".js-member_head_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + data.list.photo);
                    $(".js-member-name").html(data.list.userName);
                    $(".js-member-org-name").html("所在党支部:" + data.list.partyOrgName);
                    $(".js-member-join-time").html("入党日期:" + data.list.joinTime);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getMyPointsByMonthLine() {
    $.ajax({
        url: "/ret/partychartsget/getMyPointsByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getMyPointsInfo() {
    $.ajax({
        url: "/ret/partymemberget/getMyPointsInfo",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                $(".js-year-points").html(data.list.total);
                $(".js-all-points").html(data.list.totalAll);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}


function getMyMemberPointsList() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMyMemberPointsList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '党组织名称'
        }, {
            field: 'userName',
            width: '100px',
            title: '党员姓名'
        }, {
            field: 'pointsYear',
            title: '年度',
            width: '100px',
            formatter: function (value, row, index) {
                return "【" + value + "】年度"
            }
        }, {
            field: 'item',
            width: '100px',
            title: '获取积分项目',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "三会一课";
                } else if (value == "2") {
                    return "党组织生活会";
                } else if (value == "3") {
                    return "民主生活会";
                } else if (value == "4") {
                    return "主题党日";
                } else if (value == "5") {
                    return "民主评议";
                } else if (value == "6") {
                    return "党建述评考";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'points',
            width: '100px',
            title: '获取积分',
            formatter: function (value, row, index) {
                return value + "分"
            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '获取时间'
        }, {
            field: 'remark',
            width: '300px',
            title: '备注'
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

//查询参数
function queryParams(params) {
    var temp = {
        search: "",
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        deleted: 0
    };
    return temp;
}
