$(function () {
    query();
    $(".js-addClassCode").unbind("click").click(function () {
        doadd();
    });
    $(".js-delAll").unbind("click").click(function () {
        dodelAll();
    });
});

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getEduMajorList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'majorId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'codeNo',
                width: '150px',
                title: '专业编号'
            },
            {
                field: 'title',
                width: '150px',
                title: '专业名称'
            },
            {
                field: 'status',
                title: '状态',
                width: '50px',
                align: "center",
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "<a href=\"javascript:void(0);\" class=\"btn btn-success btn-xs\">启用</a>";
                    } else if (value == "0") {
                        return "<a href=\"javascript:void(0);\" class=\"btn btn-yellow btn-xs\">停用</a>";
                    } else {
                        return "<a href=\"javascript:void(0);\" class=\"btn btn-danger btn-xs\">未知</a>";
                    }
                }
            },
            {
                field: 'remark',
                title: '备注说明',
                width: '300px',
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.majorId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
                layer.msg(sysmsg[res.msg]);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(majorId) {
    var html = "<a href=\"javascript:void(0);edit('" + majorId + "')\" class=\"btn btn-sky btn-xs\" >修改</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);del('" + majorId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function edit(majorId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/eduget/getEduMajorById",
        type: "post",
        dataType: "json",
        data: {
            majorId: majorId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    $("#" + name).val(data.list[name]);
                }
            }
        }
    });
    $("#setMajorMoadl").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/updateEduMajor",
            type: "post",
            dataType: "json",
            data: {
                majorId: majorId,
                codeNo: $("#codeNo").val(),
                sortNo: $("#sortNo").val(),
                status: $("#status").val(),
                title: $("#title").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setMajorMoadl").modal("hide");
    });

}

function del(majorId) {
    if (confirm("确定删除当前专业吗？")) {
        $.ajax({
            url: "/set/eduset/deleteEduMajor",
            type: "post",
            dataType: "json",
            data: {
                majorId: majorId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        });
    } else {
        return;
    }
}

function doadd() {
    document.getElementById("form1").reset();
    $("#setMajorMoadl").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/insertEduMajor",
            type: "post",
            dataType: "json",
            data: {
                sortNo: $("#sortNo").val(),
                title: $("#title").val(),
                status: $("#status").val(),
                codeNo: $("#codeNo").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setMajorMoadl").modal("hide");
    });
}

function dodelAll() {
    var a = $("#myTable").bootstrapTable('getSelections');
    var majorIdArr = [];
    for (var i = 0; i < a.length; i++) {
        majorIdArr.push(a[i].codeClassId);
    }
    if (majorIdArr.length <= 0) {
        layer.msg("至少选择一个专业!")
        return;
    } else {
        if (confirm("确定删除当前专业吗？")) {
            $.ajax({
                url: "/set/eduset/deleteEduMajorBatch",
                type: "post",
                dataType: "json",
                data: {
                    majorIdArr: majorIdArr
                },
                success: function (data) {
                    if (data.status == 200) {
                        layer.msg(sysmsg[data.msg]);
                        $("#myTable").bootstrapTable("refresh");
                    } else if (data.status = "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        } else {
            return;
        }
    }
}
